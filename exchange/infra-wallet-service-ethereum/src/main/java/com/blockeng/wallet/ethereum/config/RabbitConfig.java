package com.blockeng.wallet.ethereum.config;

import org.springframework.amqp.core.*;
import com.blockeng.wallet.enums.*;
import org.springframework.context.annotation.*;

@Configuration
public class RabbitConfig {
    @Bean
    public Queue withdrawApplyQueue() {
        return new Queue(MessageChannel.FINANCE_WITHDRAW_SEND_ETH.getName());
    }

    @Bean
    public Queue userAddress() {
        return new Queue(MessageChannel.COIN_ADDRESS_MSG.getName());
    }
}
