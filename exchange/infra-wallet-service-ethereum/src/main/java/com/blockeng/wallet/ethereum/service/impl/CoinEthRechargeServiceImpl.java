package com.blockeng.wallet.ethereum.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.ethereum.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.help.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import com.blockeng.wallet.utils.*;
import org.web3j.protocol.core.methods.response.*;
import org.apache.http.util.*;
import java.util.regex.*;
import org.springframework.util.*;
import java.math.*;
import org.web3j.utils.*;
import com.clg.wallet.newclient.*;
import java.util.*;
import com.blockeng.wallet.entity.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinEthRechargeServiceImpl extends ServiceImpl<CoinRechargeMapper, CoinRecharge> implements CoinEthRechargeService
{
    private static final Logger LOG = LoggerFactory.getLogger(CoinEthRechargeServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private CoinConfigService coinConfigService;
    @Autowired
    private UserAddressService userAddressService;
    @Autowired
    private CoinRechargeService coinRechargeService;
    @Autowired
    private WalletCollectTaskService walletCollectTaskService;
    @Autowired
    private AdminAddressService adminAddressService;
    @Autowired
    private ReadProperties readProperties;
    public CoinConfig ethCoinBean;
    private Client client;

    public void rechargeCoin(final CoinConfig ethCoin) {
        this.client = ClientFactory.getClient(this.clientInfo.getClientInfoFromType("etz"));
        this.ethCoinBean = ethCoin;
        final Map<String, CoinConfig> contractInfo = this.clientInfo.getTokenList("etzToken");
        LOG.info(ethCoin.getName() + "开始查询区块*****Jack*******");
        if (null != contractInfo && this.ethCoinBean != null) {
            try {
                int latest = this.client.getBlockCount().toInteger() - ethCoin.getMinConfirm();
                final int currentCount = Integer.valueOf(this.coinConfigService.lastBlock(this.ethCoinBean.getId()));
                LOG.info("这个ID是this.ethCoinBean.getId()：{}",this.ethCoinBean.getId());
                LOG.info("最后区块高度是：{}",currentCount);
                if (latest > currentCount) {
                    final int count = latest - currentCount;
                    if (count > this.readProperties.maxCount) {
                        latest = currentCount + this.readProperties.maxCount;
                    }
                    this.recharge(contractInfo, this.ethCoinBean, currentCount, latest);
                }
                else {
                    LOG.info(ethCoin.getName() + "暂时无新的区块高度*****Jack*******");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            LOG.info(ethCoin.getName() + "查询区块结束*****Jack*******");
        }
        else {
            LOG.info("初始化失败*****Jack*******");
        }
    }

    private void recharge(final Map<String, CoinConfig> coinMap, final CoinConfig ethCoinBean, int currentBlockNumber, final int latest) {
        for (int i = currentBlockNumber + 1; i < latest; ++i) {
            CoinEthRechargeServiceImpl.LOG.info(ethCoinBean.getName() + "*****Jack*******区块开始，高度为:" + i);
            final List<EthBlock.TransactionResult> transactions = ((EthBlock.Block)this.client.getBlockByNumber("" + i).getResult()).getTransactions();
            if (!CollectionUtils.isEmpty(transactions)) {
                for (final EthBlock.TransactionResult item : transactions) {
                    final EthBlock.TransactionObject transaction = (EthBlock.TransactionObject)item;
                    final String input = transaction.getInput();
                    if (!TextUtils.isEmpty(transaction.getTo())) {
                        final String to = transaction.getTo().toLowerCase();
                        String contractAddress = "";
                        CoinConfig nowCoin = coinMap.get(to);
                        if (null != nowCoin)
                        {
                            contractAddress = nowCoin.getContractAddress();
                            if ((TextUtils.isEmpty(input)) || (input.length() < 138)) {
                                continue;
                            }
                            transaction.setTo("0x" + input.substring(34, 74));
                        }
                        else
                        {
                            if (!Pattern.matches("^0x[0]*$", input)) {
                                continue;
                            }
                            nowCoin = ethCoinBean;
                        }
                        final UserAddress userEthResult = this.userAddressService.getByCoinIdAndAddr(transaction.getTo(), nowCoin.getId());
                        if (null != userEthResult)
                        {
                            BigDecimal money;
                            if (!StringUtils.isEmpty(contractAddress))
                            {
                                String moneyStr = input.substring(74, 138);
                                int unit = this.client.getTokenDecimals(contractAddress).toInteger();
                                money = new BigDecimal(new BigInteger(moneyStr, 16)).divide(BigDecimal.TEN.pow(unit));
                            }
                            else
                            {
                                money = Convert.fromWei(new BigDecimal(transaction.getValue()), Convert.Unit.ETHER);
                            }
                            LOG.info(ethCoinBean.getName() + "*****Jack*******查询到一条记录，交易Id为:" + transaction.getHash());
                            if (((EthNewClient)this.client).isFailed(transaction.getHash()).toBoolean())
                            {
                                LOG.info(ethCoinBean.getName() + "*****Jack*******交易失败，交易Id为:" + transaction.getHash());
                            }
                            else
                            {
                                AdminAddress adminAddress = this.adminAddressService.queryAdminAccount("eth", 3);
                                if ((null == adminAddress) || (!transaction.getFrom().equalsIgnoreCase(adminAddress.getAddress())))
                                {
                                    boolean isSuccess = this.coinRechargeService.addRecord(transaction.getHash(), transaction.getTo(), userEthResult.getUserId(), money, 1, nowCoin);
                                    if (isSuccess) {
                                        addBackTask(nowCoin, money, transaction.getTo(), userEthResult);
                                    }
                                    LOG.info(ethCoinBean.getName() + (isSuccess ? "*****Jack*******增加充值任务成功:" : "*****Jack*******插入充值失败") + transaction.getHash());
                                }
                                else
                                {
                                    LOG.info(ethCoinBean.getName() + "*****Jack*******归账手续费无需到账");
                                }
                            }
                        }
                    }
                }
                currentBlockNumber = i;
            }
        }
        this.coinConfigService.updateCoinLastblock("" + currentBlockNumber, ethCoinBean.getId());
    }

    private void addBackTask(final CoinConfig config, final BigDecimal money, final String fromAddress, final UserAddress userEthResult) {
        AdminAddress payAdminAddress = this.adminAddressService.queryAdminAccount(config.getCoinType(), 2);
        AdminAddress backAdminAddress = this.adminAddressService.queryAdminAccount(config.getCoinType(), 1);
        LOG.info("开始来算账了");
        if (null == backAdminAddress || null == backAdminAddress || config.getCoinType().toLowerCase().endsWith("token")) {
            final String type = config.getCoinType().replace("Token", "");
            if (StringUtils.isEmpty(type)) {
                CoinEthRechargeServiceImpl.LOG.info(config.getCoinType() + "的归账钱包为空*****Jack*******");
            }
            else {
                payAdminAddress = this.adminAddressService.queryAdminAccount(type, 2);
                backAdminAddress = this.adminAddressService.queryAdminAccount(type, 1);
            }
        }
        String toAddress = null;
        if (null != payAdminAddress) {
            toAddress = payAdminAddress.getAddress();
            LOG.info("目标地址是payAdminAddress,status = 1,toAddress:{}",toAddress);
            BigDecimal payBalance = BigDecimal.ZERO;
            if (config.getCoinType().toLowerCase().endsWith("token")) {
                payBalance = this.client.getTokenBalance(config.getContractAddress(), toAddress).toBigDecimal();
            }
            else {
                payBalance = this.client.getBalance(toAddress).toBigDecimal();
                LOG.info("这个归集账户的余额是,payBalance:{}",payBalance);
            }
            final BigDecimal creditLimit = config.getCreditLimit();
            if (null != creditLimit && creditLimit.compareTo(BigDecimal.ZERO) > 0 && payBalance.compareTo(creditLimit) > 0 && null != backAdminAddress && !StringUtils.isEmpty((Object)backAdminAddress.getAddress())) {
                final String backAddress = backAdminAddress.getAddress();
                toAddress = (StringUtils.isEmpty(backAddress) ? toAddress : backAddress);
            }
        }
        this.walletCollectTaskService.addCollectTask(money, userEthResult.getUserId(), config.getId(), fromAddress, toAddress);
    }

    public static void main(final String[] args) {
        final String test = "^0x[0]*$";
        final String a1 = "0x";
        System.out.println(Pattern.matches(test, a1));
        final String a2 = "0x0";
        System.out.println(Pattern.matches(test, a2));
        final String a3 = "0x01";
        System.out.println(Pattern.matches(test, a3));
        final String a4 = "0x0000000000";
        System.out.println(Pattern.matches(test, a4));
        final String a5 = "0";
        System.out.println(Pattern.matches(test, a5));
    }
}
