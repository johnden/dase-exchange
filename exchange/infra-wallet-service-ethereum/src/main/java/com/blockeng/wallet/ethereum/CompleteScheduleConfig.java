package com.blockeng.wallet.ethereum;

import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.*;
import com.blockeng.wallet.ethereum.job.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.jobs.Task;
import com.blockeng.wallet.config.*;
import org.springframework.scheduling.config.*;
import org.slf4j.*;

@Configuration
@EnableScheduling
public class CompleteScheduleConfig implements SchedulingConfigurer {
    private static final Logger LOG = LoggerFactory.getLogger(CompleteScheduleConfig.class);
    @Autowired
    private EthTask ethTask;
    @Autowired
    private Task task;
    @Autowired
    private CheckCoinStatus checkCoinStatus;

    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        if (this.checkCoinStatus.allIsRunning()) {
            LOG.info("etz开始运行");
            addEthTask(taskRegistrar);
        }
    }

    private void addEthTask(ScheduledTaskRegistrar taskRegistrar) {
        if (this.checkCoinStatus.featuresIsOpen("recharge")) {
            this.ethTask.addRecharge(taskRegistrar);
        }
        this.ethTask.commitData(taskRegistrar);
        if (this.checkCoinStatus.featuresIsOpen("address")) {
            this.ethTask.createAddress(taskRegistrar);
        }
        if (this.checkCoinStatus.featuresIsOpen("draw")) {
            this.ethTask.withDraw(taskRegistrar);
        }
        if (this.checkCoinStatus.featuresIsOpen("collect")) {
            this.ethTask.collection(taskRegistrar);
        }
        if (this.checkCoinStatus.featuresIsOpen("other")) {
            this.task.service(taskRegistrar, "eth");
        }
    }
}