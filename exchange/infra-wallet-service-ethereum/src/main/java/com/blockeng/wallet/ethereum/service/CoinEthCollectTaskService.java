package com.blockeng.wallet.ethereum.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;

public interface CoinEthCollectTaskService extends IService<WalletCollectTask>
{
    void collectionTask();
}
