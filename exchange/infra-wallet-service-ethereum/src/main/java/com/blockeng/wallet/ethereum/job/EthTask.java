package com.blockeng.wallet.ethereum.job;

import org.springframework.stereotype.*;
import com.blockeng.wallet.help.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.ethereum.service.*;
import com.blockeng.wallet.service.*;
import org.springframework.scheduling.config.*;
import org.springframework.util.*;
import com.blockeng.wallet.entity.*;
import org.springframework.scheduling.*;
import java.util.*;
import org.springframework.scheduling.support.*;
import org.slf4j.*;

@Component
public class EthTask
{
    private static final Logger log = LoggerFactory.getLogger(EthTask.class);;
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private CoinEthRechargeService coinEthRechargeService;
    @Autowired
    private CoinEthAddressPoolService coinEthAddressPoolService;
    @Autowired
    private CoinEthCollectTaskService coinEthCollectTaskService;
    @Autowired
    private CoinEthWithdrawService coinEthWithdrawService;
    @Autowired
    private CommitStatusService commitStatusService;

    public void addRecharge(final ScheduledTaskRegistrar taskRegistrar) {
        final List<CoinConfig> list = this.clientInfo.getCoinConfigFormType("etz");
        if (!CollectionUtils.isEmpty(list)) {
            final CoinConfig ethCoin = list.get(0);
            if (null != ethCoin) {
                taskRegistrar.addTriggerTask(() -> this.coinEthRechargeService.rechargeCoin(ethCoin), triggerContext -> new CronTrigger(ethCoin.getTask()).nextExecutionTime(triggerContext));
            }
        }
    }

    public void commitData(final ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(() -> this.commitStatusService.commitEth(), triggerContext -> {
            final String cron = "0/15 * * * * ?";
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }

    public void createAddress(final ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(() -> this.coinEthAddressPoolService.createAddress(), triggerContext -> {
            final String cron = "0/59 * * * * ?";
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }

    public void collection(final ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(() -> this.coinEthCollectTaskService.collectionTask(), triggerContext -> {
            final String cron = "0/30 * * * * ?";
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }

    public void withDraw(final ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(() -> {
            try {
                this.coinEthWithdrawService.transaction();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }, triggerContext -> {
            final String cron = "0/30 * * * * ?";
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }
}
