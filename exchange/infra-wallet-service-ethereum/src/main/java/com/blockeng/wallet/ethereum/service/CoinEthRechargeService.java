package com.blockeng.wallet.ethereum.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;

public interface CoinEthRechargeService extends IService<CoinRecharge>
{
    void rechargeCoin(final CoinConfig paramCoinConfig);
}
