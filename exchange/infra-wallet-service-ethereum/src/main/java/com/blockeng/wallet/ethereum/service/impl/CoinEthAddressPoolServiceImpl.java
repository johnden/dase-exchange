package com.blockeng.wallet.ethereum.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.entity.*;
import com.blockeng.wallet.ethereum.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.help.*;
import com.blockeng.wallet.service.*;
import org.springframework.beans.factory.annotation.*;
import com.clg.wallet.bean.*;
import com.clg.wallet.newclient.*;
import com.blockeng.wallet.service.impl.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinEthAddressPoolServiceImpl extends ServiceImpl<AddressPoolMapper, AddressPool> implements CoinEthAddressPoolService {
    private static final Logger LOG = LoggerFactory.getLogger(UserAddressServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private AddressPoolService addressPoolService;
    @Value("${address.count}")
    public int maxAddressCount;

    public void createAddress() {
        final ClientBean clientBean = this.clientInfo.getClientInfoFromType("etz");
        if (null != clientBean) {
            this.createAddress(clientBean);
        } else {
            LOG.error("coin数据库为空");
        }
    }

    private void createAddress(final ClientBean clientBean) {
        final int count = this.addressPoolService.selectAddressCount(0L, clientBean.getCoinType());
        final Client client = ClientFactory.getClient(clientBean);
        if (count <= this.maxAddressCount) {
            int addressCount = this.maxAddressCount;
            CoinEthAddressPoolServiceImpl.LOG.info("创建地址数量:" + addressCount);
            while (addressCount > 0) {
                final AddressBean addressBean = (AddressBean) client.getNewAddress().toObj((Class) AddressBean.class);
                if (null != addressBean) {
                    if (this.addressPoolService.insertEthAddress(addressBean, clientBean.getCoinType(),  clientBean.getId())) {
                        LOG.info("创建第:" + addressCount + "个，成功" + addressBean.toString());
                    } else {
                        LOG.info("创建失败");
                    }
                } else {
                    CoinEthAddressPoolServiceImpl.LOG.info("创建失败");
                }
                addressCount--;
            }
        }
    }
}
