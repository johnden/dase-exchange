package com.blockeng.wallet.ethereum;

import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.*;


@SpringBootApplication(scanBasePackages={"com.blockeng"})
public class EthereumServiceApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(EthereumServiceApplication.class, args);
    }
}
