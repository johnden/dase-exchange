package com.blockeng.wallet.ethereum.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.ethereum.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.help.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import com.clg.wallet.help.*;
import org.springframework.util.*;
import java.util.*;
import com.clg.wallet.newclient.*;
import com.blockeng.wallet.entity.*;
import java.math.*;
import com.clg.wallet.bean.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinEthWithdrawServiceImpl extends ServiceImpl<CoinWithdrawMapper, CoinWithdraw> implements CoinEthWithdrawService
{

    private static final Logger log = LoggerFactory.getLogger(CoinEthWithdrawServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private CoinWithdrawService coinWithdrawService;

    private void sendEthOrToken(final CoinWithdraw item) throws Exception {
        final String addr = item.getAddress();
        if (StringUtils.isEmpty(addr) || !WalletUtils.isValidAddress(addr)) {
            item.setStatus(Integer.valueOf(2)).setWalletMark("地址柜式错误");
            log.info("地址格式不对");
            this.coinWithdrawService.updateDrawInfo(item);
            return;
        }
        if ("eth".equalsIgnoreCase(item.getCoinType())) {
            this.sendEth(item);
        }
        else if ("eth".equalsIgnoreCase(item.getCoinType())) {
            this.sendEthToken(item);
        }
    }

    public void transaction() {
        final List<CoinWithdraw> coinWithdraws = this.coinWithdrawService.queryOutList("eth");
        coinWithdraws.addAll(this.coinWithdrawService.queryOutList("ethToken"));
        if (CollectionUtils.isEmpty((Collection)coinWithdraws)) {
            CoinEthWithdrawServiceImpl.log.error("当前无提币信息");
            return;
        }
        for (final CoinWithdraw item : coinWithdraws) {
            try {
                this.sendEthOrToken(item);
            }
            catch (Exception e) {
                e.printStackTrace();
                this.coinWithdrawService.updateDrawInfo(item.setStatus(Integer.valueOf(6)).setWalletMark("提币异常，请检查是否打币成功:" + e.getMessage()));
            }
        }
    }

    private void sendEthToken(final CoinWithdraw item) throws Exception {
        final CoinConfig coin = this.clientInfo.getCoinConfigFormId(item.getCoinId());
        final EthNewClient client = (EthNewClient)ClientFactory.getClient(this.clientInfo.getClientInfoFromType("etz"));
        final EThTransactionBean ethTransactionBean = this.clientInfo.getEthTransactionBean(coin, item);
        log.info("开始打币" + coin.getName());
        final BigDecimal tokenBalance = client.getTokenBalance(ethTransactionBean.getContractAddress(), ethTransactionBean.getFromAddress()).toBigDecimal();
        if (tokenBalance.compareTo(item.getMum()) > 0) {
            this.coinWithdrawService.updateTx(item.setTxid("0000000000000000000000000000000000000000000").setWalletMark(""));
            final EthResult ethTokenResult = client.sentEthToken(ethTransactionBean);
            if (null != ethTokenResult && ethTokenResult.isSuccess()) {
                item.setTxid(ethTokenResult.getTxid()).setWalletMark(ethTokenResult.getInfo()).setStatus(Integer.valueOf(5));
            }
            else {
                item.setTxid(ethTokenResult.getTxid()).setWalletMark(ethTokenResult.getInfo()).setStatus(Integer.valueOf(6));
                CoinEthWithdrawServiceImpl.log.info("打币失败,id:" + item.getId());
            }
        }
        else {
            this.coinWithdrawService.updateTx(item.setStatus(Integer.valueOf(6)).setWalletMark("余额不足"));
        }
        this.coinWithdrawService.updateDrawInfo(item);
    }

    private void sendEth(final CoinWithdraw item) throws Exception {
        final CoinConfig coin = this.clientInfo.getCoinConfigFormId(item.getCoinId());
        final EthNewClient client = (EthNewClient)ClientFactory.getClient(this.clientInfo.getClientInfoFromType("etz"));
        final EThTransactionBean ethTransactionBean = this.clientInfo.getEthTransactionBean(coin, item);
        log.info("开始打币" + coin.getName());
        final BigDecimal ethBalance = client.getBalance(ethTransactionBean.getFromAddress()).toBigDecimal();
        if (ethBalance.compareTo(item.getMum()) == 1) {
            this.coinWithdrawService.updateTx(item.setTxid("0000000000000000000000000000000000000000000").setWalletMark(""));
            final EthResult ethResult = client.sentEth(ethTransactionBean);
            if (null != ethResult && ethResult.isSuccess()) {
                item.setTxid(ethResult.getTxid()).setWalletMark(ethResult.getInfo()).setStatus(Integer.valueOf(5));
            }
            else {
                item.setTxid(ethResult.getTxid()).setWalletMark(ethResult.getInfo()).setStatus(Integer.valueOf(6));
                log.info("打币失败,id:" + item.getId());
            }
        }
        else {
            this.coinWithdrawService.updateTx(item.setStatus(Integer.valueOf(6)).setWalletMark("余额不足"));
        }
        this.coinWithdrawService.updateDrawInfo(item);
    }
}
