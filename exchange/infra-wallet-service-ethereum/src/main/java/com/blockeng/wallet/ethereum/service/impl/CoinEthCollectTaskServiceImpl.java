package com.blockeng.wallet.ethereum.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.ethereum.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.help.*;
import com.blockeng.wallet.service.*;
import org.springframework.util.*;
import java.util.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.bean.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinEthCollectTaskServiceImpl extends ServiceImpl<WalletCollectTaskMapper, WalletCollectTask> implements CoinEthCollectTaskService
{
    private static final Logger LOG = LoggerFactory.getLogger(CoinEthCollectTaskServiceImpl.class);
    @Autowired
    private ETHClient ethClient;
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private UserAddressService userAddressService;
    @Autowired
    private WalletCollectTaskService walletCollectTaskService;

    public void collectionTask() {
        final List<WalletCollectTask> todoEthTask = this.walletCollectTaskService.getTodoTask("etz");
        final List<WalletCollectTask> todoTokenTask = this.walletCollectTaskService.getTodoTask("etzToken");
        this.collectionEth(todoEthTask);
        this.collectionEthToken(todoTokenTask);
    }

    private void collectionEthToken(final List<WalletCollectTask> todoTokenTask) {
        if (!CollectionUtils.isEmpty(todoTokenTask)) {
            LOG.info("开始归账，总数是" + todoTokenTask.size());
            for (final WalletCollectTask task : todoTokenTask) {
                LOG.info("开发归账:" + task.getFromAddress() + " 名称:" + task.getCoinName());
                final UserAddress userEthBean = this.userAddressService.getByCoinIdAndUserId(task.getUserId(), task.getCoinId());
                if (StringUtils.isEmpty(userEthBean.getKeystore()) && StringUtils.isEmpty(userEthBean.getPwd())) {
                    this.walletCollectTaskService.updateTaskStatus(task.getId(), "", "归账失败，不存在当前地址keystore和pwd,address:" + userEthBean.getAddress(), 1);
                    return;
                }
                if (null == userEthBean) {
                    continue;
                }
                try {
                    final CoinConfig coin = this.clientInfo.getCoinConfigFormId(task.getCoinId());
                    if (null == coin) {
                        LOG.error("不存在该币种,name:" + task.getCoinName() + " id:" + task.getId());
                        return;
                    }
                    final EthResult ethResult = this.ethClient.sendEthTokenToCenter(userEthBean, task.getToAddress(), coin);
                    if (ethResult.isSuccess()) {
                        this.walletCollectTaskService.updateTaskStatus(task.getId(), ethResult.getTxid(), ethResult.getInfo(), 1);
                        LOG.info(LOG.getName() + "归账任务成功");
                    }
                    else if (-3 == ethResult.getCode()) {
                        this.walletCollectTaskService.updateTaskStatus(task.getId(), ethResult.getTxid(), ethResult.getInfo(), 1);
                    }
                    else {
                        if (-2 != ethResult.getCode()) {
                            continue;
                        }
                        LOG.info(LOG.getName() + "打出手续费完成:txid=" + ethResult.getTxid() + " info:" + ethResult.getInfo());
                    }
                }
                catch (Exception e) {
                    LOG.info(LOG.getName() + "归账任务失败,error:" + e.toString());
                }
            }
        }
    }

    private void collectionEth(final List<WalletCollectTask> todoEthTask) {
        if (!CollectionUtils.isEmpty(todoEthTask)) {
            for (final WalletCollectTask task : todoEthTask) {
                LOG.info("开始归账:" + task.getFromAddress() + " 名称:" + task.getCoinName());
                final UserAddress userEthBean = this.userAddressService.getByCoinIdAndUserId(task.getUserId(), task.getCoinId());
                if (StringUtils.isEmpty(userEthBean.getKeystore()) && StringUtils.isEmpty(userEthBean.getPwd())) {
                    this.walletCollectTaskService.updateTaskStatus(task.getId(), "", "归账失败，不存在当前地址keystore和pwd,address:" + userEthBean.getAddress(), 1);
                    return;
                }
                if (null == userEthBean) {
                    continue;
                }
                try {
                    final EthResult ethResult = this.ethClient.sendEthToCenter(this.clientInfo.getClientInfoFromType("etz"), userEthBean, task);
                    if (ethResult.isSuccess()) {
                        this.walletCollectTaskService.updateTaskStatus(task.getId(), ethResult.getTxid(), ethResult.getInfo(), 1);
                    }
                    else {
                        if (-3 != ethResult.getCode()) {
                            continue;
                        }
                        this.walletCollectTaskService.updateTaskStatus(task.getId(), ethResult.getTxid(), ethResult.getInfo(), 1);
                    }
                }
                catch (Exception e) {
                    LOG.info(LOG.getName() + "归账任务失败,error:" + e.toString());
                }
            }
        }
    }
}
