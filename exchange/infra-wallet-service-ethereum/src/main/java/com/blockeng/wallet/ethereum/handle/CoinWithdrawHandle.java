package com.blockeng.wallet.ethereum.handle;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.utils.*;
import java.io.*;
import org.springframework.amqp.rabbit.annotation.*;
import com.blockeng.wallet.dto.*;
import org.slf4j.*;

@Component
public class CoinWithdrawHandle
{

    private static final Logger log = LoggerFactory.getLogger(CoinWithdrawHandle.class);
    @Autowired
    private UserAddressService userAddressService;
    @Autowired
    private CoinWithdrawService coinWithdrawService;

    @RabbitListener(queues={"finance.withdraw.send.eth"})
    public void coinWithdrawMessage(String message)
    {
        CoinWithdraw coinWithdraw = GsonUtils.convertObj(message, CoinWithdraw.class);
        if (null == coinWithdraw)
        {
            log.error("eth打币异常，json转换异常");
            return;
        }
        CoinWithdraw result = this.coinWithdrawService.selectById(new CoinWithdraw().setId(coinWithdraw.getId()));
        if ((null != result) && (result.getStatus().intValue() == 5))
        {
            this.coinWithdrawService.updateDrawInfo(coinWithdraw.setStatus(Integer.valueOf(5)).setWalletMark("该笔提款已经打出，请勿重复发起提币操作"));
            return;
        }
        this.coinWithdrawService.insert(coinWithdraw);
    }

    @RabbitListener(queues={"plant.user.address"})
    public String userAddress(String msg)
    {
        try
        {
            CoinAddressDTO coinAddress = GsonUtils.convertObj(msg, CoinAddressDTO.class);
            return this.userAddressService.getAddress(coinAddress);
        }
        catch (Exception e) {}
        return null;
    }
}
