package com.blockeng.wallet.ethereum.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;

public interface CoinEthAddressPoolService extends IService<AddressPool>
{
    void createAddress();
}
