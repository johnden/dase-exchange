package com.blockeng.wallet.bitcoin.job;

import com.blockeng.wallet.bitcoin.service.CoinBtcAddressPoolService;
import com.blockeng.wallet.bitcoin.service.CoinBtcRechargeService;
import com.blockeng.wallet.bitcoin.service.CoinBtcWithdrawService;
import com.blockeng.wallet.entity.CoinConfig;
import com.blockeng.wallet.help.ClientInfo;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class BtcTask {
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private CoinBtcRechargeService coinBtcRechargeService;
    @Autowired
    private CoinBtcAddressPoolService coinBtcAddressPoolService;
    @Autowired
    private CoinBtcWithdrawService coinBtcWithdrawService;

    public void addRecharge(ScheduledTaskRegistrar taskRegistrar) {
        List<CoinConfig> btcTokenCoin = this.clientInfo.getCoinConfigFormType("btc");
        if (!CollectionUtils.isEmpty(btcTokenCoin)) {
            for (CoinConfig coin : btcTokenCoin) {
                taskRegistrar.addTriggerTask(() -> this.coinBtcRechargeService.searchRecharge(coin), triggerContext -> new CronTrigger(coin.getTask()).nextExecutionTime(triggerContext));
            }
        }
    }

    public void createAddress(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(() -> this.coinBtcAddressPoolService.createAddress(), triggerContext -> {
            String cron = "0/59 * * * * ?";
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }

    public void withDraw(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(() -> this.coinBtcWithdrawService.transaction(), triggerContext -> {
            String cron = "0/30 * * * * ?";
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }
}
