package com.blockeng.wallet.bitcoin.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.blockeng.wallet.bitcoin.service.CoinBtcWithdrawService;
import com.blockeng.wallet.entity.CoinWithdraw;
import com.blockeng.wallet.help.ClientInfo;
import com.blockeng.wallet.mapper.CoinWithdrawMapper;
import com.blockeng.wallet.service.CoinWithdrawService;
import com.clg.wallet.bean.ClientBean;
import com.clg.wallet.bean.TxData;
import com.clg.wallet.newclient.Client;
import com.clg.wallet.newclient.ClientFactory;
import com.clg.wallet.utils.BigDecimalUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月25日
 * @修改记录
 */

@Service
@Transactional
public class CoinBtcWithdrawServiceImpl extends ServiceImpl<CoinWithdrawMapper, CoinWithdraw> implements CoinBtcWithdrawService
{
    private static final Logger log = LoggerFactory.getLogger(CoinBtcWithdrawServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private CoinWithdrawService coinWithdrawService;

    private void sendDefault(CoinWithdraw coinWithdraw)
    {
        ClientBean coin = this.clientInfo.getClientInfoFromId(coinWithdraw.getCoinId());
        Client client = ClientFactory.getClient(coin);
        BigDecimal balance = client.getBalance().toBigDecimal();
        if (balance.compareTo(coinWithdraw.getMum()) < 0)
        {
            coinWithdraw.setStatus(Integer.valueOf(6)).setWalletMark("余额不足");
            this.coinWithdrawService.updateDrawInfo(coinWithdraw);
            return;
        }
        String txid = client.sendNormal(new TxData().setToAddress(coinWithdraw.getAddress()).setPass(coin.getWalletPass()).setBalance(BigDecimalUtils.formatBigDecimal(coinWithdraw.getMum()))).getResult().toString();
        if (!StringUtils.isEmpty(txid))
        {
            BigDecimal fee = client.getTransactionFee(txid).toBigDecimal();
            coinWithdraw.setTxid(txid).setStatus(Integer.valueOf(5)).setWalletMark("提币成功").setChainFee(fee);
        }
        else
        {
            coinWithdraw.setStatus(Integer.valueOf(6)).setWalletMark("打币失败,未知异常");
        }
        this.coinWithdrawService.updateDrawInfo(coinWithdraw);
    }

    public void transaction()
    {
        List<CoinWithdraw> coinWithdraws = this.coinWithdrawService.queryOutList("btc");
        if (CollectionUtils.isEmpty(coinWithdraws))
        {
            log.error("当前无提币信息");
            return;
        }
        for (CoinWithdraw item : coinWithdraws) {
            try
            {
                sendDefault(item);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                this.coinWithdrawService.updateDrawInfo(item.setStatus(Integer.valueOf(6)).setWalletMark("打币异常,请检查是否打币成功:" + e.getMessage()));
            }
        }
    }
}

