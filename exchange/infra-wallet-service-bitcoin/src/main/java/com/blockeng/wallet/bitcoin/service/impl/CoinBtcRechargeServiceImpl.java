package com.blockeng.wallet.bitcoin.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.blockeng.wallet.bitcoin.service.CoinBtcRechargeService;
import com.blockeng.wallet.entity.*;
import com.blockeng.wallet.help.ClientInfo;
import com.blockeng.wallet.mapper.CoinRechargeMapper;
import com.blockeng.wallet.service.*;
import com.clg.wallet.bean.TxData;
import com.clg.wallet.newclient.Client;
import com.clg.wallet.wallet.Omni.OmniNewClient;
import com.clg.wallet.wallet.Omni.bean.BtcBlock;
import com.clg.wallet.wallet.Omni.bean.BtcTransaction;
import com.clg.wallet.wallet.Omni.bean.BtcTxOut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月25日
 * @修改记录
 */
@Service
@Transactional
public class CoinBtcRechargeServiceImpl extends ServiceImpl<CoinRechargeMapper, CoinRecharge> implements CoinBtcRechargeService
{
    private static final Logger LOG = LoggerFactory.getLogger(CoinBtcRechargeServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private CoinConfigService coinConfigService;
    @Autowired
    private UserAddressService userAddressService;
    @Value("${recharge.count}")
    private int maxCount;
    @Autowired
    private CoinRechargeService coinRechargeService;
    @Autowired
    private WalletCollectTaskService walletCollectTaskService;
    @Autowired
    private AdminAddressService adminAddressService;

    public void searchRecharge(CoinConfig coin)
    {
        LOG.info("查询充值:" + coin.getName());
        OmniNewClient omniNewClient = (OmniNewClient)this.clientInfo.getClientFromId(coin.getId());
        int latest = omniNewClient.getBlockCount().toInteger() - coin.getMinConfirm().intValue();
        int currentCount = Integer.valueOf(this.coinConfigService.lastBlock(coin.getId())).intValue();
        if (latest > currentCount)
        {
            int count = latest - currentCount;
            if (count > this.maxCount) {
                latest = currentCount + this.maxCount;
            }
            LOG.info(coin.getName() + "区块区块开始,start:" + currentCount + ",end=" + latest);
            recharge(coin, omniNewClient, currentCount, latest);
            LOG.info(coin.getName() + "区块区块结束,start:" + currentCount + ",end=" + latest);
        }
    }

    private void recharge(CoinConfig coin, OmniNewClient omniNewClient, int currentCount, int latest)
    {
        for (int count = currentCount; count <= latest; count++)
        {
            LOG.info(coin.getName() + "区块高度:" + count);
            BtcBlock btcBlock = omniNewClient.getBlockByNumber("" + count).toObj(BtcBlock.class);
            if ((null != btcBlock) && (!CollectionUtils.isEmpty(btcBlock.getTx())) && (btcBlock.getTx().size() > 0)) {
                for (int i = 1; i < btcBlock.getTx().size(); i++)
                {
                    BtcTransaction btcTransaction = omniNewClient.getTransaction(btcBlock.getTx().get(i)).toObj(BtcTransaction.class);
                    if ((null != btcTransaction) && (!CollectionUtils.isEmpty(btcTransaction.getVout())))
                    {
                        List<BtcTxOut> vout = btcTransaction.getVout();
                        for (BtcTxOut item : vout)
                        {
                            List<String> addressesList = item.getScriptPubKey().getAddresses();
                            if (!CollectionUtils.isEmpty(addressesList))
                            {
                                UserAddress account = this.userAddressService.getByCoinIdAndAddr(addressesList.get(0), coin.getId());
                                if (null != account)
                                {
                                    LOG.error("查询到一条用户数据,txid是:" + btcBlock.getTx().get(i));
                                    boolean isSuccess = this.coinRechargeService.addRecord(btcTransaction.getTxid(), account.getAddress(), account.getUserId(), item.getValue(), 1, coin);
                                    if (isSuccess)
                                    {
                                        LOG.info("[" + coin.getName() + "]receiveConfirmed", "userId, txid, amount", new Object[] { account.getId(), btcTransaction.getTxid(), btcTransaction.getAmount() });
                                        addBackTask(coin, account.getUserId());
                                    }
                                    else
                                    {
                                        LOG.info("充值失败,txid:" + btcTransaction.getTxid());
                                    }
                                }
                            }
                            else
                            {
                                LOG.error("输出地址为空,txid:" + btcBlock.getTx().get(i));
                            }
                        }
                    }
                    else
                    {
                        LOG.error("txid是:" + btcBlock.getTx().get(i) + "输出为空");
                    }
                }
            } else {
                LOG.error("区块高度:" + count + "查询失败");
            }
        }
        this.coinConfigService.updateCoinLastblock(latest + "", coin.getId());
    }

    private void addBackTask(CoinConfig config, Long userId)
    {
        Client client = this.clientInfo.getClientFromId(config.getId());
        AdminAddress backAdminAddress = this.adminAddressService.queryAdminAccount(config.getCoinType(), 1);
        if ((null != backAdminAddress) && (!StringUtils.isEmpty(backAdminAddress.getAddress())))
        {
            BigDecimal payBalance = BigDecimal.ZERO;
            payBalance = client.getBalance().toBigDecimal();
            BigDecimal creditLimit = config.getCreditLimit();
            BigDecimal creditMaxLimit = config.getCreditMaxLimit();
            if ((null != creditMaxLimit) && (creditMaxLimit.compareTo(BigDecimal.ZERO) > 0) && (payBalance.compareTo(creditMaxLimit) > 0))
            {
                BigDecimal limit = payBalance;
                if (null != creditLimit) {
                    limit = limit.subtract(creditLimit);
                }
                String txid = client.sendNormal(new TxData().setToAddress(backAdminAddress.getAddress()).setPass(config.getWalletPass()).setBalance(limit)).getResult().toString();
                if (!StringUtils.isEmpty(txid)) {
                    this.walletCollectTaskService.insert(new WalletCollectTask().setAmount(limit).setUserId(userId).setCoinId(config.getId())
                            .setToAddress(backAdminAddress.getAddress())
                            .setStatus(Integer.valueOf(1)).setMark("归集到归账地址成功").setTxid(txid));
                }
            }
        }
    }
}

