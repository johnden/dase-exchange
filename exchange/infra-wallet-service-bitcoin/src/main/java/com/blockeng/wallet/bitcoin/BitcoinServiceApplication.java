package com.blockeng.wallet.bitcoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月25日
 * @修改记录
 */
@SpringBootApplication(scanBasePackages={"com.blockeng"})
public class BitcoinServiceApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(BitcoinServiceApplication.class, args);
    }
}