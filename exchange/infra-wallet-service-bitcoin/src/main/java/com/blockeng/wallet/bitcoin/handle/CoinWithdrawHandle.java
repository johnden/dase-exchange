package com.blockeng.wallet.bitcoin.handle;


import com.blockeng.wallet.entity.CoinWithdraw;
import com.blockeng.wallet.service.CoinWithdrawService;
import com.blockeng.wallet.service.UserAddressService;
import com.clg.wallet.utils.GsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月25日
 * @修改记录
 */
@Component
public class CoinWithdrawHandle {
    private static final Logger log = LoggerFactory.getLogger(CoinWithdrawHandle.class);
    @Autowired
    private UserAddressService userAddressService;
    @Autowired
    private CoinWithdrawService coinWithdrawService;

    @RabbitListener(queues={"finance.withdraw.send.btc"})
    public boolean coinWithdrawMessage(String message)
    {
        CoinWithdraw coinWithdraw =  GsonUtils.convertObj(message, CoinWithdraw.class);
        if (null == coinWithdraw)
        {
            log.error("btc打币失败,json转换异常");
            return false;
        }
        CoinWithdraw result = this.coinWithdrawService.selectById(new CoinWithdraw().setId(coinWithdraw.getId()));
        if ((null != result) && (result.getStatus().intValue() == 5))
        {
            this.coinWithdrawService.updateDrawInfo(coinWithdraw.setStatus(Integer.valueOf(5)).setWalletMark("该笔提款已经打出,请勿重复发起提币操作"));
            return true;
        }
        return this.coinWithdrawService.insert(coinWithdraw);
    }
}
