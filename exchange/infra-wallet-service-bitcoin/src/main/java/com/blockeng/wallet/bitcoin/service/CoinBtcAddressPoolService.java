package com.blockeng.wallet.bitcoin.service;

import com.baomidou.mybatisplus.service.IService;
import com.blockeng.wallet.entity.AddressPool;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月25日
 * @修改记录
 */
public interface CoinBtcAddressPoolService extends IService<AddressPool> {
    void createAddress();
}
