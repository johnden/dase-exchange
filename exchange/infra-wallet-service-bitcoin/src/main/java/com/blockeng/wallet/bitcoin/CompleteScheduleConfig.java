package com.blockeng.wallet.bitcoin;

import com.blockeng.wallet.bitcoin.job.BtcTask;
import com.blockeng.wallet.config.CheckCoinStatus;
import com.blockeng.wallet.jobs.Task;
import com.blockeng.wallet.service.CommitStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月25日
 * @修改记录
 */

@Configuration
@EnableScheduling
public class CompleteScheduleConfig implements SchedulingConfigurer
{
    private static final Logger LOG = LoggerFactory.getLogger(CompleteScheduleConfig.class);
    @Autowired
    public CommitStatusService commitStatusService;
    @Autowired
    private CheckCoinStatus checkCoinStatus;
    @Autowired
    private BtcTask btcTask;
    @Autowired
    private Task task;

    public void configureTasks(ScheduledTaskRegistrar taskRegistrar)
    {
        if (this.checkCoinStatus.allIsRunning())
        {
            LOG.info("btc开始运行");
            addBtcTask(taskRegistrar);
        }
    }

    private void addBtcTask(ScheduledTaskRegistrar taskRegistrar)
    {
        if (this.checkCoinStatus.featuresIsOpen("recharge")) {
            this.btcTask.addRecharge(taskRegistrar);
        }
        if (this.checkCoinStatus.featuresIsOpen("address")) {
            this.btcTask.createAddress(taskRegistrar);
        }
        if (this.checkCoinStatus.featuresIsOpen("draw")) {
            this.btcTask.withDraw(taskRegistrar);
        }
        if (this.checkCoinStatus.featuresIsOpen("other")) {
            this.task.service(taskRegistrar, "btc");
        }
    }
}
