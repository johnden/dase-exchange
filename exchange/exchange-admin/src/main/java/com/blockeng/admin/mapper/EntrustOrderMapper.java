package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.entity.EntrustOrder;
import com.blockeng.admin.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 委托订单信息 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface EntrustOrderMapper extends BaseMapper<EntrustOrder> {

    IPage<EntrustOrder> selectListPage(IPage<EntrustOrder> page, @Param("ew") Wrapper<EntrustOrder> wrapper);

    List<EntrustOrder> selectListPageEmpty(@Param("current") long current, @Param("size") long size);


    List<EntrustOrder> selectListPageByOrder(@Param("ew") Wrapper<EntrustOrder> wrapper, @Param("otherEw") Wrapper<User> otherEw, @Param("current") long current, @Param("size") long size);


    List<EntrustOrder> selectListPageByUser(@Param("ew") Wrapper<User> wrapper, @Param("otherEw") Wrapper<EntrustOrder> otherEw, @Param("current") long current, @Param("size") long size);

    Integer selectListPageCount();


    Integer selectListPageByOrderCount(@Param("ew") Wrapper<EntrustOrder> wrapper);

    Integer selectListPageByUserCount(@Param("ew") Wrapper<User> wrapper);
}
