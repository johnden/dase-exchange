package com.blockeng.admin.mapper;

import com.blockeng.admin.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 系统菜单 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> selectPrivilegeMenuList(Long roleId);

    List<SysMenu> selectListByUserId(Long userId);
}
