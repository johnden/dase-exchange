package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.dto.SysUserLogDTO;
import com.blockeng.admin.entity.SysUserLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
public interface SysUserLogService extends IService<SysUserLog> {

    /**
     * 分页查询
     * @param var1
     * @param var2
     * @return
     */
    IPage<SysUserLogDTO> selectListPage(IPage<SysUserLogDTO> var1, Wrapper<SysUserLogDTO> var2);

}
