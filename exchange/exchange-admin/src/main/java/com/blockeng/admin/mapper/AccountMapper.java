package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.entity.Account;
import com.blockeng.admin.entity.AccountDetail;
import com.blockeng.admin.entity.User;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 用户财产记录 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface AccountMapper extends BaseMapper<Account> {

    List<Account> selectListPage(@Param("ew") Wrapper<Account> wrapper);

    List<Long> selectOptimizePageSql(IPage<Account> page, @Param("ew") Wrapper<Account> wrapper);

    List<Account> selectListPageEmpty(@Param("current") long current, @Param("size") long size);

    List<Account> selectListPageFromUser(@Param("current") long current, @Param("size") long size, @Param("ew") Wrapper<User> ew, @Param("accountEw") Wrapper<Account> accountEw);

    List<Account> selectListPageFromAccount(@Param("current") long current, @Param("size") long size, @Param("ew") Wrapper<Account> ew, @Param("userEw") Wrapper<User> userEw);

    Integer selectListPageCount();

    Integer selectListPageCountFromUser(@Param("ew") Wrapper<User> ew, @Param("accountEw") Wrapper<Account> accountEw);

    Integer selectListPageCountFromAccount(@Param("ew") Wrapper<Account> ew, @Param("userEw") Wrapper<User> userEw);

    /**
     * 冻结账户资金
     *
     * @param accountId 资金账户ID
     * @param amount    冻结金额
     * @return
     */
    int lockAmount(@Param("accountId") long accountId, @Param("amount") BigDecimal amount);

    /**
     * 解冻账户资金
     *
     * @param accountId 资金账户ID
     * @param amount    解冻金额
     * @return
     */
    int unlockAmount(@Param("accountId") long accountId, @Param("amount") BigDecimal amount);

    /**
     * 增加账户资金
     *
     * @param accountId 资金账户ID
     * @param amount    增加金额
     * @return
     */
    int addAmount(@Param("accountId") long accountId, @Param("amount") BigDecimal amount);

    /**
     * 扣减账户资金
     *
     * @param accountId 资金账户ID
     * @param amount    扣减金额
     * @return
     */
    int subtractAmount(@Param("accountId") long accountId, @Param("amount") BigDecimal amount);

    /**
     *初始化资金账户
     * @param coinId
     * @return
     */
    long initialBatchAccount(@Param("coinId") long coinId);
}
