package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.entity.CashWithdrawAuditRecord;
import com.blockeng.admin.mapper.CashWithdrawAuditRecordMapper;
import com.blockeng.admin.service.CashWithdrawAuditRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 提现审核记录 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
public class CashWithdrawAuditRecordServiceImpl extends ServiceImpl<CashWithdrawAuditRecordMapper, CashWithdrawAuditRecord> implements CashWithdrawAuditRecordService {

    /**
     * 法币提现审核记录
     *
     * @param orderId 法币提现订单号
     * @return
     */
    @Override
    public List<CashWithdrawAuditRecord> queryCashWithdrawAuditRecord(Long orderId) {
        QueryWrapper<CashWithdrawAuditRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id", orderId).orderByDesc("created");
        return baseMapper.selectList(wrapper);
    }
}
