package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.dto.UserBankDTO;
import com.blockeng.admin.entity.UserAuthAuditRecord;
import com.blockeng.admin.entity.UserBank;

/**
 * <p>
 * 用户人民币提现地址 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface UserBankService extends IService<UserBank> {

   IPage<UserBankDTO> selectMapPage(IPage<UserBankDTO> page, QueryWrapper<UserBankDTO> queryWrapper);
}
