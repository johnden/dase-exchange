package com.blockeng.admin.config;

import com.blockeng.admin.aop.drds.DrdsTransactionAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author qiang
 */
@Configuration
public class FlexibleTransactionConfig {

    @Bean
    public DrdsTransactionAspect drdsTransaction() {
        return new DrdsTransactionAspect();
    }
}
