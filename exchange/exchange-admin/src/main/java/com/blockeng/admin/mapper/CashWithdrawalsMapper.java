package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.dto.CashWithdrawalsCountDTO;
import com.blockeng.admin.dto.UserWithDrawalsDTO;
import com.blockeng.admin.entity.CashWithdrawals;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 提现表 Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2018-05-17
 */
public interface CashWithdrawalsMapper extends BaseMapper<CashWithdrawals> {


    IPage<UserWithDrawalsDTO> selectMapPage(IPage<UserWithDrawalsDTO> page, @Param("ew") Wrapper wrapper);

    UserWithDrawalsDTO selectOneObj(Long id);

    /**
     * 查询出规定的条数数据
     * @param pageSize 为空返回查询所有，否则返回规定的条数集合
     * @return
     */
    List<UserWithDrawalsDTO> selectCashWithdrawalsDTOList(@Param("pageSize")Integer pageSize);

    /**
     * 根据日期和用户统计提现人数
     * @param countDate
     * @param uidStrs 用户ID字符串('1,2,3')
     * @param status 状态
     * @return
     */
    Integer countByDateAndUidStrs(@Param("countDate") String countDate,
                                  @Param("uidStrs") String uidStrs,
                                  @Param("status") Integer status);

    /**
     * 提现金额，到账金额，充值币种，提现笔数，提现用户数 ，提现时间 统计统计
     * @param page
     * @param wrapper
     * @return
     */
    List<CashWithdrawalsCountDTO> selectCountMain(IPage<CashWithdrawalsCountDTO> page, @Param("ew") Wrapper wrapper);

    /**
     * <!--成功笔数，充值时间-->
     * @param wrapper
     * @return
     */
    List<CashWithdrawalsCountDTO> selectValidCounts(@Param("ew") Wrapper wrapper);


    /**
     * <!--用户数，用户id，充值时间-->
     * @param wrapper
     * @return
     */
    List<CashWithdrawalsCountDTO> selectUserCt(@Param("ew") Wrapper wrapper);

}
