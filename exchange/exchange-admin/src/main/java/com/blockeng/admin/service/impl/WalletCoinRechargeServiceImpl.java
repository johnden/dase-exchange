package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.entity.WalletCoinRecharge;
import com.blockeng.admin.mapper.WalletCoinRechargeMapper;
import com.blockeng.admin.service.WalletCoinRechargeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户充值,当前用户充值成功之后添加数据到这个表,充值一般无手续费.当status为1的时候表示充值成功 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-17
 */
@Service
public class WalletCoinRechargeServiceImpl extends ServiceImpl<WalletCoinRechargeMapper, WalletCoinRecharge> implements WalletCoinRechargeService {

}
