package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.blockeng.admin.dto.TurnoverOrderCountDTO;
import com.blockeng.admin.dto.UserCountRegDTO;
import com.blockeng.admin.dto.UserDTO;
import com.blockeng.admin.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface UserMapper extends BaseMapper<User> {

    List<UserCountRegDTO> selectRegCountPage(IPage<UserCountRegDTO> page, @Param("ew") Wrapper<UserCountRegDTO> wrapper);

    /**
     * 根据日期统计用户绑定邮箱人数
     *
     * @param countDate
     * @return
     */
    Integer countEmailBindByDate(@Param("countDate") String countDate);

    /**
     * 根据日期统计用户设置资金密码人数
     *
     * @param countDate
     * @return
     */
    Integer countSetPayPwdByDate(@Param("countDate") String countDate);

    /**
     * #持币人数
     *
     * @param coins
     * @return
     */
    List<TurnoverOrderCountDTO> selectUserCount(@Param("coins") String[] coins);

    List<Long> selectOptimizePageSql(IPage<UserDTO> page, @Param("ew") Wrapper<UserDTO> wrapper);

    List<UserDTO> selectUserList(@Param("ew") Wrapper<UserDTO> wrapper);

    IPage<UserDTO> selectListAuditPage(IPage<UserDTO> page, @Param("ew")  Wrapper<UserDTO> wrapper);
}
