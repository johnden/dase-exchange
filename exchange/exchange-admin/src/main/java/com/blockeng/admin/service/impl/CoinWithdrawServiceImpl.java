package com.blockeng.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.AuditDTO;
import com.blockeng.admin.dto.CashWithdrawalsCountDTO;
import com.blockeng.admin.dto.CoinWithDrawDTO;
import com.blockeng.admin.dto.CoinWithdrawalsCountDTO;
import com.blockeng.admin.entity.*;
import com.blockeng.admin.enums.MessageChannel;
import com.blockeng.admin.mapper.CoinWithdrawMapper;
import com.blockeng.admin.service.*;
import com.blockeng.framework.constants.Constant;
import com.blockeng.framework.enums.BusinessType;
import com.blockeng.framework.enums.CoinWithdrawStatus;
import com.blockeng.framework.exception.AccountException;
import com.blockeng.framework.exception.ExchangeException;
import com.blockeng.framework.http.Response;
import com.blockeng.framework.utils.GsonUtil;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 虚拟币提现 服务实现类
 * </p>
 *
 * @author Haliyo
 * @since 2018-05-17
 */
@Service
@Slf4j
public class CoinWithdrawServiceImpl extends ServiceImpl<CoinWithdrawMapper, CoinWithdraw> implements CoinWithdrawService, Constant {

    @Autowired
    private CoinWithdrawAuditRecordService coinWithdrawAuditRecordService;

    @Autowired
    private UserService userService;

    @Autowired
    private MultiLevelAuditService multiLevelAuditService;

    @Autowired
    private LockService lockService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CoinConfigService coinConfigService;

    @Autowired
    private CoinService coinService;

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Override
    public IPage<CoinWithdraw> selectListPage(IPage<CoinWithdraw> page, QueryWrapper<CoinWithdraw> wrapper) {
        //page.setRecords();
        return baseMapper.selectListPage(page, wrapper);
    }

    /**
     * 提币审核
     *
     * @param auditDTO 提币审核请求参数
     * @param sysUser  当前登录用户
     * @throws ExchangeException
     */
    @Override
    @Transactional
    public void coinWithdrawAudit(AuditDTO auditDTO, SysUser sysUser) throws ExchangeException {
        log.info("coinWithdrawAudit auditDTO:" + auditDTO);
        // 校验权限
        if (!multiLevelAuditService.coinWithdrawPermissionCheck(auditDTO.getId(), sysUser)) {
            throw new ExchangeException("审核权限不足");
        }
        boolean isLocked = false;
        try {
            // 通过内存锁防止重复提交审核，导致资金异常
            isLocked = lockService.getLock(REDIS_KEY_COIN_WITHDRAW_AUDIT_LOCK, String.valueOf(auditDTO.getId()), false);
            if (!isLocked) {
                throw new ExchangeException("已经提交审核，请勿重复操作");
            }
            CoinWithdraw coinWithdraw = baseMapper.selectById(auditDTO.getId());
            if (coinWithdraw == null) {
                throw new ExchangeException("提现订单不存在");
            }
            if (coinWithdraw.getStatus() != CoinWithdrawStatus.PENDING.getCode()) {
                throw new ExchangeException("此记录已审核");
            }
            // 审核轨迹
            CoinWithdrawAuditRecord coinRechargeAuditRecord = new CoinWithdrawAuditRecord();
            coinRechargeAuditRecord.setOrderId(auditDTO.getId())
                    .setStatus(auditDTO.getStatus())
                    .setRemark(auditDTO.getRemark())
                    .setStep(coinWithdraw.getStep())
                    .setAuditUserId(sysUser.getId())
                    .setAuditUserName(sysUser.getFullname());
            coinWithdrawAuditRecordService.insert(coinRechargeAuditRecord);
            User user = userService.selectById(coinWithdraw.getUserId());
            // 更新提现申请单状态
            coinWithdraw.setRemark(auditDTO.getRemark());
            if (auditDTO.getStatus().intValue() == CoinWithdrawStatus.REFUSE.getCode()) {
                // 审核拒绝
                coinWithdraw.setStatus(CoinWithdrawStatus.REFUSE.getCode())
                        .setMum(BigDecimal.ZERO)
                        .setAuditTime(new Date());
                baseMapper.updateById(coinWithdraw);
                // 解冻资金账户
                accountService.unlockAmount(coinWithdraw.getUserId(),
                        coinWithdraw.getCoinId(),
                        coinWithdraw.getNum(),
                        BusinessType.WITHDRAW,
                        coinWithdraw.getId());
                return;
            }
            // 审核通过
            Config config = configService.queryBuyCodeAndType(CONFIG_TYPE_SYSTEM, Constant.CONFIG_COIN_WITHDRAW_AUDIT_STEPS);
            if (config == null || Strings.isNullOrEmpty(config.getValue())) {
                throw new ExchangeException("没有配置审核级数");
            }
            int step = coinWithdraw.getStep();
            if (step == Integer.valueOf(config.getValue()).intValue()) {
                // 最终审核通过
                CoinConfig coinConfig = coinConfigService.selectById(coinWithdraw.getCoinId());
                if (null == coinConfig) {
                    throw new ExchangeException("没有查询到当前币种");
                }
                coinWithdraw.setStatus(CoinWithdrawStatus.PASSED.getCode());
                if (0 == coinConfig.getAutoDraw() &&
                        (null == coinConfig.getAutoDrawLimit()
                                || coinConfig.getAutoDrawLimit().compareTo(BigDecimal.ZERO) <= 0
                                || coinConfig.getAutoDrawLimit().compareTo(coinWithdraw.getNum()) >= 0)) { //自动提款
                    coinWithdraw.setType(1);
                    notifyAuditResult(coinWithdraw);
                } else { //手工提币
                    coinWithdraw.setType(2); //2手工打币
                }
                coinWithdraw.setAuditTime(new Date());
                baseMapper.updateById(coinWithdraw);
                return;
            }
            // 不是最终审核，审计级别加一级，状态为待审核
            coinWithdraw.setStatus(CoinWithdrawStatus.PENDING.getCode()).setStep(step + 1);
            baseMapper.updateById(coinWithdraw);
            return;
        } catch (AccountException e) {
            throw new ExchangeException(e.getMessage());
        } finally {
            // 释放锁
            if (isLocked) {
                lockService.unlock(REDIS_KEY_COIN_WITHDRAW_AUDIT_LOCK, String.valueOf(auditDTO.getId()));
            }
        }
    }

    /**
     * 数字货币提现审核结果通知
     *
     * @param coinWithdraw 提现申请单
     * @return
     */
    public void notifyAuditResult(CoinWithdraw coinWithdraw) {
        if (coinWithdraw.getStatus() == CoinWithdrawStatus.PASSED.getCode()) {//通知钱包服务器打币
            Coin coin = coinService.selectById(coinWithdraw.getCoinId());
            String type = coin.getType();
            type = type.replace("Token", "");
            rabbitTemplate.convertAndSend("finance.withdraw.send." + type, GsonUtil.toJson(coinWithdraw));
        }
    }

    @Override
    public IPage<CoinWithdrawalsCountDTO> selectCountMain(IPage<CoinWithdrawalsCountDTO> page, QueryWrapper<CoinWithdrawalsCountDTO> queryWrapper) {
        return page.setRecords(baseMapper.selectCountMain(page, queryWrapper));
    }

    @Override
    public List<CoinWithdrawalsCountDTO> selectValidCounts(QueryWrapper<CoinWithdrawalsCountDTO> queryWrapper) {
        return baseMapper.selectValidCounts(queryWrapper);
    }

    @Override
    public List<CoinWithdrawalsCountDTO> selectUserCt(QueryWrapper<CoinWithdrawalsCountDTO> queryWrapper) {
        return baseMapper.selectUserCt(queryWrapper);
    }

    @Override
    public Object withDrawSuccess(CoinWithDrawDTO coinWithDrawDTO) {
        CoinWithdraw coinWithdraw = selectById(coinWithDrawDTO.getId());
        if (null == coinWithdraw) {
            return Response.err(50034, "未找到该笔体现订单");
        }
        coinWithdraw.setStatus(5).setTxid(coinWithDrawDTO.getTxid()).setChainFee(coinWithDrawDTO.getChainFee());
        // rabbitTemplate.convertSendAndReceive(MessageChannel.FINANCE_WITHDRAW_RESULT.getChannel(),GsonUtil.toJson(coinWithdraw));
        rabbitTemplate.convertAndSend(MessageChannel.FINANCE_WITHDRAW_RESULT.getChannel(), GsonUtil.toJson(coinWithdraw));
        return Response.ok();
    }
}
