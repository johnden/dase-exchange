package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.entity.AccountDetail;
import com.blockeng.admin.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 资金账户流水 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-16
 */
public interface AccountDetailMapper extends BaseMapper<AccountDetail> {

    List<AccountDetail> selectAccountDetailList(@Param("ew") Wrapper<AccountDetail> wrapper);

    List<Long> selectOptimizePageSql(IPage<AccountDetail> page, @Param("ew") Wrapper<AccountDetail> wrapper);

    List<AccountDetail> selectListPageEmpty(@Param("current") long current, @Param("size") long size);

    List<AccountDetail> selectListPageFromUser(@Param("current") long current, @Param("size") long size, @Param("ew") Wrapper<User> ew, @Param("ewOther") Wrapper<AccountDetail> ewOther);

    List<AccountDetail> selectListPageFromAccount(@Param("current") long current, @Param("size") long size, @Param("ew") Wrapper<AccountDetail> ew, @Param("ewOther") Wrapper<User> ewOther);

    Integer selectListPageCount();

    Integer selectListPageCountFromUser(@Param("ew") Wrapper<User> ew, @Param("ewOther") Wrapper<AccountDetail> ewOther);

    Integer selectListPageCountFromAccount(@Param("ew") Wrapper<AccountDetail> ew, @Param("ewOther") Wrapper<User> ewOther);

}
