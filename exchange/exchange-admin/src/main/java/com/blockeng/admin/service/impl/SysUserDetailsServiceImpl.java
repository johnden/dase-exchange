package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.admin.entity.SysMenu;
import com.blockeng.admin.entity.SysPrivilege;
import com.blockeng.admin.entity.SysUser;
import com.blockeng.admin.service.SysMenuService;
import com.blockeng.admin.service.SysPrivilegeService;
import com.blockeng.admin.service.SysUserRoleService;
import com.blockeng.admin.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qiang
 */
@Service
public class SysUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysMenuService sysMenuService;

    @Autowired
    private SysPrivilegeService sysPrivilegeService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<SysUser> ew = new QueryWrapper<>();
        ew.eq("username", username);
        SysUser user = sysUserService.selectOne(ew);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        }
        // 查询权限信息
        List<SysPrivilege> privileges;
        List<SysMenu> menus;
        // 如果是管理员，加载所有权限和菜单
        if (sysUserRoleService.isAdminUser(user.getId())) {
            privileges = sysPrivilegeService.selectList(new QueryWrapper<>());
            QueryWrapper<SysMenu> wrapper = new QueryWrapper<>();
            wrapper.orderByAsc("id");
            menus = sysMenuService.selectList(wrapper);
        } else {
            privileges = sysPrivilegeService.selectListByUserId(user.getId());
            menus = sysMenuService.selectListByUserId(user.getId());
        }
        user.setMenus(menus);
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (SysPrivilege privilege : privileges) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(privilege.getName());
            grantedAuthorities.add(grantedAuthority);
        }
        user.setAuthorities(grantedAuthorities);
        return user;
    }
}
