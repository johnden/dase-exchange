package com.blockeng.admin.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.annotation.AuditLog;
import com.blockeng.admin.common.ResultMap;
import com.blockeng.admin.entity.AccountDetail;
import com.blockeng.admin.entity.User;
import com.blockeng.admin.enums.SysLogTypeEnum;
import com.blockeng.admin.service.AccountDetailService;
import com.blockeng.admin.service.UserService;
import com.blockeng.admin.view.ReportCsvUtils;
import com.google.common.base.Strings;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.util.CsvContext;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 资金账户流水 前端控制器
 * </p>
 *
 * @author Haliyo
 * @since 2018-05-16
 */
@RestController
@RequestMapping("/accountDetail")
@Api(value = "资金账户流水", description = "资金账户流水管理")
public class AccountDetailController {

    @Autowired
    private AccountDetailService accountDetailService;

    @Autowired
    private UserService userService;

    @AuditLog(value = "按条件分页查询资金账户流水列表", type = SysLogTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('account_detail_query')")
    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "每页显示条数，默认 10", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "current", value = "当前页", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "accountId", value = "账户ID", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "coinId", value = "币种ID", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = " 用户ID", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userName", value = "用户名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "mobile", value = "手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "amountStart", value = "起始金额", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "amountEnd", value = "截止金额", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataType = "String", paramType = "query"),
    })
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")}, value = "按条件分页查询资金账户流水列表", httpMethod = "GET")
    public Object selectPage(@ApiIgnore Page<AccountDetail> page,
                             String accountId,
                             String userId,
                             String coinId,
                             String userName,
                             String mobile,
                             String amountStart,
                             String amountEnd,
                             String startTime,
                             String endTime) {

        QueryWrapper<AccountDetail> wrapper = new QueryWrapper<>();
        if (!Strings.isNullOrEmpty(accountId)) {
            wrapper.eq("a.account_id", accountId);
        }
        if (!Strings.isNullOrEmpty(coinId)) {
            wrapper.eq("a.coin_id", coinId);
        }
        if (!Strings.isNullOrEmpty(userId)) {
            wrapper.eq("a.user_id", userId);
        }
        if (!Strings.isNullOrEmpty(amountStart)) {
            wrapper.ge("a.amount", amountStart);
        }
        if (!Strings.isNullOrEmpty(amountEnd)) {
            wrapper.le("a.amount", amountEnd);
        }
        if (!Strings.isNullOrEmpty(startTime)) {
            wrapper.ge("a.created", startTime + " 00:00:00");
        }
        if (!Strings.isNullOrEmpty(endTime)) {
            wrapper.le("a.created", endTime + " 23:59:59");
        }
        if (!(Strings.isNullOrEmpty(mobile) && Strings.isNullOrEmpty(userName))) {
            List<Long> userIds = new ArrayList<>();
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            if (!Strings.isNullOrEmpty(mobile)) {
                queryWrapper.eq("mobile", mobile);
            }
            if (!Strings.isNullOrEmpty(userName)) {
                queryWrapper.eq("username", userName);
            }
            userService.selectList(queryWrapper).forEach(user -> {
                userIds.add(user.getId());
            });
            if (CollectionUtils.isEmpty(userIds)) {
                return ResultMap.getSuccessfulResult(page);
            }
            wrapper.in("a.user_id", userIds);
        }
        IPage<AccountDetail> accountDetails = accountDetailService.selectListPage(page, wrapper);
        return ResultMap.getSuccessfulResult(accountDetails);

        // long size = page.getSize();
        // long total = 0;
        // long current = (page.getCurrent() - 1) * size;
        // List<AccountDetail> accountDetails = null;
        // boolean accountEwEmptyOfWhere = accountEw.isEmptyOfWhere();
        //
        // boolean userEwEmptyOfWhere = userEw.isEmptyOfWhere();
        //
        // if (!accountEwEmptyOfWhere) { // 用account作为主要查询条件
        //     accountDetails = accountDetailService.selectListPageFromAccount(current, size, accountEw, userEw);
        // } else if (!userEwEmptyOfWhere) { // 用user作为主要查询条件
        //     accountDetails = accountDetailService.selectListPageFromUser(current, size, userEw, accountEw);
        // } else { //直接搜索,无任何查询条件
        //     accountDetails = accountDetailService.selectListPageEmpty(current, size);
        // }
        // if (!CollectionUtils.isEmpty(accountDetails)) {
        //     if (accountDetails.size() < size && page.getCurrent() <= 1) {
        //         total = accountDetails.size();
        //     } else {
        //         if (!accountEwEmptyOfWhere) {
        //             total = accountDetailService.selectListPageCountFromAccount(accountEw, userEw);
        //         }
        //         else if (!userEwEmptyOfWhere) {
        //             total = accountDetailService.selectListPageCountFromUser(userEw, accountEw);
        //         }
        //         else {
        //             total = accountDetailService.selectListPageCount();
        //         }
        //     }
        // }
        // return ResultMap.getSuccessfulResult(new PageDTO().setCurrent(page.getCurrent()).setSize(size).setTotal(total).setRecords(accountDetails));
    }


    @AuditLog(value = "导出资金账户流水", type = SysLogTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('account_detail_export')")
    @GetMapping
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")}, value = "导出资金账户流水", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "accountId", value = "账户ID", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "coinId", value = "币种ID", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = " 用户ID", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userName", value = "用户名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "mobile", value = "手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "amountStart", value = "起始金额", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "amountEnd", value = "截止金额", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataType = "String", paramType = "query"),
    })
    @RequestMapping({"/exportList"})
    public void export(@ApiIgnore Page<AccountDetail> page,
                       String accountId,
                       String userId,
                       String coinId,
                       String userName,
                       String mobile,
                       String amountStart,
                       String amountEnd,
                       String startTime,
                       String endTime
            , HttpServletResponse response) {
        String fileName = "资金流水.csv";
        String[] header = {"ID", "账户ID", "关联账户ID", "用户名", "币种名称", "金额", "收付类型", "业务类型", "关联订单号", "发生时间", "备注"};
        String[] properties = {"idStr", "accountIdStr", "refAccountIdStr", "userName", "coinName", "amount", "directionStr", "businessTypeStr", "orderIdStr", "createdStr", "remark"};
        CellProcessor[] PROCESSORS = new CellProcessor[]{
                null,
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = "\t" + String.valueOf(value);
                        return v;
                    }
                },
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = "\t" + String.valueOf(value);
                        return v;
                    }
                },
                null,
                null,
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = String.valueOf(value);
                        if (value != null) {
                            DecimalFormat df = new DecimalFormat("0.00000000");
                            v = df.format(value);
                        }
                        return "\t" + v;
                    }
                },
                null,
                null,
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = "\t" + String.valueOf(value);
                        return v;
                    }
                },
                null,
                null
        };
        page.setCurrent(1);
        page.setSize(100000);//限制导出数

        QueryWrapper<AccountDetail> wrapper = new QueryWrapper<>();
        if (!Strings.isNullOrEmpty(accountId)) {
            wrapper.eq("a.account_id", accountId);
        }
        if (!Strings.isNullOrEmpty(coinId)) {
            wrapper.eq("a.coin_id", coinId);
        }
        if (!Strings.isNullOrEmpty(userId)) {
            wrapper.eq("a.user_id", userId);
        }
        if (!Strings.isNullOrEmpty(amountStart)) {
            wrapper.ge("a.amount", amountStart);
        }
        if (!Strings.isNullOrEmpty(amountEnd)) {
            wrapper.le("a.amount", amountEnd);
        }
        if (!Strings.isNullOrEmpty(startTime)) {
            wrapper.ge("a.created", startTime + " 00:00:00");
        }
        if (!Strings.isNullOrEmpty(endTime)) {
            wrapper.le("a.created", endTime + " 23:59:59");
        }
        if (!(Strings.isNullOrEmpty(mobile) && Strings.isNullOrEmpty(userName))) {
            List<Long> userIds = new ArrayList<>();
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            if (!Strings.isNullOrEmpty(mobile)) {
                queryWrapper.eq("mobile", mobile);
            }
            if (!Strings.isNullOrEmpty(userName)) {
                queryWrapper.eq("username", userName);
            }
            userService.selectList(queryWrapper).forEach(user -> {
                userIds.add(user.getId());
            });
            if (CollectionUtils.isEmpty(userIds)) {
                try {
                    ReportCsvUtils.reportListCsv(response, header, properties, fileName, null, PROCESSORS);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            wrapper.in("a.user_id", userIds);
        }
        IPage<AccountDetail> retpage = accountDetailService.selectListPage(page, wrapper);
        try {
            ReportCsvUtils.reportListCsv(response, header, properties, fileName, retpage.getRecords(), PROCESSORS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
