package com.blockeng.admin.dto.mappers;

import com.blockeng.admin.dto.MineDataDTO;
import com.blockeng.admin.entity.MineData;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author qiang
 */
@Mapper
public interface MineDataMapper {

    MineDataMapper INSTANCE = Mappers.getMapper(MineDataMapper.class);

    MineData from(MineDataDTO form);
}
