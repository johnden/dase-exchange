package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.dto.UserCountLoginDTO;
import com.blockeng.admin.entity.UserLoginLog;
import com.blockeng.admin.mapper.*;
import com.blockeng.admin.service.UserLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户登录日志 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogMapper, UserLoginLog> implements UserLoginLogService {

    @Override
    public IPage<UserCountLoginDTO> selectLoginCountpage(IPage<UserCountLoginDTO> page, Wrapper<UserCountLoginDTO> wrapper) {
        // 目前没有记录登录日志，无法统计登录人数
        List<UserCountLoginDTO> countRegDTOList = new ArrayList<>(0);
        page.setRecords(countRegDTOList);
        return page;
    }
}
