package com.blockeng.admin.dto;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class PageDTO {

    private long current;

    private long size;

    private long total;

    private List records;

}
