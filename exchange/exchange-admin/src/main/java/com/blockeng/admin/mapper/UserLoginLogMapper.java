package com.blockeng.admin.mapper;

import com.blockeng.admin.entity.UserLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录日志 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

}
