package com.blockeng.admin.web.statistics;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.annotation.AuditLog;
import com.blockeng.admin.common.ResultMap;
import com.blockeng.admin.dto.CashRechargeCountDTO;
import com.blockeng.admin.enums.SysLogTypeEnum;
import com.blockeng.admin.service.CashRechargeService;
import com.google.common.base.Strings;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * CNY充值统计
 *
 * @author lxl
 * @date 2018-05-31
 */

@Slf4j
@Api(value = "CNY充值统计controller", tags = {"CNY充值统计"})
@RestController
@RequestMapping("/cashRechargeCount")
public class CashRechargeCountController {

    @Autowired
    private CashRechargeService cashRechargeService;

    @AuditLog(value = "CNY充值统计", type = SysLogTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('cash_recharge_statistics_query')")
    @GetMapping
    @RequestMapping({"/getList"})
    @ResponseBody
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")}, value = "CNY充值统计列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页数", required = true, dataType = "int"),
            @ApiImplicitParam(name = "size", value = "每页条数", required = true, dataType = "int"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataType = "String")
    })
    @ApiResponses({
            @ApiResponse(code = 0, message = "成功", response = CashRechargeCountDTO.class),
            @ApiResponse(code = 1, message = "失败")
    })
    public ResultMap getList(@RequestParam(value = "current", defaultValue = "1") int current,
                             @RequestParam(value = "size", defaultValue = "10") int size,
                             @RequestParam(value = "startTime", defaultValue = "") String startTime,
                             @RequestParam(value = "endTime", defaultValue = "") String endTime,
                             String coinName) {
        //Map<String, Object> paramMap = new HashMap<>();
        Page<CashRechargeCountDTO> pager = new Page<>(current, size);

        QueryWrapper<CashRechargeCountDTO> queryWrapper = new QueryWrapper<>();
        if (!Strings.isNullOrEmpty(startTime)) {
            queryWrapper.ge("c.created", startTime);
        }
        if(StringUtils.isNotBlank(endTime)){
            endTime=endTime+" 23:59:59";
        }
        if (!Strings.isNullOrEmpty(endTime)) {
            queryWrapper.ge("c.created", endTime);
        }
        IPage<CashRechargeCountDTO> pt = cashRechargeService.selectCountMain(pager, queryWrapper);
        if (pt != null && pt.getRecords().size() > 0) {
            String dates[] = new String[pt.getRecords().size()];
            int i = 0;
            for (CashRechargeCountDTO c : pt.getRecords()) {
                dates[i] = c.getCreated();
                i++;
            }
            queryWrapper.in("created", dates);
            List<CashRechargeCountDTO> pt2 = cashRechargeService.selectValidCounts(queryWrapper);
            List<CashRechargeCountDTO> pt3 = cashRechargeService.selectUserCt(queryWrapper);
            String validCounts = "0";
            for (CashRechargeCountDTO c : pt.getRecords()) {
                for (CashRechargeCountDTO c2 : pt2) {
                    if (c.getCreated().equals(c2.getCreated())) {
                        c.setValidCounts(c2.getValidCounts());
                    }
                }
                if (c.getValidCounts() == null) {
                    c.setValidCounts(validCounts);
                }
                int oldCount = 0;
                for (CashRechargeCountDTO c3 : pt3) {
                    if (c.getCreated().equals(c3.getCreated())) {
                        if (Integer.valueOf(c3.getUserCt()) > oldCount) {
                            c.setUserId(c3.getUserId());
                            c.setUserCt(c3.getUserCt());
                            oldCount = Integer.valueOf(c3.getUserCt());
                        }
                    }
                }
            }
        }
        return ResultMap.getSuccessfulResult(pt);
    }
}
