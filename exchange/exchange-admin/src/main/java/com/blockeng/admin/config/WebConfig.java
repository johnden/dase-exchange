package com.blockeng.admin.config;

import com.blockeng.admin.view.XlsxStreamingView;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.BeanNameViewResolver;

/**
 * @author qiang
 */
@Configuration
public class WebConfig {

    @Bean
    public BeanNameViewResolver beanNameViewResolver() {
        BeanNameViewResolver resolver = new BeanNameViewResolver();
        return resolver;
    }

    @Bean
    public XlsxStreamingView xlsxStreamingView() {
        return new XlsxStreamingView();
    }
}
