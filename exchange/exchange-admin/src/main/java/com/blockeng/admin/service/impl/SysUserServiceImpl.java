package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.entity.SysRole;
import com.blockeng.admin.entity.SysUser;
import com.blockeng.admin.entity.SysUserRole;
import com.blockeng.admin.mapper.SysUserMapper;
import com.blockeng.admin.security.JwtToken;
import com.blockeng.admin.service.SysRoleService;
import com.blockeng.admin.service.SysUserRoleService;
import com.blockeng.admin.service.SysUserService;
import com.google.common.base.Joiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 平台用户 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
@Service
@Transactional
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtTokenUtil;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRoleService sysRoleService;

    @Override
    public SysUser login(String username, String password) {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        SysUser sysUser = (SysUser) authentication.getPrincipal();
        String accessToken = jwtTokenUtil.generateToken(sysUser);
        sysUser.setToken("Bearer "+accessToken);
        return sysUser;
    }

    @Override
    public String refreshToken(String oldToken) {
        String token = oldToken.substring("Bearer ".length());
        if (!jwtTokenUtil.isTokenExpired(token)) {
            return jwtTokenUtil.refreshToken(token);
        }
        return "error";
    }

    @Override
    public IPage<SysUser> selectSysUserPage(IPage<SysUser> page, QueryWrapper<SysUser> ew) {
        IPage<SysUser> sysUserpage = super.selectPage(page, ew);
        for (SysUser sysUser : sysUserpage.getRecords()) {
            QueryWrapper<SysUserRole> sysUserRoleQueryWrapper = new QueryWrapper<>();
            sysUserRoleQueryWrapper.eq("user_id", sysUser.getId());
            sysUserRoleQueryWrapper.select("role_id");
            List<SysUserRole> sysUserRoles = sysUserRoleService.selectList(sysUserRoleQueryWrapper);
            List<Long> roleIds = sysUserRoles.stream().map(sysUserRole -> sysUserRole.getRoleId()).collect(Collectors.toList());

            QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<SysRole>();
            sysRoleQueryWrapper.in("id", roleIds);
            sysRoleQueryWrapper.select("code");
            List<SysRole> sysRoles = sysRoleService.selectList(sysRoleQueryWrapper);
            List<String> roleCodes = sysRoles.stream().map(sysRole -> sysRole.getCode()).collect(Collectors.toList());
            String roleStrings = Joiner.on(",").skipNulls().join(roleCodes);
            sysUser.setRoleStrings(roleStrings);
        }
        return sysUserpage;
    }
}