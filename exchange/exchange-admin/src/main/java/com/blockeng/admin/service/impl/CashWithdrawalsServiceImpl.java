package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.AuditDTO;
import com.blockeng.admin.dto.CashWithdrawalsCountDTO;
import com.blockeng.admin.dto.UserWithDrawalsDTO;
import com.blockeng.admin.entity.*;
import com.blockeng.admin.mapper.CashWithdrawalsMapper;
import com.blockeng.admin.service.*;
import com.blockeng.dto.SmsDTO;
import com.blockeng.feign.SmsServiceClient;
import com.blockeng.framework.constants.Constant;
import com.blockeng.framework.enums.*;
import com.blockeng.framework.exception.AccountException;
import com.blockeng.framework.exception.ExchangeException;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 提现表 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
@Slf4j
public class CashWithdrawalsServiceImpl extends ServiceImpl<CashWithdrawalsMapper, CashWithdrawals> implements CashWithdrawalsService, Constant {

    @Autowired
    private CashWithdrawAuditRecordService cashRechargeAuditRecord;

    @Autowired
    private UserService userService;

    @Autowired
    private SmsServiceClient smsServiceClient;

    @Autowired
    private LockService lockService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private MultiLevelAuditService multiLevelAuditService;

    @Override
    public IPage<UserWithDrawalsDTO> selectMapPage(IPage<UserWithDrawalsDTO> page, QueryWrapper<UserWithDrawalsDTO> queryWrapper) {
        return baseMapper.selectMapPage(page, queryWrapper);
    }

    @Override
    public UserWithDrawalsDTO selectOneObj(Long id) {
        return baseMapper.selectOneObj(id);
    }

    /**
     * 法币提现审核
     *
     * @param auditDTO 提币审核请求参数
     * @param sysUser  当前登录用户
     * @return
     */
    @Override
    @Transactional
    public void cashWithdrawAudit(AuditDTO auditDTO, SysUser sysUser) throws ExchangeException {
        // 校验权限
        if (!multiLevelAuditService.cashWithdrawPermissionCheck(auditDTO.getId(), sysUser)) {
            throw new ExchangeException("审核权限不足");
        }
        boolean isLocked = false;
        try {
            // 通过内存锁防止重复提交审核，导致资金异常
            isLocked = lockService.getLock(REDIS_KEY_CASH_WITHDRAW_AUDIT_LOCK, String.valueOf(auditDTO.getId()), false);
            if (!isLocked) {
                throw new ExchangeException("已经提交审核，请勿重复操作");
            }
            CashWithdrawals cashWithdraw = baseMapper.selectById(auditDTO.getId());
            if (cashWithdraw == null) {
                throw new ExchangeException("提现订单不存在");
            }
            if (cashWithdraw.getStatus() != CashRechargeStatus.PENDING.getCode()) {
                throw new ExchangeException("此记录已审核");
            }
            // 审核轨迹
            CashWithdrawAuditRecord cashWithdrawAuditRecord = new CashWithdrawAuditRecord();
            cashWithdrawAuditRecord.setOrderId(Long.valueOf(auditDTO.getId()))
                    .setStatus(auditDTO.getStatus())
                    .setRemark(auditDTO.getRemark())
                    .setStep(cashWithdraw.getStep())
                    .setAuditUserId(sysUser.getId())
                    .setAuditUserName(sysUser.getFullname());
            cashRechargeAuditRecord.insert(cashWithdrawAuditRecord);
            User user = userService.selectById(cashWithdraw.getUserId());
            cashWithdraw.setRemark(auditDTO.getRemark());
            if (auditDTO.getStatus().intValue() == CashWithdrawStatus.REFUSE.getCode()) {
                // 审核拒绝
                cashWithdraw.setStatus(CashRechargeStatus.REFUSE.getCode());
                cashWithdraw.setLastTime(new Date());
                baseMapper.updateById(cashWithdraw);
                // 更新资金账户(解冻资金账户)
                accountService.unlockAmount(user.getId(), cashWithdraw.getCoinId(), cashWithdraw.getNum(), BusinessType.WITHDRAW, cashWithdraw.getId());
                // 提现审核拒绝短信通知用户
                Map<String, Object> templateParam = new HashMap<>();
                templateParam.put("num", cashWithdraw.getMum());
                templateParam.put("reason", auditDTO.getRemark());
                SmsDTO smsDTO = new SmsDTO();
                smsDTO.setMobile(user.getMobile())
                        .setCountryCode(user.getCountryCode())
                        .setEmail(user.getEmail())
                        .setTemplateCode(SmsTemplate.CASH_WITHDRAW_REFUSE.getCode())
                        .setTemplateParam(templateParam);
                smsServiceClient.sendTo(smsDTO);
                return;
            }
            // 审核通过
            Config config = configService.queryBuyCodeAndType(CONFIG_TYPE_SYSTEM, Constant.CONFIG_CASH_WITHDRAW_AUDIT_STEPS);
            if (config == null || Strings.isNullOrEmpty(config.getValue())) {
                throw new ExchangeException("没有配置审核级数");
            }
            // 当前审核级数
            int step = cashWithdraw.getStep();
            if (step == Integer.valueOf(config.getValue()).intValue()) {
                // 最终审核通过
                cashWithdraw.setStatus(CashRechargeStatus.SUCCESS.getCode());
                cashWithdraw.setLastTime(new Date());
                baseMapper.updateById(cashWithdraw);
                // 变更账户资金
                this.withAmount(cashWithdraw, user.getId());
                // 法币提现给用户打款后，银行有短信通知，因此平台不在发送短信通知用户
                return;
            }
            // 不是最终审核：状态设置为待审核，审计级别加1级
            cashWithdraw.setStatus(CashRechargeStatus.PENDING.getCode()).setStep(step + 1);
            baseMapper.updateById(cashWithdraw);
            return;
        } catch (AccountException e) {
            throw new ExchangeException(e.getMessage());
        } finally {
            // 释放锁
            if (isLocked) {
                lockService.unlock(REDIS_KEY_CASH_WITHDRAW_AUDIT_LOCK, String.valueOf(auditDTO.getId()));
            }
        }
    }

    /**
     * 提现审核通过修改资金账户
     *
     * @param cashWithdraw 提现申请单
     * @param userId       用户ID
     */
    private boolean withAmount(CashWithdrawals cashWithdraw, Long userId) {
        Account account = accountService.queryByUserIdAndCoinId(userId, cashWithdraw.getCoinId());
        if (account == null) {
            log.error("资金账户异常：userId：{}，coinId：{}", userId, cashWithdraw.getCoinId());
            throw new AccountException("资金账户异常");
        }
        // 解冻资金账户
        accountService.unlockAmount(userId, cashWithdraw.getCoinId(), cashWithdraw.getNum(), BusinessType.WITHDRAW, cashWithdraw.getId());
        User c2cAdmin = userService.queryAdminUser(AdminUserType.C2C_ADMIN);
        if (c2cAdmin == null) {
            log.error("尚未配置C2C管理员用户");
            throw new AccountException("尚未配置C2C管理员用户");
        }
        accountService.transferAmount(userId,
                c2cAdmin.getId(),
                cashWithdraw.getCoinId(),
                cashWithdraw.getNum(),
                BusinessType.WITHDRAW,
                cashWithdraw.getId(),
                BusinessType.WITHDRAW.getDesc());
        return true;
    }


    public IPage<CashWithdrawalsCountDTO> selectCountMain(IPage<CashWithdrawalsCountDTO> page, QueryWrapper<CashWithdrawalsCountDTO> queryWrapper) {
        return page.setRecords(baseMapper.selectCountMain(page, queryWrapper));
    }

    @Override
    public List<CashWithdrawalsCountDTO> selectValidCounts(QueryWrapper<CashWithdrawalsCountDTO> queryWrapper) {
        return baseMapper.selectValidCounts(queryWrapper);
    }

    @Override
    public List<CashWithdrawalsCountDTO> selectUserCt(QueryWrapper<CashWithdrawalsCountDTO> queryWrapper) {
        return baseMapper.selectUserCt(queryWrapper);
    }
}
