package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.dto.UserCountLoginDTO;
import com.blockeng.admin.entity.UserLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户登录日志 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface UserLoginLogService extends IService<UserLoginLog> {

    /**
     * 登陆统计
     * @param page
     * @param wrapper
     * @return
     */
    IPage<UserCountLoginDTO> selectLoginCountpage(IPage<UserCountLoginDTO> page, Wrapper<UserCountLoginDTO> wrapper);
}
