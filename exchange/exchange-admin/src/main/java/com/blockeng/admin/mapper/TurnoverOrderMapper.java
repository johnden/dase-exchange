package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.blockeng.admin.dto.TradeTopVolumeDTO;
import com.blockeng.admin.dto.TurnoverOrderCountDTO;
import com.blockeng.admin.entity.TurnoverOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 成交订单 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface TurnoverOrderMapper extends BaseMapper<TurnoverOrder> {

    IPage<TurnoverOrder> selectListPage(IPage<TurnoverOrder> page, @Param("ew") Wrapper<TurnoverOrder> wrapper);

    /**
     * 根据日期和用户统计参与交易人数
     * @param countDate
     * @param uidStrs 用户ID字符串('1,2,3')
     * @return
     */
    Integer countTradeByDateAndUidStrs(@Param("countDate") String countDate,@Param("uidStrs") String uidStrs);

    /**
     * 分页查询交易排行
     * @param page
     * @param wrapper
     * @return
     */
    List<TradeTopVolumeDTO> selectTradeTopVolumePage(IPage<TradeTopVolumeDTO> page, @Param("ew") Wrapper<TradeTopVolumeDTO> wrapper);

    /**
     * 交易币种 ，交易市场，交易量，交易笔数，统计时间
     * @param page
     * @param paramMap
     * @return
     */
    List<TurnoverOrderCountDTO> selectCountMain(IPage<TurnoverOrderCountDTO> page, Map<String,Object> paramMap);


    /**
     * #卖方 最多交易用户，该用户交易量，
     * @param coins
     * @return
     */
    List<TurnoverOrderCountDTO>selectSellUserCount(@Param("coins")String[]coins);


    /**
     * #买方 最多交易用户，该用户交易量，
     * @param coins
     * @return
     */
    List<TurnoverOrderCountDTO>selectBuyUserCount(@Param("coins")String[]coins);

}
