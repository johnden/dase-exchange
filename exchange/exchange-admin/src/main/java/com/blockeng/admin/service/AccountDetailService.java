package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.entity.AccountDetail;
import com.blockeng.admin.entity.User;

import java.util.List;

/**
 * <p>
 * 资金账户流水 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-16
 */
public interface AccountDetailService extends IService<AccountDetail> {

    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<AccountDetail> selectListPage(IPage<AccountDetail> page, QueryWrapper<AccountDetail> queryWrapper);


    /**
     * 分页查询
     *
     * @return
     */
    List<AccountDetail> selectListPageEmpty(long current, long size);


    /**
     * 分页查询
     *
     * @param ew      account
     * @param ewOther User
     * @return
     */
    List<AccountDetail> selectListPageFromUser(long current, long size, Wrapper<User> ew, Wrapper<AccountDetail> ewOther);


    /**
     * 空查询
     *
     * @param ew      account
     * @param ewOther User
     * @return 空查询
     */
    List<AccountDetail> selectListPageFromAccount(long current, long size, Wrapper<AccountDetail> ew, Wrapper<User> ewOther);

    /**
     * count
     *
     * @return 查询总是,
     */
    Integer selectListPageCount();

    /**
     * count
     *
     * @param ew account
     * @return 更具ew查询总数
     */
    Integer selectListPageCountFromAccount(Wrapper<AccountDetail> ew, Wrapper<User> ewOther);

    /**
     * count
     *
     * @param ew account
     * @return 更具user查询总数
     */
    Integer selectListPageCountFromUser(Wrapper<User> ew, Wrapper<AccountDetail> ewOther);

}
