package com.blockeng.admin.aop.drds;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;

/**
 * @author qiang
 */
@Aspect
@Component
public class DrdsTransactionAspect {

    @Resource
    private PlatformTransactionManager transactionManager;

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Pointcut("@annotation(com.blockeng.admin.annotation.DrdsTransaction)")
    public void pointcut() {}

    /**
     * 开启DRDS柔性事务
     * @param point
     * @return
     */
    @Around("pointcut()")
    public Object openFlexibleTransactionExt(final ProceedingJoinPoint point) {
        DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
        definition.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_NESTED);
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager, definition);
        Object object = transactionTemplate.execute(new TransactionCallback() {
            @Override
            public Object doInTransaction(TransactionStatus transactionStatus) {
                try {
                    flexibleSet();
                    Object[] args = point.getArgs();
                    return point.proceed(args);
                } catch (Throwable throwable) {
                    transactionStatus.setRollbackOnly();
                    throwable.printStackTrace();
                }
                return null;
            }
        });
        return object;
    }

    private void flexibleSet() {
        jdbcTemplate.execute("SET AUTOCOMMIT = off");
        jdbcTemplate.execute("set drds_transaction_policy = 'flexible'");
    }
}
