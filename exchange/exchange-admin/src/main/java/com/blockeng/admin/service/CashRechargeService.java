package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.dto.AuditDTO;
import com.blockeng.admin.dto.CashRechargeCountDTO;
import com.blockeng.admin.dto.UserCashRechargeDTO;
import com.blockeng.admin.entity.CashRecharge;
import com.blockeng.admin.entity.SysUser;
import com.blockeng.framework.exception.ExchangeException;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 法币充值 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface CashRechargeService extends IService<CashRecharge> {

    IPage<UserCashRechargeDTO> selectMapPage(IPage<UserCashRechargeDTO> page, QueryWrapper<UserCashRechargeDTO> queryWrapper);

    UserCashRechargeDTO selectOneObj(Long id);

    /**
     * 查询出规定的条数数据
     *
     * @param pageSize 为空返回查询所有，否则返回规定的条数集合
     * @return
     */
    List<UserCashRechargeDTO> selectUserCashRechargeDTOList(Integer pageSize);

    /**
     * 法币充值审核
     *
     * @param auditDTO 提币审核请求参数
     * @param sysUser  当前登录用户
     * @return
     */
    void cashRechargeAudit(AuditDTO auditDTO, SysUser sysUser) throws ExchangeException;

    /**
     * 统计
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<CashRechargeCountDTO> selectCountMain(IPage<CashRechargeCountDTO> page, QueryWrapper<CashRechargeCountDTO> queryWrapper);


    /**
     * <!--成功笔数，充值时间-->
     * @param queryWrapper
     * @return
     */
    List<CashRechargeCountDTO> selectValidCounts(QueryWrapper<CashRechargeCountDTO> queryWrapper);


    /**
     * <!--用户数，用户id，充值时间-->
     * @param queryWrapper
     * @return
     */
    List<CashRechargeCountDTO> selectUserCt(QueryWrapper<CashRechargeCountDTO> queryWrapper);
}
