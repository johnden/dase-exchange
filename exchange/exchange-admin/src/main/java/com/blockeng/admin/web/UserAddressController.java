package com.blockeng.admin.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.annotation.AuditLog;
import com.blockeng.admin.common.ResultMap;
import com.blockeng.admin.entity.Coin;
import com.blockeng.admin.entity.UserAddress;
import com.blockeng.admin.enums.SysLogTypeEnum;
import com.blockeng.admin.service.CoinService;
import com.blockeng.admin.service.UserAddressService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户钱包地址 前端控制器
 * </p>
 *
 * @author lxl
 * @since 2018-05-19
 */
@RestController
@RequestMapping("/userAddress")
@Api(value = "用户钱包地址controller", tags = { "用户钱包地址" })
public class UserAddressController {

    @Autowired
    private UserAddressService userAddressService;
    
    @Autowired
    private CoinService coinService;

    @AuditLog(value = "查询用户的钱包地址",type = SysLogTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('user_wallet_address_query')")
    @GetMapping("/selectPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "每页显示条数，默认 10", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "current", value = "当前页", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "query")
    })
    @ApiOperation(authorizations = {@Authorization(value="Authorization")},value = "用户的钱包地址" ,httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code =0,message = "成功",response=UserAddress.class),
            @ApiResponse(code =1,message = "失败")
    })
    public ResultMap selectUserWalletList(int size,int current,Long userId){
        Page<UserAddress> page = new Page<UserAddress>(current, size);
        QueryWrapper<UserAddress> ew = new QueryWrapper<>();
        if(userId==null){
            return ResultMap.getFailureResult("用户id不能为空！");
        }
        ew.eq("user_id",userId);
        ew.orderByDesc("id");
        List<UserAddress> userAddressList=userAddressService.selectPage(page,ew).getRecords();
        if(userAddressList!=null&&userAddressList.size()>0){
            Long coinIds[]=new Long[userAddressList.size()];
            int i=0;
            for (UserAddress u:userAddressList) {
                coinIds[i]=u.getCoinId();
                i++;
            }
            QueryWrapper<Coin> ew1 = new QueryWrapper<>();
            ew1.in("id",coinIds);
            List<Coin> coinLis=coinService.selectList(ew1);
            for (UserAddress u:userAddressList) {
                for (Coin cn:coinLis) {
                    if(u.getCoinId().equals(cn.getId())){
                        u.setCoinName(cn.getName());
                    }
                }
            }
        }
        page.setRecords(userAddressList);
        return ResultMap.getSuccessfulResult(page);
    }
}
