package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.AccountDTO;
import com.blockeng.admin.entity.Account;
import com.blockeng.admin.entity.AccountDetail;
import com.blockeng.admin.entity.User;
import com.blockeng.admin.mapper.AccountMapper;
import com.blockeng.admin.service.AccountDetailService;
import com.blockeng.admin.service.AccountService;
import com.blockeng.admin.service.UserService;
import com.blockeng.framework.enums.*;
import com.blockeng.framework.exception.AccountException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户财产记录 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
@Slf4j
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Autowired
    private AccountDetailService accountDetailService;

    @Autowired
    private UserService userService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Override
    public IPage<Account> selectListPage(IPage<Account> page, QueryWrapper<Account> wrapper) {
        List<Long> ids = baseMapper.selectOptimizePageSql(page, wrapper);
        wrapper.in("u.id", ids);
        page.setRecords(baseMapper.selectListPage(wrapper));
        return page;
    }

    /**
     * 根据用户ID和币种名称查询资金账户
     *
     * @param userId 用户ID
     * @param coinId 币种ID
     * @return
     */
    @Override
    public Account queryByUserIdAndCoinId(long userId, long coinId) {
        QueryWrapper<Account> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId)
                .eq("coin_id", coinId)
                .eq("status", BaseStatus.EFFECTIVE.getCode())
                .last("LIMIT 1");
        List<Account> accountList = baseMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(accountList)) {
            return null;
        }
        return accountList.get(0);
    }

    /**
     * 解冻资金
     *
     * @param userId       用户ID
     * @param coinId       币种ID
     * @param amount       冻结金额
     * @param businessType 业务类型
     * @param orderId      关联订单号
     * @return
     */
    @Override
    @Transactional
    public boolean unlockAmount(long userId,
                                long coinId,
                                BigDecimal amount,
                                BusinessType businessType,
                                long orderId) throws AccountException {
        Account account = this.queryByUserIdAndCoinId(userId, coinId);
        if (account == null) {
            log.error("解冻资金-资金账户异常，userId:{}, coinId:{}", userId, coinId);
            throw new AccountException("资金账户异常");
        }
        if (baseMapper.unlockAmount(account.getId(), amount) > 0) {
            // 保存流水
            AccountDetail accountDetail = new AccountDetail(userId, coinId, account.getId(), account.getId(),
                    orderId, AmountDirection.INCOME.getType(), businessType.getCode(), amount, "解冻");
            accountDetailService.insert(accountDetail);
            return true;
        }
        log.error("解冻资金失败，orderId:{}, userId:{}, coinId:{}, amount:{}, businessType:{}",
                orderId, userId, coinId, amount, businessType.getCode());
        throw new AccountException("解冻资金失败");
    }

    /**
     * 扣减资金
     *
     * @param userId       用户ID
     * @param coinId       币种ID
     * @param fee
     * @param amount       冻结金额
     * @param businessType 业务类型
     * @param remark       备注
     * @param orderId      关联订单号
     * @return
     */
    @Override
    @Transactional
    public boolean subtractAmount(long userId,
                                  long coinId,
                                  BigDecimal fee,
                                  BigDecimal amount,
                                  BusinessType businessType,
                                  String remark,
                                  long orderId) throws AccountException {
        Account account = this.queryByUserIdAndCoinId(userId, coinId);
        if (account == null) {
            log.error("扣减资金-资金账户异常，userId:{}, coinId:{}", userId, coinId);
            throw new com.blockeng.framework.exception.AccountException("资金账户异常");
        }
        if (baseMapper.subtractAmount(account.getId(), amount) > 0) {
            // 保存流水
            AccountDetail accountDetail = new AccountDetail(userId,
                    coinId,
                    account.getId(),
                    account.getId(),
                    orderId,
                    AmountDirection.OUT.getType(),
                    businessType.getCode(),
                    amount,
                    fee,
                    remark);
            accountDetailService.insert(accountDetail);
            return true;
        }
        log.error("扣减资金失败，orderId:{}, userId:{}, coinId:{}, amount:{}, businessType:{}",
                orderId, userId, coinId, amount, businessType.getCode());
        throw new com.blockeng.framework.exception.AccountException("扣减资金失败");
    }

    /**
     * 资金划转
     *
     * @param fromUserId   转出用户ID
     * @param toUserId     转入用户ID
     * @param coinId       币种ID
     * @param amount       金额
     * @param businessType 业务类型
     * @param orderId      关联订单号
     * @param remark       备注
     * @return
     */
    @Override
    @Transactional
    public boolean transferAmount(long fromUserId,
                                  long toUserId,
                                  long coinId,
                                  BigDecimal amount,
                                  BusinessType businessType,
                                  long orderId,
                                  String remark) throws AccountException {
        Account fromAccount = this.queryByUserIdAndCoinId(fromUserId, coinId);
        if (fromAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", fromUserId, coinId);
            throw new AccountException("资金账户异常");
        }
        Account toAccount = this.queryByUserIdAndCoinId(toUserId, coinId);
        if (toAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", toUserId, coinId);
            throw new AccountException("资金账户异常");
        }
        // 扣减资金
        if (baseMapper.subtractAmount(fromAccount.getId(), amount) > 0) {
            // 保存流水
            AccountDetail fromAccountDetail = new AccountDetail(fromUserId,
                    coinId,
                    fromAccount.getId(),
                    toAccount.getId(),
                    orderId,
                    AmountDirection.OUT.getType(),
                    businessType.getCode(),
                    amount,
                    remark);
            accountDetailService.insert(fromAccountDetail);
            // 增加资金
            if (baseMapper.addAmount(toAccount.getId(), amount) > 0) {
                // 保存流水
                AccountDetail toAccountDetail = new AccountDetail(toUserId,
                        coinId,
                        toAccount.getId(),
                        fromAccount.getId(),
                        orderId,
                        AmountDirection.INCOME.getType(),
                        businessType.getCode(),
                        amount,
                        remark);
                accountDetailService.insert(toAccountDetail);
                return true;
            }
        }
        log.error("资金划转，orderId:{}, fromUserId:{}, toUserId:{}, coinId:{}, amount:{}, businessType:{}, remark:{}",
                orderId, fromUserId, toUserId, coinId, amount, businessType.getCode(), remark);
        throw new AccountException("资金划转失败");
    }

    /**
     * 平台充值
     *
     * @param businessType 业务类型
     * @return
     * @throws AccountException
     */
    @Override
    @Transactional
    public boolean rechargeAmount(AccountDTO accountDTO, BusinessType businessType) throws AccountException {
        User c2cAdmin = userService.queryAdminUser(AdminUserType.C2C_ADMIN);
        if (c2cAdmin == null) {
            log.error("尚未配置C2C管理员用户");
            throw new AccountException("尚未配置C2C管理员用户");
        }
        // if (c2cAdmin.getId().equals(accountDTO.getUserId())) {
        //     log.error("充值用户是c2c管理员");
        //     throw new AccountException("充值用户不能是c2c管理员");
        // }
        // fromUserId 转出用户ID
        Account fromAccount = this.queryByUserIdAndCoinId(c2cAdmin.getId(), accountDTO.getCoinId());
        if (fromAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", c2cAdmin.getId(), accountDTO.getCoinId());
            throw new AccountException("资金账户异常");
        }
        // 转入用户ID
        Account toAccount = this.queryByUserIdAndCoinId(accountDTO.getUserId(), accountDTO.getCoinId());
        if (toAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", accountDTO.getUserId(), accountDTO.getCoinId());
            throw new AccountException("资金账户异常");
        }
        if (!fromAccount.getUserId().equals(c2cAdmin.getId()) ) {
            //账面资金大于等于充值金额
            if (fromAccount.getBalanceAmount().compareTo(accountDTO.getAmount()) == -1) {
                throw new AccountException("C2C管理员账号资金不够");
            }
            // 扣减资金
            if (baseMapper.subtractAmount(fromAccount.getId(), accountDTO.getAmount()) > 0) {
                // 保存流水
                AccountDetail fromAccountDetail = new AccountDetail(c2cAdmin.getId(),
                        accountDTO.getCoinId(),
                        fromAccount.getId(),
                        toAccount.getId(),
                        0L,
                        AmountDirection.OUT.getType(),
                        businessType.getCode(),
                        accountDTO.getAmount(),
                        "后台充值");
                accountDetailService.insert(fromAccountDetail);
                // 增加资金
                if (baseMapper.addAmount(toAccount.getId(), accountDTO.getAmount()) > 0) {
                    // 保存流水
                    AccountDetail toAccountDetail = new AccountDetail(toAccount.getUserId(),
                            accountDTO.getCoinId(),
                            toAccount.getId(),
                            fromAccount.getId(),
                            0L,
                            AmountDirection.INCOME.getType(),
                            businessType.getCode(),
                            accountDTO.getAmount(),
                            "后台充值");
                    accountDetailService.insert(toAccountDetail);
                    return true;
                }
            }
        } else {
            // 增加资金
            if (baseMapper.addAmount(toAccount.getId(), accountDTO.getAmount()) > 0) {
                // 保存流水
                AccountDetail toAccountDetail = new AccountDetail(toAccount.getUserId(),
                        accountDTO.getCoinId(),
                        toAccount.getId(),
                        fromAccount.getId(),
                        0L,
                        AmountDirection.INCOME.getType(),
                        businessType.getCode(),
                        accountDTO.getAmount(),
                        "后台充值");
                accountDetailService.insert(toAccountDetail);
                return true;
            }
        }
        throw new AccountException("充值失败");
    }

    @Override
    public void initialBatchAccount(long coinId) {
        DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
        definition.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_NESTED);
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager, definition);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    // jdbcTemplate.execute("SET AUTOCOMMIT = off");
                    // jdbcTemplate.execute("set drds_transaction_policy = 'flexible'");
/*                    String sql = "INSERT INTO account ( user_id, coin_id ) SELECT id AS user_id, "+coinId+" FROM user";
                    jdbcTemplate.execute(sql);*/
                    //baseMapper.initialBatchAccount(coinId);

                    String sql = "SELECT id FROM user";
                    SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);
                    List<Account> pool = new ArrayList<>(200);
                    while(sqlRowSet.next()) {
                        long id = sqlRowSet.getLong("id");
                        Account account = new Account()
                                .setUserId(id)
                                .setStatus(AccountStatus.ENABLE.getCode())
                                .setCoinId(coinId)
                                .setWithdrawalsAmount(BigDecimal.ZERO)
                                .setBalanceAmount(BigDecimal.ZERO)
                                .setFreezeAmount(BigDecimal.ZERO)
                                .setRechargeAmount(BigDecimal.ZERO)
                                .setVersion(0L);
                        pool.add(account);
                        if (pool.size() >= 200 || sqlRowSet.isLast()) {
                            accountService.insertBatch(pool, 200);
                            pool.clear();
                        }
                    }
                } catch (Throwable throwable) {
                    status.setRollbackOnly();
                    log.error(throwable.getMessage());
                }
            }
        });
    }

    @Override
    public List<Account> selectListPageFromAccount(long current, long size, QueryWrapper<Account> ew, QueryWrapper<User> ewOther) {
        return this.baseMapper.selectListPageFromAccount(current, size, ew, ewOther);
    }

    @Override
    public Integer selectListPageCount() {
        return this.baseMapper.selectListPageCount();
    }

    @Override
    public Integer selectListPageCountFromAccount(QueryWrapper<Account> ew, QueryWrapper<User> userEw) {
        return this.baseMapper.selectListPageCountFromAccount(ew, userEw);
    }

    @Override
    public Integer selectListPageCountFromUser(QueryWrapper<User> ew, QueryWrapper<Account> accountEw) {
        return this.baseMapper.selectListPageCountFromUser(ew, accountEw);
    }

    @Override
    public List<Account> selectListPageEmpty(long current, long size) {
        return this.baseMapper.selectListPageEmpty(current, size);
    }

    @Override
    public List<Account> selectListPageFromUser(long current, long size, QueryWrapper<User> ew, QueryWrapper<Account> accountEw) {
        return this.baseMapper.selectListPageFromUser(current, size, ew, accountEw);
    }
}