package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.TurnoverOrderCountDTO;
import com.blockeng.admin.dto.UserCountRegDTO;
import com.blockeng.admin.dto.UserDTO;
import com.blockeng.admin.entity.User;
import com.blockeng.admin.mapper.UserMapper;
import com.blockeng.admin.service.CashRechargeService;
import com.blockeng.admin.service.UserService;
import com.blockeng.framework.enums.AdminUserType;
import com.blockeng.framework.enums.BaseStatus;
import com.blockeng.framework.enums.CashRechargeStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private CashRechargeService cashRechargeService;

    /**
     * 获取C2C管理员账户（法币充值提现）
     *
     * @return
     */
    @Override
    public User queryAdminUser(AdminUserType adminUserType) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", adminUserType.getUserName())
                .eq("status", BaseStatus.EFFECTIVE.getCode())
                .eq("type", adminUserType.getType());
        List<User> users = baseMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(users)) {
            return null;
        }
        return users.get(0);
    }

    @Override
    public IPage<UserCountRegDTO> selectRegCountpage(IPage<UserCountRegDTO> page, QueryWrapper<UserCountRegDTO> wrapper) {
        List<UserCountRegDTO> countRegDTOList = baseMapper.selectRegCountPage(page, wrapper);
        for (UserCountRegDTO countRegDTO : countRegDTOList) {
            //手机注册,所以绑定手机人数=注册人数
            countRegDTO.setMobileBindNum(countRegDTO.getRegNum());
            countRegDTO.setEmailBindNum(baseMapper.countEmailBindByDate(countRegDTO.getCountDate()));
            countRegDTO.setSetPayPwdNum(baseMapper.countSetPayPwdByDate(countRegDTO.getCountDate()));

            List<Long> userIds = new ArrayList<>();
            QueryWrapper<User> userWrapper = new QueryWrapper<>();
            String startTime = countRegDTO.getCountDate() + " 00:00:00";
            String endTime = countRegDTO.getCountDate() + " 23:59:59";
            userWrapper.between("created", startTime, endTime);
            baseMapper.selectList(userWrapper).forEach(user -> {
                userIds.add(user.getId());
            });
            // 注册用户充值数量
            if (!CollectionUtils.isEmpty(userIds)) {
                QueryWrapper cashRechargeWrapper = new QueryWrapper();
                cashRechargeWrapper.between("created", startTime, endTime);
                cashRechargeWrapper.eq("status", CashRechargeStatus.SUCCESS.getCode());
                cashRechargeWrapper.in("user_id", userIds);
                int count = cashRechargeService.selectCount(cashRechargeWrapper);
                countRegDTO.setRechargeNum(count);
            } else {
                countRegDTO.setRechargeNum(0);
            }
        }
        page.setRecords(countRegDTOList);
        return page;
    }

    @Override
    public List<TurnoverOrderCountDTO> selectUserCount(String[] coins) {
        return baseMapper.selectUserCount(coins);
    }

    @Override
    public IPage<UserDTO> selectListPage(IPage<UserDTO> page, QueryWrapper<UserDTO> queryWrapper) {
        List<Long> userIds = baseMapper.selectOptimizePageSql(page, queryWrapper);
        queryWrapper = new QueryWrapper<>();
        queryWrapper.in("u.id", userIds);
        page.setRecords(baseMapper.selectUserList(queryWrapper));
        return page;
    }

    @Override
    public IPage<UserDTO> selectListAuditpage(IPage<UserDTO> page, QueryWrapper<UserDTO> queryWrapper) {
        return baseMapper.selectListAuditPage(page, queryWrapper);
    }
}
