package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.entity.SysUser;

/**
 * <p>
 * 平台用户 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
public interface SysUserService extends IService<SysUser> {

    SysUser login(String username, String password);

    String refreshToken(String oldToken);

    IPage<SysUser> selectSysUserPage(IPage<SysUser> page, QueryWrapper<SysUser> ew);
}
