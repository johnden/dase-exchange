package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.entity.EntrustOrder;
import com.blockeng.admin.entity.User;
import com.blockeng.admin.mapper.EntrustOrderMapper;
import com.blockeng.admin.service.EntrustOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 委托订单信息 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
public class EntrustOrderServiceImpl extends ServiceImpl<EntrustOrderMapper, EntrustOrder> implements EntrustOrderService {

    @Override
    public IPage<EntrustOrder> selectListPage(IPage<EntrustOrder> page, QueryWrapper<EntrustOrder> wrapper) {
        //page.setRecords(entrustOrderMapper.selectListPage(page, wrapper));
        return baseMapper.selectListPage(page, wrapper);
    }

    @Override
    public List<EntrustOrder> selectListPageEmpty(long current, long size) {
        return baseMapper.selectListPageEmpty(current,size);
    }

    @Override
    public List<EntrustOrder> selectListPageByOrder(QueryWrapper<EntrustOrder> wrapper, QueryWrapper<User> otherEw, long current, long size) {
        return baseMapper.selectListPageByOrder(wrapper,otherEw,current,size);
    }

    @Override
    public List<EntrustOrder> selectListPageByUser(QueryWrapper<User> wrapper, QueryWrapper<EntrustOrder> otherEw, long current, long size) {
        return baseMapper.selectListPageByUser(wrapper,otherEw,current,size);
    }

    @Override
    public Integer selectListPageCount() {
        return baseMapper.selectListPageCount();
    }

    @Override
    public Integer selectListPageByOrderCount(QueryWrapper<EntrustOrder> wrapper) {
        return baseMapper.selectListPageByOrderCount(wrapper);
    }

    @Override
    public Integer selectListPageByUserCount(QueryWrapper<User> wrapper) {
        return baseMapper.selectListPageByUserCount(wrapper);
    }

}
