package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.admin.entity.SysUser;

/**
 * <p>
 * 平台用户 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
