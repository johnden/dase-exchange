package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.entity.EntrustOrder;
import com.blockeng.admin.entity.User;

import java.util.List;

/**
 * <p>
 * 委托订单信息 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface EntrustOrderService extends IService<EntrustOrder> {

    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<EntrustOrder> selectListPage(IPage<EntrustOrder> page, QueryWrapper<EntrustOrder> queryWrapper);

    List<EntrustOrder> selectListPageEmpty(long current, long size);

    List<EntrustOrder> selectListPageByOrder(QueryWrapper<EntrustOrder> wrapper, QueryWrapper<User> otherEw, long current, long size);

    List<EntrustOrder> selectListPageByUser(QueryWrapper<User> wrapper, QueryWrapper<EntrustOrder> otherEw, long current, long size);

    Integer selectListPageCount();

    Integer selectListPageByOrderCount(QueryWrapper<EntrustOrder> wrapper);

    Integer selectListPageByUserCount(QueryWrapper<User> wrapper);
}
