package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.dto.CoinRechargeCountDTO;
import com.blockeng.admin.dto.CoinRechargeDTO;
import com.blockeng.admin.entity.CoinRecharge;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户充值,当前用户充值成功之后添加数据到这个表,充值一般无手续费.当status为1的时候表示充值成功 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-17
 */
public interface CoinRechargeService extends IService<CoinRecharge> {

    IPage<CoinRechargeDTO> selectMapPage(IPage<CoinRechargeDTO> page, QueryWrapper<CoinRechargeDTO> queryWrapper);

    /**
     * 统计
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<CoinRechargeCountDTO> selectCountMain (IPage<CoinRechargeCountDTO> page, QueryWrapper<CoinRechargeCountDTO> queryWrapper);


    /**
     * <!--成功笔数，充值时间-->
     * @param paramMap
     * @return
     */
    List<CoinRechargeCountDTO> selectValidCounts(Map<String,Object> paramMap);


    /**
     * <!--用户数，用户id，充值时间-->
     * @param queryWrapper
     * @return
     */
    List<CoinRechargeCountDTO> selectUserCt(QueryWrapper<CoinRechargeCountDTO> queryWrapper);

}
