package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.admin.entity.CashRechargeAuditRecord;

/**
 * <p>
 * 充值审核记录表 Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2018-05-17
 */
public interface CashRechargeAuditRecordMapper extends BaseMapper<CashRechargeAuditRecord> {

}
