package com.blockeng.admin.web.statistics;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.annotation.AuditLog;
import com.blockeng.admin.common.ResultMap;
import com.blockeng.admin.dto.TradeTopVolumeDTO;
import com.blockeng.admin.enums.SysLogTypeEnum;
import com.blockeng.admin.service.TurnoverOrderService;
import com.google.common.base.Strings;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * <p>
 * 交易统计 前端控制器
 * </p>
 *
 * @author Haliyo
 * @since 2018-05-13
 */
@Slf4j
@RestController
@RequestMapping("/trade/count")
@Api(value = "交易统计", description = "交易统计控制器")
public class TradeCountController {

    @Autowired
    private TurnoverOrderService turnoverOrderService;
    
    @AuditLog(value = "交易量排行查询",type = SysLogTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('trade_statistics_query')")
    @GetMapping("/top/volume")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "每页显示条数，默认 10", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "current", value = "当前页", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataType = "String", paramType = "query"),
    })
    @ApiOperation(authorizations = {@Authorization(value="Authorization")},value = "交易量排行" ,httpMethod = "GET")
    public Object selectVolumepage(@ApiIgnore Page<TradeTopVolumeDTO> page,
                             String startTime,String endTime) {
        QueryWrapper<TradeTopVolumeDTO> ew = new QueryWrapper<>();
        if(!Strings.isNullOrEmpty(startTime)){
            ew.ge("a.created",startTime);
        }
        if(!Strings.isNullOrEmpty(endTime)){
            ew.le("a.created",endTime);
        }
        //交易成功
        ew.eq("a.status",1);
        return ResultMap.getSuccessfulResult(turnoverOrderService.selectTradeTopVolumepage(page, ew));
    }
}
