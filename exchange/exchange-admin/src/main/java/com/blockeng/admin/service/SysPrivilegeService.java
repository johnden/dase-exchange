package com.blockeng.admin.service;

import com.blockeng.admin.entity.SysPrivilege;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 权限配置 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
public interface SysPrivilegeService extends IService<SysPrivilege> {

    List<SysPrivilege> selectListByUserId(Long userId);
}
