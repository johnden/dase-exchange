package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.admin.entity.WalletCoinRecharge;

/**
 * <p>
 * 用户充值,当前用户充值成功之后添加数据到这个表,充值一般无手续费.当status为1的时候表示充值成功 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-17
 */
public interface WalletCoinRechargeMapper extends BaseMapper<WalletCoinRecharge> {

}
