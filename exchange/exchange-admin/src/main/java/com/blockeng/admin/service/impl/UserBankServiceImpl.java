package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.UserBankDTO;
import com.blockeng.admin.entity.UserBank;
import com.blockeng.admin.mapper.UserBankMapper;
import com.blockeng.admin.service.UserBankService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户人民币提现地址 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
public class UserBankServiceImpl extends ServiceImpl<UserBankMapper, UserBank> implements UserBankService {

    @Override
    public IPage<UserBankDTO> selectMapPage(IPage<UserBankDTO> page, QueryWrapper<UserBankDTO> queryWrapper) {
        return page.setRecords(baseMapper.selectMapPage(page, queryWrapper));
    }
}
