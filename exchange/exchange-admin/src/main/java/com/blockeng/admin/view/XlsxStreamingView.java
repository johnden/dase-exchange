package com.blockeng.admin.view;

import com.blockeng.admin.entity.EntrustOrder;
import com.xuxueli.poi.excel.ExcelExportUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

/**
 * @author qiang
 */
@Component
public class XlsxStreamingView extends AbstractXlsxStreamingView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

        //response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
        String fileName = URLDecoder.decode((String) model.get("name")+".xlsx", "utf-8");
        response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
        List<EntrustOrder> list = (List) model.get("list");

        // create excel xls sheet
/*        Sheet sheet = workbook.createSheet("AbstractXlsxStreamingView");

        // create header row
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("ID");
        header.createCell(1).setCellValue("Name");

        // Create data cells
        int rowCount = 1;
        for (EntrustOrder course : list) {
            Row courseRow = sheet.createRow(rowCount++);
            courseRow.createCell(0).setCellValue(course.getId());
            courseRow.createCell(1).setCellValue(course.getCreated());
        }*/
        workbook = ExcelExportUtil.exportWorkbook(list);
/*        OutputStream os = new BufferedOutputStream(response.getOutputStream());
        try {
            response.setContentType("application/octet-stream");
            String fileName = URLDecoder.decode((String) model.get("name")+".xlsx", "utf-8");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
            os.write(ExcelExportUtil.exportToBytes(list));
            //os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(os != null){
                os.close();
            }
        }*/
    }
}
