package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.entity.AccountDetail;
import com.blockeng.admin.entity.User;
import com.blockeng.admin.mapper.AccountDetailMapper;
import com.blockeng.admin.service.AccountDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 资金账户流水 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-16
 */
@Service
@Transactional
public class AccountDetailServiceImpl extends ServiceImpl<AccountDetailMapper, AccountDetail> implements AccountDetailService {

    @Override
    public IPage<AccountDetail> selectListPage(IPage<AccountDetail> page, QueryWrapper<AccountDetail> wrapper) {
        List<Long> ids = baseMapper.selectOptimizePageSql(page, wrapper);
        wrapper.in("a.id", ids);
        page.setRecords(baseMapper.selectAccountDetailList(wrapper));
        return page;
    }

    @Override
    public List<AccountDetail> selectListPageFromAccount(long current, long size, Wrapper<AccountDetail> ew, Wrapper<User> ewOther) {
        return this.baseMapper.selectListPageFromAccount(current, size, ew, ewOther);
    }

    @Override
    public Integer selectListPageCount() {
        return this.baseMapper.selectListPageCount();
    }

    @Override
    public Integer selectListPageCountFromAccount(Wrapper<AccountDetail> ew, Wrapper<User> ewOther) {
        return this.baseMapper.selectListPageCountFromAccount( ew, ewOther);
    }

    @Override
    public Integer selectListPageCountFromUser(Wrapper<User> ew, Wrapper<AccountDetail> ewOther) {
        return this.baseMapper.selectListPageCountFromUser( ew, ewOther);
    }

    @Override
    public List<AccountDetail> selectListPageEmpty(long current, long size) {
        return this.baseMapper.selectListPageEmpty(current, size);
    }

    @Override
    public List<AccountDetail> selectListPageFromUser(long current, long size, Wrapper<User> ew, Wrapper<AccountDetail> ewOther) {
        return this.baseMapper.selectListPageFromUser(current, size, ew, ewOther);
    }
}
