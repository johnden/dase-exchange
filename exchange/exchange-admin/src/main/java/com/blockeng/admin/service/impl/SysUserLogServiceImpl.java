package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.SysUserLogDTO;
import com.blockeng.admin.entity.SysUserLog;
import com.blockeng.admin.mapper.SysUserLogMapper;
import com.blockeng.admin.service.SysUserLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
@Service
public class SysUserLogServiceImpl extends ServiceImpl<SysUserLogMapper, SysUserLog> implements SysUserLogService {



    @Override
    public IPage<SysUserLogDTO> selectListPage(IPage<SysUserLogDTO> page, Wrapper<SysUserLogDTO> wrapper) {
        page.setRecords(baseMapper.selectListPage(page, wrapper));
        return page;
    }
}
