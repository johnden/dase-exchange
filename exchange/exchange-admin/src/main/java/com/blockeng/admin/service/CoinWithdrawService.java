package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.dto.AuditDTO;
import com.blockeng.admin.dto.CashWithdrawalsCountDTO;
import com.blockeng.admin.dto.CoinWithDrawDTO;
import com.blockeng.admin.dto.CoinWithdrawalsCountDTO;
import com.blockeng.admin.entity.CoinWithdraw;
import com.blockeng.admin.entity.SysUser;
import com.blockeng.framework.exception.ExchangeException;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 虚拟币提现 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-17
 */
public interface CoinWithdrawService extends IService<CoinWithdraw> {

    /**
     * 分页查询
     *
     * @param page
     * @param wrapper
     * @return
     */
    IPage<CoinWithdraw> selectListPage(IPage<CoinWithdraw> page, QueryWrapper<CoinWithdraw> wrapper);

    /**
     * 提币审核
     *
     * @param auditDTO 提币审核请求参数
     * @param sysUser  当前登录用户
     */
    void coinWithdrawAudit(AuditDTO auditDTO, SysUser sysUser) throws ExchangeException;

    /**
     * 统计
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<CoinWithdrawalsCountDTO> selectCountMain (IPage<CoinWithdrawalsCountDTO> page, QueryWrapper<CoinWithdrawalsCountDTO> queryWrapper);


    /**
     * <!--成功笔数，充值时间-->
     * @param queryWrapper
     * @return
     */
    List<CoinWithdrawalsCountDTO> selectValidCounts(QueryWrapper<CoinWithdrawalsCountDTO> queryWrapper);


    /**
     * <!--用户数，用户id，充值时间-->
     * @param queryWrapper
     * @return
     */
    List<CoinWithdrawalsCountDTO> selectUserCt(QueryWrapper<CoinWithdrawalsCountDTO> queryWrapper);

    Object withDrawSuccess(CoinWithDrawDTO coinWithDrawDTO);

}
