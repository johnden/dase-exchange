package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.blockeng.admin.dto.CoinWithdrawalsCountDTO;
import com.blockeng.admin.entity.CoinWithdraw;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 当用户发起提币的时候,吧数据插入到该表 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-17
 */
public interface CoinWithdrawMapper extends BaseMapper<CoinWithdraw> {

    IPage<CoinWithdraw> selectListPage(IPage<CoinWithdraw> page, @Param("ew") Wrapper<CoinWithdraw> wrapper);

    /**
     * 根据日期和用户统计提币人数
     * @param countDate
     * @param uidStrs 用户ID字符串('1,2,3')
     * @param status 充值状态
     * @return
     */
    Integer countByDateAndUidStrs(@Param("countDate") String countDate,
                                  @Param("uidStrs") String uidStrs,
                                  @Param("status") Integer status);

    /**
     * 提币金额，到账金额，提币币种，提币笔数，提币用户数 ，提币时间 统计统计
     * @param page
     * @param wrapper
     * @return
     */
    List<CoinWithdrawalsCountDTO> selectCountMain (IPage<CoinWithdrawalsCountDTO> page, @Param("ew") Wrapper<CoinWithdrawalsCountDTO> wrapper);

    /**
     * <!--成功笔数，充值时间-->
     * @param wrapper
     * @return
     */
    List<CoinWithdrawalsCountDTO> selectValidCounts(@Param("ew") Wrapper<CoinWithdrawalsCountDTO> wrapper);


    /**
     * <!--用户数，用户id，充值时间-->
     * @param wrapper
     * @return
     */
    List<CoinWithdrawalsCountDTO> selectUserCt(@Param("ew") Wrapper<CoinWithdrawalsCountDTO> wrapper);

}
