package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.blockeng.admin.dto.UserBankDTO;
import com.blockeng.admin.entity.UserBank;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户人民币提现地址 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface UserBankMapper extends BaseMapper<UserBank> {

    List<UserBankDTO> selectMapPage(IPage<UserBankDTO> page, @Param("ew") Wrapper<UserBankDTO> wrapper);
}