package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.entity.MinePool;
import com.blockeng.admin.mapper.MinePoolMapper;
import com.blockeng.admin.service.MinePoolService;
import org.springframework.stereotype.Service;

/**
 * @Description: 矿池成员 服务实现类
 * @Author: Chen Long
 * @Date: Created in 2018/6/29 下午4:00
 * @Modified by: Chen Long
 */
@Service
public class MinePoolServiceImpl extends ServiceImpl<MinePoolMapper, MinePool> implements MinePoolService {

}
