package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.blockeng.admin.dto.CashRechargeCountDTO;
import com.blockeng.admin.dto.UserCashRechargeDTO;
import com.blockeng.admin.entity.CashRecharge;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 充值表 Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2018-05-17
 */
public interface CashRechargeMapper extends BaseMapper<CashRecharge> {

    IPage<UserCashRechargeDTO> selectMapPage(IPage<UserCashRechargeDTO> page, @Param("ew") Wrapper wrapper);



    UserCashRechargeDTO selectOneObj(Long id);

    /**
     * 查询出规定的条数数据
     * @param pageSize 为空返回查询所有，否则返回规定的条数集合
     * @return
     */
    List<UserCashRechargeDTO> selectUserCashRechargeDTOList(@Param("pageSize") Integer pageSize);

    /**
     * 充值金额，到账金额，充值币种，充值笔数，充值用户数 ，充值时间 统计统计
     * @param page
     * @param wrapper
     * @return
     */
    List<CashRechargeCountDTO> selectCountMain (IPage<CashRechargeCountDTO> page, @Param("ew") Wrapper<CashRechargeCountDTO> wrapper);

    /**
     * <!--成功笔数，充值时间-->
     * @param wrapper
     * @return
     */
    List<CashRechargeCountDTO> selectValidCounts(@Param("ew") Wrapper<CashRechargeCountDTO> wrapper);

    /**
     * <!--用户数，用户id，充值时间-->
     * @param wrapper
     * @return
     */
    List<CashRechargeCountDTO> selectUserCt(@Param("ew") Wrapper<CashRechargeCountDTO> wrapper);

}
