package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.dto.UserWorkIssueDTO;
import com.blockeng.admin.entity.WorkIssue;

/**
 * <p>
 * 工单记录 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface WorkIssueService extends IService<WorkIssue> {

    IPage<UserWorkIssueDTO> selectMapPage(IPage<UserWorkIssueDTO> page, QueryWrapper<UserWorkIssueDTO> queryWrapper);

    UserWorkIssueDTO selectOneObj(Long id);
}