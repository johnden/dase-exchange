package com.blockeng.admin.mapper;

import com.blockeng.admin.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
