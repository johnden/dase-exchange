package com.blockeng.admin.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * Created by kuangxiaoguo on 16/9/11.
 * <p>
 * DES加密工具类
 */
@Component
public class DESUtil {

    @Value("${plant.aes.key}")
    public String aesKey;//加密key

    /**
     * 加密
     *
     * @return 返回解密内容
     */
    public String decrypt(String data) {
        return DecryptCoder.decrypt(data, aesKey);
    }

    /**
     * 解密
     *
     * @return 返回加密内容
     */
    public String encrypt(String data) {
        return EncryptCoder.encrypt(data, aesKey);
    }


    public static void main(String[] args) {
        String ltcdecrypt = EncryptCoder.encrypt("bitcoinpassword", "!q@i#q$i%l^i&u*(");
        System.out.println(ltcdecrypt);
//        ltcdecrypt = DecryptCoder.decrypt(ltcdecrypt, "!h@w#c$w%x^t&a*(");
//        System.out.println(ltcdecrypt);
//
//        ltcdecrypt = EncryptCoder.encrypt("ssHpDCSyW4K4zX5tdmapuERarAX6B", "!h@w#c$w%x^t&a*(");
//        System.out.println(ltcdecrypt);
//        ltcdecrypt = DecryptCoder.decrypt(ltcdecrypt, "!h@w#c$w%x^t&a*(");
//        System.out.println(ltcdecrypt);


    }
}

