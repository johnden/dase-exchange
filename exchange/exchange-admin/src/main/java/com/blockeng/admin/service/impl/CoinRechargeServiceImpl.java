package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.CoinRechargeCountDTO;
import com.blockeng.admin.dto.CoinRechargeDTO;
import com.blockeng.admin.entity.CoinRecharge;
import com.blockeng.admin.mapper.CoinRechargeMapper;
import com.blockeng.admin.service.CoinRechargeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户充值,当前用户充值成功之后添加数据到这个表,充值一般无手续费.当status为1的时候表示充值成功 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-17
 */
@Service
public class CoinRechargeServiceImpl extends ServiceImpl<CoinRechargeMapper, CoinRecharge> implements CoinRechargeService {

    @Override
    public IPage<CoinRechargeDTO> selectMapPage(IPage<CoinRechargeDTO> page, QueryWrapper<CoinRechargeDTO> queryWrapper) {
        //return page.setRecords(baseMapper.selectMapPage(page,paramMap));
        return baseMapper.selectMapPage(page,queryWrapper);
    }

    @Override
    public IPage<CoinRechargeCountDTO> selectCountMain(IPage<CoinRechargeCountDTO> page, QueryWrapper<CoinRechargeCountDTO> queryWrapper) {
        //return page.setRecords(baseMapper.selectCountMain(page, paramMap));
        return baseMapper.selectCountMain(page, queryWrapper);
    }

    @Override
    public List<CoinRechargeCountDTO> selectValidCounts(Map<String, Object> paramMap) {
        return baseMapper.selectValidCounts(paramMap);
    }

    @Override
    public List<CoinRechargeCountDTO> selectUserCt(QueryWrapper<CoinRechargeCountDTO> queryWrapper) {
        return baseMapper.selectUserCt(queryWrapper);
    }
}
