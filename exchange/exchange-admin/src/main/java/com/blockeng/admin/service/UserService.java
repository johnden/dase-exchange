package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.dto.TurnoverOrderCountDTO;
import com.blockeng.admin.dto.UserCountRegDTO;
import com.blockeng.admin.dto.UserDTO;
import com.blockeng.admin.entity.User;
import com.blockeng.framework.constants.Constant;
import com.blockeng.framework.enums.AdminUserType;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface UserService extends IService<User>, Constant {

    /**
     * 获取管理员账户
     *
     * @param adminUserType 管理员类型
     * @return
     */
    User queryAdminUser(AdminUserType adminUserType);

    /**
     * 注册统计
     *
     * @param page
     * @param wrapper
     * @return
     */
    IPage<UserCountRegDTO> selectRegCountpage(IPage<UserCountRegDTO> page, QueryWrapper<UserCountRegDTO> wrapper);

    /**
     * #持币人数
     *
     * @param coins
     * @return
     */
    List<TurnoverOrderCountDTO> selectUserCount(String[] coins);

    IPage<UserDTO> selectListPage(IPage<UserDTO> page, QueryWrapper<UserDTO> queryWrapper);

    IPage<UserDTO> selectListAuditpage(IPage<UserDTO> page, QueryWrapper<UserDTO> queryWrapper);
}
