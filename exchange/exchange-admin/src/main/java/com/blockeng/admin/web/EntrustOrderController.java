package com.blockeng.admin.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.annotation.AuditLog;
import com.blockeng.admin.common.ResultMap;
import com.blockeng.admin.dto.PageDTO;
import com.blockeng.admin.entity.EntrustOrder;
import com.blockeng.admin.entity.User;
import com.blockeng.admin.enums.SysLogTypeEnum;
import com.blockeng.admin.service.EntrustOrderService;
import com.blockeng.admin.view.XlsxStreamingView;
import com.google.common.base.Strings;
import com.xuxueli.poi.excel.ExcelExportUtil;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.List;

/**
 * <p>
 * 委托订单信息_币币交易 前端控制器
 * </p>
 *
 * @author Haliyo
 * @since 2018-05-13
 */
@Controller
@RequestMapping("/entrustOrder")
@Api(value = "委托订单_币币交易", description = "币币交易委托订单管理")
public class EntrustOrderController {

    @Autowired
    private EntrustOrderService entrustOrderService;

    @AuditLog(value = "查询委托订单列表", type = SysLogTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('trade_entrust_order_query')")
    @GetMapping
    @ResponseBody
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "每页显示条数，默认 10", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "current", value = "当前页", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "marketId", value = "交易市场ID", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "交易方式(1买 2卖)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "priceType", value = "价格类型(1 市价 2 限价)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "tradeType", value = "交易类型(1 开仓 2 平仓)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "交易状态(0未成交 1已成交 2已取消 4异常单)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "订单ID", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户ID", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "userName", value = "用户名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "mobile", value = "手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataType = "String", paramType = "query"),
    })
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")}, value = "按条件分页查询委托订单列表", httpMethod = "GET")
    public Object selectPage(@ApiIgnore Page<EntrustOrder> page,
                             String marketId, String type,
                             String priceType, String tradeType,
                             String status, String id,
                             String userId, String userName,
                             String mobile, String startTime,
                             String endTime) {
        QueryWrapper<EntrustOrder> ew = new QueryWrapper<>();
        QueryWrapper<User> userEw = new QueryWrapper<>();

        if (StringUtils.isNotBlank(endTime)) {
            endTime = endTime + " 23:59:59";
        }
        if (!Strings.isNullOrEmpty(userName)) {
            userEw.eq("b.username", userName);
        }
        if (!Strings.isNullOrEmpty(mobile)) {
            userEw.eq("b.mobile", mobile);
        }
        if (!Strings.isNullOrEmpty(marketId)) {
            ew.eq("market_id", marketId);
        }
        if (!Strings.isNullOrEmpty(type)) {
            ew.eq("a.type", type);
        }
        if (!Strings.isNullOrEmpty(priceType)) {
            ew.eq("a.price_type", priceType);
        }
        if (!Strings.isNullOrEmpty(tradeType)) {
            ew.eq("a.trade_type", tradeType);
        }
        if (!Strings.isNullOrEmpty(status)) {
            ew.eq("a.status", status);
        }
        if (!Strings.isNullOrEmpty(id)) {
            ew.eq("a.id", id);
        }
        if (!Strings.isNullOrEmpty(userId)) {
            ew.eq("a.user_id", userId);
        }
        if (!Strings.isNullOrEmpty(startTime)) {
            ew.ge("a.created", startTime);
        }
        if (!Strings.isNullOrEmpty(endTime)) {
            ew.le("a.created", endTime);
        }

        Integer total;
        List<EntrustOrder> entrustOrders;
        if (!ew.isEmptyOfWhere()) {
            entrustOrders = entrustOrderService.selectListPageByOrder(ew, userEw, page.getCurrent() - 1, page.getSize());
            total = entrustOrderService.selectListPageByOrderCount(ew);
        } else if (!userEw.isEmptyOfWhere()) {
            entrustOrders = entrustOrderService.selectListPageByUser(userEw, ew, page.getCurrent() - 1, page.getSize());
            total = entrustOrderService.selectListPageByUserCount(userEw);
        } else {
            entrustOrders = entrustOrderService.selectListPageEmpty(page.getCurrent() - 1, page.getSize());
            total = entrustOrderService.selectListPageCount();
        }

        return ResultMap.getSuccessfulResult(new PageDTO().setCurrent(page.getCurrent()).setSize(page.getSize()).setTotal(total).setRecords(entrustOrders));
    }

    @AuditLog(value = "币币交易委托订单导出", type = SysLogTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('trade_entrust_order_export')")
    @GetMapping({"/exportList"})
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")}, value = "委托订单导出", notes = "委托订单导出", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "marketId", value = "交易市场ID", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "交易方式(1买 2卖)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "priceType", value = "价格类型(1 市价 2 限价)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "tradeType", value = "交易类型(1 开仓 2 平仓)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "交易状态(0未成交 1已成交 2已取消 4异常单)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "订单ID", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户ID", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "userName", value = "用户名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "mobile", value = "手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataType = "String", paramType = "query"),
    })
    public void exportList(
            ModelMap model,
            @ApiIgnore Page<EntrustOrder> page,
                                   String marketId, String type,
                                   String priceType, String tradeType,
                                   String status, String id,
                                   String userId, String userName,
                                   String mobile, String startTime,
                                   String endTime,
            HttpServletResponse response) throws Exception {
        QueryWrapper<EntrustOrder> ew = new QueryWrapper<>();
        QueryWrapper<User> userEw = new QueryWrapper<>();

        if (StringUtils.isNotBlank(endTime)) {
            endTime = endTime + " 23:59:59";
        }
        if (!Strings.isNullOrEmpty(userName)) {
            userEw.eq("b.username", userName);
        }
        if (!Strings.isNullOrEmpty(mobile)) {
            userEw.eq("b.mobile", mobile);
        }
        if (!Strings.isNullOrEmpty(marketId)) {
            ew.eq("market_id", marketId);
        }
        if (!Strings.isNullOrEmpty(type)) {
            ew.eq("a.type", type);
        }
        if (!Strings.isNullOrEmpty(priceType)) {
            ew.eq("a.price_type", priceType);
        }
        if (!Strings.isNullOrEmpty(tradeType)) {
            ew.eq("a.trade_type", tradeType);
        }
        if (!Strings.isNullOrEmpty(status)) {
            ew.eq("a.status", status);
        }
        if (!Strings.isNullOrEmpty(id)) {
            ew.eq("a.id", id);
        }
        if (!Strings.isNullOrEmpty(userId)) {
            ew.eq("a.user_id", userId);
        }
        if (!Strings.isNullOrEmpty(startTime)) {
            ew.ge("a.created", startTime);
        }
        if (!Strings.isNullOrEmpty(endTime)) {
            ew.le("a.created", endTime);
        }
        List<EntrustOrder> orderList = entrustOrderService.selectListPageByOrder(ew, userEw, page.getCurrent() - 1, page.getSize());
/*        String[] header = {"订单ID", "用户ID", "用户名", "交易市场", "委托价格", "委托数量", "预计成交额", "已成交量", "手续费", "冻结金额", "交易方式", "状态", "委托时间"};
        String[] properties = {"idStr", "userIdStr", "userName", "marketName", "price", "volume", "predictTurnoverAmount", "dealStr", "fee", "freezeStr", "typeStr", "statusStr", "createdStr"};

        CellProcessor[] PROCESSORS = new CellProcessor[]{
                null,
                null,
                null,
                null,
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = String.valueOf(value);
                        if (value != null) {
                            DecimalFormat df = new DecimalFormat("0.00000000");
                            v = df.format(value);
                        }
                        return "\t" + v;
                    }
                },
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = String.valueOf(value);
                        if (value != null) {
                            DecimalFormat df = new DecimalFormat("0.00000000");
                            v = df.format(value);
                        }
                        return "\t" + v;
                    }
                },
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = String.valueOf(value);
                        if (value != null) {
                            DecimalFormat df = new DecimalFormat("0.00000000");
                            v = df.format(value);
                        }
                        return "\t" + v;
                    }
                },
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = "\t" + String.valueOf(value);
                        return v;
                    }
                },
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = String.valueOf(value);
                        if (value != null) {
                            DecimalFormat df = new DecimalFormat("0.00000000");
                            v = df.format(value);
                        }
                        return "\t" + v;
                    }
                },
                new CellProcessorAdaptor() {
                    @Override
                    public String execute(Object value, CsvContext context) {
                        String v = "\t" + String.valueOf(value);
                        return v;
                    }
                },
                null,
                null,
                null
        };
        String fileName = "币币交易委托.csv";
        try {
            ReportCsvUtils.reportListCsv(response, header, properties, fileName, retpage.getRecords(), PROCESSORS);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

/*        model.put("name", "币币交易委托");
        model.put("list", orderList);
        return new ModelAndView(new XlsxStreamingView(), model);*/

        ExcelExportUtil.exportToFile(orderList, "C:\\Users\\qiang\\Downloads\\Compressed\\1.xls");

/*        OutputStream os = new BufferedOutputStream(response.getOutputStream());
        try {
            response.setContentType("application/octet-stream");
            String fileName = URLDecoder.decode("用户列表.xls", "UTF-8");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));

            ExcelExportUtil.exportToFile(orderList, "C:\\Users\\qiang\\Downloads\\Compressed\\1.xlsx");
            *//*os.write(ExcelExportUtil.exportToBytes(orderList));
            os.flush();*//*
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(os != null){
                os.close();
            }
        }*/
    }
}
