package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.dto.CoinRechargeCountDTO;
import com.blockeng.admin.dto.CoinRechargeDTO;
import com.blockeng.admin.entity.CoinRecharge;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户充值,当前用户充值成功之后添加数据到这个表,充值一般无手续费.当status为1的时候表示充值成功 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-17
 */
public interface CoinRechargeMapper extends BaseMapper<CoinRecharge> {

     IPage<CoinRechargeDTO> selectMapPage(IPage<CoinRechargeDTO> page, @Param("ew") Wrapper<CoinRechargeDTO> wrapper);

    /**
     * 根据日期和用户统计充币人数
     * @param countDate
     * @param uidStrs 用户ID字符串('1,2,3')
     * @param status 充值状态
     * @return
     */
    Integer countByDateAndUidStrs(@Param("countDate") String countDate,
                                  @Param("uidStrs") String uidStrs,
                                  @Param("status") Integer status);

    /**
     * 充值金额，到账金额，充值币种，充值笔数，充值用户数 ，充值时间 统计统计
     * @param page
     * @param wrapper
     * @return
     */
    IPage<CoinRechargeCountDTO> selectCountMain (IPage<CoinRechargeCountDTO> page, @Param("ew") Wrapper wrapper);

    /**
     * <!--成功笔数，充值时间-->
     * @param paramMap
     * @return
     */
    List<CoinRechargeCountDTO> selectValidCounts( Map<String,Object> paramMap);


    /**
     * <!--用户数，用户id，充值时间-->
     * @param wrapper
     * @return
     */
    List<CoinRechargeCountDTO> selectUserCt(@Param("ew") Wrapper<CoinRechargeCountDTO> wrapper);
}
