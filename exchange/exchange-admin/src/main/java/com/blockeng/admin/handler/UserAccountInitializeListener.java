package com.blockeng.admin.handler;

import com.blockeng.admin.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author qiang
 */
@Component
@Slf4j
public class UserAccountInitializeListener {

    @Autowired
    private AccountService accountService;

    @RabbitListener(queues = {"useraccount.initialize"})
    public void initialize(long coinId) {
        try {
            //批量初始化资金账户
            accountService.initialBatchAccount(coinId);
        }catch (Exception e) {
            e.printStackTrace();
        }
/*        try(Connection conn = dataSource.getConnection()) {
            conn.setAutoCommit(false);
            try (Statement stmt = conn.createStatement()) {
                stmt.execute("set drds_transaction_policy = 'flexible'");

                String sql = "SELECT id FROM user LIMIT 2";
                SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);
                List<Account> pool = new ArrayList<>(200);

                try {
                    while(sqlRowSet.next()) {
                        long id = sqlRowSet.getLong("id");
*//*                    Account account = new Account()
                            .setUserId(id)
                            .setStatus(AccountStatus.ENABLE.getCode())
                            .setCoinId(coinId)
                            .setWithdrawalsAmount(BigDecimal.ZERO)
                            .setBalanceAmount(BigDecimal.ZERO)
                            .setFreezeAmount(BigDecimal.ZERO)
                            .setRechargeAmount(BigDecimal.ZERO)
                            .setVersion(0L);

                    accountService.insert(account);*//*
                        jdbcTemplate.execute("SET AUTOCOMMIT = off");
                        jdbcTemplate.execute("set drds_transaction_policy = 'flexible'");
                        jdbcTemplate.execute("INSERT INTO account ( id,user_id,coin_id,status,balance_amount,freeze_amount,recharge_amount,withdrawals_amount,version ) VALUES ( 1021952283184906241,1014460164548149249,1021759767760285697,1,0,0,0,0,0)");
                        if (sqlRowSet.isLast()) {
                            throw new Exception("回滚测试");
                        }
                    }

*//*                    pool.add(account);
                    if (pool.size() >= 200 || sqlRowSet.isLast()) {
                        accountService.insertBatch(pool);
                        pool.clear();
                    }*//*
                    //accountService.insert(account);
                    conn.commit();
                    log.info("提交事务");
                }catch (Exception e) {
                    conn.rollback();
                    log.error(e.getMessage());
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
    }


  /*  @DrdsTransaction
    //@Transactional(rollbackFor = Exception.class)
    public void sync() {
*//*        String sql = "SELECT id FROM user LIMIT 2";
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);
        while(sqlRowSet.next()) {
            long id = sqlRowSet.getLong("id");
            jdbcTemplate.execute("INSERT INTO account ( id,user_id,coin_id,status,balance_amount,freeze_amount,recharge_amount,withdrawals_amount,version ) VALUES ( 1021952283184906241,1014460164548149249,1021759767760285697,1,0,0,0,0,0)");
        }*//*

        DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
        definition.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_NESTED);
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager, definition);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    flexibleSet();
                    String sql = "SELECT id FROM user LIMIT 2";
                    SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);
                    List<Account> pool = new ArrayList<>(200);
                    while(sqlRowSet.next()) {
                        long id = sqlRowSet.getLong("id");
                        Account account = new Account()
                                .setUserId(id)
                                .setStatus(AccountStatus.ENABLE.getCode())
                                .setCoinId(1021759767760285697L)
                                .setWithdrawalsAmount(BigDecimal.ZERO)
                                .setBalanceAmount(BigDecimal.ZERO)
                                .setFreezeAmount(BigDecimal.ZERO)
                                .setRechargeAmount(BigDecimal.ZERO)
                                .setVersion(0L);

                        //accountService.insert(account);
                        pool.add(account);
                        if (pool.size() >= 200 || sqlRowSet.isLast()) {
                            accountService.insertBatch(pool);
                            pool.clear();
                        }

                        if (sqlRowSet.isLast()) {
                            throw new Exception("回滚测试");
                        }
                        //jdbcTemplate.execute("INSERT INTO account ( id,user_id,coin_id,status,balance_amount,freeze_amount,recharge_amount,withdrawals_amount,version ) VALUES ( 1021952283184906241,1014460164548149249,1021759767760285697,1,0,0,0,0,0)");
                    }
                } catch (Throwable throwable) {
                    status.setRollbackOnly();
                    throwable.printStackTrace();
                }
            }
        });
    }

    private void flexibleSet() {
        jdbcTemplate.execute("SET AUTOCOMMIT = off");
        jdbcTemplate.execute("set drds_transaction_policy = 'flexible'");
    }*/
}