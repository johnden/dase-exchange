package com.blockeng.admin.mapper;

import com.blockeng.admin.entity.SysPrivilege;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 权限配置 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-11
 */
public interface SysPrivilegeMapper extends BaseMapper<SysPrivilege> {

    List<SysPrivilege> selectListByUserId(Long userId);
}
