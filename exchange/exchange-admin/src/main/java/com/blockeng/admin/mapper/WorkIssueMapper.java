package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.dto.UserWorkIssueDTO;
import com.blockeng.admin.entity.WorkIssue;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 工单记录 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface WorkIssueMapper extends BaseMapper<WorkIssue> {

    IPage<UserWorkIssueDTO> selectMapPage(IPage<UserWorkIssueDTO> page, @Param("ew") Wrapper<UserWorkIssueDTO> ew);

    UserWorkIssueDTO selectOneObj(Long id);
}