package com.blockeng.admin.web.minepool;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.annotation.AuditLog;
import com.blockeng.admin.dto.AuditMinePoolDTO;
import com.blockeng.admin.dto.CreateMinePoolDTO;
import com.blockeng.admin.entity.MinePool;
import com.blockeng.admin.enums.SysLogTypeEnum;
import com.blockeng.admin.service.MinePoolService;
import com.blockeng.framework.enums.BaseStatus;
import com.blockeng.framework.http.Response;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description:
 * @Author: Chen Long
 * @Date: Created in 2018/6/29 下午4:14
 * @Modified by: Chen Long
 */
@RestController
@RequestMapping("/mine/pool")
@Api(value = "矿池", description = "矿池", tags = "矿池")
public class MinePoolController {

    @Autowired
    private MinePoolService minePoolService;

    /**
     * 创建矿池
     *
     * @param createMinePoolDTO
     * @param result
     * @return
     */
    @AuditLog(value = "创建矿池", type = SysLogTypeEnum.INSERT)
    @PostMapping
    @PreAuthorize("hasAuthority('mine_pool_create')")
    @ApiOperation(value = "创建矿池", httpMethod = "POST", authorizations = {@Authorization(value = "Authorization")})
    public Response create(@RequestBody @Valid CreateMinePoolDTO createMinePoolDTO, BindingResult result) {
        Long userId = createMinePoolDTO.getCreateUser();
        QueryWrapper<MinePool> wrapper = new QueryWrapper<>();
        wrapper.eq("create_user", userId);
        MinePool minePool = minePoolService.selectOne(wrapper);
        if (minePool != null) {
            return Response.err(50030, "此用户已经创建了一个矿池");
        }
        minePool = new MinePool();
        minePool.setName(createMinePoolDTO.getName())
                .setDescription(createMinePoolDTO.getDescription())
                .setCreateUser(userId)
                .setStatus(BaseStatus.INVALID.getCode());
        minePoolService.insert(minePool);
        return Response.ok();
    }

    /**
     * 查询矿池
     *
     * @param name    矿池名称
     * @param userId  用户ID
     * @param status  用户名
     * @param current 当前页码
     * @param size    每页显示数据条数
     * @return
     */
    @AuditLog(value = "查询矿池", type = SysLogTypeEnum.SELECT)
    @GetMapping
    @PreAuthorize("hasAuthority('mine_pool_query')")
    @ApiOperation(value = "按条件分页查询矿池", httpMethod = "GET", authorizations = {@Authorization(value = "Authorization")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户ID", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "current", value = "当前页码", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页显示数据条数", dataType = "int", paramType = "query")
    })
    public Response minePoolList(@RequestParam(value = "name", defaultValue = "") String name,
                                 @RequestParam(value = "userId", defaultValue = "") String userId,
                                 @RequestParam(value = "status", defaultValue = "") String status,
                                 @RequestParam(value = "current", defaultValue = "1") int current,
                                 @RequestParam(value = "size", defaultValue = "10") int size) {
        Page<MinePool> page = new Page<>(current, size);
        QueryWrapper<MinePool> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(userId)) {
            wrapper.eq("create_user", userId);
        }
        if (!StringUtils.isEmpty(status)) {
            wrapper.eq("status", Integer.parseInt(status));
        }
        return Response.ok(minePoolService.selectPage(page, wrapper));
    }

    /**
     * 审核矿池
     *
     * @param auditMinePoolDTO 审核请求参数
     * @return
     */
    @AuditLog(value = "审核矿池", type = SysLogTypeEnum.AUDIT)
    @PutMapping
    @PreAuthorize("hasAuthority('mine_pool_audit')")
    @ApiOperation(value = "审核矿池", httpMethod = "PUT", authorizations = {@Authorization(value = "Authorization")})
    @ApiImplicitParam(name = "auditMinePoolDTO", value = "审核请求参数", required = true, dataType = "AuditMinePoolDTO", paramType = "body")
    public Response audit(@RequestBody AuditMinePoolDTO auditMinePoolDTO) {
        MinePool minePool = minePoolService.selectById(auditMinePoolDTO.getId());
        if (minePool != null) {
            minePool.setStatus(auditMinePoolDTO.getStatus())
                    .setRemark(auditMinePoolDTO.getRemark());
            minePoolService.updateById(minePool);
        }
        return Response.ok();
    }
}
