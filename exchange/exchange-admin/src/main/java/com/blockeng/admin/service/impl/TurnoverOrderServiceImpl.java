package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.TradeTopVolumeDTO;
import com.blockeng.admin.dto.TurnoverOrderCountDTO;
import com.blockeng.admin.entity.TurnoverOrder;
import com.blockeng.admin.mapper.TurnoverOrderMapper;
import com.blockeng.admin.service.TurnoverOrderService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 成交订单 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
public class TurnoverOrderServiceImpl extends ServiceImpl<TurnoverOrderMapper, TurnoverOrder> implements TurnoverOrderService {

    @Override
    public IPage<TurnoverOrder> selectListPage(IPage<TurnoverOrder> page, QueryWrapper<TurnoverOrder> wrapper) {
        //page.setRecords(baseMapper.selectListPage(page, wrapper));
        return baseMapper.selectListPage(page, wrapper);
    }

    @Override
    public IPage<TradeTopVolumeDTO> selectTradeTopVolumepage(IPage<TradeTopVolumeDTO> page, QueryWrapper<TradeTopVolumeDTO> wrapper) {
        page.setRecords(baseMapper.selectTradeTopVolumePage(page, wrapper));
        return page;
    }

    @Override
    public IPage<TurnoverOrderCountDTO> selectCountMain(IPage<TurnoverOrderCountDTO> page, Map<String, Object> paramMap) {
            return page.setRecords(baseMapper.selectCountMain(page, paramMap));
    }

    @Override
    public List<TurnoverOrderCountDTO> selectSellUserCount(String[]coins ) {
        return baseMapper.selectSellUserCount(coins);
    }

    @Override
    public List<TurnoverOrderCountDTO> selectBuyUserCount(String[]coins ) {
        return baseMapper.selectBuyUserCount(coins);
    }

}
