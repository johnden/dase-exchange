package com.blockeng.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.entity.MinePool;

/**
 * <p>
 * 矿池 服务类
 * </p>
 *
 * @author qiang
 * @since 2018-05-12
 */
public interface MinePoolService extends IService<MinePool> {

}
