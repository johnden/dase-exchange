package com.blockeng.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.admin.dto.UserWorkIssueDTO;
import com.blockeng.admin.entity.WorkIssue;
import com.blockeng.admin.mapper.WorkIssueMapper;
import com.blockeng.admin.service.WorkIssueService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工单记录 服务实现类
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
@Service
public class WorkIssueServiceImpl extends ServiceImpl<WorkIssueMapper, WorkIssue> implements WorkIssueService {

    @Override
    public IPage<UserWorkIssueDTO> selectMapPage(IPage<UserWorkIssueDTO> page, QueryWrapper<UserWorkIssueDTO> queryWrapper) {
        //return page.setRecords(baseMapper.selectMapPage(page, queryWrapper));
        return baseMapper.selectMapPage(page, queryWrapper);
    }

    @Override
    public UserWorkIssueDTO selectOneObj(Long id) {
            return baseMapper.selectOneObj(id);
    }
}