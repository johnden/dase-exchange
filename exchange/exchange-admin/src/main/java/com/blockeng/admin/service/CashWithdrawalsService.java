package com.blockeng.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.admin.dto.AuditDTO;
import com.blockeng.admin.dto.CashWithdrawalsCountDTO;
import com.blockeng.admin.dto.UserWithDrawalsDTO;
import com.blockeng.admin.entity.CashWithdrawals;
import com.blockeng.admin.entity.SysUser;
import com.blockeng.framework.exception.ExchangeException;
import com.blockeng.framework.exception.ExchangeException;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 提现表 服务类
 * </p>
 *
 * @author lxl
 * @since 2018-05-17
 */
public interface CashWithdrawalsService extends IService<CashWithdrawals> {

    IPage<UserWithDrawalsDTO> selectMapPage(IPage<UserWithDrawalsDTO> page, QueryWrapper<UserWithDrawalsDTO> queryWrapper);

    UserWithDrawalsDTO selectOneObj(Long id);

    /**
     * 法币提现审核
     *
     * @param auditDTO 提币审核请求参数
     * @param sysUser  当前登录用户
     * @return
     */
    void cashWithdrawAudit(AuditDTO auditDTO, SysUser sysUser) throws ExchangeException;

    /**
     * 统计
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<CashWithdrawalsCountDTO> selectCountMain (IPage<CashWithdrawalsCountDTO> page, QueryWrapper<CashWithdrawalsCountDTO> queryWrapper);


    /**
     * <!--成功笔数，充值时间-->
     * @param queryWrapper
     * @return
     */
    List<CashWithdrawalsCountDTO> selectValidCounts(QueryWrapper<CashWithdrawalsCountDTO> queryWrapper);


    /**
     * <!--用户数，用户id，充值时间-->
     * @param queryWrapper
     * @return
     */
    List<CashWithdrawalsCountDTO> selectUserCt(QueryWrapper<CashWithdrawalsCountDTO> queryWrapper);
}
