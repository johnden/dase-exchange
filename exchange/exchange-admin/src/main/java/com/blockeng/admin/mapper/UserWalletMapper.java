package com.blockeng.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.admin.dto.UserWalletDTO;
import com.blockeng.admin.entity.AccountDetail;
import com.blockeng.admin.entity.UserWallet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户钱包表 Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2018-05-13
 */
public interface UserWalletMapper extends BaseMapper<UserWallet> {

    List<Long> selectOptimizePageSql(IPage<UserWalletDTO> page, @Param("ew") Wrapper<UserWalletDTO> wrapper);

    List<UserWalletDTO> selectUserWalletList(@Param("ew") Wrapper<UserWalletDTO> wrapper);
}
