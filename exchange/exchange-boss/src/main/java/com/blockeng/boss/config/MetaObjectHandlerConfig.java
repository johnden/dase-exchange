package com.blockeng.boss.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MetaObjectHandlerConfig extends MetaObjectHandler {
    private static final Logger log = LoggerFactory.getLogger(MetaObjectHandlerConfig.class);

    public void insertFill(MetaObject metaObject) {
    }

    public void updateFill(MetaObject metaObject) {
    }
}

