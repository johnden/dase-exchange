package com.blockeng.boss.service;

public interface LockService {
    boolean getLock(String redisKey, String value, boolean waitLock);

    boolean unlock(String redisKey, String value);
}

