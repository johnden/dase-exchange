package com.blockeng.boss.service;

import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.blockeng.boss.dto.Symbol;

public interface SymbolService {
    @Cached(name="markets:", cacheType=CacheType.LOCAL)
    Symbol queryById(Long marketId);
}

