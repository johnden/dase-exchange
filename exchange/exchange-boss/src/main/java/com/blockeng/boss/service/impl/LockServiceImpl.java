package com.blockeng.boss.service.impl;

import com.blockeng.boss.service.LockService;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class LockServiceImpl implements LockService {
    @Autowired
    private StringRedisTemplate redisTemplate;

    public boolean getLock(String redisKey, String value, boolean waitLock) {
        long mills = System.currentTimeMillis() + 2000L;
        String key = new StringBuffer(redisKey).append(value).toString();
        try {
            while (!this.redisTemplate.opsForValue().setIfAbsent(key, value).booleanValue()) {
                if (!waitLock || System.currentTimeMillis() >= mills) {
                    return false;
                }
                Thread.sleep(0L, 100);
            }
            this.redisTemplate.expire(key, 30000L, TimeUnit.MILLISECONDS);
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }

    public boolean unlock(String redisKey, String value) {
        String key = new StringBuffer(redisKey).append(value).toString();
        return this.redisTemplate.delete(key);
    }
}

