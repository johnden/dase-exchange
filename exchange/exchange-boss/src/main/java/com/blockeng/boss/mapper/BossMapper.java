package com.blockeng.boss.mapper;

import com.blockeng.boss.dto.Symbol;
import com.blockeng.boss.entity.Account;
import com.blockeng.boss.entity.AccountDetail;
import com.blockeng.boss.entity.TurnoverOrder;
import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Param;

 public interface BossMapper {
     int queryDealOrderCount(@Param(value = "sellOrderId") Long sellOrderId, @Param(value = "buyOrderId") Long buyOrderId);

     Symbol querySymbol(@Param(value = "marketId") Long marketId);

     int saveDealOrder(TurnoverOrder turnoverOrder);

     int modifyEntrustBuyOrder(@Param(value = "orderId") Long orderId, @Param(value = "dealVolume") BigDecimal dealVolume);

     int modifyEntrustSellOrder(@Param(value = "orderId") Long orderId, @Param(value = "dealVolume") BigDecimal dealVolume);

     Account queryAccount(@Param(value = "userId") Long userId, @Param(value = "coinId") Long coinId);

     int subtractAmount(@Param(value = "accountId") Long accountId, @Param(value = "amount") BigDecimal amount, @Param(value = "returnAmount") BigDecimal returnAmount);

     int addAmount(@Param(value = "accountId") Long accountId, @Param(value = "amount") BigDecimal amount);

     int batchAddAccountDetail(List<AccountDetail> accountDetails);
}

