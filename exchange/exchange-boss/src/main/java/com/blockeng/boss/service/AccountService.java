package com.blockeng.boss.service;

import com.blockeng.framework.enums.BusinessType;
import java.math.BigDecimal;

 public interface AccountService {
     boolean transferBuyAmount(Long fromUserId, Long toUserId, Long coinId, BigDecimal amount, BigDecimal buyFee, BigDecimal sellFee, BigDecimal returnAmount,
                               BigDecimal subtractAmount, BusinessType businessType, Long orderId);

     boolean transferSellAmount(Long fromUserId, Long toUserId, Long coinId, BigDecimal amount, BusinessType businessType, Long orderId);
}

