package com.blockeng.boss.service.impl;

import com.blockeng.boss.entity.Account;
import com.blockeng.boss.entity.AccountDetail;
import com.blockeng.boss.mapper.BossMapper;
import com.blockeng.boss.service.AccountService;
import com.blockeng.framework.enums.AmountDirection;
import com.blockeng.framework.enums.BusinessType;
import com.blockeng.framework.exception.AccountException;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
    private static final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);
    @Autowired
    private BossMapper bossMapper;

    public boolean transferBuyAmount(Long fromUserId, Long toUserId, Long coinId, BigDecimal amount, BigDecimal buyFee, BigDecimal sellFee, BigDecimal returnAmount,
                                     BigDecimal subtractAmount, BusinessType businessType, Long orderId) {
        Account fromAccount = this.bossMapper.queryAccount(fromUserId, coinId);
        if (fromAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", fromUserId, coinId);
            throw new AccountException("资金账户异常");
        }
        Account toAccount = this.bossMapper.queryAccount(toUserId, coinId);
        if (toAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", toUserId, coinId);
            throw new AccountException("资金账户异常");
        }
        int count1 = this.bossMapper.subtractAmount(fromAccount.getId(), subtractAmount, returnAmount);
        int count2 = this.bossMapper.addAmount(toAccount.getId(), amount.subtract(sellFee));
        if (count1 == 1 && count2 == 1) {
            ArrayList<AccountDetail> accountDetails = new ArrayList<AccountDetail>(2);
            if (returnAmount.compareTo(BigDecimal.ZERO) == 1) {
                AccountDetail returnAccountDetail = new AccountDetail(fromUserId, coinId, fromAccount.getId(), fromAccount.getId(), orderId, Integer.valueOf(AmountDirection.INCOME.getType()), businessType.getCode(), returnAmount, BigDecimal.ZERO, "解冻");
                accountDetails.add(returnAccountDetail);
            }
            AccountDetail fromAccountDetail = new AccountDetail(fromUserId, coinId, fromAccount.getId(), toAccount.getId(), orderId, Integer.valueOf(AmountDirection.OUT.getType()), businessType.getCode(), amount.add(buyFee), buyFee, businessType.getDesc());
            AccountDetail toAccountDetail = new AccountDetail(toUserId, coinId, toAccount.getId(), fromAccount.getId(), orderId, Integer.valueOf(AmountDirection.INCOME.getType()), businessType.getCode(), amount.subtract(sellFee), sellFee, businessType.getDesc());
            accountDetails.add(fromAccountDetail);
            accountDetails.add(toAccountDetail);
            this.bossMapper.batchAddAccountDetail(accountDetails);
            return true;
        }
        log.error("资金划转失败，orderId:{}, fromUserId:{}, toUserId:{}, coinId:{}, amount:{}, buyFee:{}, sellFee:{}, businessType:{}", new Object[]{orderId, fromUserId, toUserId, coinId, amount, buyFee, sellFee, businessType.getCode()});
        return false;
    }

    public boolean transferSellAmount(Long fromUserId, Long toUserId, Long coinId, BigDecimal amount, BusinessType businessType, Long orderId) {
        Account fromAccount = this.bossMapper.queryAccount(fromUserId, coinId);
        if (fromAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", fromUserId, coinId);
            throw new AccountException("资金账户异常");
        }
        Account toAccount = this.bossMapper.queryAccount(toUserId, coinId);
        if (toAccount == null) {
            log.error("资金划转-资金账户异常，userId:{}, coinId:{}", toUserId, coinId);
            throw new AccountException("资金账户异常");
        }
        int count1 = this.bossMapper.subtractAmount(fromAccount.getId(), amount, BigDecimal.ZERO);
        int count2 = this.bossMapper.addAmount(toAccount.getId(), amount);
        if (count1 > 0 && count2 > 0) {
            ArrayList<AccountDetail> accountDetails = new ArrayList<AccountDetail>(2);
            AccountDetail fromAccountDetail = new AccountDetail(fromUserId, coinId, fromAccount.getId(), toAccount.getId(),
                    orderId, Integer.valueOf(AmountDirection.OUT.getType()), businessType.getCode(), amount, BigDecimal.ZERO, businessType.getDesc());
            AccountDetail toAccountDetail = new AccountDetail(toUserId, coinId, toAccount.getId(), fromAccount.getId(),
                    orderId, Integer.valueOf(AmountDirection.INCOME.getType()), businessType.getCode(), amount, BigDecimal.ZERO, businessType.getDesc());
            accountDetails.add(fromAccountDetail);
            accountDetails.add(toAccountDetail);
            this.bossMapper.batchAddAccountDetail(accountDetails);
            return true;
        }
        log.error("资金划转，orderId:{}, fromUserId:{}, toUserId:{}, coinId:{}, amount:{}, businessType:{}", new Object[]{orderId, fromUserId, toUserId, coinId, amount, businessType.getCode()});
        throw new AccountException("资金划转失败");
    }
}

