package com.blockeng.boss.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="turnover_order")
public class DealOrder implements Serializable {
    private static final long serialVersionUID = -5057531952746023252L;
    @Id
    private String objectId;
    @Field(value="market_id")
    private Long marketId;
    @Indexed
    private String symbol;
    @Field(value="market_name")
    private String marketName;
    @Field(value="market_type")
    private int marketType;
    @Field(value="trade_type")
    private int tradeType;
    @Field(value="buy_order_id")
    private Long buyOrderId;
    @Field(value="buy_coin_id")
    private Long buyCoinId;
    @Field(value="buy_user_id")
    private Long buyUserId;
    @Field(value="buy_price")
    private BigDecimal buyPrice;
    @Field(value="buy_volume")
    private BigDecimal buyVolume;
    @Field(value="buy_fee_rate")
    private BigDecimal buyFeeRate;
    @Field(value="sell_order_id")
    private Long sellOrderId;
    @Field(value="sell_coin_id")
    private Long sellCoinId;
    @Field(value="sell_user_id")
    private Long sellUserId;
    @Field(value="sell_price")
    private BigDecimal sellPrice;
    @Field(value="sell_volume")
    private BigDecimal sellVolume;
    @Field(value="sell_fee_rate")
    private BigDecimal sellFeeRate;
    @Field(value="volume")
    private BigDecimal vol;
    private BigDecimal amount;
    private BigDecimal price;
    @Indexed
    private Date created;

    public String getObjectId() {
        return this.objectId;
    }

    public Long getMarketId() {
        return this.marketId;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public String getMarketName() {
        return this.marketName;
    }

    public int getMarketType() {
        return this.marketType;
    }

    public int getTradeType() {
        return this.tradeType;
    }

    public Long getBuyOrderId() {
        return this.buyOrderId;
    }

    public Long getBuyCoinId() {
        return this.buyCoinId;
    }

    public Long getBuyUserId() {
        return this.buyUserId;
    }

    public BigDecimal getBuyPrice() {
        return this.buyPrice;
    }

    public BigDecimal getBuyVolume() {
        return this.buyVolume;
    }

    public BigDecimal getBuyFeeRate() {
        return this.buyFeeRate;
    }

    public Long getSellOrderId() {
        return this.sellOrderId;
    }

    public Long getSellCoinId() {
        return this.sellCoinId;
    }

    public Long getSellUserId() {
        return this.sellUserId;
    }

    public BigDecimal getSellPrice() {
        return this.sellPrice;
    }

    public BigDecimal getSellVolume() {
        return this.sellVolume;
    }

    public BigDecimal getSellFeeRate() {
        return this.sellFeeRate;
    }

    public BigDecimal getVol() {
        return this.vol;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public Date getCreated() {
        return this.created;
    }

    public DealOrder setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }

    public DealOrder setMarketId(Long marketId) {
        this.marketId = marketId;
        return this;
    }

    public DealOrder setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public DealOrder setMarketName(String marketName) {
        this.marketName = marketName;
        return this;
    }

    public DealOrder setMarketType(int marketType) {
        this.marketType = marketType;
        return this;
    }

    public DealOrder setTradeType(int tradeType) {
        this.tradeType = tradeType;
        return this;
    }

    public DealOrder setBuyOrderId(Long buyOrderId) {
        this.buyOrderId = buyOrderId;
        return this;
    }

    public DealOrder setBuyCoinId(Long buyCoinId) {
        this.buyCoinId = buyCoinId;
        return this;
    }

    public DealOrder setBuyUserId(Long buyUserId) {
        this.buyUserId = buyUserId;
        return this;
    }

    public DealOrder setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
        return this;
    }

    public DealOrder setBuyVolume(BigDecimal buyVolume) {
        this.buyVolume = buyVolume;
        return this;
    }

    public DealOrder setBuyFeeRate(BigDecimal buyFeeRate) {
        this.buyFeeRate = buyFeeRate;
        return this;
    }

    public DealOrder setSellOrderId(Long sellOrderId) {
        this.sellOrderId = sellOrderId;
        return this;
    }

    public DealOrder setSellCoinId(Long sellCoinId) {
        this.sellCoinId = sellCoinId;
        return this;
    }

    public DealOrder setSellUserId(Long sellUserId) {
        this.sellUserId = sellUserId;
        return this;
    }

    public DealOrder setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
        return this;
    }

    public DealOrder setSellVolume(BigDecimal sellVolume) {
        this.sellVolume = sellVolume;
        return this;
    }

    public DealOrder setSellFeeRate(BigDecimal sellFeeRate) {
        this.sellFeeRate = sellFeeRate;
        return this;
    }

    public DealOrder setVol(BigDecimal vol) {
        this.vol = vol;
        return this;
    }

    public DealOrder setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public DealOrder setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public DealOrder setCreated(Date created) {
        this.created = created;
        return this;
    }

    public String toString() {
        return "DealOrder(objectId=" + this.getObjectId() + ", marketId=" + this.getMarketId() + ", symbol=" + this.getSymbol() + ", marketName=" + this.getMarketName() + ", marketType=" + this.getMarketType() + ", tradeType=" + this.getTradeType() + ", buyOrderId=" + this.getBuyOrderId() + ", buyCoinId=" + this.getBuyCoinId() + ", buyUserId=" + this.getBuyUserId() + ", buyPrice=" + this.getBuyPrice() + ", buyVolume=" + this.getBuyVolume() + ", buyFeeRate=" + this.getBuyFeeRate() + ", sellOrderId=" + this.getSellOrderId() + ", sellCoinId=" + this.getSellCoinId() + ", sellUserId=" + this.getSellUserId() + ", sellPrice=" + this.getSellPrice() + ", sellVolume=" + this.getSellVolume() + ", sellFeeRate=" + this.getSellFeeRate() + ", vol=" + this.getVol() + ", amount=" + this.getAmount() + ", price=" + this.getPrice() + ", created=" + this.getCreated() + ")";
    }
}

