package com.blockeng.boss.handle;

import com.blockeng.boss.dto.DealOrder;
import com.blockeng.boss.service.BossService;
import com.blockeng.framework.dto.MatchDTO;
import com.blockeng.framework.exception.ExchangeException;
import com.google.gson.Gson;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class SettlementTask {
    @Autowired
    private BossService bossService;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Async
    @Retryable(value={ExchangeException.class}, maxAttempts=5, backoff=@Backoff(delay=1000L, multiplier=1.5))
    public void settlement(DealOrder dealOrder) {
        boolean result = this.bossService.settlement(dealOrder);
        if (result) {
            Query query;
            this.mongoTemplate.insert(dealOrder);
            BigDecimal buyVolume = dealOrder.getBuyVolume();
            BigDecimal sellVolume = dealOrder.getSellVolume();
            if (buyVolume.compareTo(sellVolume) == 1) {
                query = new Query(Criteria.where("_id").is(dealOrder.getBuyOrderId()));
                Update update = new Update().set("volume", buyVolume.subtract(dealOrder.getVol()));
                this.mongoTemplate.updateFirst(query, update, "entrust_order");
                this.mongoTemplate.remove(new Query(Criteria.where("_id").is(dealOrder.getSellOrderId())), "entrust_order");
            } else if (buyVolume.compareTo(sellVolume) == -1) {
                query = new Query(Criteria.where((String)"_id").is(dealOrder.getSellOrderId()));
                Update update = new Update().set("volume", sellVolume.subtract(dealOrder.getVol()));
                this.mongoTemplate.updateFirst(query, update, "entrust_order");
                query = new Query(Criteria.where("_id").is(dealOrder.getBuyOrderId()));
                update = new Update().set("status", 1);
                this.mongoTemplate.updateFirst(query, update, "entrust_order");
            } else {
                query = new Query(Criteria.where("_id").in(new Object[]{dealOrder.getBuyOrderId(), dealOrder.getSellOrderId()}));
                this.mongoTemplate.remove(query, "entrust_order");
            }
            MatchDTO matchDTO = new MatchDTO().setSymbol(dealOrder.getSymbol()).setBuyUserId(dealOrder.getBuyUserId()).setSellUserId(dealOrder.getSellUserId()).setPrice(dealOrder.getPrice());
            this.kafkaTemplate.send("ticker", new Gson().toJson(matchDTO));
        }
    }
}

