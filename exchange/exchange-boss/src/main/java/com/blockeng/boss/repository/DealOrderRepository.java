package com.blockeng.boss.repository;

import com.blockeng.boss.dto.DealOrder;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

public interface DealOrderRepository<T, ID extends Serializable> extends CrudRepository<DealOrder, Long> {
}

