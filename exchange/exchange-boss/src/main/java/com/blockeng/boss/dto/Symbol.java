package com.blockeng.boss.dto;

public class Symbol {
    private Long buyCoinId;
    private Long sellCoinId;

    public Long getBuyCoinId() {
        return this.buyCoinId;
    }

    public Long getSellCoinId() {
        return this.sellCoinId;
    }

    public Symbol setBuyCoinId(Long buyCoinId) {
        this.buyCoinId = buyCoinId;
        return this;
    }

    public Symbol setSellCoinId(Long sellCoinId) {
        this.sellCoinId = sellCoinId;
        return this;
    }

    public String toString() {
        return "Symbol(buyCoinId=" + this.getBuyCoinId() + ", sellCoinId=" + this.getSellCoinId() + ")";
    }
}

