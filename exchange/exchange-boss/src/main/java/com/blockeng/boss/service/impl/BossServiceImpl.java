package com.blockeng.boss.service.impl;

import com.blockeng.boss.dto.DealOrder;
import com.blockeng.boss.dto.Symbol;
import com.blockeng.boss.entity.TurnoverOrder;
import com.blockeng.boss.enums.MessageChannel;
import com.blockeng.boss.mapper.BossMapper;
import com.blockeng.boss.service.AccountService;
import com.blockeng.boss.service.BossService;
import com.blockeng.boss.service.SymbolService;
import com.blockeng.framework.enums.BusinessType;
import com.blockeng.framework.enums.OrderStatus;
import com.blockeng.framework.enums.OrderType;
import com.blockeng.framework.exception.ExchangeException;
import com.blockeng.framework.utils.GsonUtil;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BossServiceImpl implements BossService {
    private static final Logger log = LoggerFactory.getLogger(BossServiceImpl.class);
    @Autowired
    private BossMapper bossMapper;
    @Autowired
    private SymbolService symbolService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private AccountService accountService;
    private ExecutorService executor = new ThreadPoolExecutor(100, Integer.MAX_VALUE, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(1024), new ThreadPoolExecutor.CallerRunsPolicy());
    private ListeningExecutorService service = MoreExecutors.listeningDecorator(this.executor);

    @Transactional
    public boolean settlement(DealOrder dealOrder) {
        try {
            BigDecimal volume = dealOrder.getVol();
            BigDecimal price = dealOrder.getPrice();
            BigDecimal buyPrice = dealOrder.getBuyPrice();
            BigDecimal amount = volume.multiply(price);
            BigDecimal buyFee = amount.multiply(dealOrder.getBuyFeeRate());
            BigDecimal returnAmount = buyPrice.subtract(price).multiply(volume).multiply(BigDecimal.ONE.add(dealOrder.getBuyFeeRate()));
            BigDecimal sellerFee = amount.multiply(dealOrder.getSellFeeRate());
            BigDecimal subtractAmount = buyPrice.multiply(volume).multiply(BigDecimal.ONE.add(dealOrder.getBuyFeeRate()));
            if (dealOrder.getBuyCoinId() == null || dealOrder.getSellCoinId() == null) {
                Symbol market = this.symbolService.queryById(dealOrder.getMarketId());
                dealOrder.setBuyCoinId(market.getBuyCoinId());
                dealOrder.setSellCoinId(market.getSellCoinId());
            }
            TurnoverOrder turnoverOrder =getTurnoverOrder(dealOrder);
            if (dealOrder.getTradeType() == 1) {
                turnoverOrder.setOrderId(dealOrder.getBuyOrderId());
            } else {
                turnoverOrder.setOrderId(dealOrder.getSellOrderId());
            }
            turnoverOrder.setDealBuyFee(buyFee).setDealBuyFeeRate(dealOrder.getBuyFeeRate()).setDealSellFee(sellerFee).setDealSellFeeRate(dealOrder.getSellFeeRate()).setVolume(volume).setPrice(price).setAmount(amount).setStatus(Integer.valueOf(OrderStatus.DEAL.getCode()));
            this.bossMapper.saveDealOrder(turnoverOrder);
            this.rabbitTemplate.convertAndSend(MessageChannel.TX_SUM.getChannel(), GsonUtil.toJson(turnoverOrder));
            this.modifyEntrustOrder(dealOrder.getBuyOrderId(), volume, OrderType.BUY);
            this.modifyEntrustOrder(dealOrder.getSellOrderId(), volume, OrderType.SELL);
            ListenableFuture updateBuyAccountTask = this.service.submit(() -> {
                boolean result = this.accountService.transferBuyAmount(dealOrder.getBuyUserId(), dealOrder.getSellUserId(), dealOrder.getBuyCoinId(), amount, buyFee, sellerFee, returnAmount, subtractAmount, BusinessType.TRADE_DEAL, turnoverOrder.getId());
                return result;
            });
            updateBuyAccountTask.addListener(() -> {
                try {
                    updateBuyAccountTask.get();
                }
                catch (Exception e) {
                    throw new ExchangeException("撮合完成：更新买单资金账户异常" + e.getMessage());
                }
            }, this.service);
            ListenableFuture updateSellAccountTask = this.service.submit(() -> {
                boolean result = this.accountService.transferSellAmount(dealOrder.getSellUserId(), dealOrder.getBuyUserId(), dealOrder.getSellCoinId(), volume, BusinessType.TRADE_DEAL, turnoverOrder.getId());
                return result;
            });
            updateSellAccountTask.addListener(() -> {
                try {
                    updateSellAccountTask.get();
                }
                catch (Exception e) {
                    throw new ExchangeException("撮合完成：更新卖单资金账户异常" + e.getMessage());
                }
            }, this.service);
            return true;
        }
        catch (Exception e) {
            throw new ExchangeException("资金清算异常" + e.getMessage());
        }
    }

    public TurnoverOrder getTurnoverOrder(DealOrder dealOrder)
    {
        TurnoverOrder turnoverOrder = new TurnoverOrder();

        turnoverOrder.setMarketId( dealOrder.getMarketId() );
        turnoverOrder.setSymbol( dealOrder.getSymbol() );
        turnoverOrder.setMarketName( dealOrder.getMarketName() );
        turnoverOrder.setTradeType( dealOrder.getTradeType() );
        turnoverOrder.setSellUserId( dealOrder.getSellUserId() );
        turnoverOrder.setSellCoinId( dealOrder.getSellCoinId() );
        turnoverOrder.setSellOrderId( dealOrder.getSellOrderId() );
        turnoverOrder.setSellPrice( dealOrder.getSellPrice() );
        turnoverOrder.setSellFeeRate( dealOrder.getSellFeeRate() );
        turnoverOrder.setSellVolume( dealOrder.getSellVolume() );
        turnoverOrder.setBuyUserId( dealOrder.getBuyUserId() );
        turnoverOrder.setBuyCoinId( dealOrder.getBuyCoinId() );
        turnoverOrder.setBuyOrderId( dealOrder.getBuyOrderId() );
        turnoverOrder.setBuyVolume( dealOrder.getBuyVolume() );
        turnoverOrder.setBuyPrice( dealOrder.getBuyPrice() );
        turnoverOrder.setBuyFeeRate( dealOrder.getBuyFeeRate() );
        turnoverOrder.setAmount( dealOrder.getAmount() );
        turnoverOrder.setPrice( dealOrder.getPrice() );
        turnoverOrder.setCreated( dealOrder.getCreated() );
        return  turnoverOrder;
    }

    public boolean modifyEntrustOrder(Long orderId, BigDecimal dealVolume, OrderType orderType) {
        if (orderType == OrderType.BUY) {
            return this.bossMapper.modifyEntrustBuyOrder(orderId, dealVolume) > 0;
        }
        return this.bossMapper.modifyEntrustSellOrder(orderId, dealVolume) > 0;
    }
}

