package com.blockeng.boss.service;

import com.blockeng.boss.dto.DealOrder;
import com.blockeng.framework.exception.ExchangeException;

public interface BossService {
    boolean settlement(DealOrder dealOrder) throws ExchangeException;
}

