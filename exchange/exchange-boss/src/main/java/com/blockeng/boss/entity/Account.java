/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.blockeng.boss.entity.Account
 */
package com.blockeng.boss.entity;

import java.math.BigDecimal;

public class Account {
    private Long id;
    private Long userId;
    private Long coinId;
    private Integer status;
    private BigDecimal balanceAmount;
    private BigDecimal freezeAmount;

    public Long getId() {
        return this.id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public Long getCoinId() {
        return this.coinId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public BigDecimal getBalanceAmount() {
        return this.balanceAmount;
    }

    public BigDecimal getFreezeAmount() {
        return this.freezeAmount;
    }

    public Account setId(Long id) {
        this.id = id;
        return this;
    }

    public Account setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Account setCoinId(Long coinId) {
        this.coinId = coinId;
        return this;
    }

    public Account setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Account setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
        return this;
    }

    public Account setFreezeAmount(BigDecimal freezeAmount) {
        this.freezeAmount = freezeAmount;
        return this;
    }
}

