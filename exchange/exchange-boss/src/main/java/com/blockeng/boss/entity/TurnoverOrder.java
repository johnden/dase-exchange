/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.blockeng.boss.entity.TurnoverOrder
 */
package com.blockeng.boss.entity;

import java.math.BigDecimal;
import java.util.Date;

public class TurnoverOrder {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long marketId;
    private String symbol;
    private String marketName;
    private Integer tradeType;
    private Long sellUserId;
    private Long sellCoinId;
    private Long sellOrderId;
    private BigDecimal sellPrice;
    private BigDecimal sellFeeRate;
    private BigDecimal sellVolume;
    private Long buyUserId;
    private Long BuyCoinId;
    private Long buyOrderId;
    private BigDecimal buyVolume;
    private BigDecimal buyPrice;
    private BigDecimal buyFeeRate;
    private Long orderId;
    private BigDecimal amount;
    private BigDecimal price;
    private BigDecimal volume;
    private BigDecimal dealSellFee;
    private BigDecimal dealSellFeeRate;
    private BigDecimal dealBuyFee;
    private BigDecimal dealBuyFeeRate;
    private Integer status;
    private Date lastUpdateTime;
    private Date created;

    public Long getId() {
        return this.id;
    }

    public Long getMarketId() {
        return this.marketId;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public String getMarketName() {
        return this.marketName;
    }

    public Integer getTradeType() {
        return this.tradeType;
    }

    public Long getSellUserId() {
        return this.sellUserId;
    }

    public Long getSellCoinId() {
        return this.sellCoinId;
    }

    public Long getSellOrderId() {
        return this.sellOrderId;
    }

    public BigDecimal getSellPrice() {
        return this.sellPrice;
    }

    public BigDecimal getSellFeeRate() {
        return this.sellFeeRate;
    }

    public BigDecimal getSellVolume() {
        return this.sellVolume;
    }

    public Long getBuyUserId() {
        return this.buyUserId;
    }

    public Long getBuyCoinId() {
        return this.BuyCoinId;
    }

    public Long getBuyOrderId() {
        return this.buyOrderId;
    }

    public BigDecimal getBuyVolume() {
        return this.buyVolume;
    }

    public BigDecimal getBuyPrice() {
        return this.buyPrice;
    }

    public BigDecimal getBuyFeeRate() {
        return this.buyFeeRate;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public BigDecimal getVolume() {
        return this.volume;
    }

    public BigDecimal getDealSellFee() {
        return this.dealSellFee;
    }

    public BigDecimal getDealSellFeeRate() {
        return this.dealSellFeeRate;
    }

    public BigDecimal getDealBuyFee() {
        return this.dealBuyFee;
    }

    public BigDecimal getDealBuyFeeRate() {
        return this.dealBuyFeeRate;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Date getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public Date getCreated() {
        return this.created;
    }

    public TurnoverOrder setId(Long id) {
        this.id = id;
        return this;
    }

    public TurnoverOrder setMarketId(Long marketId) {
        this.marketId = marketId;
        return this;
    }

    public TurnoverOrder setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public TurnoverOrder setMarketName(String marketName) {
        this.marketName = marketName;
        return this;
    }

    public TurnoverOrder setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
        return this;
    }

    public TurnoverOrder setSellUserId(Long sellUserId) {
        this.sellUserId = sellUserId;
        return this;
    }

    public TurnoverOrder setSellCoinId(Long sellCoinId) {
        this.sellCoinId = sellCoinId;
        return this;
    }

    public TurnoverOrder setSellOrderId(Long sellOrderId) {
        this.sellOrderId = sellOrderId;
        return this;
    }

    public TurnoverOrder setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
        return this;
    }

    public TurnoverOrder setSellFeeRate(BigDecimal sellFeeRate) {
        this.sellFeeRate = sellFeeRate;
        return this;
    }

    public TurnoverOrder setSellVolume(BigDecimal sellVolume) {
        this.sellVolume = sellVolume;
        return this;
    }

    public TurnoverOrder setBuyUserId(Long buyUserId) {
        this.buyUserId = buyUserId;
        return this;
    }

    public TurnoverOrder setBuyCoinId(Long BuyCoinId) {
        this.BuyCoinId = BuyCoinId;
        return this;
    }

    public TurnoverOrder setBuyOrderId(Long buyOrderId) {
        this.buyOrderId = buyOrderId;
        return this;
    }

    public TurnoverOrder setBuyVolume(BigDecimal buyVolume) {
        this.buyVolume = buyVolume;
        return this;
    }

    public TurnoverOrder setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
        return this;
    }

    public TurnoverOrder setBuyFeeRate(BigDecimal buyFeeRate) {
        this.buyFeeRate = buyFeeRate;
        return this;
    }

    public TurnoverOrder setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public TurnoverOrder setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public TurnoverOrder setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public TurnoverOrder setVolume(BigDecimal volume) {
        this.volume = volume;
        return this;
    }

    public TurnoverOrder setDealSellFee(BigDecimal dealSellFee) {
        this.dealSellFee = dealSellFee;
        return this;
    }

    public TurnoverOrder setDealSellFeeRate(BigDecimal dealSellFeeRate) {
        this.dealSellFeeRate = dealSellFeeRate;
        return this;
    }

    public TurnoverOrder setDealBuyFee(BigDecimal dealBuyFee) {
        this.dealBuyFee = dealBuyFee;
        return this;
    }

    public TurnoverOrder setDealBuyFeeRate(BigDecimal dealBuyFeeRate) {
        this.dealBuyFeeRate = dealBuyFeeRate;
        return this;
    }

    public TurnoverOrder setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public TurnoverOrder setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
        return this;
    }

    public TurnoverOrder setCreated(Date created) {
        this.created = created;
        return this;
    }
}

