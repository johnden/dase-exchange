/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.blockeng.boss.entity.AccountDetail
 */
package com.blockeng.boss.entity;

import java.math.BigDecimal;
import java.util.Date;

public class AccountDetail {
    private Long id;
    private Long userId;
    private Long coinId;
    private Long accountId;
    private Long refAccountId;
    private Long orderId;
    private Integer direction;
    private String businessType;
    private BigDecimal amount;
    private BigDecimal fee;
    private String remark;
    private Date created;

    public AccountDetail(Long userId, Long coinId, Long accountId, Long refAccountId, Long orderId, Integer direction, String businessType, BigDecimal amount, BigDecimal fee, String remark) {
        this.userId = userId;
        this.coinId = coinId;
        this.accountId = accountId;
        this.refAccountId = refAccountId;
        this.orderId = orderId;
        this.direction = direction;
        this.businessType = businessType;
        this.amount = amount;
        this.fee = fee;
        this.remark = remark;
    }

    public Long getId() {
        return this.id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public Long getCoinId() {
        return this.coinId;
    }

    public Long getAccountId() {
        return this.accountId;
    }

    public Long getRefAccountId() {
        return this.refAccountId;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public Integer getDirection() {
        return this.direction;
    }

    public String getBusinessType() {
        return this.businessType;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public BigDecimal getFee() {
        return this.fee;
    }

    public String getRemark() {
        return this.remark;
    }

    public Date getCreated() {
        return this.created;
    }

    public AccountDetail setId(Long id) {
        this.id = id;
        return this;
    }

    public AccountDetail setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public AccountDetail setCoinId(Long coinId) {
        this.coinId = coinId;
        return this;
    }

    public AccountDetail setAccountId(Long accountId) {
        this.accountId = accountId;
        return this;
    }

    public AccountDetail setRefAccountId(Long refAccountId) {
        this.refAccountId = refAccountId;
        return this;
    }

    public AccountDetail setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public AccountDetail setDirection(Integer direction) {
        this.direction = direction;
        return this;
    }

    public AccountDetail setBusinessType(String businessType) {
        this.businessType = businessType;
        return this;
    }

    public AccountDetail setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public AccountDetail setFee(BigDecimal fee) {
        this.fee = fee;
        return this;
    }

    public AccountDetail setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public AccountDetail setCreated(Date created) {
        this.created = created;
        return this;
    }
}

