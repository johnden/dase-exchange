package com.blockeng.boss.handle;

import com.blockeng.boss.dto.DealOrder;
import com.blockeng.boss.handle.SettlementTask;
import com.blockeng.framework.utils.GsonUtil;
import com.mongodb.client.MongoCollection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class MatchHandle {
    private static final Logger log = LoggerFactory.getLogger(MatchHandle.class);
    @Autowired
    private SettlementTask settlementTask;
    @Autowired
    private MongoTemplate mongoTemplate;
    private ExecutorService executor = new ThreadPoolExecutor(500, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(1024), new ThreadPoolExecutor.CallerRunsPolicy());

    @Bean(value={"orderTxTaskContainerFactory"})
    public SimpleRabbitListenerContainerFactory pointTaskContainerFactory(SimpleRabbitListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setPrefetchCount(Integer.valueOf(64));
        factory.setConcurrentConsumers(Integer.valueOf(64));
        factory.setMaxConcurrentConsumers(Integer.valueOf(64));
        factory.setTaskExecutor(this.executor);
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    @RabbitListener(queues={"order.tx"}, containerFactory="orderTxTaskContainerFactory")
    public void receiveMessage(String tx) {
        DealOrder dealOrder = null;
        try {
            dealOrder = GsonUtil.convertObj(tx, DealOrder.class);
            this.settlementTask.settlement(dealOrder);
        }
        catch (Exception e) {
            log.error("资金清算异常" + e.getMessage());
            Document document = new Document();
            document.put("event_type", "order.tx");
            document.put("msg", e.getMessage());
            document.put("event_data", dealOrder);
            this.mongoTemplate.getCollection("events").insertOne(document);
        }
    }
}

