package com.blockeng.boss.enums;

public enum MessageChannel {
    TX_SUM("calc.account.tx.sum");
    
    private String channel;

    MessageChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}

