package com.blockeng.framework.utils;


import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OkHttpUtils {
    private static OkHttpUtils mOkhtttpUtils;

    private OkHttpClient mOkHttpClien;


    private OkHttpUtils() {
        mOkHttpClien = new OkHttpClient.Builder()
                .connectTimeout(5000, TimeUnit.MILLISECONDS)
                .readTimeout(5000, TimeUnit.MILLISECONDS)
                .writeTimeout(5000, TimeUnit.MILLISECONDS)
                .build();
    }

    public static OkHttpUtils getInstance() {
        if (mOkhtttpUtils == null) {
            synchronized (OkHttpUtils.class) {
                if (mOkhtttpUtils == null) {
                    return mOkhtttpUtils = new OkHttpUtils();
                }
            }
        }
        return mOkhtttpUtils;
    }

    public <T> T doGet(String url, Class<T> tClass) {
        Request request = new Request.Builder()
                .get()
                .url(url)
                .build();
        try {
            Response execute = mOkHttpClien.newCall(request).execute();
            return GsonUtil.convertObj(execute.body().string(), tClass);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T> T doPost(String url, Map<String, String> map, Class<T> tClass) {

        FormBody.Builder builder = new FormBody.Builder();
        for (String key : map.keySet()) {
            builder.add(key, map.get(key));
        }
        FormBody formBody = builder.build();
        Request request = new Request.Builder()
                .post(formBody)
                .url(url)
                .build();
        try {
            return GsonUtil.convertObj(mOkHttpClien.newCall(request).execute().body().string(), tClass);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
