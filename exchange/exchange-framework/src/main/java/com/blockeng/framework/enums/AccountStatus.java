package com.blockeng.framework.enums;

/**
 * @Description: 资金账户状态
 * @Author: Chen Long
 * @Date: Created in 2018/8/2 下午5:04
 * @Modified by: Chen Long
 */
public enum AccountStatus {

    ENABLE(1, "启用"),
    DISABLE(0, "禁用");

    /**
     * 代码
     */
    private int code;

    /**
     * 描述
     */
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    AccountStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
