package com.blockeng.dto;

import java.math.BigDecimal;

public class CreateKLineDTO {
    private String symbol;
    private BigDecimal price;
    private BigDecimal volume;

    public String getSymbol() {
        return this.symbol;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public BigDecimal getVolume() {
        return this.volume;
    }

    public CreateKLineDTO setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public CreateKLineDTO setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public CreateKLineDTO setVolume(BigDecimal volume) {
        this.volume = volume;
        return this;
    }
}

