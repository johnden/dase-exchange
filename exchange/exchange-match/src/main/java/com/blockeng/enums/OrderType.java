package com.blockeng.enums;

/*
 * Exception performing whole class analysis ignored.
 */
public enum OrderType {
    BUY(1, "买"),
    SELL(2, "卖");
    
    private int code;
    private String desc;

    private OrderType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return this.code;
    }

    public String getDesc() {
        return this.desc;
    }

    public static OrderType getByCode(int code) {
        for (OrderType orderType : OrderType.values()) {
            if (orderType.getCode() != code) continue;
            return orderType;
        }
        return null;
    }
}

