package com.blockeng.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="tx")
public class Tx
implements Serializable {
    private static final long serialVersionUID = -6388382264433675142L;
    private Long marketId;
    private String symbol;
    private String marketName;
    private int marketType;
    private int tradeType;
    private Long buyOrderId;
    private Long buyCoinId;
    private Long buyUserId;
    private BigDecimal buyPrice;
    private BigDecimal buyVolume;
    private BigDecimal buyFeeRate;
    private Long sellOrderId;
    private Long sellCoinId;
    private Long sellUserId;
    private BigDecimal sellPrice;
    private BigDecimal sellVolume;
    private BigDecimal sellFeeRate;
    private BigDecimal vol;
    private BigDecimal amount;
    private BigDecimal price;
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date created;

    public Long getMarketId() {
        return this.marketId;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public String getMarketName() {
        return this.marketName;
    }

    public int getMarketType() {
        return this.marketType;
    }

    public int getTradeType() {
        return this.tradeType;
    }

    public Long getBuyOrderId() {
        return this.buyOrderId;
    }

    public Long getBuyCoinId() {
        return this.buyCoinId;
    }

    public Long getBuyUserId() {
        return this.buyUserId;
    }

    public BigDecimal getBuyPrice() {
        return this.buyPrice;
    }

    public BigDecimal getBuyVolume() {
        return this.buyVolume;
    }

    public BigDecimal getBuyFeeRate() {
        return this.buyFeeRate;
    }

    public Long getSellOrderId() {
        return this.sellOrderId;
    }

    public Long getSellCoinId() {
        return this.sellCoinId;
    }

    public Long getSellUserId() {
        return this.sellUserId;
    }

    public BigDecimal getSellPrice() {
        return this.sellPrice;
    }

    public BigDecimal getSellVolume() {
        return this.sellVolume;
    }

    public BigDecimal getSellFeeRate() {
        return this.sellFeeRate;
    }

    public BigDecimal getVol() {
        return this.vol;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public Date getCreated() {
        return this.created;
    }

    public Tx setMarketId(Long marketId) {
        this.marketId = marketId;
        return this;
    }

    public Tx setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public Tx setMarketName(String marketName) {
        this.marketName = marketName;
        return this;
    }

    public Tx setMarketType(int marketType) {
        this.marketType = marketType;
        return this;
    }

    public Tx setTradeType(int tradeType) {
        this.tradeType = tradeType;
        return this;
    }

    public Tx setBuyOrderId(Long buyOrderId) {
        this.buyOrderId = buyOrderId;
        return this;
    }

    public Tx setBuyCoinId(Long buyCoinId) {
        this.buyCoinId = buyCoinId;
        return this;
    }

    public Tx setBuyUserId(Long buyUserId) {
        this.buyUserId = buyUserId;
        return this;
    }

    public Tx setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
        return this;
    }

    public Tx setBuyVolume(BigDecimal buyVolume) {
        this.buyVolume = buyVolume;
        return this;
    }

    public Tx setBuyFeeRate(BigDecimal buyFeeRate) {
        this.buyFeeRate = buyFeeRate;
        return this;
    }

    public Tx setSellOrderId(Long sellOrderId) {
        this.sellOrderId = sellOrderId;
        return this;
    }

    public Tx setSellCoinId(Long sellCoinId) {
        this.sellCoinId = sellCoinId;
        return this;
    }

    public Tx setSellUserId(Long sellUserId) {
        this.sellUserId = sellUserId;
        return this;
    }

    public Tx setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
        return this;
    }

    public Tx setSellVolume(BigDecimal sellVolume) {
        this.sellVolume = sellVolume;
        return this;
    }

    public Tx setSellFeeRate(BigDecimal sellFeeRate) {
        this.sellFeeRate = sellFeeRate;
        return this;
    }

    public Tx setVol(BigDecimal vol) {
        this.vol = vol;
        return this;
    }

    public Tx setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Tx setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Tx setCreated(Date created) {
        this.created = created;
        return this;
    }
}

