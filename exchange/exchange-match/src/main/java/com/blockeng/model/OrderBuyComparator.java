package com.blockeng.model;

import java.util.Comparator;

public class OrderBuyComparator
implements Comparator<Order> {
    @Override
    public int compare(Order order1, Order order2) {
        if (order1.getPrice().compareTo(order2.getPrice()) > 0) {
            return -1;
        }
        if (order1.getPrice().compareTo(order2.getPrice()) < 0) {
            return 1;
        }
        if (order1.getCreated().compareTo(order2.getCreated()) == 1) {
            return -1;
        }
        if(order1.getCreated().compareTo(order2.getCreated()) != 0)
        {
            return 1;
        }
        return 0;
    }
}

