package com.blockeng.model;

import java.util.Comparator;

public class OrderSellComparator
implements Comparator<Order> {
    @Override
    public int compare(Order o1, Order o2) {
        if (o1.getPrice().compareTo(o2.getPrice()) > 0) {
            return 1;
        }
        if (o1.getPrice().compareTo(o2.getPrice()) < 0) {
            return -1;
        }
        if (o1.getCreated().compareTo(o2.getCreated()) == 1) {
            return 1;
        }
        if (o1.getCreated().compareTo(o2.getCreated()) == 0) {
            return 0;
        }
        return -1;
    }
}

