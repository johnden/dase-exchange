package com.blockeng.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import org.jopenexchg.matcher.Market;
import org.jopenexchg.matcher.OrderType;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="entrust_order")
public class Order
implements Serializable {
    private static final long serialVersionUID = 9179887994734199109L;
    private Long id;
    @Field(value="user_id")
    private Long userId;
    @Field(value="market_id")
    private Long marketId;
    private String symbol;
    @Field(value="market_name")
    private String marketName;
    @Field(value="market_type")
    private int marketType;
    @Field(value="coin_id")
    private Long coinId;
    private BigDecimal price;
    @Field(value="merge_low_price")
    private BigDecimal mergeLowPrice;
    @Field(value="merge_high_price")
    private BigDecimal mergeHighPrice;
    @Field(value="fee_rate")
    private BigDecimal feeRate;
    private BigDecimal volume;
    private BigDecimal deal;
    private Integer type;
    private OrderType side;
    private Integer status;
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date created;
    private BigDecimal over;
    private boolean delflg = false;
    private Market market = null;

    public OrderType getSide() {
        return this.type == 1 ? OrderType.BUY : OrderType.SELL;
    }

    public BigDecimal getOver() {
        return this.volume.subtract(this.deal);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Order order = (Order)o;
        return Objects.equals(this.id, order.id);
    }

    public int hashCode() {
        return Objects.hash(this.id);
    }

    public Long getId() {
        return this.id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public Long getMarketId() {
        return this.marketId;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public String getMarketName() {
        return this.marketName;
    }

    public int getMarketType() {
        return this.marketType;
    }

    public Long getCoinId() {
        return this.coinId;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public BigDecimal getMergeLowPrice() {
        return this.mergeLowPrice;
    }

    public BigDecimal getMergeHighPrice() {
        return this.mergeHighPrice;
    }

    public BigDecimal getFeeRate() {
        return this.feeRate;
    }

    public BigDecimal getVolume() {
        return this.volume;
    }

    public BigDecimal getDeal() {
        return this.deal;
    }

    public Integer getType() {
        return this.type;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Date getCreated() {
        return this.created;
    }

    public boolean isDelflg() {
        return this.delflg;
    }

    public Market getMarket() {
        return this.market;
    }

    public Order setId(Long id) {
        this.id = id;
        return this;
    }

    public Order setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Order setMarketId(Long marketId) {
        this.marketId = marketId;
        return this;
    }

    public Order setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public Order setMarketName(String marketName) {
        this.marketName = marketName;
        return this;
    }

    public Order setMarketType(int marketType) {
        this.marketType = marketType;
        return this;
    }

    public Order setCoinId(Long coinId) {
        this.coinId = coinId;
        return this;
    }

    public Order setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Order setMergeLowPrice(BigDecimal mergeLowPrice) {
        this.mergeLowPrice = mergeLowPrice;
        return this;
    }

    public Order setMergeHighPrice(BigDecimal mergeHighPrice) {
        this.mergeHighPrice = mergeHighPrice;
        return this;
    }

    public Order setFeeRate(BigDecimal feeRate) {
        this.feeRate = feeRate;
        return this;
    }

    public Order setVolume(BigDecimal volume) {
        this.volume = volume;
        return this;
    }

    public Order setDeal(BigDecimal deal) {
        this.deal = deal;
        return this;
    }

    public Order setType(Integer type) {
        this.type = type;
        return this;
    }

    public Order setSide(OrderType side) {
        this.side = side;
        return this;
    }

    public Order setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Order setCreated(Date created) {
        this.created = created;
        return this;
    }

    public Order setOver(BigDecimal over) {
        this.over = over;
        return this;
    }

    public Order setDelflg(boolean delflg) {
        this.delflg = delflg;
        return this;
    }

    public Order setMarket(Market market) {
        this.market = market;
        return this;
    }

    public String toString() {
        return "Order(id=" + this.getId() + ", userId=" + this.getUserId() + ", marketId=" + this.getMarketId() + ", symbol=" + this.getSymbol() + ", marketName=" + this.getMarketName() + ", marketType=" + this.getMarketType() + ", coinId=" + this.getCoinId() + ", price=" + this.getPrice() + ", mergeLowPrice=" + this.getMergeLowPrice() + ", mergeHighPrice=" + this.getMergeHighPrice() + ", feeRate=" + this.getFeeRate() + ", volume=" + this.getVolume() + ", deal=" + this.getDeal() + ", type=" + this.getType() + ", side=" + this.getSide() + ", status=" + this.getStatus() + ", created=" + this.getCreated() + ", over=" + this.getOver() + ", delflg=" + this.isDelflg() + ", market=" + this.getMarket() + ")";
    }
}

