package com.blockeng.web;

import com.blockeng.core.handlers.OrderInHandler;
import com.blockeng.data.MarketData;
import com.blockeng.data.MatchData;

import java.util.HashMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class TrustOrderHandler {
    private static final Logger log = LoggerFactory.getLogger(TrustOrderHandler.class);

    @GetMapping
    public Mono<String> index() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("in", OrderInHandler.size.intValue());
        result.put("wait", MatchData.queue.size());
        Set<String> keys = MatchData.marketMap.keySet();
        for (String key : keys) {
            MarketData marketData = MatchData.marketMap.get(key);
            result.put(key, marketData.buyQueue.size() + "," + marketData.sellQueue.size());
        }
        return Mono.just(result.toString());
    }

    @GetMapping(value={"/queues"})
    public Mono<String> queues(@RequestParam(required=true) String symbol) {
        HashMap<String, Object[]> result = new HashMap<String, Object[]>();
        MarketData marketData = MatchData.marketMap.get(symbol);
        result.put("buy", marketData.buyQueue.toArray());
        result.put("sell", marketData.sellQueue.toArray());
        return Mono.just(result.toString());
    }
}

