package com.blockeng.core.handlers;

import com.blockeng.data.MatchData;
import com.blockeng.model.Order;
import com.google.gson.Gson;

import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class OrderInHandler {
    private static final Logger log = LoggerFactory.getLogger(OrderInHandler.class);
    public static AtomicInteger size = new AtomicInteger(0);

    @RabbitListener(queues={"order.in"}, concurrency="24")
    public void in(String payload) {
        size.getAndIncrement();
        Order orderVo = new Gson().fromJson(payload, Order.class);
        MatchData.queue.offer(orderVo);
    }
}

