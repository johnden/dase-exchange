package com.blockeng.core.task;

import com.blockeng.data.MarketData;
import com.blockeng.data.MatchData;
import com.blockeng.dto.CreateKLineDTO;
import com.blockeng.model.Tx;
import com.blockeng.util.GsonUtil;
import java.math.BigDecimal;
import java.util.Optional;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value=2)
public class MatchTask implements CommandLineRunner {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void run(String ... args) {
        do {
            com.blockeng.model.Order order;
            if ((order = MatchData.queue.poll()) == null) {
                continue;
            }
            this.match(order);
        } while (true);
    }

    public void match(com.blockeng.model.Order order) {
        MarketData marketData = MatchData.marketMap.get(order.getSymbol());
        if (!Optional.ofNullable(marketData).isPresent()) {
            marketData = new MarketData();
            MatchData.marketMap.put(order.getSymbol(), marketData);
        }
        BigDecimal volume = order.getVolume();
        if (order.getStatus() == 2) {
            com.blockeng.model.Order cancelOrder = null;
            boolean flag = order.getType() == 1 ? marketData.buyQueue.remove(order) : marketData.sellQueue.remove(order);
            if (flag) {
                cancelOrder = MatchData.orderMap.get(order.getId());
                if (cancelOrder == null) {
                    cancelOrder = order;
                } else {
                    MatchData.orderMap.remove(order.getId());
                }
            }
            if (cancelOrder != null) {
                this.rabbitTemplate.convertAndSend("order.cancel", GsonUtil.toJson(cancelOrder));
            }
            return;
        }
        if (order.getType() == 1) {
            if (marketData.sellQueue.size() == 0) {
                marketData.buyQueue.offer(order);
                return;
            }
            com.blockeng.model.Order sellOrder = marketData.sellQueue.poll();
            if (order.getPrice().compareTo(sellOrder.getPrice()) == -1) {
                marketData.buyQueue.offer(order);
                marketData.sellQueue.offer(sellOrder);
                return;
            }
            if (order.getVolume().compareTo(sellOrder.getVolume()) >= 0) {
                volume = sellOrder.getVolume();
            }
            CreateKLineDTO createKLine = new CreateKLineDTO().setSymbol(order.getSymbol()).setVolume(volume).setPrice(sellOrder.getPrice());
            this.rabbitTemplate.convertAndSend("marketdata.kline", GsonUtil.toJson(createKLine));
            Tx tx = MatchData.generateTx(order, sellOrder, 1, volume, sellOrder.getPrice());
            this.rabbitTemplate.convertAndSend("order.tx", GsonUtil.toJson(tx));
            if (order.getVolume().compareTo(sellOrder.getVolume()) == 1) {
                order.setVolume(order.getVolume().subtract(volume));
                MatchData.orderMap.put(order.getId(), order);
                MatchData.orderMap.remove(sellOrder.getId());
                this.match(order);
            } else if (order.getVolume().compareTo(sellOrder.getVolume()) == -1) {
                sellOrder.setVolume(sellOrder.getVolume().subtract(volume));
                marketData.sellQueue.remove(sellOrder);
                marketData.sellQueue.offer(sellOrder);
                MatchData.orderMap.put(sellOrder.getId(), sellOrder);
                MatchData.orderMap.remove(order.getId());
            }
        } else {
            if (marketData.buyQueue.size() == 0) {
                marketData.sellQueue.offer(order);
                return;
            }
            com.blockeng.model.Order buyOrder =marketData.buyQueue.poll();
            if (order.getPrice().compareTo(buyOrder.getPrice()) == 1) {
                marketData.sellQueue.offer(order);
                marketData.buyQueue.offer(buyOrder);
                return;
            }
            if (order.getVolume().compareTo(buyOrder.getVolume()) >= 0) {
                volume = buyOrder.getVolume();
            }
            CreateKLineDTO createKLine = new CreateKLineDTO().setSymbol(order.getSymbol()).setVolume(volume).setPrice(buyOrder.getPrice());
            this.rabbitTemplate.convertAndSend("marketdata.kline", GsonUtil.toJson(createKLine));
            Tx tx = MatchData.generateTx(buyOrder, order,2, volume, buyOrder.getPrice());
            this.rabbitTemplate.convertAndSend("order.tx", GsonUtil.toJson(tx));
            if (order.getVolume().compareTo(buyOrder.getVolume()) == 1) {
                order.setVolume(order.getVolume().subtract(volume));
                MatchData.orderMap.put(order.getId(), order);
                MatchData.orderMap.remove(buyOrder.getId());
                this.match(order);
            } else if (order.getVolume().compareTo(buyOrder.getVolume()) == -1) {
                buyOrder.setVolume(buyOrder.getVolume().subtract(volume));
                marketData.buyQueue.remove(buyOrder);
                marketData.buyQueue.offer(buyOrder);
                MatchData.orderMap.put(buyOrder.getId(), buyOrder);
                MatchData.orderMap.remove(order.getId());
            }
        }
    }
}

