package com.blockeng.repository;

import com.blockeng.entity.EntrustOrder;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository<T, ID extends Serializable>
extends CrudRepository<EntrustOrder, Long> {
}

