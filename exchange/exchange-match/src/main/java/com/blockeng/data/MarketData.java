package com.blockeng.data;

import com.blockeng.model.Order;
import com.blockeng.model.OrderBuyComparator;
import com.blockeng.model.OrderSellComparator;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MarketData {
    public PriorityQueue<Order> buyQueue = new PriorityQueue(new OrderBuyComparator());
    public PriorityQueue<Order> sellQueue = new PriorityQueue(new OrderSellComparator());
}

