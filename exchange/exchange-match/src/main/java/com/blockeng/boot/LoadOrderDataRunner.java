package com.blockeng.boot;

import com.blockeng.core.handlers.OrderInHandler;
import com.blockeng.data.MatchData;
import com.blockeng.entity.EntrustOrder;
import com.blockeng.repository.OrderRepository;
import com.mongodb.client.MongoCollection;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.IndexDefinition;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Component
@Order(value=1)
public class LoadOrderDataRunner
implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(LoadOrderDataRunner.class);
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private OrderRepository orderRepository;

    public  void run(String ... args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("开始加载未撮合订单...");
        MongoCollection mongoCollection = this.mongoTemplate.getCollection("entrust_order");
        mongoCollection.drop();
        String sql = "SELECT id, user_id, market_id, symbol, market_name, price, ( volume - deal ) AS volume, amount, fee_rate, fee, 0 AS deal, freeze, type, status, last_update_time, created FROM entrust_order WHERE `status` = 0 ORDER BY created ASC";
        this.mongoTemplate.indexOps(EntrustOrder.class).ensureIndex(new Index().on("symbol", Sort.Direction.ASC));
        this.mongoTemplate.indexOps(EntrustOrder.class).ensureIndex(new Index().on("price", Sort.Direction.ASC));
        this.mongoTemplate.indexOps(EntrustOrder.class).ensureIndex(new Index().on("user_id", Sort.Direction.ASC));
        this.mongoTemplate.indexOps(EntrustOrder.class).ensureIndex(new Index().on("volume", Sort.Direction.ASC));
        SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(sql);
        ArrayList<EntrustOrder> pool = new ArrayList<EntrustOrder>(200);
        while (sqlRowSet.next()) {
            EntrustOrder entrustOrder = new EntrustOrder();
            long id = sqlRowSet.getLong("id");
            long user_id = sqlRowSet.getLong("user_id");
            long market_id = sqlRowSet.getLong("market_id");
            String symbol = sqlRowSet.getString("symbol");
            String market_name = sqlRowSet.getString("market_name");
            BigDecimal price = sqlRowSet.getBigDecimal("price");
            BigDecimal volume = sqlRowSet.getBigDecimal("volume");
            BigDecimal amount = sqlRowSet.getBigDecimal("amount");
            BigDecimal fee_rate = sqlRowSet.getBigDecimal("fee_rate");
            BigDecimal fee = sqlRowSet.getBigDecimal("fee");
            BigDecimal deal = sqlRowSet.getBigDecimal("deal");
            BigDecimal freeze = sqlRowSet.getBigDecimal("freeze");
            int type = sqlRowSet.getInt("type");
            int status = sqlRowSet.getInt("status");
            Date lastUpdateTime = sqlRowSet.getDate("last_update_time");
            Date created = sqlRowSet.getDate("created");
            entrustOrder.setId(Long.valueOf(id));
            entrustOrder.setUserId(Long.valueOf(user_id));
            entrustOrder.setMarketId(Long.valueOf(market_id));
            entrustOrder.setSymbol(symbol);
            entrustOrder.setMarketName(market_name);
            entrustOrder.setPrice(price);
            entrustOrder.setVolume(volume);
            entrustOrder.setAmount(amount);
            entrustOrder.setFeeRate(fee_rate);
            entrustOrder.setFee(fee);
            entrustOrder.setDeal(deal);
            entrustOrder.setFreeze(freeze);
            entrustOrder.setType(Integer.valueOf(type));
            entrustOrder.setStatus(Integer.valueOf(status));
            entrustOrder.setLastUpdateTime(lastUpdateTime);
            entrustOrder.setCreated(created);
            pool.add(entrustOrder);
            if (pool.size() >= 200 || sqlRowSet.isLast()) {
                this.orderRepository.saveAll(pool);
                pool.clear();
                log.info("loading " + OrderInHandler.size.get());
            }
            OrderInHandler.size.getAndIncrement();
            com.blockeng.model.Order order = getOrder(entrustOrder);
            MatchData.queue.offer(order);
        }
        stopWatch.stop();
        log.info("=====> Load time：{}", stopWatch.getTotalTimeMillis());
    }

    public  com.blockeng.model.Order getOrder(EntrustOrder entrustOrder)
    {
        com.blockeng.model.Order order = new com.blockeng.model.Order();
        order.setId(entrustOrder.getId());
        order.setUserId(entrustOrder.getUserId());
        order.setMarketId(entrustOrder.getMarketId());
        order.setSymbol(entrustOrder.getSymbol());
        order.setMarketName(entrustOrder.getMarketName());
        order.setPrice(entrustOrder.getPrice());
        order.setFeeRate(entrustOrder.getFeeRate());
        order.setVolume(entrustOrder.getVolume());
        order.setDeal(entrustOrder.getDeal());
        order.setType(entrustOrder.getType());
        order.setStatus(entrustOrder.getStatus());
        order.setCreated(entrustOrder.getCreated());
        return  order;
    }
}

