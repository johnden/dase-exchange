package com.blockeng.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class GsonUtil {
    static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").disableHtmlEscaping().create();

    public static <T> List<T> convertList(String json, TypeToken<List<T>> token) {
        if (StringUtils.isEmpty(json)) {
            return new ArrayList();
        }
        return (List)gson.fromJson(json, token.getType());
    }

    public static <T> T convertObj(String json, Class<T> cls) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        return (T)gson.fromJson(json, cls);
    }

    public static String toJson(Object obj) {
        if (obj == null) {
            return "";
        }
        return gson.toJson(obj);
    }

    public static String getJsonObjectAsString(JsonObject jsonObject, String name) {
        if (jsonObject == null || StringUtils.isEmpty(name)) {
            return null;
        }
        JsonElement jsonElement = jsonObject.get(name);
        return jsonElement == null ? null : jsonElement.getAsString();
    }

    public static JsonObject getJsonObjectChild(JsonObject jsonObject, String name) {
        if (jsonObject == null || StringUtils.isEmpty(name)) {
            return null;
        }
        JsonElement jsonElement = jsonObject.get(name);
        return jsonElement == null ? null : jsonElement.getAsJsonObject();
    }

    public static boolean getJsonObjectAsBoolean(JsonObject jsonObject, String name) {
        if (jsonObject == null || StringUtils.isEmpty(name)) {
            return false;
        }
        JsonElement jsonElement = jsonObject.get(name);
        return jsonElement == null ? false : jsonElement.getAsBoolean();
    }
}

