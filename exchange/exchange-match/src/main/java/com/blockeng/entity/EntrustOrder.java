package com.blockeng.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="entrust_order")
public class EntrustOrder {
    @Id
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    @Field(value="user_id")
    @TextIndexed
    private Long userId;
    @Field(value="market_id")
    private Long marketId;
    @Field(value="symbol")
    @TextIndexed
    private String symbol;
    @Field(value="market_name")
    private String marketName;
    @TextIndexed
    private BigDecimal price;
    @TextIndexed
    private BigDecimal volume;
    private BigDecimal amount;
    @Field(value="fee_rate")
    private BigDecimal feeRate;
    private BigDecimal fee;
    private BigDecimal deal;
    private BigDecimal freeze;
    private Integer type;
    @TextIndexed
    private Integer status;
    @Field(value="last_update_time")
    private Date lastUpdateTime;
    @TextIndexed
    private Date created;

    public Long getId() {
        return this.id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public Long getMarketId() {
        return this.marketId;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public String getMarketName() {
        return this.marketName;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public BigDecimal getVolume() {
        return this.volume;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public BigDecimal getFeeRate() {
        return this.feeRate;
    }

    public BigDecimal getFee() {
        return this.fee;
    }

    public BigDecimal getDeal() {
        return this.deal;
    }

    public BigDecimal getFreeze() {
        return this.freeze;
    }

    public Integer getType() {
        return this.type;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Date getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public Date getCreated() {
        return this.created;
    }

    public EntrustOrder setId(Long id) {
        this.id = id;
        return this;
    }

    public EntrustOrder setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public EntrustOrder setMarketId(Long marketId) {
        this.marketId = marketId;
        return this;
    }

    public EntrustOrder setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public EntrustOrder setMarketName(String marketName) {
        this.marketName = marketName;
        return this;
    }

    public EntrustOrder setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public EntrustOrder setVolume(BigDecimal volume) {
        this.volume = volume;
        return this;
    }

    public EntrustOrder setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public EntrustOrder setFeeRate(BigDecimal feeRate) {
        this.feeRate = feeRate;
        return this;
    }

    public EntrustOrder setFee(BigDecimal fee) {
        this.fee = fee;
        return this;
    }

    public EntrustOrder setDeal(BigDecimal deal) {
        this.deal = deal;
        return this;
    }

    public EntrustOrder setFreeze(BigDecimal freeze) {
        this.freeze = freeze;
        return this;
    }

    public EntrustOrder setType(Integer type) {
        this.type = type;
        return this;
    }

    public EntrustOrder setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public EntrustOrder setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
        return this;
    }

    public EntrustOrder setCreated(Date created) {
        this.created = created;
        return this;
    }
}

