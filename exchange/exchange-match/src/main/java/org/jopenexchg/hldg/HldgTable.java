
package org.jopenexchg.hldg;

import java.util.*;

import org.jopenexchg.pool.*;

public final class HldgTable
{
	static final int DEF_HLDG_TABLE_SIZE = 1000;
	
	private AllocOnlyPool<Hldg> hldgPool = null;
	private HashMap<HldgKey, Hldg> hldgTbl = null;
	
	public HldgTable(int maxSize) 
		throws InstantiationException, IllegalAccessException
	{
		if(maxSize <= 0)
		{
			maxSize = DEF_HLDG_TABLE_SIZE;
		}
	
		hldgPool = new AllocOnlyPool<Hldg>(Hldg.class, maxSize);
		hldgTbl = new HashMap<HldgKey, Hldg>(maxSize);
	}

	public final Hldg findHldg(HldgKey key)
	{
		if(key == null)
		{
			return null;
		}
		
		return hldgTbl.get(key);
	}
	
	public final void putHldg(Hldg hldg)
	{
		if(hldg != null && hldg.key != null)
		{
			hldgTbl.put(hldg.key, hldg);
		}
	}

	public final Hldg getHldg(HldgKey key)
	{
		if(key == null)
		{
			return null;
		}
		
		Hldg hldg = hldgTbl.get(key);
		if(hldg == null)
		{
			hldg = hldgPool.getObj();
			if(hldg == null)
			{
				return null;
			}
			hldg.key = key;
			return hldg;
		}
		else
		{
			return hldg;
		}
	}
	
}
