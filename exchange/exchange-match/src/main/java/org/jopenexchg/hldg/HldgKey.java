
package org.jopenexchg.hldg;

public final class HldgKey
{
	/**
	 *  FIVE ELEMENTS IN ALL
	 */
	public byte accType;
	public int accNo;
	
	public int stockid;
	
	public short pbu;
	public short hldgType;

	public final int hashCode()
	{
		return accType + accNo + stockid + pbu + hldgType;
	}

	public final boolean equals(HldgKey obj)
	{
		if(obj == null)
		{
			return false;
		}
		
		if (this == obj)
		{
			return true;
		}
		
		if(this.accType != obj.accType)
		{
			return false;
		}
		
		if(this.accNo != obj.accNo)
		{
			return false;
		}
		
		if(this.stockid != obj.stockid)
		{
			return false;
		}
		
		if(this.pbu != obj.pbu)
		{
			return false;
		}
		
		if(this.hldgType != obj.hldgType)
		{
			return false;
		}
		
		return true;
	}
	
}