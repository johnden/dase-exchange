
package org.jopenexchg.pool;

import java.lang.reflect.Array;
import java.util.LinkedList;


public final class RecyclablePool<T extends WithId> 
{
	private static int MIN_POOL_SIZE = 100;
	
	// The following 2 fields will be valid when init() once
	private T slotArray[] = null;
	private boolean useflagArray[] = null;
	private LinkedList<T> freeList = null;
	private long poolSize = 0;
	
	@SuppressWarnings("unchecked")
	public RecyclablePool(Class<T> elemType, int size)
		throws InstantiationException, IllegalAccessException
	{
		if(size <= MIN_POOL_SIZE)
		{
			size = MIN_POOL_SIZE;
		}

		slotArray = (T[])(Array.newInstance(elemType, size));
		useflagArray = new boolean[size];
		freeList = new LinkedList<T>();
	
		for(int i = 0; i < size; i++)
		{
			T item = elemType.newInstance();
			item.setId(i);			
			
			slotArray[i] = item;
			useflagArray[i] = false;
			freeList.add(item);				
		} 
		
		poolSize = size;
	}

	public final int capacity()
	{
		return slotArray.length;
	}

	public final T getObj()
	{
		if(freeList.size() > 0)
		{
			T obj = freeList.poll();
			if(obj != null)
			{
				int id = obj.getId();
				// mark as used
				useflagArray[id] = true;
			}
			return obj;
		}
		else
		{
			return null;
		}
	}	

	public final void putObj(T obj)
	{
		if(obj != null)
		{
			int id = obj.getId();
			
			if((0 <= id) && (id < poolSize))
			{
				if( useflagArray[id] == true)
				{
					freeList.add(obj);
					useflagArray[id] = false;
				}
			}
		}

	}

	public final T findUsedObj(int id)
	{
		if((0 <= id) && (id < useflagArray.length))
		{
			if( useflagArray[id] == true)
			{
				return slotArray[id];
			}			
		}		

		return null;
	}

	public final int size()
	{
		return slotArray.length - freeList.size();

	}

	public final void finalize()
	{
		slotArray = null;
		useflagArray = null;
		freeList = null;
	}
}


