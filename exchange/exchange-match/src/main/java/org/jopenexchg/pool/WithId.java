
package org.jopenexchg.pool;

public interface WithId
{
	int getId();
	
	void setId(int id);
}
