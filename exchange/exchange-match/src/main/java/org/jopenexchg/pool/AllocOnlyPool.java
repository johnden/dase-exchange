
package org.jopenexchg.pool;

import java.lang.reflect.Array;

public final class AllocOnlyPool<T> 
{
	private static int MIN_POOL_SIZE = 100;
	
	// The following 2 fields will be valid when init() once
	private T slotArray[] = null;
	private int usedCnt = 0;
	private int poolSize = 0;

	public AllocOnlyPool(Class<T> elemType, int size)
		throws InstantiationException, IllegalAccessException
	{
		if(size <= MIN_POOL_SIZE)
		{
			size = MIN_POOL_SIZE;
		}

		slotArray = (T[])(Array.newInstance(elemType, size));
	
		for(int i = 0; i < size; i++)
		{
			T item = elemType.newInstance();
			slotArray[i] = item;
		} 
		
		poolSize = size;
	}

	public final int capacity()
	{
		return poolSize;
	}
	public final T getObj()
	{
		if(usedCnt >= poolSize)
		{
			return null;
		}
		else
		{
			usedCnt = usedCnt + 1;
			return slotArray[usedCnt];
		}
	}	

	public final int size()
	{
		return usedCnt;

	}

	public final void finalize()
	{
		slotArray = null;
	}
}
