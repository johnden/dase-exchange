
package org.jopenexchg.matcher;

import java.util.*;

public final class TradedInst
{
	static final int UL_PRICE = Integer.MAX_VALUE;
	static final int LL_PRICE = 0;
	static final int NO_PRICE = 0;

	public int stockid = -1;

	public byte stockname[] = null;

	public TreeMap<Long, PriceLeader> buyPrcList = null;

	public TreeMap<Long, PriceLeader> sellPrcList = null;

	public long prevClsPrc = NO_PRICE;
	public long openPrc = NO_PRICE;
	public long highPrc = LL_PRICE;
	public long lowPrc = UL_PRICE;

	public long totalValue = 0;
	public long totalAmount = 0;
	
	public TradedInst(int stockId, String stockName)
	{
		this.stockid = stockId;
		this.stockname = stockName.getBytes();
		
		buyPrcList = new TreeMap<Long, PriceLeader>();
		sellPrcList = new TreeMap<Long, PriceLeader>();
	}

	public final TreeMap<Long, PriceLeader> getPrcList(boolean isBuy)
	{
		if(isBuy)
		{
			return buyPrcList;
		}
		else
		{
			return sellPrcList;
		}
	}

	public final TreeMap<Long, PriceLeader> getPeerPrcTree(boolean iAmBuy)
	{
		if(!iAmBuy)
		{
			return buyPrcList;
		}
		else
		{
			return sellPrcList;
		}
	}	

	public final void addtoPrcList(boolean isBuy, PriceLeader prcLdr)
	{
		if(isBuy)
		{
			buyPrcList.put(prcLdr.prior, prcLdr);
		}
		else
		{
			sellPrcList.put(prcLdr.prior, prcLdr);
		}
	}
	

	public final Map.Entry<Long, PriceLeader> getBestPeerPrcLdr(boolean iAmBuy)
	{
		Map.Entry<Long, PriceLeader> bestPrcLdr = null;
		TreeMap<Long, PriceLeader> prcList = getPeerPrcTree(iAmBuy);
		
		bestPrcLdr = prcList.firstEntry();
		
		return bestPrcLdr;
	}
	
}
