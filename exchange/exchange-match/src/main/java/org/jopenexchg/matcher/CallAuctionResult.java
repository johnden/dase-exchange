
package org.jopenexchg.matcher;

public final class CallAuctionResult 
{
	public long ordPrc = 0;
	public long price = 0;
	public long volume = 0;
}
