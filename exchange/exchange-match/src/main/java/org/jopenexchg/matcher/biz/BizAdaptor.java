
package org.jopenexchg.matcher.biz;

import org.jopenexchg.matcher.*;


public interface BizAdaptor
{
	public long calcPrior(Order order);


	public long calcMaxPrior(boolean isbuy, long price);

	public long ordPrc2Price(long ordPrc);

	public long price2OrdPrc(long price);
	
}
