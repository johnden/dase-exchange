
package org.jopenexchg.matcher.biz.impl;

import org.jopenexchg.matcher.Order;
import org.jopenexchg.matcher.biz.BizAdaptor;

public final class BondBizAdaptorImpl implements BizAdaptor
{
	final static long MAX_PRIOR_ADJUST = 0;
	final static long PRIOR_SHIFT = 0;
	
	private final long calcBasePrior(boolean isbuy, long price)
	{
		long basePrior = 0;
		
		if(isbuy)
		{
			basePrior = -price;
		}
		else
		{
			basePrior = price;
		}
		
		return basePrior;		
	}
	
	@Override
	public final long calcPrior(Order order)
	{
		return calcBasePrior(order.isbuy, order.price);
	}

	@Override
	public final long calcMaxPrior(boolean isbuy, long price)
	{
		return calcBasePrior(isbuy, price);
	}

	@Override
	public final long ordPrc2Price(long ordPrc)
	{
		return 10000 - ordPrc;
	}

	@Override
	public final long price2OrdPrc(long price) {
		// TODO Auto-generated method stub
		return 10000 - price;
	}

}
