
package org.jopenexchg.matcher;

import java.util.*;

public final class TradedInstList
{
	static final int INIT_CAPACITY = 2000;

	private HashMap<Integer, TradedInst> list = new HashMap<Integer, TradedInst>(INIT_CAPACITY);

	public final void addStock(TradedInst stock)
	{
		if(stock != null)
		{
			list.put(stock.stockid, stock);
		}
	}
	
	public final TradedInst getStock(int stockid)
	{
		return list.get(stockid);
	}
	
}
