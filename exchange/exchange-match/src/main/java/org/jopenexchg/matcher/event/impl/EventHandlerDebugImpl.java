
package org.jopenexchg.matcher.event.impl;

import org.jopenexchg.matcher.Order;
import org.jopenexchg.matcher.event.EventHandler;

public final class EventHandlerDebugImpl implements EventHandler
{

	@Override
	public void enterOrderBook(final Order order)
	{
		System.out.println("");
		System.out.println("[enterOrderBook]");
		System.out.println(" + " + order);
	}

	@Override
	public void leaveOrderBook(final Order order)
	{
		System.out.println("");
		System.out.println("[leaveOrderBook]");
		System.out.println(" - " + order);
	}

	@Override
	public void match(final Order newOrder, final Order oldOrder, long matchQty, long matchPrice)
	{
		System.out.println("");
		System.out.println("[match]: matchPrice = " + matchPrice + " matchQty = " + matchQty);
		System.out.println("     new " + newOrder);
		System.out.println("     old " + oldOrder);
	}

	@Override
	public void noMoreMatch(final Order order)
	{
		System.out.println("");
		System.out.println("[noMoreMatch]");
		System.out.println(" + " + order);
	}

	
	@Override
	public void incomingOrder(final Order order)
	{
		System.out.println("------------------------");
		System.out.println("[incomingOrder]");
		System.out.println(" + " + order);		
	}

	
	@Override
	public void callAuctionMatch(final Order buyOrder, final Order sellOrder, long matchQty,
			long matchPrice)
	{
		System.out.println("");
		System.out.println("[ocall match]: matchPrice = " + matchPrice + " matchQty = " + matchQty);
		System.out.println("     buy " + buyOrder);
		System.out.println("     sell " + sellOrder);
		
	}

	
	@Override
	public void noMoreCallAuction(Order order)
	{
		// TODO Auto-generated method stub
		
	}

}
