
package org.jopenexchg.matcher;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

public final class Market {
    static final int UL_PRICE = Integer.MAX_VALUE;
    static final int LL_PRICE = 0;
    static final BigDecimal NO_PRICE = BigDecimal.ZERO;
    public String symbol;
    public byte[] marketName = null;
    public TreeMap<BigDecimal, PriceLeader> bids = null;
    public TreeMap<BigDecimal, PriceLeader> asks = null;
    public BigDecimal prevClsPrc = NO_PRICE;
    public BigDecimal openPrc = NO_PRICE;
    public long highPrc = 0L;
    public long lowPrc = Integer.MAX_VALUE;
    public long totalValue = 0L;
    public long totalAmount = 0L;

    public Market(String symbol) {
        this.symbol = symbol;
        this.bids = new TreeMap();
        this.asks = new TreeMap();
    }

    public final TreeMap<BigDecimal, PriceLeader> getPrcList(OrderType type) {
        if (type.equals(OrderType.BUY)) {
            return this.bids;
        }
        return this.asks;
    }

    public final TreeMap<BigDecimal, PriceLeader> getPeerPrcTree(OrderType type) {
        if (!type.equals(OrderType.BUY)) {
            return this.bids;
        }
        return this.asks;
    }

    public final void addtoPrcList(OrderType type, PriceLeader prcLdr) {
        if (type.equals(OrderType.BUY)) {
            this.bids.put(prcLdr.score, prcLdr);
        } else {
            this.asks.put(prcLdr.score, prcLdr);
        }
    }

    public final Map.Entry<BigDecimal, PriceLeader> getBestPeerPrcLdr(OrderType type) {
        Map.Entry<BigDecimal, PriceLeader> bestPrcLdr = null;
        TreeMap prcList = this.getPeerPrcTree(type);
        bestPrcLdr = prcList.firstEntry();
        return bestPrcLdr;
    }
}

