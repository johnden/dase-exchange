
package org.jopenexchg.matcher;

import java.math.BigDecimal;
import java.util.*;

import org.jopenexchg.pool.WithId;


public final class PriceLeader implements WithId
{
	private int id = 0;

	public long prior = 0;

	public long ordPrc = 0;
	public long price = 0;
	public long accumQty = 0;
	public BigDecimal score = BigDecimal.ZERO;
	public BigDecimal priceAmount = BigDecimal.ZERO;
	public BigDecimal over = BigDecimal.ZERO;

	public LinkedList<Order> orderList = new LinkedList<Order>();

	@Override
	public final int getId()
	{
		return id;
	}

	@Override
	public final void setId(int id)
	{
		this.id = id;
	}

}
