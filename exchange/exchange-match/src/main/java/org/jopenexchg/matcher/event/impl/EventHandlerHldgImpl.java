
package org.jopenexchg.matcher.event.impl;

import org.jopenexchg.hldg.*;
import org.jopenexchg.matcher.Order;
import org.jopenexchg.matcher.event.EventHandler;

public final class EventHandlerHldgImpl implements EventHandler
{
	private HldgTable hldgTbl = null;
	private HldgKey key = null;

	public EventHandlerHldgImpl(HldgTable hldgTbl)
	{
		this.hldgTbl = hldgTbl;
		this.key = new HldgKey();
	}
	
	private final Hldg getRelatedHldg(Order order)
	{
		key.accNo = order.accNo;
		key.accType = order.acctType;
		key.hldgType = 0;
		key.pbu = order.pbu;
		key.stockid = order.stockid;
		
		return hldgTbl.getHldg(key);		
	}
	
	@Override
	public final void enterOrderBook(Order order)
	{
	}

	@Override
	public final void leaveOrderBook(Order order)
	{
	}
	@Override
	public final void match(Order newOrder, Order oldOrder, long matchQty,
			long matchPrice)
	{
		long value;
		
		if(oldOrder.hldg == null)
		{
			oldOrder.hldg = getRelatedHldg(oldOrder);
		}
		
		if(newOrder.hldg == null)
		{
			newOrder.hldg = getRelatedHldg(newOrder);
		}		
		
		if(oldOrder.isbuy)
		{
			oldOrder.hldg.A += matchQty;
			newOrder.hldg.A -= matchQty;
		}
		else
		{
			oldOrder.hldg.A -= matchQty;
			newOrder.hldg.A += matchQty;			
		}
		
		value = matchPrice * matchQty;
		
	}

	@Override
	public final void noMoreMatch(Order order)
	{
	}

	
	@Override
	public final void incomingOrder(Order order)
	{
	}

	@Override
	public final void callAuctionMatch(Order buyOrder, Order sellOrder, long matchQty,
			long matchPrice)
	{
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public final void noMoreCallAuction(Order order)
	{
		// TODO Auto-generated method stub
		
	}

}
