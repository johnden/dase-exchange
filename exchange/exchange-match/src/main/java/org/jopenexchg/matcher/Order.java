
package org.jopenexchg.matcher;

import org.jopenexchg.hldg.*;

public final class Order
{
	public short pbu = 0;
	public int reff = 0;

	public byte acctType = 'A';
	public int accNo = 0;
	public boolean isbuy = true;
	public int stockid = 0;
	public long ordQty = 0;
	public long ordPrc = 0;
	public long price = 0;
	public long remQty = 0;
	public boolean delflg = false;

	public TradedInst stock = null;
	public Hldg hldg = null;

	public String toString()
	{
		StringBuffer temp = new StringBuffer(256);

		temp.append("isBuy = ");
		temp.append(isbuy);

		temp.append("; stockid = ");
		temp.append(stockid);

		temp.append("; ordPrice = ");
		temp.append(ordPrc);

		temp.append("; ordQty = ");
		temp.append(ordQty);

		temp.append("; remQty = ");
		temp.append(remQty);

		return temp.toString();
	}

}
