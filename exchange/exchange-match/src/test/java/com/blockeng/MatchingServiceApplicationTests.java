package com.blockeng;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @author hugo
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MatchingServiceApplicationTests {

}
