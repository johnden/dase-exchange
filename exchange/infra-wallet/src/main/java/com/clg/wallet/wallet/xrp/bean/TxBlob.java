package com.clg.wallet.wallet.xrp.bean;

public class TxBlob
{
    private String engineResult;
    private Integer engineResultCode;
    private String status;
    private String hash;
    private boolean isSuccess;

    public TxBlob setEngineResult(String engineResult)
    {
        this.engineResult = engineResult;return this;
    }

    public TxBlob setEngineResultCode(Integer engineResultCode)
    {
        this.engineResultCode = engineResultCode;return this;
    }

    public TxBlob setStatus(String status)
    {
        this.status = status;return this;
    }

    public TxBlob setHash(String hash)
    {
        this.hash = hash;return this;
    }

    public TxBlob setSuccess(boolean isSuccess)
    {
        this.isSuccess = isSuccess;return this;
    }


    public String getEngineResult()
    {
        return this.engineResult;
    }

    public Integer getEngineResultCode()
    {
        return this.engineResultCode;
    }

    public String getStatus()
    {
        return this.status;
    }

    public String getHash()
    {
        return this.hash;
    }

    public boolean isSuccess()
    {
        return this.isSuccess;
    }
}
