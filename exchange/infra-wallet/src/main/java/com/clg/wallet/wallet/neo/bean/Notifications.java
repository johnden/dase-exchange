package com.clg.wallet.wallet.neo.bean;

public class Notifications
{
    private String contract;
    private State state;

    public Notifications setContract(String contract)
    {
        this.contract = contract;return this;
    }

    public Notifications setState(State state)
    {
        this.state = state;return this;
    }



    public String getContract()
    {
        return this.contract;
    }

    public State getState()
    {
        return this.state;
    }
}
