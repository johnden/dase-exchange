package com.clg.wallet.utils;

import java.math.*;
import org.bitcoinj.core.*;

public class BigDecimalUtils
{
    public static BigDecimal formatBigDecimal(BigDecimal decimal)
    {
        if (null == decimal) {
            return BigDecimal.ZERO;
        }
        return decimal.setScale(4, 1);
    }

    public static BigDecimal formatBigDecimal(Double data)
    {
        return new BigDecimal(data.doubleValue()).setScale(4, 1);
    }

    public static BigDecimal formatBigDecimal(BigDecimal decimal, int sacle)
    {
        if (null == decimal) {
            return BigDecimal.ZERO;
        }
        return decimal.setScale(sacle, 1);
    }

    public static BigDecimal formatBigDecimal(Double data, int sacle)
    {
        return new BigDecimal(data.doubleValue()).setScale(sacle, 1);
    }

    public static BigInteger byteArrayToBig(String byteArray)
    {
        String balanceInfo = StringParser.bytesToHexString(Utils.reverseBytes(StringParser.hexStrToByteArray(byteArray)));
        BigInteger balanceBig = new BigInteger(balanceInfo, 16);
        return balanceBig;
    }

    public static BigDecimal realBalance(String balanceStr, int unint)
    {
        if (IntegerUtils.isInt(balanceStr))
        {
            BigDecimal balance = new BigDecimal(balanceStr);
            balance = formatBigDecimal(balance.divide(BigDecimal.TEN.pow(unint)));
            return balance;
        }
        return BigDecimal.ZERO;
    }

    public static BigDecimal chainBalance(String balanceStr, int unint)
    {
        BigDecimal balance = new BigDecimal(balanceStr);
        balance = formatBigDecimal(balance.multiply(BigDecimal.TEN.pow(unint)));
        return balance;
    }

    public static boolean isInteger(BigDecimal bigDecimal)
    {
        if ((null != bigDecimal) && (new BigDecimal(bigDecimal.intValue()).compareTo(bigDecimal) == 0)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args)
    {
        System.out.println(byteArrayToBig("001cd8b751020000"));
    }
}
