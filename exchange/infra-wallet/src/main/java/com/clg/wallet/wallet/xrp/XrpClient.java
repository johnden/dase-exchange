package com.clg.wallet.wallet.xrp;

import com.clg.wallet.bean.*;
import com.clg.wallet.help.*;
import java.math.*;
import com.alibaba.fastjson.*;
import com.clg.wallet.utils.*;
import java.util.*;
import com.clg.wallet.wallet.xrp.bean.*;
import org.springframework.util.*;
import org.slf4j.*;

public class XrpClient
{
    private ClientBean mClientBean;
    private static final Logger LOG = LoggerFactory.getLogger(XrpClient.class);
    private static JsonRpcClient client = JsonRpcClient.getInstance();
    private BigDecimal minMoney = BigDecimal.valueOf(20L);

    public XrpClient(ClientBean clientBean)
    {
        this.mClientBean = clientBean;
    }

    public AccountInfo getAccountInfo(String account)
    {
        if (!checkResultStatus()) {
            return null;
        }
        JSONObject obj = new JSONObject();
        obj.put("account", account);
        obj.put("ledger", "validated");
        JSONArray dataDTO = new JSONArray();
        dataDTO.add(obj);
        AccountInfo accountInfo = new AccountInfo();

        JSONObject json = client.invoke(this.mClientBean, "account_info", dataDTO, JSONObject.class);
        if (checkResultStatus())
        {
            String balanceStr = json.getJSONObject("account_data").getString("Balance");
            BigDecimal balance = BigDecimal.ZERO;
            if (IntegerUtils.isInt(balanceStr)) {
                balance = BigDecimalUtils.formatBigDecimal(new BigDecimal(balanceStr).divide(BigDecimal.TEN.pow(6)));
            }
            accountInfo.setLedgerHash(json.getString("ledger_hash")).setLedgerIndex(json.getLong("ledger_index")).setStatus(json.getString("status")).setValidated(json.getBoolean("validated").booleanValue()).setAccount(json.getJSONObject("account_data").getString("WcgAccount")).setBalance(balance).setSequence(json.getJSONObject("account_data").getLong("Sequence"));
        }
        return accountInfo;
    }

    public BigDecimal getBalance(String account)
    {
        AccountInfo accountInfo = getAccountInfo(account);
        if (null != accountInfo) {
            return accountInfo.getBalance();
        }
        return BigDecimal.ZERO;
    }

    public ResultXrpTx getAccountTx(String account, Long start, Long end)
    {
        if (!checkResultStatus())
        {
            LOG.error("服务器正在更新信息");
            return null;
        }
        ArrayList<Tx> txList = new ArrayList();
        JSONArray dataDTO = new JSONArray();
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("account", account);
        jsonObj.put("ledger_index_min", start);
        jsonObj.put("ledger_index_max", end);
        jsonObj.put("binary", Boolean.valueOf(false));
        jsonObj.put("count", Boolean.valueOf(false));
        jsonObj.put("limit", Integer.valueOf(200));
        jsonObj.put("forward", Boolean.valueOf(false));
        dataDTO.add(jsonObj);
        JSONObject json = (JSONObject)client.invoke(this.mClientBean, "account_tx", dataDTO, JSONObject.class);
        long maxBlockNumber = json.getLongValue("ledger_index_max");
        if (maxBlockNumber <= start.longValue())
        {
            LOG.error("没有发现新的区块");
            return null;
        }
        JSONArray transactions = json.getJSONArray("transactions");
        ResultXrpTx resultTx = new ResultXrpTx();
        if ((null != transactions) && (transactions.size() > 0))
        {
            LOG.info("发现新的交易数据");
            for (int i = 0; i < transactions.size(); i++)
            {
                JSONObject jsonObject = transactions.getJSONObject(i);
                String hash = jsonObject.getJSONObject("tx").getString("hash");
                Tx tx = getTransaction(hash);
                if ((null != tx) && (tx.isSuccess())) {
                    txList.add(tx);
                }
            }
        }
        else
        {
            LOG.error("未找到新的充值订单");
        }
        Collections.reverse(txList);
        resultTx.setTxList(txList);
        resultTx.setBlockNumber(Long.valueOf(json.getLongValue("ledger_index_max")));
        return resultTx;
    }

    public ResultXrpTx getAccountTx(String account)
    {
        return getAccountTx(account, Long.valueOf(-1L), Long.valueOf(-1L));
    }

    private Tx getTransaction(String hash)
    {
        if (!checkResultStatus()) {
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("transaction", hash);
        jsonObject.put("binary", Boolean.valueOf(false));
        JSONArray jsonData = new JSONArray();
        jsonData.add(jsonObject);
        JSONObject data = client.invoke(this.mClientBean, "tx", jsonData, JSONObject.class);
        return getTx(data);
    }

    private Tx getTx(JSONObject data)
    {
        JSONArray memos = data.getJSONArray("Memos");
        String memo = "";
        Tx tx = new Tx();
        if (null != memos) {
            memo = memos.getJSONObject(0).getJSONObject("Memo").getString("MemoData");
        }
        String balanceStr = data.getString("Amount");
        BigDecimal balance = BigDecimal.ZERO;
        if (IntegerUtils.isInt(balanceStr)) {
            balance = BigDecimalUtils.formatBigDecimal(new BigDecimal(balanceStr).divide(BigDecimal.TEN.pow(6)));
        }
        JSONObject meta = data.getJSONObject("meta");
        String transactionResult = "";
        if (null != meta) {
            transactionResult = meta.getString("TransactionResult");
        }
        tx.setFromAddress(data.getString("WcgAccount")).setToAddress(data.getString("Destination")).setLastLedgerSequence(data.getLong("LastLedgerSequence")).setHash(data.getString("hash")).setMemo(memo).setBalance(balance).setStatus(data.getString("status")).setValidated(data.getBoolean("validated").booleanValue()).setTransactionType(data.getString("TransactionType"));
        tx.setSuccess(("success".equalsIgnoreCase(tx.getStatus())) && (tx.isValidated()) && ("tesSUCCESS".equalsIgnoreCase(transactionResult)));
        return tx;
    }

    public ServerInfo getServerInfo()
    {
        ServerInfo serverState = new ServerInfo();
        JSONObject json = client.invoke(this.mClientBean, "server_info", new String[0], JSONObject.class);
        JSONObject info = json.getJSONObject("info");
        String status = info.getString("server_state");
        String complete_ledgers = info.getString("complete_ledgers");
        Long seq = info.getJSONObject("validated_ledger").getLong("seq");
        serverState.setComplete_ledgers(complete_ledgers).setSeq(seq).setStatus("full".equalsIgnoreCase(status));
        return serverState;
    }

    public JSONObject txBlob(String txBlob)
    {
        if (!checkResultStatus()) {
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tx_blob", txBlob);
        JSONArray jsonData = new JSONArray();
        jsonData.add(jsonObject);
        JSONObject json = client.invoke(this.mClientBean, "submit", jsonData, JSONObject.class);
        return json;
    }

    private boolean checkResultStatus()
    {
        JSONObject json = client.invoke(this.mClientBean, "server_info", new String[0], JSONObject.class);
        if (null != json)
        {
            String status = json.getString("status");
            String serverState = json.getJSONObject("info").getString("server_state");
            String complete = json.getJSONObject("info").getString("complete_ledgers");
            LOG.info("serverState:" + serverState + "  complete:" + complete);
            if (("success".equalsIgnoreCase(status)) && ("full".equalsIgnoreCase(serverState))) {
                return true;
            }
        }
        throw new RuntimeException("服务器同步未完成,请稍候");
    }

    public TxBlob sendMoney(String priKey, String account, String to, BigDecimal toBalance, String memo)
    {
        if (!checkResultStatus()) {
            return null;
        }
        toBalance = BigDecimalUtils.formatBigDecimal(toBalance, 6);
        BigDecimal formBalance = getAccountInfo(account).getBalance();
        if (formBalance.subtract(this.minMoney).compareTo(toBalance) < 0) {
            return null;
        }
        JSONObject json = new JSONObject();
        JSONObject txJson = new JSONObject();
        txJson.put("Account", account);
        txJson.put("TransactionType", "Payment");
        txJson.put("Destination", to);
        txJson.put("Amount", String.valueOf(toBalance.multiply(BigDecimal.TEN.pow(6)).toBigInteger()));
        if (!StringUtils.isEmpty(memo)) {
            txJson.put("DestinationTag", memo);
        }
        json.put("tx_json", txJson);
        json.put("offline", Boolean.valueOf(false));
        json.put("secret", priKey);
        json.put("fee_mult_max", Integer.valueOf(1000));
        json.put("secret", priKey);

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(json);
        JSONObject data = client.invoke(this.mClientBean, "sign", jsonArray, JSONObject.class);
        String txBolb = data.getString("tx_blob");

        JSONObject jsonObject = txBlob(txBolb);
        String engineResult = jsonObject.getString("engine_result");
        Integer engineResultCode = Integer.valueOf(jsonObject.getIntValue("engine_result_code"));
        String status = jsonObject.getString("status");
        String hash = jsonObject.getJSONObject("tx_json").getString("hash");
        TxBlob txBlob = new TxBlob().setEngineResult(engineResult).setEngineResultCode(engineResultCode).setStatus(status).setHash(hash);
        txBlob.setSuccess(txIsOk(engineResult, status, engineResultCode));
        return txBlob;
    }

    public boolean txIsOk(String engineResult, String status, Integer engineResultCode)
    {
        if (("tesSUCCESS".equalsIgnoreCase(engineResult)) &&
                (0 == engineResultCode.intValue()) &&
                ("success".equalsIgnoreCase(status))) {
            return true;
        }
        return false;
    }

    private void getInfo()
    {
        JSONObject json = client.invoke(this.mClientBean, "dcrwallet", new String[] { "getinfo" }, JSONObject.class);
        System.out.println(json);
    }

    public static void main(String[] args)
    {
        ClientBean clientBean = new ClientBean();
        clientBean.setRpcIp("47.90.127.176");
        clientBean.setRpcPort("8009");
        XrpClient xprClient = new XrpClient(clientBean);
        xprClient.getInfo();
    }
}
