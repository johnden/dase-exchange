package com.clg.wallet.wallet.Omni.bean;

import java.math.*;

public class OmniTxItem
{
    private String account;
    private String address;
    private String category;
    private BigDecimal amount;
    private BigDecimal fee;
    private int confirmations;
    private String blockhash;
    private int blockindex;
    private int blocktime;
    private String txid;
    private int time;
    private int timereceived;

    public OmniTxItem setAccount(String account)
    {
        this.account = account;return this;
    }

    public OmniTxItem setAddress(String address)
    {
        this.address = address;return this;
    }

    public OmniTxItem setCategory(String category)
    {
        this.category = category;return this;
    }

    public OmniTxItem setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public OmniTxItem setFee(BigDecimal fee)
    {
        this.fee = fee;return this;
    }

    public OmniTxItem setConfirmations(int confirmations)
    {
        this.confirmations = confirmations;return this;
    }

    public OmniTxItem setBlockhash(String blockhash)
    {
        this.blockhash = blockhash;return this;
    }

    public OmniTxItem setBlockindex(int blockindex)
    {
        this.blockindex = blockindex;return this;
    }

    public OmniTxItem setBlocktime(int blocktime)
    {
        this.blocktime = blocktime;return this;
    }

    public OmniTxItem setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public OmniTxItem setTime(int time)
    {
        this.time = time;return this;
    }

    public OmniTxItem setTimereceived(int timereceived)
    {
        this.timereceived = timereceived;return this;
    }


    public String getAccount()
    {
        return this.account;
    }

    public String getAddress()
    {
        return this.address;
    }

    public String getCategory()
    {
        return this.category;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public BigDecimal getFee()
    {
        return this.fee;
    }

    public int getConfirmations()
    {
        return this.confirmations;
    }

    public String getBlockhash()
    {
        return this.blockhash;
    }

    public int getBlockindex()
    {
        return this.blockindex;
    }

    public int getBlocktime()
    {
        return this.blocktime;
    }

    public String getTxid()
    {
        return this.txid;
    }

    public int getTime()
    {
        return this.time;
    }

    public int getTimereceived()
    {
        return this.timereceived;
    }
}
