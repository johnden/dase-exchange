package com.clg.wallet.wallet.neo.bean;

import java.math.*;

public class TxVout
{
    private Integer n;
    private String asset;
    private BigDecimal value;
    private String address;

    public TxVout setN(Integer n)
    {
        this.n = n;return this;
    }

    public TxVout setAsset(String asset)
    {
        this.asset = asset;return this;
    }

    public TxVout setValue(BigDecimal value)
    {
        this.value = value;return this;
    }

    public TxVout setAddress(String address)
    {
        this.address = address;return this;
    }


    public Integer getN()
    {
        return this.n;
    }

    public String getAsset()
    {
        return this.asset;
    }

    public BigDecimal getValue()
    {
        return this.value;
    }

    public String getAddress()
    {
        return this.address;
    }
}
