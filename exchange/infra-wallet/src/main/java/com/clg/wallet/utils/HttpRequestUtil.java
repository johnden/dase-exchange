package com.clg.wallet.utils;

import org.apache.http.entity.*;
import org.apache.http.util.*;
import java.io.*;
import org.apache.http.impl.client.*;
import org.apache.http.client.config.*;
import org.springframework.util.*;
import org.apache.http.client.methods.*;
import com.clg.wallet.exception.*;
import com.google.gson.*;
import org.apache.http.*;
import org.apache.http.message.*;
import org.apache.http.client.entity.*;
import java.util.*;
import org.slf4j.*;

public class HttpRequestUtil
{
    private static final Logger LOG = LoggerFactory.getLogger(HttpRequestUtil.class);

    public static <T> T postJson(final String path, final String jsonParam, final Class<T> tClass) {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final HttpPost post = new HttpPost(path);
        try {
            if (null != jsonParam) {
                final StringEntity entity = new StringEntity(jsonParam, "utf-8");
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json");
                post.setEntity((HttpEntity)entity);
            }
            final CloseableHttpResponse response = httpClient.execute((HttpUriRequest)post);
            final String strResult = EntityUtils.toString(response.getEntity());
            HttpRequestUtil.LOG.info("Request POST " + path + " Param: " + jsonParam);
            HttpRequestUtil.LOG.info("Request POST " + path + " Response: " + strResult);
            final Gson gson = new Gson();
            return (T)gson.fromJson(strResult, (Class)tClass);
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T postRow(final String path, final String param, final Class<T> tClass) {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final HttpPost post = new HttpPost(path);
        final RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(20000).setConnectTimeout(20000).build();
        post.setConfig(requestConfig);
        try {
            if (StringUtils.isEmpty((Object)param)) {
                final StringEntity entity = new StringEntity(param, "UTF-8");
                entity.setContentEncoding("UTF-8");
                post.setEntity((HttpEntity)entity);
            }
            final CloseableHttpResponse response = httpClient.execute((HttpUriRequest)post);
            final String strResult = EntityUtils.toString(response.getEntity());
            HttpRequestUtil.LOG.info("Request POST " + path + " Param: " + param);
            HttpRequestUtil.LOG.info("Request POST " + path + " Response: " + strResult);
            return GsonUtils.convertObj(strResult, tClass);
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T getJson(final String path, final Class<T> tClass) {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final HttpGet httpget = new HttpGet(path);
        try {
            final CloseableHttpResponse response = httpClient.execute((HttpUriRequest)httpget);
            final String strResult = EntityUtils.toString(response.getEntity());
            try {
                HttpRequestUtil.LOG.info("Request GET " + path + " Response: " + strResult);
                final Gson gson = new Gson();
                return (T)gson.fromJson(strResult, (Class)tClass);
            }
            catch (JsonSyntaxException e2) {
                throw new HttpClientException("10000000", "\u8c03\u7528\u63a5\u53e3\u5931\u8d25\uff01");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getJson(final String path) {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final HttpGet httpget = new HttpGet(path);
        try {
            final CloseableHttpResponse response = httpClient.execute((HttpUriRequest)httpget);
            final String strResult = EntityUtils.toString(response.getEntity());
            return strResult;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String post(String path, Map<String, String> param)
            throws HttpClientException
    {
        try
        {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost post = new HttpPost(path);

            post.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

            List<NameValuePair> postParam = new ArrayList();
            if (param != null) {
                for (Map.Entry<String, String> entry : param.entrySet()) {
                    postParam.add(new BasicNameValuePair((String)entry.getKey(), (String)entry.getValue()));
                }
            }
            post.setEntity(new UrlEncodedFormEntity(postParam, "UTF-8"));
            CloseableHttpResponse response = httpClient.execute(post);
            String strResult = EntityUtils.toString(response.getEntity());
            LOG.info("接口响应结果{}", strResult);
            return strResult;
        }
        catch (Exception e)
        {
            LOG.error("http请求异常，异常堆栈{}", e);
            throw new HttpClientException("HttpUtils.post请求异常，异常堆栈信息{}" + e.getMessage());
        }
    }
}
