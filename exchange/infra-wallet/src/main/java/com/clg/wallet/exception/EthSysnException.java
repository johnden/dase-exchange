package com.clg.wallet.exception;

public class EthSysnException extends RuntimeException
{
    public EthSysnException() {}

    public EthSysnException(String msg)
    {
        super(msg);
    }
}
