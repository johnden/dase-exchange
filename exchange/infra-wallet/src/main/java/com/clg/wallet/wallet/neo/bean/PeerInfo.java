package com.clg.wallet.wallet.neo.bean;

public class PeerInfo
{
    private String address;
    private Long port;

    public PeerInfo setAddress(String address)
    {
        this.address = address;return this;
    }

    public PeerInfo setPort(Long port)
    {
        this.port = port;return this;
    }



    public String getAddress()
    {
        return this.address;
    }

    public Long getPort()
    {
        return this.port;
    }
}
