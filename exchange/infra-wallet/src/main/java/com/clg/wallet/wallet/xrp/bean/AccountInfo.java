package com.clg.wallet.wallet.xrp.bean;

import java.math.*;

public class AccountInfo
{
    private boolean validated;
    private String status;
    private Long ledgerIndex;
    private String ledgerHash;
    private Long sequence;
    private BigDecimal balance;
    private String account;

    public AccountInfo setValidated(boolean validated)
    {
        this.validated = validated;return this;
    }

    public AccountInfo setStatus(String status)
    {
        this.status = status;return this;
    }

    public AccountInfo setLedgerIndex(Long ledgerIndex)
    {
        this.ledgerIndex = ledgerIndex;return this;
    }

    public AccountInfo setLedgerHash(String ledgerHash)
    {
        this.ledgerHash = ledgerHash;return this;
    }

    public AccountInfo setSequence(Long sequence)
    {
        this.sequence = sequence;return this;
    }

    public AccountInfo setBalance(BigDecimal balance)
    {
        this.balance = balance;return this;
    }

    public AccountInfo setAccount(String account)
    {
        this.account = account;return this;
    }
    

    public boolean isValidated()
    {
        return this.validated;
    }

    public String getStatus()
    {
        return this.status;
    }

    public Long getLedgerIndex()
    {
        return this.ledgerIndex;
    }

    public String getLedgerHash()
    {
        return this.ledgerHash;
    }

    public Long getSequence()
    {
        return this.sequence;
    }

    public BigDecimal getBalance()
    {
        return this.balance;
    }

    public String getAccount()
    {
        return this.account;
    }
}
