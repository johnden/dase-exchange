package com.clg.wallet.wallet.bitshare;

import java.util.*;
import java.math.*;

public interface BitShares
{
    boolean is_locked()
            throws BitSharesException;

    boolean unlock(String paramString)
            throws BitSharesException;

    TransferResult transfer(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6, String paramString7)
            throws BitSharesException;

    List<Transaction> get_account_history(String paramString, int paramInt)
            throws BitSharesException;

    AccountInfo get_account(String paramString)
            throws BitSharesException;

    DynamicGlobalProperties get_dynamic_global_properties()
            throws BitSharesException;

    List<AccountBalances> list_account_balances(String paramString)
            throws BitSharesException;

    interface TransferResult
    {
        Long ref_block_num();
    }

    interface Transaction
    {
        String id();

        String memo();

        String description();

        String from();

        String to();

        BigInteger amount();

        String asset_id();

        Long block_num();

        Long virtual_op();
    }

    interface AccountInfo
    {
        String id();

        String name();
    }

    interface DynamicGlobalProperties
    {
        Long head_block_number();

        Long last_irreversible_block_num();
    }

    interface AccountBalances
    {
        String asset_id();

        BigInteger amount();
    }
}
