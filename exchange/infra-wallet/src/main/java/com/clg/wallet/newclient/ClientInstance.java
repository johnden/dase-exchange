package com.clg.wallet.newclient;

import java.util.*;
import com.clg.wallet.bean.*;
import org.web3j.protocol.http.*;
import org.web3j.protocol.core.*;
import org.web3j.protocol.*;
import com.googlecode.jsonrpc4j.*;
import org.springframework.util.*;
import com.clg.wallet.exception.*;
import java.net.*;
import com.clg.wallet.wallet.Omni.*;
import com.clg.wallet.utils.*;
import com.clg.wallet.wallet.bitcoin.*;
import org.slf4j.*;

public class ClientInstance
{
    private static final Logger LOG = LoggerFactory.getLogger(ClientInstance.class);
    private static HashMap<String, Object> clientMap = new HashMap();

    private static <T> T getClient(String rpcIp, String rpcPort)
    {
        return (T)clientMap.get(rpcIp + rpcPort);
    }

    private static void setClient(String rpcIp, String rpcPort, Object obj)
    {
        clientMap.put(rpcIp + rpcPort, obj);
    }

    public static Web3j getEthClient(ClientBean coin)
    {
        if (null != coin)
        {
            String rpcIP = coin.getRpcIp();
            String rpcPort = coin.getRpcPort();
            Web3j web3j = getClient(rpcIP, rpcPort);
            if (web3j != null) {
                return web3j;
            }
            StringBuffer rpcUrl = new StringBuffer("http://");
            rpcUrl.append(rpcIP).append(":").append(rpcPort).append("/");
            web3j = new JsonRpc2_0Web3j(new HttpService(rpcUrl.toString()));
            setClient(rpcIP, rpcPort, web3j);
            return web3j;
        }
        LOG.info("eth钱包初始化参数不能为空");
        throw new IllegalArgumentException("RPC no setting");
    }

    public static JsonRpcHttpClient getJsonRpcClient(ClientBean coin)
    {
        try
        {
            String rpcIP = coin.getRpcIp();
            String rpcPort = coin.getRpcPort();
            JsonRpcHttpClient client = getClient(rpcIP, rpcPort);
            if (null != client) {
                return client;
            }
            if ((!StringUtils.isEmpty(coin.getRpcUser())) && (!StringUtils.isEmpty(coin.getRpcPwd()))) {
                Authenticator.setDefault(new Authenticator()
                {
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(coin.getRpcUser(), coin.getRpcPwd().toCharArray());
                    }
                });
            }
            StringBuffer urlValue = new StringBuffer(200);
            urlValue.append("http://");
            urlValue.append(rpcIP);
            urlValue.append(":");
            urlValue.append(rpcPort);
            if (!StringUtils.isEmpty(coin.getContext())) {
                urlValue.append("/").append(coin.getContext());
            }
            URL url = new URL(urlValue.toString());
            JsonRpcHttpClient jsonRpcHttpClient = new JsonRpcHttpClient(url);
            setClient(coin.getRpcIp(), coin.getRpcPort(), jsonRpcHttpClient);
            return jsonRpcHttpClient;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            throw new AchainException(e.getMessage());
        }
    }

    public static JSONRPCClient getNewOmni(ClientBean coin)
            throws Exception
    {
        if (null == coin) {
            ClientInstance.LOG.info("btc钱包初始化参数不能为空");
            throw new IllegalArgumentException("RPC no setting");
        }
        final JSONRPCClient bitcoin = getClient(coin.getRpcIp(), coin.getRpcPort());
        if (null != bitcoin) {
            return bitcoin;
        }
        if (!TaskStringUtils.isAllEmpty(coin.getRpcIp(), coin.getRpcPort())) {
            if (!TaskStringUtils.isAllEmpty(coin.getRpcUser(), coin.getRpcPwd())) {
                final String rpcUrl = "http://" + coin.getRpcUser() + ":" + coin.getRpcPwd() + "@" + coin.getRpcIp() + ":" + coin.getRpcPort();
                try {
                    return new JSONRPCClient(new URL(rpcUrl));
                }
                catch (MalformedURLException var4) {
                    throw new Exception(var4);
                }
            }
            final String rpcUrl = "http://" + coin.getRpcIp() + ":" + coin.getRpcPort();
            try {
                return new JSONRPCClient(new URL(rpcUrl));
            }
            catch (MalformedURLException var4) {
                throw new Exception(var4);
            }
        }
        LOG.info("钱包初始化参数错误" + coin.getName());
        throw new IllegalArgumentException("RPC no setting");
    }

    public static Bitcoin getBitCoinClient(ClientBean coin)
            throws Exception
    {
        if (null == coin) {
            LOG.info("btc钱包初始化参数不能为空");
            throw new IllegalArgumentException("RPC no setting");
        }
        final Bitcoin bitcoin = getClient(coin.getRpcIp(), coin.getRpcPort());
        if (null != bitcoin) {
            return bitcoin;
        }
        if (TaskStringUtils.isAllEmpty(coin.getRpcUser(), coin.getRpcPwd(), coin.getRpcIp(), coin.getRpcPort())) {
            LOG.info("钱包初始化参数错误" + coin.getName());
            throw new IllegalArgumentException("RPC no setting");
        }
        final String rpcUrl = "http://" + coin.getRpcUser() + ":" + coin.getRpcPwd() + "@" + coin.getRpcIp() + ":" + coin.getRpcPort();
        BitcoinJSONRPCClient client;
        try {
            client = new BitcoinJSONRPCClient(new URL(rpcUrl));
        }
        catch (MalformedURLException var4) {
            throw new Exception(var4);
        }
        Bitcoin.Info info;
        try {
            info = client.getNetworkinfo();
        }
        catch (Exception e) {
            info = client.getInfo();
        }
        if (null != info && StringUtils.isEmpty(info.errors())) {
            LOG.info("钱包初始化结束" + coin.getName());
            setClient(coin.getRpcIp(), coin.getRpcPort(), client);
            return client;
        }
        LOG.info("钱包初始化异常" + coin.getName());
        throw new NullPointerException("Get CoinClient Error");
    }

    public static OmniCoreJSONRPCClient getUsdtClient(ClientBean coin)
            throws Exception
    {

        if (null == coin) {
            LOG.info("usdt钱包初始化参数不能为空");
            throw new IllegalArgumentException("RPC no setting");
        }
        final OmniCoreJSONRPCClient bitcoin = getClient(coin.getRpcIp(), coin.getRpcPort());
        if (null != bitcoin) {
            return bitcoin;
        }
        if (TaskStringUtils.isAllEmpty(coin.getRpcUser(), coin.getRpcPwd(), coin.getRpcIp(), coin.getRpcPort())) {
            LOG.info("钱包初始化参数错误" + coin.getName());
            throw new IllegalArgumentException("RPC no setting");
        }
        final String rpcUrl = "http://" + coin.getRpcUser() + ":" + coin.getRpcPwd() + "@" + coin.getRpcIp() + ":" + coin.getRpcPort();
        OmniCoreJSONRPCClient client;
        try {
            client = new OmniCoreJSONRPCClient(new URL(rpcUrl));
        }
        catch (MalformedURLException var4) {
            throw new Exception(var4);
        }
        final OmniCore.OmniInfo info = client.omni_getinfo();
        if (null != info) {
            LOG.info("钱包初始化结束" + coin.getName());
            setClient(coin.getRpcIp(), coin.getRpcPort(), client);
            return client;
        }
        LOG.info("钱包初始化异常" + coin.getName());
        throw new NullPointerException("Get CoinClient Error");
    }
}
