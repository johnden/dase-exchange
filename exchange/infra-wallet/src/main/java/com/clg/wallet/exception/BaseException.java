package com.clg.wallet.exception;

public class BaseException extends RuntimeException
{
    private static final long serialVersionUID = -5664737418659908401L;
    private String errorCode;
    private String errorDesc;

    public BaseException() {}

    public BaseException(String message)
    {
        super(message);
        this.errorDesc = message;
    }

    public BaseException(Throwable cause)
    {
        super(cause);
    }

    public BaseException(String message, Throwable cause)
    {
        super(message, cause);
        this.errorDesc = message;
    }

    public BaseException(String errorCode, String message)
    {
        super(message);
        this.errorCode = errorCode;
        this.errorDesc = message;
    }

    public BaseException(String errorCode, String message, Throwable cause)
    {
        super(message, cause);
        this.errorDesc = message;
        this.errorCode = errorCode;
    }

    public String getErrorCode()
    {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public String getErrorDesc()
    {
        return this.errorDesc;
    }

    public void setErrorDesc(String errorDesc)
    {
        this.errorDesc = errorDesc;
    }
}
