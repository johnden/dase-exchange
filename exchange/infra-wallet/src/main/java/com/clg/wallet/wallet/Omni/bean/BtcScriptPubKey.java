package com.clg.wallet.wallet.Omni.bean;

import java.util.*;

public class BtcScriptPubKey
{
    private String asm;
    private String hex;
    private int reqSigs;
    private String type;
    private List<String> addresses;

    public BtcScriptPubKey setAsm(String asm)
    {
        this.asm = asm;return this;
    }

    public BtcScriptPubKey setHex(String hex)
    {
        this.hex = hex;return this;
    }

    public BtcScriptPubKey setReqSigs(int reqSigs)
    {
        this.reqSigs = reqSigs;return this;
    }

    public BtcScriptPubKey setType(String type)
    {
        this.type = type;return this;
    }

    public BtcScriptPubKey setAddresses(List<String> addresses)
    {
        this.addresses = addresses;return this;
    }



    public String getAsm()
    {
        return this.asm;
    }

    public String getHex()
    {
        return this.hex;
    }

    public int getReqSigs()
    {
        return this.reqSigs;
    }

    public String getType()
    {
        return this.type;
    }

    public List<String> getAddresses()
    {
        return this.addresses;
    }
}
