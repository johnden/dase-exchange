package com.clg.wallet.bean;

import java.math.*;

public class ClientBean
{
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String coinType;
    private BigDecimal creditLimit;
    private String rpcIp;
    private String rpcPort;
    private String rpcUser;
    private String rpcPwd;
    private String lastBlock;
    private String walletUser;
    private String contractAddress;
    private Integer minConfirm;
    private String walletPass;
    private String context;
    private String task;
    private Integer status;

    public ClientBean setId(Long id)
    {
        this.id = id;return this;
    }

    public ClientBean setName(String name)
    {
        this.name = name;return this;
    }

    public ClientBean setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }

    public ClientBean setCreditLimit(BigDecimal creditLimit)
    {
        this.creditLimit = creditLimit;return this;
    }

    public ClientBean setRpcIp(String rpcIp)
    {
        this.rpcIp = rpcIp;return this;
    }

    public ClientBean setRpcPort(String rpcPort)
    {
        this.rpcPort = rpcPort;return this;
    }

    public ClientBean setRpcUser(String rpcUser)
    {
        this.rpcUser = rpcUser;return this;
    }

    public ClientBean setRpcPwd(String rpcPwd)
    {
        this.rpcPwd = rpcPwd;return this;
    }

    public ClientBean setLastBlock(String lastBlock)
    {
        this.lastBlock = lastBlock;return this;
    }

    public ClientBean setWalletUser(String walletUser)
    {
        this.walletUser = walletUser;return this;
    }

    public ClientBean setContractAddress(String contractAddress)
    {
        this.contractAddress = contractAddress;return this;
    }

    public ClientBean setMinConfirm(Integer minConfirm)
    {
        this.minConfirm = minConfirm;return this;
    }

    public ClientBean setWalletPass(String walletPass)
    {
        this.walletPass = walletPass;return this;
    }

    public ClientBean setContext(String context)
    {
        this.context = context;return this;
    }

    public ClientBean setTask(String task)
    {
        this.task = task;return this;
    }

    public ClientBean setStatus(Integer status)
    {
        this.status = status;return this;
    }


    public Long getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    public BigDecimal getCreditLimit()
    {
        return this.creditLimit;
    }

    public String getRpcIp()
    {
        return this.rpcIp;
    }

    public String getRpcPort()
    {
        return this.rpcPort;
    }

    public String getRpcUser()
    {
        return this.rpcUser;
    }

    public String getRpcPwd()
    {
        return this.rpcPwd;
    }

    public String getLastBlock()
    {
        return this.lastBlock;
    }

    public String getWalletUser()
    {
        return this.walletUser;
    }

    public String getContractAddress()
    {
        return this.contractAddress;
    }

    public Integer getMinConfirm()
    {
        return this.minConfirm;
    }

    public String getWalletPass()
    {
        return this.walletPass;
    }

    public String getContext()
    {
        return this.context;
    }

    public String getTask()
    {
        return this.task;
    }

    public Integer getStatus()
    {
        return this.status;
    }
}
