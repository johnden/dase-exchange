package com.clg.wallet.bean;

import com.clg.wallet.enums.*;
import com.alibaba.fastjson.*;
import java.math.*;
import java.util.*;
import com.clg.wallet.utils.*;

public class ResultDTO
{
    private int statusCode;
    private String resultDesc;
    private Object result;

    public ResultDTO setStatusCode(int statusCode)
    {
        this.statusCode = statusCode;return this;
    }

    public ResultDTO setResultDesc(String resultDesc)
    {
        this.resultDesc = resultDesc;return this;
    }

    public ResultDTO setResult(Object result)
    {
        this.result = result;return this;
    }

    public int getStatusCode()
    {
        return this.statusCode;
    }

    public String getResultDesc()
    {
        return this.resultDesc;
    }

    public Object getResult()
    {
        return this.result;
    }

    public ResultDTO(int statusCode, String resultDesc)
    {
        this.statusCode = statusCode;
        this.resultDesc = resultDesc;
    }

    public ResultDTO(int statusCode, String resultDesc, Object result)
    {
        this.statusCode = statusCode;
        this.resultDesc = resultDesc;
        this.result = result;
    }

    private ResultDTO(ResultCode resultCode)
    {
        this(resultCode.getCode(), resultCode.getMessage());
    }

    private ResultDTO(ResultCode resultCode, Object data)
    {
        this(resultCode.getCode(), resultCode.getMessage(), data);
    }

    public static ResultDTO successResult()
    {
        return new ResultDTO(ResultCode.SUCCESS);
    }

    public static ResultDTO successResult(Object data)
    {
        return new ResultDTO(ResultCode.SUCCESS, data);
    }

    public static ResultDTO errorResult(ResultCode resultCode)
    {
        return new ResultDTO(resultCode);
    }

    public static ResultDTO errorResult(int code, String message)
    {
        return new ResultDTO(code, message);
    }

    public int toInteger()
    {
        boolean isInt = IntegerUtils.isInt(this.result.toString());
        if (isInt) {
            return Integer.valueOf(this.result.toString()).intValue();
        }
        throw new RuntimeException("����Integer����");
    }

    public JSONObject toJSONObj()
    {
        if ((null != this.result) && ((this.result instanceof JSONObject))) {
            return (JSONObject)this.result;
        }
        throw new RuntimeException("����JSONObject����");
    }

    public JSONArray toJSONArray()
    {
        if ((null != this.result) && ((this.result instanceof JSONArray))) {
            return (JSONArray)this.result;
        }
        throw new RuntimeException("����JSONArray����");
    }

    public double toDouble()
    {
        if ((null != this.result) && ((this.result instanceof Double))) {
            return ((Double)this.result).doubleValue();
        }
        throw new RuntimeException("����double����");
    }

    public boolean toBoolean()
    {
        if ((null != this.result) && ((this.result instanceof Boolean))) {
            return ((Boolean)this.result).booleanValue();
        }
        throw new RuntimeException("����booleant����");
    }

    public BigDecimal toBigDecimal()
    {
        if ((null != this.result) && ((this.result instanceof BigDecimal))) {
            return BigDecimalUtils.formatBigDecimal(new BigDecimal(this.result.toString()), 18);
        }
        throw new RuntimeException("����BigDecimal����");
    }

    public List toList()
    {
        if ((null != this.result) && ((this.result instanceof List))) {
            return (List)this.result;
        }
        throw new RuntimeException("����List����");
    }

    public <T> T toObj(Class<T> t)
    {
        return (T)GsonUtils.convertObj(GsonUtils.toJson(this.result), t);
    }

    public ResultDTO() {}
}
