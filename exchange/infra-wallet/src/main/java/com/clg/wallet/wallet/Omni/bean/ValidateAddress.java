package com.clg.wallet.wallet.Omni.bean;

public class ValidateAddress
{

    private boolean isvalid;
    private String address;
    private boolean ismine;
    private boolean isscript;
    private String pubkey;
    private boolean iscompressed;
    private String account;

    public ValidateAddress setIsvalid(boolean isvalid)
    {
        this.isvalid = isvalid;return this;
    }

    public ValidateAddress setAddress(String address)
    {
        this.address = address;return this;
    }

    public ValidateAddress setIsmine(boolean ismine)
    {
        this.ismine = ismine;return this;
    }

    public ValidateAddress setIsscript(boolean isscript)
    {
        this.isscript = isscript;return this;
    }

    public ValidateAddress setPubkey(String pubkey)
    {
        this.pubkey = pubkey;return this;
    }

    public ValidateAddress setIscompressed(boolean iscompressed)
    {
        this.iscompressed = iscompressed;return this;
    }

    public ValidateAddress setAccount(String account)
    {
        this.account = account;return this;
    }


    public boolean isIsvalid()
    {
        return this.isvalid;
    }

    public String getAddress()
    {
        return this.address;
    }

    public boolean isIsmine()
    {
        return this.ismine;
    }

    public boolean isIsscript()
    {
        return this.isscript;
    }

    public String getPubkey()
    {
        return this.pubkey;
    }

    public boolean isIscompressed()
    {
        return this.iscompressed;
    }

    public String getAccount()
    {
        return this.account;
    }
}
