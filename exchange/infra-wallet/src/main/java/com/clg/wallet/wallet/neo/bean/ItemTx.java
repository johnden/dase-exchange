package com.clg.wallet.wallet.neo.bean;

import java.math.*;

public class ItemTx
{
    private String txid;
    private String fromAddress;
    private String toAddress;
    private String assertId;
    private BigDecimal amount;
    private Integer blockNumber;

    public ItemTx setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public ItemTx setFromAddress(String fromAddress)
    {
        this.fromAddress = fromAddress;return this;
    }

    public ItemTx setToAddress(String toAddress)
    {
        this.toAddress = toAddress;return this;
    }

    public ItemTx setAssertId(String assertId)
    {
        this.assertId = assertId;return this;
    }

    public ItemTx setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public ItemTx setBlockNumber(Integer blockNumber)
    {
        this.blockNumber = blockNumber;return this;
    }



    public String getTxid()
    {
        return this.txid;
    }

    public String getFromAddress()
    {
        return this.fromAddress;
    }

    public String getToAddress()
    {
        return this.toAddress;
    }

    public String getAssertId()
    {
        return this.assertId;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public Integer getBlockNumber()
    {
        return this.blockNumber;
    }
}
