package com.clg.wallet.newclient;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clg.wallet.bean.AddressBean;
import com.clg.wallet.bean.ClientBean;
import com.clg.wallet.bean.EThTransactionBean;
import com.clg.wallet.bean.EthResult;
import com.clg.wallet.bean.ResultDTO;
import com.clg.wallet.bean.TimeBlockCount;
import com.clg.wallet.bean.TxData;
import com.clg.wallet.enums.ResultCode;
import com.clg.wallet.exception.EthSysnException;
import com.clg.wallet.help.EthToken;
import com.clg.wallet.help.Greeter;
import com.clg.wallet.help.Transfer;
import com.clg.wallet.help.WalletUtils;
import com.clg.wallet.utils.GsonUtils;
import com.clg.wallet.utils.HttpRequestUtil;
import com.clg.wallet.utils.IntegerUtils;
import com.clg.wallet.utils.PassUtils;
import com.clg.wallet.utils.StringParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.crypto.Wallet;
import org.web3j.crypto.WalletFile;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.DefaultBlockParameterNumber;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.EthAccounts;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlock.Block;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetCode;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthTransaction;
import org.web3j.protocol.core.methods.response.NetPeerCount;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.tx.Contract;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;
import org.web3j.utils.Files;

public class EthNewClient extends NormalClient
{
    private static final Logger LOG = LoggerFactory.getLogger(EthNewClient.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final HashMap<String, EthToken> abiHash = new HashMap();
    private static Map<String, Integer> unitMap = new HashMap();
    private final Web3j web3j;

    public Web3j getWeb3j()
    {
        return this.web3j;
    }

    public static String mKey = "R2MBYWJSW8PIESWSF78S57BNT9W8CWGJSX";
    private String ethUrl = "https://etzscan.com/publicAPI?module=proxy&action=eth_blockNumber&apikey=";
    private String etcUrl = "https://api.gastracker.io/v1/blocks/latest";

    public EthNewClient(ClientBean clientBean)
    {
        super(clientBean);
        this.web3j = ClientInstance.getEthClient(this.mClientBean);
    }

    public ResultDTO getNewAddress()
    {
        String defaultPass = PassUtils.getPass();
        return getNewAddress(defaultPass);
    }

    public ResultDTO getNewAddress(String password)
    {
        return new ResultDTO().setResult(createAddress(password));
    }

    public static AddressBean createAddressAndSave(String password, String path)
    {
        try
        {
            File file = new File(path);
            LOG.error("文件夹的路径是：{}",path);
            if (!file.exists()) {
                file.mkdirs();
            }
            if (!file.exists()) {
                throw new FileNotFoundException("创建文件失败,请手工创建文件夹:" + path);
            }
            ECKeyPair ecKeyPair = Keys.createEcKeyPair();
            WalletFile walletFile = Wallet.createStandard(password, ecKeyPair);
            DateTimeFormatter format = DateTimeFormatter.ofPattern("'UTC--'yyyy-MM-dd'T'HH-mm-ss.nVV'--'");
            ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
            String fileName = now.format(format) + walletFile.getAddress() + ".json";
            File destination = new File(path, fileName);
            objectMapper.writeValue(destination, walletFile);
            String address = "0x" + walletFile.getAddress();
            LOG.error("文件名是：{}",fileName);
            return new AddressBean().setFileName(fileName).setKeystore(Files.readString(destination)).setPwd(password).setAddress(address);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new EthSysnException("创建地址异常");
        }
    }

    public AddressBean createAddress(String password)
    {
        try
        {
            ECKeyPair ecKeyPair = Keys.createEcKeyPair();
            WalletFile walletFile = Wallet.createStandard(password, ecKeyPair);
            String address = "0x" + walletFile.getAddress();
            String keystore = GsonUtils.toJson(walletFile);
            if ((StringUtils.isEmpty(address)) || (StringUtils.isEmpty(keystore)) || (StringUtils.isEmpty(password))) {
                throw new EthSysnException(StringUtils.isEmpty(keystore) ? "keystore不能为空" : StringUtils.isEmpty(address) ? "address不能为空" : "password不能为空");
            }
            AddressBean addressBean = new AddressBean().setKeystore(keystore).setPwd(password).setAddress(address);
            LOG.info(addressBean.toString());
            return addressBean;
        }
        catch (Exception e)
        {
            LOG.error("获取地址失败");
            throw new EthSysnException("获取地址失败");
        }
    }

    public ResultDTO getBalance()
    {
        throw new EthSysnException("请传入地址");
    }

    public ResultDTO getBalance(String address)
    {
        if (StringUtils.isEmpty(address)) {
            throw new EthSysnException("请传入地址");
        }
        BigDecimal balance = BigDecimal.ZERO;
        try
        {
            if (TextUtils.isEmpty(address)) {
                throw new EthSysnException("查询余额地址不能为空");
            }
            balance = Convert.fromWei(new BigDecimal((this.web3j.ethGetBalance(address, DefaultBlockParameterName.LATEST).send()).getBalance()), Convert.Unit.ETHER);
        }
        catch (Exception var4)
        {
            LOG.error(var4.getMessage());
        }
        return new ResultDTO().setResult(balance);
    }

    public ResultDTO getBalanceByAccount(String account)
    {
        return null;
    }

    public ResultDTO getTokenBalance(String assertId, String address)
    {
        if (TextUtils.isEmpty(address)) {
            throw new EthSysnException("查询token余额地址不能为空");
        }
        EthToken abi = abiHash.get(assertId);
        if (null == abi)
        {
            abi = getAbi(assertId);
            abiHash.put(assertId, abi);
        }
        BigDecimal balanceValue = null;
        try
        {
            balanceValue = new BigDecimal(abi.balanceOf(address).send());
            int uint = (abi.decimals().send()).intValue();
            balanceValue = balanceValue.divide(BigDecimal.TEN.pow(uint));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new EthSysnException("查询异常");
        }
        return new ResultDTO().setResult(balanceValue);
    }

    public EthResult sentEth(EThTransactionBean eThTransactionBean)
            throws Exception
    {
        if (!checkServer()) {
            throw new EthSysnException("服务器正在同步中,请不要进行,转账充值操作");
        }
        BigDecimal ethBalance = (BigDecimal)getBalance(eThTransactionBean.getFromAddress()).getResult();

        ethBalance = ethBalance.subtract(getEthFee());
        if (ethBalance.compareTo(new BigDecimal(0)) < 0) {
            return new EthResult().setCode(-3).setInfo("钱包账户余额为" + ethBalance.toString() + ",不能转币").setSuccess(false);
        }
        if ((!eThTransactionBean.isAllTransaction()) && (ethBalance.compareTo(eThTransactionBean.getBalance()) < 0)) {
            return

                    new EthResult().setInfo("打出失败,打出账户余额不足,当前账户余额+" + ethBalance.toString() + ",需要打出的币:" + eThTransactionBean.getBalance()).setCode(-3).setSuccess(false);
        }
        Credentials credentials = WalletUtils.loadCredentials(eThTransactionBean.getFromUserPass(), getKeystorePath(eThTransactionBean.getFromUserKeystore(), eThTransactionBean.getFromUserKeystorePath()));

        String txid = (Transfer.sendFunds(this.web3j, credentials, eThTransactionBean.getToAddress(), eThTransactionBean.isAllTransaction() ? ethBalance : eThTransactionBean.getBalance(), Convert.Unit.ETHER, eThTransactionBean.getAddPrice()).sendAsync().get()).getTransactionHash();
        if (StringUtils.isEmpty(txid)) {
            throw new Exception("txid is empty!");
        }
        return new EthResult().setTxid(txid).setSuccess(true).setInfo("打币成功");
    }

    public boolean checkServer()
    {
        if (StringUtils.isEmpty(mKey))
        {
            LOG.error("服务器正在同步中,请不要进行,转账充值操作");
            return false;
        }
        int explorerBlockNumber = getExplorerBlockNumber(mKey).toInteger();
        int blockCount = getBlockCount().toInteger();
        if (explorerBlockNumber - blockCount > 10)
        {
            LOG.error("服务器正在同步中,请不要进行,转账充值操作");
            return false;
        }
        return true;
    }

    public EthResult sentEthToken(EThTransactionBean eThTransactionBean)
            throws Exception
    {
        if (!checkServer()) {
            throw new EthSysnException("服务器正在同步中,请不要进行,转账充值操作");
        }
        EthToken abi = getAbi(eThTransactionBean.getContractAddress(),
                getKeystorePath(eThTransactionBean.getFromUserKeystore(), eThTransactionBean.getFromUserKeystorePath()), eThTransactionBean
                        .getFromUserPass(), eThTransactionBean.getAddPrice());

        BigDecimal toTokenBalance = eThTransactionBean.getTokenBalance();

        BigDecimal fromTokenBalance = (BigDecimal)getTokenBalance(eThTransactionBean.getContractAddress(), eThTransactionBean.getFromAddress()).getResult();
        if ((!eThTransactionBean.isAllTransaction()) && (fromTokenBalance.compareTo(toTokenBalance) < 0)) {
            return

                    new EthResult().setInfo("打出失败,打出账户余额不足,当前账户余额+" + fromTokenBalance.toString() + ",需要打出的币:" + eThTransactionBean.getTokenBalance()).setCode(-3).setSuccess(false);
        }
        BigDecimal ethBalance = (BigDecimal)getBalance(eThTransactionBean.getFromAddress()).getResult();
        BigDecimal tokenSpentPrice = getEthTokenFee();
        if (ethBalance.compareTo(tokenSpentPrice) < 0) {
            return new EthResult().setCode(-2).setSuccess(false).setInfo("当前token无手续费,无法转账,请支付手续费");
        }
        String txid = (abi.transfer(eThTransactionBean.getToAddress(), getRealBalance(abi, eThTransactionBean.isAllTransaction() ? fromTokenBalance : eThTransactionBean.getTokenBalance())).send()).getTransactionHash();
        if (StringUtils.isEmpty(txid)) {
            return new EthResult().setTxid(txid).setSuccess(false).setInfo("打币失败");
        }
        return new EthResult().setTxid(txid).setSuccess(true).setInfo("打币成功");
    }

    public ResultDTO getListAddress()
    {
        List<String> accounts = new ArrayList();
        try
        {
            accounts = (this.web3j.ethAccounts().send()).getAccounts();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return new ResultDTO().setResult(accounts);
    }

    public ResultDTO getPeers()
    {
        return null;
    }

    public ResultDTO getBlockCount()
    {
        int count = 0;
        BigInteger latest = null;
        try
        {
            latest = (this.web3j.ethBlockNumber().send()).getBlockNumber();
            count = latest.intValue();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return new ResultDTO().setResult(Integer.valueOf(count));
    }

    public ResultDTO getBlockHash(long blockNumber)
    {
        String hash = "";
        try
        {
            hash = (this.web3j.ethGetBlockByNumber(new DefaultBlockParameterNumber(blockNumber), true).send()).getBlock().getHash();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return new ResultDTO().setResult(hash);
    }

    public ResultDTO getConnectionCount()
    {
        int count = 0;
        try
        {
            count = (this.web3j.netPeerCount().send()).getQuantity().intValue();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return new ResultDTO().setResult(Integer.valueOf(count));
    }

    public ResultDTO dumpPrivKey(String address)
    {
        return null;
    }

    public ResultDTO validateAddress(String address)
    {
        boolean validAddress = WalletUtils.isValidAddress(address);
        return new ResultDTO().setResult(Boolean.valueOf(validAddress));
    }

    public ResultDTO sendNormal(TxData tx)
    {
        return null;
    }

    public ResultDTO sendNormalToken(TxData tx)
    {
        return null;
    }

    public ResultDTO getTxBlockNumber(String txid)
    {
        int blockNumber = 0;
        try
        {
            BigInteger search = ((this.web3j.ethGetTransactionByHash(txid).send()).getResult()).getBlockNumber();
            blockNumber = search.intValue();
        }
        catch (Exception var6)
        {
            LOG.error(var6.getMessage());
        }
        return new ResultDTO().setResult(Integer.valueOf(blockNumber));
    }

    public ResultDTO getBlockByNumber(String txIdOrNumber)
    {
        return getBlockByNumber(txIdOrNumber, true);
    }

    public ResultDTO getBlockByNumber(String txIdOrNumber, boolean isFull)
    {
        EthBlock.Block block = null;
        try
        {
            if (IntegerUtils.isInt(txIdOrNumber)) {
                block = (this.web3j.ethGetBlockByNumber(new DefaultBlockParameterNumber(Integer.valueOf(txIdOrNumber).intValue()), isFull).send()).getBlock();
            } else {
                block = (this.web3j.ethGetBlockByHash(txIdOrNumber, isFull).send()).getBlock();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new EthSysnException("获取区块异常");
        }
        return new ResultDTO().setResult(block);
    }

    public ResultDTO getAddrFromAcount(String account)
    {
        return null;
    }

    public boolean validateAssertId(String assertId)
    {
        return WalletUtils.isValidAddress(assertId);
    }

    public ResultDTO getAccountState(String address)
    {
        return null;
    }

    public ResultDTO getTokenDecimals(String assetId)
    {
        if (unitMap.containsKey(assetId)) {
            return new ResultDTO().setResult(unitMap.get(assetId));
        }
        EthToken abi = getAbi(assetId);
        int i = 0;
        try
        {
            i = (abi.decimals().send()).intValue();
            unitMap.put(assetId, Integer.valueOf(i));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return new ResultDTO().setResult(unitMap.get(assetId));
    }

    public ResultDTO getBestBlockHash()
    {
        BigInteger latest = null;
        try
        {
            latest = (this.web3j.ethBlockNumber().send()).getBlockNumber();
            return getBlockHash(latest.longValue());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new EthSysnException("获取最高的区块hash异常");
        }
    }

    public ResultDTO getPendingTx()
    {
        return null;
    }

    public ResultDTO getInfo()
    {
        try
        {
            return new ResultDTO().setResult((this.web3j.web3ClientVersion().send()).getWeb3ClientVersion());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new EthSysnException("客户端连接异常");
        }
    }

    public ResultDTO getTransactionConfirmed(String txid)
    {
        int count = 0;
        try
        {
            BigInteger latest = (this.web3j.ethBlockNumber().send()).getBlockNumber();
            BigInteger search = ((this.web3j.ethGetTransactionByHash(txid).send()).getResult()).getBlockNumber();
            count = latest.subtract(search).intValue();
        }
        catch (Exception var6)
        {
            LOG.error(var6.getMessage());
        }
        return new ResultDTO().setResult(Integer.valueOf(count));
    }

    public ResultDTO getTransactionFee(String txid)
    {
        try
        {
            TransactionReceipt transactionReceipt = (this.web3j.ethGetTransactionReceipt(txid).send()).getTransactionReceipt().get();
            ResultDTO resultDTO = isFailed(txid);
            if ((resultDTO.getStatusCode() == ResultCode.SUCCESS.getCode()) && (!resultDTO.toBoolean()))
            {
                Transaction transaction = (this.web3j.ethGetTransactionByHash(txid).send()).getTransaction().get();
                BigInteger gasPrice = transaction.getGasPrice();
                BigInteger cumulativeGasUsed = transactionReceipt.getGasUsed();
                return new ResultDTO().setResult(new BigDecimal(gasPrice.multiply(cumulativeGasUsed)).divide(BigDecimal.TEN.pow(18)));
            }
            if (resultDTO.toBoolean()) {
                return new ResultDTO().setStatusCode(ResultCode.TX_FAILED.getCode());
            }
            return resultDTO;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public ResultDTO getTransaction(String txid)
    {
        try
        {
            return new ResultDTO().setResult((this.web3j.ethGetTransactionByHash(txid).send()).getTransaction().get());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new EthSysnException("获取交易信息异常");
        }
    }

    public Integer getTransactionCount(String address, DefaultBlockParameterName type)
    {
        int transactionCount = -1;
        try
        {
            EthGetTransactionCount send = this.web3j.ethGetTransactionCount(address, type).send();
            if ((null != send) && (!StringUtils.isEmpty(send.getResult()))) {
                transactionCount = send.getTransactionCount().intValue();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return Integer.valueOf(transactionCount);
    }

    public EthToken getAbi(String tokenAddress)
    {
        return getAbi(tokenAddress, null, null, Long.valueOf(0L));
    }

    public EthToken getAbi(String tokenAddress, String key, String pass, Long priceNum)
    {
        if (StringUtils.isEmpty(tokenAddress)) {
            return null;
        }
        try
        {
            Credentials credentials = null;
            if ((!StringUtils.isEmpty(key)) && (!StringUtils.isEmpty(pass))) {
                credentials = WalletUtils.loadCredentials(pass, key);
            } else {
                credentials = WalletUtils.loadCredentials("a50509a11524842229797", "{\"address\":\"9d6383c04ee6405ef3de27809cd3eb7d5b75101e\",\"id\":\"6a00ab41-c546-43b9-ab45-50d287fd8694\",\"version\":3,\"crypto\":{\"cipher\":\"aes-128-ctr\",\"ciphertext\":\"5121766623525cf23bd02f8b6d1d92f356fd2d91a9d5659ae9f4fde94b8ef4f4\",\"cipherparams\":{\"iv\":\"0d21b28a23ed34c2d5c49db1ce162130\"},\"kdf\":\"scrypt\",\"kdfparams\":{\"dklen\":32,\"n\":262144,\"p\":1,\"r\":8,\"salt\":\"3b1d97ccccb6ebb7e9ca697e665ab182cab9f32550f936b78b74b79803225f82\"},\"mac\":\"5208678e2a05bd19d133d19678c8402e180ae20e4db80e2d5f4eadb33279c941\"}}");
            }
            BigInteger gasPrice = getBigPrice();
            if ((null != priceNum) && (priceNum.longValue() > 0L)) {
                gasPrice = gasPrice.add(BigInteger.valueOf(priceNum.longValue()));
            }
            return EthToken.load(tokenAddress, this.web3j, credentials, gasPrice, BigInteger.valueOf(120000L));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public BigInteger getGasPrice()
    {
        try
        {
            return (this.web3j.ethGasPrice().send()).getGasPrice();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return BigInteger.valueOf(35000000000L);
    }

    public ResultDTO isFailed(String txid)
    {
        try
        {
            boolean failed = true;
            TransactionReceipt transactionReceipt = (this.web3j.ethGetTransactionReceipt(txid).send()).getTransactionReceipt().get();
            if ((null != transactionReceipt) && ("0x1".equalsIgnoreCase(transactionReceipt.getStatus()))) {
                if (!StringUtils.isEmpty(transactionReceipt.getBlockNumber()))
                {
                    if (StringUtils.isEmpty(transactionReceipt.getFrom())) {
                        failed = CollectionUtils.isEmpty(transactionReceipt.getLogs());
                    } else {
                        failed = false;
                    }
                }
                else {
                    return new ResultDTO().setStatusCode(ResultCode.TX_BLOCKING.getCode());
                }
            }
            return new ResultDTO().setResult(Boolean.valueOf(failed));
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public BigDecimal getEthFee()
    {
        return BigDecimal.valueOf(0.01);
    }

    public BigDecimal getEthTokenFee()
    {
        BigInteger gasPrice = getBigPrice();
        BigDecimal spentEth = new BigDecimal(gasPrice.multiply(BigInteger.valueOf(120000L))).divide(BigDecimal.TEN.pow(18));
        return spentEth;
    }

    private BigInteger getBigPrice()
    {
        BigInteger gasPrice = getGasPrice();
        if (gasPrice.compareTo(Contract.GAS_PRICE) < 0) {
            gasPrice = Contract.GAS_PRICE;
        }
        return gasPrice.add(BigInteger.valueOf(5000000000L));
    }

    private BigInteger getRealBalance(EthToken abi, BigDecimal balance)
            throws Exception
    {
        int uint = (abi.decimals().send()).intValue();
        return balance.multiply(BigDecimal.TEN.pow(uint)).toBigInteger();
    }

    public String getKeystorePath(String fromUserKeystore, String fromUserKeystorePath)
    {
        if (StringParser.isJson(fromUserKeystore)) {
            return fromUserKeystore;
        }
        if (!StringUtils.isEmpty(fromUserKeystorePath)) {
            return fromUserKeystorePath + fromUserKeystore;
        }
        String type = this.mClientBean.getCoinType();
        type = type + "ereum";
        String mainnetKeyDirectory = WalletUtils.getMainnetKeyDirectory(type);
        return mainnetKeyDirectory + fromUserKeystore;
    }

    public String getKeystorePath(String keystoreName)
    {
        return getKeystorePath(keystoreName, "");
    }

    public boolean isContract(String address)
    {
        try
        {
            return !"0x".equalsIgnoreCase((this.web3j.ethGetCode(address, DefaultBlockParameterName.LATEST).send()).getCode());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException("查询是不是合约信息失败");
        }
    }

    public ResultDTO getExplorerBlockNumber(String key)
    {
        TimeBlockCount timeBlockCount = this.timeMap.get(this.mClientBean.getCoinType());
        if (null == timeBlockCount) {
            this.timeMap.put(this.mClientBean.getCoinType(), new TimeBlockCount()
                    .setTime(Long.valueOf(System.currentTimeMillis()))
                    .setBlockCount(Long.valueOf(0L)));
        } else if (((timeBlockCount.getBlockCount().longValue() > 0L ? 1 : 0) & (timeBlockCount.getTime().longValue() - System.currentTimeMillis() <= 5000L ? 1 : 0)) != 0) {
            return new ResultDTO().setResult(timeBlockCount.getBlockCount());
        }
        if ("etc".equalsIgnoreCase(this.mClientBean.getCoinType()))
        {
            JSONObject txData = HttpRequestUtil.getJson(this.etcUrl, JSONObject.class);
            if (null != txData)
            {
                JSONArray items = txData.getJSONArray("items");
                if ((null != items) && (items.size() > 0))
                {
                    JSONObject itemData = items.getJSONObject(0);
                    Integer blockCount = itemData.getInteger("height");
                    if (null != blockCount) {
                        return new ResultDTO().setResult(blockCount);
                    }
                }
            }
        }
        else if ("etz".equalsIgnoreCase(this.mClientBean.getCoinType()))
        {
            mKey = key;
            com.clg.wallet.bean.EthBlockNumber ethBlockNumber = HttpRequestUtil.getJson(this.ethUrl + key, com.clg.wallet.bean.EthBlockNumber.class);
            if (null != ethBlockNumber)
            {
                String result = ethBlockNumber.getResult();
                int blockCount = Integer.parseInt(result, 10);
                LOG.info("获取到的最新区块高度是：{}",blockCount);
                return new ResultDTO().setResult(Integer.valueOf(blockCount));
            }
        }
        return new ResultDTO().setResult(Integer.valueOf(0)).setStatusCode(ResultCode.EXPLORER_ERROR.getCode());
    }

    public ResultDTO sendContract(String keystore, String pass, BigInteger initialSupply, String tokenName, String tokenSymbol)
    {
        Credentials credentials = null;
        try
        {
            credentials = WalletUtils.loadCredentials(pass, keystore);
            System.out.println(credentials);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new ResultDTO().setStatusCode(ResultCode.CREATE_CREDENTIALS_ERROR.getCode());
        }
        try
        {
            Greeter greeter = (Greeter)Greeter.deploy(this.web3j, credentials, getBigPrice(), Contract.GAS_LIMIT, initialSupply, tokenName, tokenSymbol).send();
            String contractAddress = greeter.getContractAddress();
            return new ResultDTO().setResult(contractAddress);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return new ResultDTO().setStatusCode(ResultCode.CREATE_CONTRACT_TIME_OUT.getCode());
    }

    public static void main(String[] args)
    {
        ClientBean clientBean = new ClientBean().setRpcPort("8089").setRpcIp("47.52.75.55").setCoinType("etc");
        EthNewClient client = (EthNewClient)ClientFactory.getClient(clientBean);
        try
        {
            String code = (client.getWeb3j().ethGetCode("0xd71eef0b84e3afe44ecec65111368f3d06b4adba", DefaultBlockParameterName.LATEST).send()).getCode();
            System.out.println(code);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
