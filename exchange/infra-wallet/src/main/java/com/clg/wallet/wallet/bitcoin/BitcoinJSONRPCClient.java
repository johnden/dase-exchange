package com.clg.wallet.wallet.bitcoin;

import java.nio.charset.*;
import java.util.logging.*;
import java.net.*;
import javax.net.ssl.*;
import java.io.*;
import com.clg.wallet.utils.*;
import java.util.*;

public class BitcoinJSONRPCClient implements Bitcoin
{
    private static final Logger logger = Logger.getLogger(BitcoinJSONRPCClient.class.getCanonicalName());
    public final URL rpcURL;
    private URL noAuthURL;
    private String authStr;
    public static final URL DEFAULT_JSONRPC_URL;
    public static final URL DEFAULT_JSONRPC_TESTNET_URL;
    private HostnameVerifier hostnameVerifier;
    private SSLSocketFactory sslSocketFactory;
    private int connectTimeout;

    public BitcoinJSONRPCClient(String rpcUrl)
            throws MalformedURLException
    {
        this(new URL(rpcUrl));
    }

    public BitcoinJSONRPCClient(URL rpc)
    {
        this.hostnameVerifier = null;
        this.sslSocketFactory = null;
        this.connectTimeout = 0;
        this.rpcURL = rpc;
        try
        {
            this.noAuthURL = new URI(rpc.getProtocol(), null, rpc.getHost(), rpc.getPort(), rpc.getPath(), rpc.getQuery(), null).toURL();
        }
        catch (MalformedURLException var3)
        {
            throw new IllegalArgumentException(rpc.toString(), var3);
        }
        catch (URISyntaxException var4)
        {
            throw new IllegalArgumentException(rpc.toString(), var4);
        }
        this.authStr = (rpc.getUserInfo() == null ? null : String.valueOf(Base64Coder.encode(rpc.getUserInfo().getBytes(Charset.forName("ISO8859-1")))));
    }

    public BitcoinJSONRPCClient(boolean testNet)
    {
        this(testNet ? DEFAULT_JSONRPC_TESTNET_URL : DEFAULT_JSONRPC_URL);
    }

    public BitcoinJSONRPCClient()
    {
        this(DEFAULT_JSONRPC_TESTNET_URL);
    }

    public HostnameVerifier getHostnameVerifier()
    {
        return this.hostnameVerifier;
    }

    public void setHostnameVerifier(HostnameVerifier hostnameVerifier)
    {
        this.hostnameVerifier = hostnameVerifier;
    }

    public SSLSocketFactory getSslSocketFactory()
    {
        return this.sslSocketFactory;
    }

    public void setSslSocketFactory(SSLSocketFactory sslSocketFactory)
    {
        this.sslSocketFactory = sslSocketFactory;
    }

    public void setConnectTimeout(int timeout)
    {
        if (timeout < 0) {
            throw new IllegalArgumentException("timeout can not be negative");
        }
        this.connectTimeout = timeout;
    }

    public int getConnectTimeout()
    {
        return this.connectTimeout;
    }

    public byte[] prepareRequest(final String method, final Object... params)
    {
        return JSON.stringify(new LinkedHashMap() {
            {
                this.put("method", method);
                this.put("params", params);
                this.put("id", "1");
            }
        }).getBytes(BitcoinJSONRPCClient.QUERY_CHARSET);
    }

    private static byte[] loadStream(InputStream in, boolean close)
            throws IOException
    {
        final ByteArrayOutputStream o = new ByteArrayOutputStream();
        final byte[] buffer = new byte[1024];
        while (true) {
            final int nr = in.read(buffer);
            if (nr == -1) {
                return o.toByteArray();
            }
            if (nr == 0) {
                throw new IOException("Read timed out");
            }
            o.write(buffer, 0, nr);
        }
    }
    public static final Charset QUERY_CHARSET = Charset.forName("ISO8859-1");
    public Object loadResponse(InputStream in, Object expectedID, boolean close)
            throws IOException, BitcoinException
    {
        Object var6;
        try {
            final String r = new String(loadStream(in, close), BitcoinJSONRPCClient.QUERY_CHARSET);
            BitcoinJSONRPCClient.logger.log(Level.FINE, "Bitcoin JSON-RPC response:\n{0}", r);
            try {
                final Map response = (Map)JSON.parse(r);
                if (!expectedID.equals(response.get("id"))) {
                    throw new BitcoinRPCException("Wrong response ID (expected: " + String.valueOf(expectedID) + ", response: " + response.get("id") + ")");
                }
                if (response.get("error") != null) {
                    throw new BitcoinException(JSON.stringify(response.get("error")));
                }
                var6 = response.get("result");
            }
            catch (ClassCastException var7) {
                throw new BitcoinRPCException("Invalid server response format (data: \"" + r + "\")");
            }
        }
        finally {
            if (close) {
                in.close();
            }
        }
        return var6;
    }

    public Object query(String method, Object... o)
            throws BitcoinException
    {
        try {
            final HttpURLConnection conn = (HttpURLConnection)this.noAuthURL.openConnection();
            if (this.connectTimeout != 0) {
                conn.setConnectTimeout(this.connectTimeout);
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);
            if (conn instanceof HttpsURLConnection) {
                if (this.hostnameVerifier != null) {
                    ((HttpsURLConnection)conn).setHostnameVerifier(this.hostnameVerifier);
                }
                if (this.sslSocketFactory != null) {
                    ((HttpsURLConnection)conn).setSSLSocketFactory(this.sslSocketFactory);
                }
            }
            conn.setRequestProperty("Authorization", "Basic " + this.authStr);
            final byte[] r = this.prepareRequest(method, o);
            BitcoinJSONRPCClient.logger.log(Level.FINE, "Bitcoin JSON-RPC request:\n{0}", new String(r, BitcoinJSONRPCClient.QUERY_CHARSET));
            conn.getOutputStream().write(r);
            conn.getOutputStream().close();
            final int responseCode = conn.getResponseCode();
            if (responseCode != 200) {
                final String response = new String(loadStream(conn.getErrorStream(), true));
                final BitcoinRPCException bitcoinRPCException = new BitcoinRPCException("RPC Query Failed (method: " + method + ", params: " + Arrays.deepToString(o) + ", response header: " + responseCode + " " + conn.getResponseMessage() + ", response: " + new String(loadStream(conn.getErrorStream(), true)));
                bitcoinRPCException.setErrorResponse(response);
                throw bitcoinRPCException;
            }
            return this.loadResponse(conn.getInputStream(), "1", true);
        }
        catch (IOException var6) {
            throw new BitcoinRPCException("RPC Query Failed (method: " + method + ", params: " + Arrays.deepToString(o) + ")", var6);
        }
    }

    public void addNode(String node, Bitcoin.AddNoteCmd command)
            throws BitcoinException
    {
        this.query("addnode", node, command.toString());
    }

    public String createRawTransaction(List<Bitcoin.TxInput> inputs, List<Bitcoin.TxOutput> outputs)
            throws BitcoinException
    {
        final List<Map> pInputs = new ArrayList<Map>();
        for (final TxInput txInput : inputs) {
            pInputs.add(new LinkedHashMap() {
                {
                    this.put("txid", txInput.txid());
                    this.put("vout", txInput.vout());
                }
            });
        }
        final Map<String, Double> pOutputs = new LinkedHashMap<String, Double>();
        for (final TxOutput txOutput : outputs) {
            final Double oldValue;
            if ((oldValue = pOutputs.put(txOutput.address(), txOutput.amount())) != null) {
                pOutputs.put(txOutput.address(), BitcoinUtil.normalizeAmount(oldValue + txOutput.amount()));
            }
        }
        return (String)this.query("createrawtransaction", pInputs, pOutputs);
    }

    public Bitcoin.RawTransaction decodeRawTransaction(String hex)
            throws BitcoinException
    {
        return new RawTransactionImpl((Map)query("decoderawtransaction", new Object[] { hex }));
    }

    @Override
    public String dumpPrivKey(final String address) throws BitcoinException {
        return (String)this.query("dumpprivkey", address);
    }

    @Override
    public String getAccount(final String address) throws BitcoinException {
        return (String)this.query("getaccount", address);
    }

    @Override
    public String getAccountAddress(final String account) throws BitcoinException {
        return (String)this.query("getaccountaddress", account);
    }

    @Override
    public List<String> getAddressesByAccount(final String account) throws BitcoinException {
        return (List<String>)this.query("getaddressesbyaccount", account);
    }

    @Override
    public double getBalance() throws BitcoinException {
        return ((Number)this.query("getbalance", new Object[0])).doubleValue();
    }

    @Override
    public double getBalance(final String account) throws BitcoinException {
        return ((Number)this.query("getbalance", account)).doubleValue();
    }

    @Override
    public double getBalance(final String account, final int minConf) throws BitcoinException {
        return ((Number)this.query("getbalance", account, minConf)).doubleValue();
    }

    @Override
    public Block getBlock(final String blockHash) throws BitcoinException {
        return new BlockMapWrapper((Map)this.query("getblock", blockHash));
    }

    @Override
    public int getBlockCount() throws BitcoinException {
        return ((Number)this.query("getblockcount", new Object[0])).intValue();
    }

    @Override
    public String getBlockHash(final int blockId) throws BitcoinException {
        return (String)this.query("getblockhash", blockId);
    }

    @Override
    public int getConnectionCount() throws BitcoinException {
        return ((Number)this.query("getconnectioncount", new Object[0])).intValue();
    }

    @Override
    public double getDifficulty() throws BitcoinException {
        return ((Number)this.query("getdifficulty", new Object[0])).doubleValue();
    }

    @Override
    public boolean getGenerate() throws BitcoinException {
        return (boolean)this.query("getgenerate", new Object[0]);
    }

    @Override
    public double getHashesPerSec() throws BitcoinException {
        return ((Number)this.query("gethashespersec", new Object[0])).doubleValue();
    }

    @Override
    public Info getNetworkinfo() throws BitcoinException {
        return new InfoMapWrapper((Map)this.query("getnetworkinfo", new Object[0]));
    }

    @Override
    public Info getInfo() throws BitcoinException {
        return new InfoMapWrapper((Map)this.query("getinfo", new Object[0]));
    }

    @Override
    public MiningInfo getMiningInfo() throws BitcoinException {
        return new MiningInfoMapWrapper((Map)this.query("getmininginfo", new Object[0]));
    }

    @Override
    public String getNewAddress() throws BitcoinException {
        return (String)this.query("getnewaddress", new Object[0]);
    }

    @Override
    public String getNewAddress(final String account) throws BitcoinException {
        return (String)this.query("getnewaddress", account);
    }

    @Override
    public PeerInfo getPeerInfo() throws BitcoinException {
        return new PeerInfoMapWrapper((Map)this.query("getmininginfo", new Object[0]));
    }

    @Override
    public String getRawTransactionHex(final String txId) throws BitcoinException {
        return (String)this.query("getrawtransaction", txId);
    }

    @Override
    public RawTransaction getRawTransaction(final String txId) throws BitcoinException {
        return new RawTransactionImpl((Map<String, Object>)this.query("getrawtransaction", txId, 1));
    }

    @Override
    public double getReceivedByAccount(final String account) throws BitcoinException {
        return ((Number)this.query("getreceivedbyaccount", account)).doubleValue();
    }

    @Override
    public double getReceivedByAccount(final String account, final int minConf) throws BitcoinException {
        return ((Number)this.query("getreceivedbyaccount", account, minConf)).doubleValue();
    }

    @Override
    public double getReceivedByAddress(final String address) throws BitcoinException {
        return ((Number)this.query("getreceivedbyaddress", address)).doubleValue();
    }

    @Override
    public double getReceivedByAddress(final String address, final int minConf) throws BitcoinException {
        return ((Number)this.query("getreceivedbyaddress", address, minConf)).doubleValue();
    }

    @Override
    public RawTransaction getTransaction(final String txId) throws BitcoinException {
        return new RawTransactionImpl((Map<String, Object>)this.query("gettransaction", txId));
    }

    @Override
    public TxOutSetInfo getTxOutSetInfo() throws BitcoinException {
        final Map txoutsetinfoResult = (Map)this.query("gettxoutsetinfo", new Object[0]);
        return new TxOutSetInfo() {
            @Override
            public int height() {
                return ((Number)txoutsetinfoResult.get("height")).intValue();
            }

            @Override
            public String bestBlock() {
                return (String)txoutsetinfoResult.get("bestblock");
            }

            @Override
            public int transactions() {
                return ((Number)txoutsetinfoResult.get("transactions")).intValue();
            }

            @Override
            public int txOuts() {
                return ((Number)txoutsetinfoResult.get("txouts")).intValue();
            }

            @Override
            public int bytesSerialized() {
                return ((Number)txoutsetinfoResult.get("bytes_serialized")).intValue();
            }

            @Override
            public String hashSerialized() {
                return (String)txoutsetinfoResult.get("hash_serialized");
            }

            @Override
            public double totalAmount() {
                return ((Number)txoutsetinfoResult.get("total_amount")).doubleValue();
            }

            @Override
            public String toString() {
                return txoutsetinfoResult.toString();
            }
        };
    }

    @Override
    public Work getWork() throws BitcoinException {
        final Map workResult = (Map)this.query("getwork", new Object[0]);
        return new Work() {
            public String midstate()
            {
                return (String)workResult.get("midstate");
            }

            public String data()
            {
                return (String)workResult.get("data");
            }

            public String hash1()
            {
                return (String)workResult.get("hash1");
            }

            public String target()
            {
                return (String)workResult.get("target");
            }

            public String toString()
            {
                return workResult.toString();
            }
        };
    }

    @Override
    public void importPrivKey(final String bitcoinPrivKey) throws BitcoinException {
        this.query("importprivkey", bitcoinPrivKey);
    }

    @Override
    public void importPrivKey(final String bitcoinPrivKey, final String label) throws BitcoinException {
        this.query("importprivkey", bitcoinPrivKey, label);
    }

    @Override
    public void importPrivKey(final String bitcoinPrivKey, final String label, final boolean rescan) throws BitcoinException {
        this.query("importprivkey", bitcoinPrivKey, label, rescan);
    }

    @Override
    public Map<String, Number> listAccounts() throws BitcoinException {
        return (Map<String, Number>)this.query("listaccounts", new Object[0]);
    }

    @Override
    public Map<String, Number> listAccounts(final int minConf) throws BitcoinException {
        return (Map<String, Number>)this.query("listaccounts", minConf);
    }

    @Override
    public List<ReceivedAddress> listReceivedByAccount() throws BitcoinException {
        return new ReceivedAddressListWrapper((List<Map<String, Object>>)this.query("listreceivedbyaccount", new Object[0]));
    }

    @Override
    public List<ReceivedAddress> listReceivedByAccount(final int minConf) throws BitcoinException {
        return new ReceivedAddressListWrapper((List<Map<String, Object>>)this.query("listreceivedbyaccount", minConf));
    }

    @Override
    public List<ReceivedAddress> listReceivedByAccount(final int minConf, final boolean includeEmpty) throws BitcoinException {
        return new ReceivedAddressListWrapper((List<Map<String, Object>>)this.query("listreceivedbyaccount", minConf, includeEmpty));
    }

    @Override
    public List<ReceivedAddress> listReceivedByAddress() throws BitcoinException {
        return new ReceivedAddressListWrapper((List<Map<String, Object>>)this.query("listreceivedbyaddress", new Object[0]));
    }

    @Override
    public List<ReceivedAddress> listReceivedByAddress(final int minConf) throws BitcoinException {
        return new ReceivedAddressListWrapper((List<Map<String, Object>>)this.query("listreceivedbyaddress", minConf));
    }

    @Override
    public List<ReceivedAddress> listReceivedByAddress(final int minConf, final boolean includeEmpty) throws BitcoinException {
        return new ReceivedAddressListWrapper((List<Map<String, Object>>)this.query("listreceivedbyaddress", minConf, includeEmpty));
    }

    @Override
    public TransactionsSinceBlock listSinceBlock() throws BitcoinException {
        return new TransactionsSinceBlockImpl((Map)this.query("listsinceblock", new Object[0]));
    }

    @Override
    public TransactionsSinceBlock listSinceBlock(final String blockHash) throws BitcoinException {
        return new TransactionsSinceBlockImpl((Map)this.query("listsinceblock", blockHash));
    }

    @Override
    public TransactionsSinceBlock listSinceBlock(final String blockHash, final int targetConfirmations) throws BitcoinException {
        return new TransactionsSinceBlockImpl((Map)this.query("listsinceblock", blockHash, targetConfirmations));
    }

    @Override
    public List<Transaction> listTransactions() throws BitcoinException {
        return new TransactionListMapWrapper((List<Map>)this.query("listtransactions", new Object[0]));
    }

    @Override
    public List<Transaction> listTransactions(final String account) throws BitcoinException {
        return new TransactionListMapWrapper((List<Map>)this.query("listtransactions", account));
    }

    @Override
    public List<Transaction> listTransactions(final String account, final int count) throws BitcoinException {
        return new TransactionListMapWrapper((List<Map>)this.query("listtransactions", account, count));
    }

    @Override
    public List<Transaction> listTransactions(final String account, final int count, final int from) throws BitcoinException {
        return new TransactionListMapWrapper((List<Map>)this.query("listtransactions", account, count, from));
    }

    @Override
    public List<Unspent> listUnspent() throws BitcoinException {
        return new UnspentListWrapper((List<Map>)this.query("listunspent", new Object[0]));
    }

    @Override
    public List<Unspent> listUnspent(final int minConf) throws BitcoinException {
        return new UnspentListWrapper((List<Map>)this.query("listunspent", minConf));
    }

    @Override
    public List<Unspent> listUnspent(final int minConf, final int maxConf) throws BitcoinException {
        return new UnspentListWrapper((List<Map>)this.query("listunspent", minConf, maxConf));
    }

    @Override
    public List<Unspent> listUnspent(final int minConf, final int maxConf, final String... addresses) throws BitcoinException {
        return new UnspentListWrapper((List<Map>)this.query("listunspent", minConf, maxConf, addresses));
    }

    @Override
    public String sendFrom(final String fromAccount, final String toBitcoinAddress, final double amount) throws BitcoinException {
        return (String)this.query("sendfrom", fromAccount, toBitcoinAddress, amount);
    }

    @Override
    public String sendFrom(final String fromAccount, final String toBitcoinAddress, final double amount, final int minConf) throws BitcoinException {
        return (String)this.query("sendfrom", fromAccount, toBitcoinAddress, amount, minConf);
    }

    @Override
    public String sendFrom(final String fromAccount, final String toBitcoinAddress, final double amount, final int minConf, final String comment) throws BitcoinException {
        return (String)this.query("sendfrom", fromAccount, toBitcoinAddress, amount, minConf, comment);
    }

    @Override
    public String sendFrom(final String fromAccount, final String toBitcoinAddress, final double amount, final int minConf, final String comment, final String commentTo) throws BitcoinException {
        return (String)this.query("sendfrom", fromAccount, toBitcoinAddress, amount, minConf, comment, commentTo);
    }

    @Override
    public String sendMany(final String fromAccount, final List<TxOutput> outputs) throws BitcoinException {
        final Map<String, Double> pOutputs = new LinkedHashMap<String, Double>();
        for (final TxOutput txOutput : outputs) {
            final Double oldValue;
            if ((oldValue = pOutputs.put(txOutput.address(), txOutput.amount())) != null) {
                pOutputs.put(txOutput.address(), BitcoinUtil.normalizeAmount(oldValue + txOutput.amount()));
            }
        }
        return (String)this.query("sendmany", fromAccount, pOutputs);
    }

    @Override
    public String sendMany(final String fromAccount, final List<TxOutput> outputs, final int minConf) throws BitcoinException {
        final Map<String, Double> pOutputs = new LinkedHashMap<String, Double>();
        for (final TxOutput txOutput : outputs) {
            final Double oldValue;
            if ((oldValue = pOutputs.put(txOutput.address(), txOutput.amount())) != null) {
                pOutputs.put(txOutput.address(), BitcoinUtil.normalizeAmount(oldValue + txOutput.amount()));
            }
        }
        return (String)this.query("sendmany", fromAccount, pOutputs, minConf);
    }

    @Override
    public String sendMany(final String fromAccount, final List<TxOutput> outputs, final int minConf, final String comment) throws BitcoinException {
        final Map<String, Double> pOutputs = new LinkedHashMap<String, Double>();
        for (final TxOutput txOutput : outputs) {
            final Double oldValue;
            if ((oldValue = pOutputs.put(txOutput.address(), txOutput.amount())) != null) {
                pOutputs.put(txOutput.address(), BitcoinUtil.normalizeAmount(oldValue + txOutput.amount()));
            }
        }
        return (String)this.query("sendmany", fromAccount, pOutputs, minConf, comment);
    }

    @Override
    public String sendRawTransaction(final String hex) throws BitcoinException {
        return (String)this.query("sendrawtransaction", hex);
    }

    @Override
    public String sendToAddress(final String toAddress, final double amount) throws BitcoinException {
        return (String)this.query("sendtoaddress", toAddress, amount);
    }

    @Override
    public String sendToAddress(final String toAddress, final double amount, final String comment) throws BitcoinException {
        return (String)this.query("sendtoaddress", toAddress, amount, comment);
    }

    @Override
    public String sendToAddress(final String toAddress, final double amount, final String comment, final String commentTo) throws BitcoinException {
        return (String)this.query("sendtoaddress", toAddress, amount, comment, commentTo);
    }

    @Override
    public String signMessage(final String address, final String message) throws BitcoinException {
        return (String)this.query("signmessage", address, message);
    }

    @Override
    public String signRawTransaction(final String hex) throws BitcoinException {
        final Map result = (Map)this.query("signrawtransaction", hex);
        if (((Boolean)result.get("complete")).booleanValue()) {
            return (String)result.get("hex");
        }
        throw new BitcoinException("Incomplete");
    }

    @Override
    public void walletLock() throws BitcoinException {
        final Object walletlock = this.query("walletlock", new Object[0]);
        System.out.println(walletlock);
    }

    @Override
    public void walletPassPhrase(final String pass, final Long unLockTime) throws BitcoinException {
        final Object walletpassphrase = this.query("walletpassphrase", pass, unLockTime);
        System.out.println(walletpassphrase);
    }

    @Override
    public void changePass(final String oldPass, final String newPass) throws BitcoinException {
        final Object changePass = this.query("walletpassphrasechange", oldPass, newPass);
        System.out.println(changePass);
    }

    @Override
    public void stop() throws BitcoinException {
        this.query("stop", new Object[0]);
    }

    @Override
    public AddressValidationResult validateAddress(final String address) throws BitcoinException {
        final Map validationResult = (Map)this.query("validateaddress", address);
        return new AddressValidationResult() {
            @Override
            public boolean isValid() {
                return ((Boolean)validationResult.get("isvalid")).booleanValue();
            }

            @Override
            public String address() {
                return (String)validationResult.get("address");
            }

            @Override
            public boolean isMine() {
                return ((Boolean)validationResult.get("ismine")).booleanValue();
            }

            @Override
            public boolean isScript() {
                return ((Boolean)validationResult.get("isscript")).booleanValue();
            }

            @Override
            public String pubKey() {
                return (String)validationResult.get("pubkey");
            }

            @Override
            public boolean isCompressed() {
                return ((Boolean)validationResult.get("iscompressed")).booleanValue();
            }

            @Override
            public String account() {
                return (String)validationResult.get("account");
            }

            @Override
            public String toString() {
                return validationResult.toString();
            }
        };
    }

    @Override
    public boolean verifyMessage(final String address, final String signature, final String message) throws BitcoinException {
        return (boolean)this.query("verifymessage", address, signature, message);
    }

    static {
        String user = "user";
        String password = "pass";
        String host = "localhost";
        String port = null;
        try {
            final File home = new File(System.getProperty("user.home"));
            File f;
            if (!(f = new File(home, ".bitcoin" + File.separatorChar + "bitcoin.conf")).exists() && !(f = new File(home, "AppData" + File.separatorChar + "Roaming" + File.separatorChar + "Bitcoin" + File.separatorChar + "bitcoin.conf")).exists()) {
                f = null;
            }
            if (f != null) {
                BitcoinJSONRPCClient.logger.fine("Bitcoin configuration file found");
                final Properties p = new Properties();
                final FileInputStream i = new FileInputStream(f);
                try {
                    p.load(i);
                }
                finally {
                    i.close();
                }
                user = p.getProperty("rpcuser", user);
                password = p.getProperty("rpcpassword", password);
                host = p.getProperty("rpcconnect", host);
                port = p.getProperty("rpcport", port);
            }
        }
        catch (Exception var14) {
            BitcoinJSONRPCClient.logger.log(Level.SEVERE, null, var14);
        }
        try {
            DEFAULT_JSONRPC_URL = new URL("http://" + user + ':' + password + "@" + host + ":" + ((port == null) ? "8332" : port) + "/");
            DEFAULT_JSONRPC_TESTNET_URL = new URL("http://" + user + ':' + password + "@" + host + ":" + ((port == null) ? "18332" : port) + "/");
        }
        catch (MalformedURLException var15) {
            throw new RuntimeException(var15);
        }
    }

    private class UnspentListWrapper extends ListMapWrapper<Unspent>
    {
        public UnspentListWrapper(final List<Map> var1) {
            super(var1);
        }

        @Override
        protected Unspent wrap(final Map m) {
            return new Unspent() {
                @Override
                public String txid() {
                    return MapWrapper.mapStr(m, "txid");
                }

                @Override
                public int vout() {
                    return MapWrapper.mapInt(m, "vout");
                }

                @Override
                public String address() {
                    return MapWrapper.mapStr(m, "address");
                }

                @Override
                public String scriptPubKey() {
                    return MapWrapper.mapStr(m, "scriptPubKey");
                }

                @Override
                public String account() {
                    return MapWrapper.mapStr(m, "account");
                }

                @Override
                public double amount() {
                    return MapWrapper.mapDouble(m, "amount");
                }

                @Override
                public int confirmations() {
                    return MapWrapper.mapInt(m, "confirmations");
                }
            };
        }
    }

    private class TransactionsSinceBlockImpl implements TransactionsSinceBlock
    {
        public final List<Transaction> transactions;
        public final String lastBlock;

        public TransactionsSinceBlockImpl(final Map r) {
            this.transactions = new TransactionListMapWrapper((List<Map>)r.get("transactions"));
            this.lastBlock = (String)r.get("lastblock");
        }

        @Override
        public List<Transaction> transactions() {
            return this.transactions;
        }

        @Override
        public String lastBlock() {
            return this.lastBlock;
        }
    }

    private class TransactionListMapWrapper extends ListMapWrapper<Transaction>
    {
        public TransactionListMapWrapper(final List<Map> var1) {
            super(var1);
        }

        @Override
        protected Transaction wrap(final Map m) {
            return new Transaction() {
                private RawTransaction raw = null;

                @Override
                public String account() {
                    return MapWrapper.mapStr(m, "account");
                }

                @Override
                public String address() {
                    return MapWrapper.mapStr(m, "address");
                }

                @Override
                public String category() {
                    return MapWrapper.mapStr(m, "category");
                }

                @Override
                public double amount() {
                    return MapWrapper.mapDouble(m, "amount");
                }

                @Override
                public double fee() {
                    return MapWrapper.mapDouble(m, "fee");
                }

                @Override
                public int confirmations() {
                    return MapWrapper.mapInt(m, "confirmations");
                }

                @Override
                public String blockHash() {
                    return MapWrapper.mapStr(m, "blockhash");
                }

                @Override
                public int blockIndex() {
                    return MapWrapper.mapInt(m, "blockindex");
                }

                @Override
                public Date blockTime() {
                    return MapWrapper.mapCTime(m, "blocktime");
                }

                @Override
                public String txId() {
                    return MapWrapper.mapStr(m, "txid");
                }

                @Override
                public Date time() {
                    return MapWrapper.mapCTime(m, "time");
                }

                @Override
                public Date timeReceived() {
                    return MapWrapper.mapCTime(m, "timereceived");
                }

                @Override
                public String comment() {
                    return MapWrapper.mapStr(m, "comment");
                }

                @Override
                public String commentTo() {
                    return MapWrapper.mapStr(m, "to");
                }

                @Override
                public RawTransaction raw() {
                    if (this.raw == null) {
                        try {
                            this.raw = BitcoinJSONRPCClient.this.getRawTransaction(this.txId());
                        }
                        catch (BitcoinException var2) {
                            throw new RuntimeException(var2);
                        }
                    }
                    return this.raw;
                }

                @Override
                public String toString() {
                    return m.toString();
                }
            };
        }
    }

    private static class ReceivedAddressListWrapper extends AbstractList<ReceivedAddress>
    {
        private final List<Map<String, Object>> wrappedList;

        public ReceivedAddressListWrapper(final List<Map<String, Object>> wrappedList) {
            this.wrappedList = wrappedList;
        }

        @Override
        public ReceivedAddress get(final int index) {
            final Map<String, Object> e = this.wrappedList.get(index);
            return new ReceivedAddress() {
                public String address()
                {
                    return (String)e.get("address");
                }

                public String account()
                {
                    return (String)e.get("account");
                }

                public double amount()
                {
                    return ((Number)e.get("amount")).doubleValue();
                }

                public int confirmations()
                {
                    return ((Number)e.get("confirmations")).intValue();
                }

                public String toString()
                {
                    return e.toString();
                }
            };
        }

        @Override
        public int size() {
            return this.wrappedList.size();
        }
    }

    private class RawTransactionImpl extends MapWrapper implements RawTransaction
    {
        public RawTransactionImpl(final Map<String, Object> var1) {
            super(var1);
        }

        @Override
        public String hex() {
            return this.mapStr("hex");
        }

        @Override
        public String txId() {
            return this.mapStr("txid");
        }

        @Override
        public int version() {
            return this.mapInt("version");
        }

        @Override
        public long lockTime() {
            return this.mapLong("locktime");
        }

        @Override
        public List<In> vIn() {
            final List<Map<String, Object>> vIn = (List)this.m.get("vin");
            return new AbstractList<In>() {
                @Override
                public In get(final int index) {
                    return new InImpl(vIn.get(index));
                }

                @Override
                public int size() {
                    return vIn.size();
                }
            };
        }

        @Override
        public List<Out> vOut() {
            final List<Map<String, Object>> vOut = (List)this.m.get("vout");
            return new AbstractList<Out>() {
                @Override
                public Out get(final int index) {
                    return new OutImpl(vOut.get(index));
                }

                @Override
                public int size() {
                    return vOut.size();
                }
            };
        }

        @Override
        public String blockHash() {
            return this.mapStr("blockhash");
        }

        @Override
        public int confirmations() {
            return this.mapInt("confirmations");
        }

        @Override
        public Date time() {
            return this.mapCTime("time");
        }

        @Override
        public Date blocktime() {
            return this.mapCTime("blocktime");
        }

        private class OutImpl extends MapWrapper implements Out
        {
            public OutImpl(final Map m) {
                super(m);
            }

            @Override
            public double value() {
                return this.mapDouble("value");
            }

            @Override
            public int n() {
                return this.mapInt("n");
            }

            @Override
            public ScriptPubKey scriptPubKey() {
                return new ScriptPubKeyImpl((Map)this.m.get("scriptPubKey"));
            }

            @Override
            public TxInput toInput() {
                return new BasicTxInput(this.transaction().txId(), this.n());
            }

            @Override
            public RawTransaction transaction() {
                return RawTransactionImpl.this;
            }

            private class ScriptPubKeyImpl extends MapWrapper implements ScriptPubKey
            {
                public ScriptPubKeyImpl(final Map m) {
                    super(m);
                }

                @Override
                public String asm() {
                    return this.mapStr("asm");
                }

                @Override
                public String hex() {
                    return this.mapStr("hex");
                }

                @Override
                public int reqSigs() {
                    return this.mapInt("reqSigs");
                }

                @Override
                public String type() {
                    return this.mapStr(this.type());
                }

                @Override
                public List<String> addresses() {
                    return (List)this.m.get("addresses");
                }
            }
        }

        private class InImpl extends MapWrapper implements In
        {
            public InImpl(final Map m) {
                super(m);
            }

            @Override
            public String txid() {
                return this.mapStr("txid");
            }

            @Override
            public int vout() {
                return this.mapInt("vout");
            }

            @Override
            public Map<String, Object> scriptSig() {
                return (Map)this.m.get("scriptSig");
            }

            @Override
            public long sequence() {
                return this.mapLong("sequence");
            }

            @Override
            public RawTransaction getTransaction() {
                try {
                    return BitcoinJSONRPCClient.this.getRawTransaction(this.mapStr("txid"));
                }
                catch (BitcoinException var2) {
                    throw new RuntimeException(var2);
                }
            }

            @Override
            public Out getTransactionOutput() {
                return this.getTransaction().vOut().get(this.mapInt("vout"));
            }
        }
    }

    private class PeerInfoMapWrapper extends MapWrapper implements PeerInfo
    {
        public PeerInfoMapWrapper(final Map m) {
            super(m);
        }

        @Override
        public String addr() {
            return this.mapStr("addr");
        }

        @Override
        public String services() {
            return this.mapStr("services");
        }

        @Override
        public int lastsend() {
            return this.mapInt("lastsend");
        }

        @Override
        public int lastrecv() {
            return this.mapInt("lastrecv");
        }

        @Override
        public int bytessent() {
            return this.mapInt("bytessent");
        }

        @Override
        public int bytesrecv() {
            return this.mapInt("bytesrecv");
        }

        @Override
        public int blocksrequested() {
            return this.mapInt("blocksrequested");
        }

        @Override
        public Date conntime() {
            return this.mapCTime("conntime");
        }

        @Override
        public int version() {
            return this.mapInt("version");
        }

        @Override
        public String subver() {
            return this.mapStr("subver");
        }

        @Override
        public boolean inbound() {
            return this.mapBool("inbound");
        }

        @Override
        public int startingheight() {
            return this.mapInt("startingheight");
        }

        @Override
        public int banscore() {
            return this.mapInt("banscore");
        }
    }

    private class MiningInfoMapWrapper extends MapWrapper implements MiningInfo
    {
        public MiningInfoMapWrapper(final Map m) {
            super(m);
        }

        @Override
        public int blocks() {
            return this.mapInt("blocks");
        }

        @Override
        public int currentblocksize() {
            return this.mapInt("currentblocksize");
        }

        @Override
        public int currentblocktx() {
            return this.mapInt("currentblocktx");
        }

        @Override
        public double difficulty() {
            return this.mapDouble("difficulty");
        }

        @Override
        public String errors() {
            return this.mapStr("errors");
        }

        @Override
        public int genproclimit() {
            return this.mapInt("genproclimit");
        }

        @Override
        public double networkhashps() {
            return this.mapDouble("networkhashps");
        }

        @Override
        public int pooledtx() {
            return this.mapInt("pooledtx");
        }

        @Override
        public boolean testnet() {
            return this.mapBool("testnet");
        }

        @Override
        public String chain() {
            return this.mapStr("chain");
        }

        @Override
        public boolean generate() {
            return this.mapBool("generate");
        }
    }

    private class InfoMapWrapper extends MapWrapper implements Info
    {
        public InfoMapWrapper(final Map m) {
            super(m);
        }

        @Override
        public int version() {
            return this.mapInt("version");
        }

        @Override
        public int protocolversion() {
            return this.mapInt("protocolversion");
        }

        @Override
        public int walletversion() {
            return this.mapInt("walletversion");
        }

        @Override
        public double balance() {
            return this.mapDouble("balance");
        }

        @Override
        public int blocks() {
            return this.mapInt("blocks");
        }

        @Override
        public int timeoffset() {
            return this.mapInt("timeoffset");
        }

        @Override
        public int connections() {
            return this.mapInt("connections");
        }

        @Override
        public String proxy() {
            return this.mapStr("proxy");
        }

        @Override
        public double difficulty() {
            return this.mapDouble("difficulty");
        }

        @Override
        public boolean testnet() {
            return this.mapBool("testnet");
        }

        @Override
        public int keypoololdest() {
            return this.mapInt("keypoololdest");
        }

        @Override
        public int keypoolsize() {
            return this.mapInt("keypoolsize");
        }

        @Override
        public int unlocked_until() {
            return this.mapInt("unlocked_until");
        }

        @Override
        public double paytxfee() {
            return this.mapDouble("paytxfee");
        }

        @Override
        public double relayfee() {
            return this.mapDouble("relayfee");
        }

        @Override
        public String errors() {
            return this.mapStr("errors");
        }
    }

    private class BlockMapWrapper extends MapWrapper implements Block
    {
        public BlockMapWrapper(final Map m) {
            super(m);
        }

        @Override
        public String hash() {
            return this.mapStr("hash");
        }

        @Override
        public int confirmations() {
            return this.mapInt("confirmations");
        }

        @Override
        public int size() {
            return this.mapInt("size");
        }

        @Override
        public int height() {
            return this.mapInt("height");
        }

        @Override
        public int version() {
            return this.mapInt("version");
        }

        @Override
        public String merkleRoot() {
            return this.mapStr("");
        }

        @Override
        public List<String> tx() {
            return (List)this.m.get("tx");
        }

        @Override
        public Date time() {
            return this.mapCTime("time");
        }

        @Override
        public long nonce() {
            return this.mapLong("nonce");
        }

        @Override
        public String bits() {
            return this.mapStr("bits");
        }

        @Override
        public double difficulty() {
            return this.mapDouble("difficulty");
        }

        @Override
        public String previousHash() {
            return this.mapStr("previousblockhash");
        }

        @Override
        public String nextHash() {
            return this.mapStr("nextblockhash");
        }

        @Override
        public Block previous() throws BitcoinException {
            return this.m.containsKey("previousblockhash") ? BitcoinJSONRPCClient.this.getBlock(this.previousHash()) : null;
        }

        @Override
        public Block next() throws BitcoinException {
            return this.m.containsKey("nextblockhash") ? BitcoinJSONRPCClient.this.getBlock(this.nextHash()) : null;
        }
    }
}
