package com.clg.wallet.wallet.neo.bean;

import java.util.*;

public class AssetState
{

    private int version;
    private String id;
    private String type;
    private List<AssetLanguage> name;
    private double amount;
    private double available;
    private int precision;
    private String owner;
    private String admin;
    private String issuer;
    private double expiration;
    private boolean frozen;

    public AssetState setVersion(int version)
    {
        this.version = version;return this;
    }

    public AssetState setId(String id)
    {
        this.id = id;return this;
    }

    public AssetState setType(String type)
    {
        this.type = type;return this;
    }

    public AssetState setName(List<AssetLanguage> name)
    {
        this.name = name;return this;
    }

    public AssetState setAmount(double amount)
    {
        this.amount = amount;return this;
    }

    public AssetState setAvailable(double available)
    {
        this.available = available;return this;
    }

    public AssetState setPrecision(int precision)
    {
        this.precision = precision;return this;
    }

    public AssetState setOwner(String owner)
    {
        this.owner = owner;return this;
    }

    public AssetState setAdmin(String admin)
    {
        this.admin = admin;return this;
    }

    public AssetState setIssuer(String issuer)
    {
        this.issuer = issuer;return this;
    }

    public AssetState setExpiration(double expiration)
    {
        this.expiration = expiration;return this;
    }

    public AssetState setFrozen(boolean frozen)
    {
        this.frozen = frozen;return this;
    }



    public int getVersion()
    {
        return this.version;
    }

    public String getId()
    {
        return this.id;
    }

    public String getType()
    {
        return this.type;
    }

    public List<AssetLanguage> getName()
    {
        return this.name;
    }

    public double getAmount()
    {
        return this.amount;
    }

    public double getAvailable()
    {
        return this.available;
    }

    public int getPrecision()
    {
        return this.precision;
    }

    public String getOwner()
    {
        return this.owner;
    }

    public String getAdmin()
    {
        return this.admin;
    }

    public String getIssuer()
    {
        return this.issuer;
    }

    public double getExpiration()
    {
        return this.expiration;
    }

    public boolean isFrozen()
    {
        return this.frozen;
    }
}
