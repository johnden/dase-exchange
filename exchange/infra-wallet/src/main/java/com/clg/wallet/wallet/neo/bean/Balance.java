package com.clg.wallet.wallet.neo.bean;

import java.math.*;

public class Balance
{
    private BigDecimal balance;
    private BigDecimal confirmed;

    public Balance setBalance(BigDecimal balance)
    {
        this.balance = balance;return this;
    }

    public Balance setConfirmed(BigDecimal confirmed)
    {
        this.confirmed = confirmed;return this;
    }



    public BigDecimal getBalance()
    {
        return this.balance;
    }

    public BigDecimal getConfirmed()
    {
        return this.confirmed;
    }
}
