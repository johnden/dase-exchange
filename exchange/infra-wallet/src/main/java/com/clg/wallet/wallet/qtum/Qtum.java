package com.clg.wallet.wallet.qtum;

import com.clg.wallet.wallet.bitcoin.*;

public interface Qtum extends Bitcoin
{
    double qtum_getbalance(String paramString1, String paramString2, int paramInt)
            throws BitcoinException;

     QtumSender qtum_send(String paramString1, String paramString2, String paramString3, double paramDouble, int paramInt)
            throws BitcoinException;
     interface QtumSender
    {
        public abstract String txid();

        public abstract String sender();

        public abstract String hash160();
    }
}
