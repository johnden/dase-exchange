package com.clg.wallet.enums;

public interface CoinType
{
    public static final String BTC = "btc";
    public static final String ETH = "eth";
    public static final String ETHTOKEN = "ethToken";
    public static final String ACT = "act";
    public static final String ACTTOKEN = "actToken";
    public static final String ETC = "etc";
    public static final String NEO = "neo";
    public static final String NEOTOKEN = "neoToken";
    public static final String XRP = "xrp";
    public static final String WCG = "wcg";
    public static final String BTC_TOKEN = "btcToken";
}
