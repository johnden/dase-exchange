package com.clg.wallet.bean;

public class AddressBean
{
    private String fileName;
    private String keystore;
    private String pwd;
    private String path;
    private String address;

    public AddressBean setFileName(String fileName)
    {
        this.fileName = fileName;return this;
    }

    public AddressBean setKeystore(String keystore)
    {
        this.keystore = keystore;return this;
    }

    public AddressBean setPwd(String pwd)
    {
        this.pwd = pwd;return this;
    }

    public AddressBean setPath(String path)
    {
        this.path = path;return this;
    }

    public AddressBean setAddress(String address)
    {
        this.address = address;return this;
    }

    public String getFileName()
    {
        return this.fileName;
    }

    public String getKeystore()
    {
        return this.keystore;
    }

    public String getPwd()
    {
        return this.pwd;
    }

    public String getPath()
    {
        return this.path;
    }

    public String getAddress()
    {
        return this.address;
    }
}
