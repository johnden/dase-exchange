package com.clg.wallet.help;

import org.web3j.protocol.*;
import java.math.*;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.tx.gas.*;
import java.io.*;
import org.web3j.protocol.exceptions.*;
import org.web3j.utils.*;
import org.web3j.crypto.*;
import org.web3j.protocol.core.*;
import org.web3j.tx.*;

public class Transfer extends ManagedTransaction
{
    public static final BigInteger GAS_LIMIT = BigInteger.valueOf(22000L);;
    
    public Transfer(final Web3j web3j, final TransactionManager transactionManager) {
        super(web3j, transactionManager);
    }
    
    private TransactionReceipt send(final String toAddress, final BigDecimal value, final Convert.Unit unit) throws IOException, InterruptedException, TransactionException {
        BigInteger gasPrice = this.requestCurrentGasPrice();
        if (gasPrice.compareTo(DefaultGasProvider.GAS_PRICE) < 0) {
            gasPrice = DefaultGasProvider.GAS_PRICE;
        }
        gasPrice = gasPrice.add(BigInteger.valueOf(5000000000L));
        return this.send(toAddress, value, unit, gasPrice, Transfer.GAS_LIMIT);
    }
    
    private TransactionReceipt send(final String toAddress, final BigDecimal value, final Convert.Unit unit, final Long priceNum) throws IOException, InterruptedException, TransactionException {
        BigInteger gasPrice = this.requestCurrentGasPrice();
        if (gasPrice.compareTo(DefaultGasProvider.GAS_PRICE) < 0) {
            gasPrice = DefaultGasProvider.GAS_PRICE;
        }
        gasPrice = gasPrice.add(BigInteger.valueOf(5000000000L));
        if (null != priceNum && priceNum > 0L) {
            gasPrice = gasPrice.add(BigInteger.valueOf(priceNum));
        }
        return this.send(toAddress, value, unit, gasPrice, Transfer.GAS_LIMIT);
    }
    
    private TransactionReceipt send(final String toAddress, final BigDecimal value, final Convert.Unit unit, final BigInteger gasPrice, final BigInteger gasLimit) throws IOException, InterruptedException, TransactionException {
        final BigDecimal weiValue = Convert.toWei(value, unit);
        if (!Numeric.isIntegerValue(weiValue)) {
            throw new UnsupportedOperationException("Non decimal Wei value provided: " + value + " " + unit.toString() + " = " + weiValue + " Wei");
        }
        final String resolvedAddress = this.ensResolver.resolve(toAddress);
        return this.send(resolvedAddress, "", weiValue.toBigIntegerExact(), gasPrice, gasLimit);
    }
    
    public static RemoteCall<TransactionReceipt> sendFunds(final Web3j web3j, final Credentials credentials, final String toAddress, final BigDecimal value, final Convert.Unit unit) throws InterruptedException, IOException, TransactionException {
        return sendFunds(web3j, credentials, toAddress, value, unit, 0L);
    }
    
    public static RemoteCall<TransactionReceipt> sendFunds(final Web3j web3j, final Credentials credentials, final String toAddress, final BigDecimal value, final Convert.Unit unit, final Long priceNume) throws InterruptedException, IOException, TransactionException {
        final TransactionManager transactionManager = new RawTransactionManager(web3j, credentials, 80, 15000);
        return new RemoteCall(() -> new Transfer(web3j, transactionManager).send(toAddress, value, unit, priceNume));
    }
    
    public RemoteCall<TransactionReceipt> sendFunds(final String toAddress, final BigDecimal value, final Convert.Unit unit) {
        return new RemoteCall(() -> this.send(toAddress, value, unit));
    }
    
    public RemoteCall<TransactionReceipt> sendFunds(final String toAddress, final BigDecimal value, final Convert.Unit unit, final BigInteger gasPrice, final BigInteger gasLimit) {
        return new RemoteCall(() -> this.send(toAddress, value, unit, gasPrice, gasLimit));
    }
}
