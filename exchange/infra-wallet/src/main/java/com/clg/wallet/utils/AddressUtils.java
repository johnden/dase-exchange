package com.clg.wallet.utils;

import org.bitcoinj.params.*;
import org.web3j.crypto.*;
import org.bitcoinj.core.*;

public class AddressUtils
{

    public static String createAddress()
    {
        try
        {
            ECKeyPair ecKeyPair = Keys.createEcKeyPair();
            String key = ecKeyPair.getPrivateKey().toString(16);
            ECKey ecKey = new ECKey();
            Address address = ecKey.toAddress(MainNetParams.get());
            System.out.println(address);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String[] args)
    {
        createAddress();
    }
}
