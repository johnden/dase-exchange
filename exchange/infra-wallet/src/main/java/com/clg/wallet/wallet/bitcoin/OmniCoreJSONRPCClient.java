package com.clg.wallet.wallet.bitcoin;

import java.util.logging.*;
import java.net.*;
import java.util.*;
import com.clg.wallet.utils.*;

public class OmniCoreJSONRPCClient extends BitcoinJSONRPCClient implements OmniCore
{
    private static final Logger logger = Logger.getLogger(OmniCoreJSONRPCClient.class.getCanonicalName());

    public OmniCoreJSONRPCClient(String rpcUrl)
            throws MalformedURLException
    {
        super(rpcUrl);
    }

    public OmniCoreJSONRPCClient(URL rpc)
    {
        super(rpc);
    }

    public OmniCore.OmniInfo omni_getinfo()
            throws BitcoinException
    {
        return new OmniInfoMapWrapper((Map)query("omni_getinfo", new Object[0]));
    }

    public OmniCore.OmniBalance omni_getbalance(String address, int propertyid)
            throws BitcoinException
    {
        return new OmniBalanceMapWrapper((Map)query("omni_getbalance", new Object[] { address, Integer.valueOf(propertyid) }));
    }

    public OmniCore.OmniTransaction omni_gettransaction(String txid)
            throws BitcoinException
    {
        return new OmniTransactionMapWrapper((Map)query("omni_gettransaction", new Object[] { txid }));
    }

    public String omni_send(String fromaddress, String toaddress, int propertyid, double amount)
            throws BitcoinException
    {
        return (String)query("omni_send", new Object[] { fromaddress, toaddress, Integer.valueOf(propertyid), amount + "" });
    }

    private class OmniTransactionMapWrapper
            extends MapWrapper
            implements OmniCore.OmniTransaction
    {
        public OmniTransactionMapWrapper(Map m)
        {
            super(m);
        }

        public String txid()
        {
            return mapStr("txid");
        }

        public String sendingaddress()
        {
            return mapStr("sendingaddress");
        }

        public String referenceaddress()
        {
            return mapStr("referenceaddress");
        }

        public boolean ismine()
        {
            return mapBool("ismine");
        }

        public int confirmations()
        {
            return mapInt("confirmations");
        }

        public double amount()
        {
            return Double.valueOf(mapStr("amount")).doubleValue();
        }

        public double fee()
        {
            return mapDouble("fee").doubleValue();
        }

        public long blocktime()
        {
            return mapLong("blocktime");
        }

        public boolean valid()
        {
            return mapBool("valid");
        }

        public int positioninblock()
        {
            return mapInt("positioninblock");
        }

        public int version()
        {
            return mapInt("version");
        }

        public int type_int()
        {
            return mapInt("type_int");
        }

        public String type()
        {
            return mapStr("typeType");
        }
    }

    private class OmniBalanceMapWrapper
            extends MapWrapper
            implements OmniCore.OmniBalance
    {
        public OmniBalanceMapWrapper(Map m)
        {
            super(m);
        }

        public double balance()
        {
            return Double.valueOf(mapStr("balance")).doubleValue();
        }

        public double reserved()
        {
            return Double.valueOf(mapStr("reserved")).doubleValue();
        }
    }

    private class OmniInfoMapWrapper
            extends MapWrapper
            implements OmniCore.OmniInfo
    {
        public OmniInfoMapWrapper(Map m)
        {
            super(m);
        }

        public int omnicoreversion_int()
        {
            return mapInt("omnicoreversion_int");
        }

        public String omnicoreversion()
        {
            return mapStr("omnicoreversion");
        }

        public String mastercoreversion()
        {
            return mapStr("mastercoreversion");
        }

        public String bitcoincoreversion()
        {
            return mapStr("bitcoincoreversion");
        }

        public String commitinfo()
        {
            return mapStr("commitinfo");
        }

        public int block()
        {
            return mapInt("block");
        }

        public long blocktime()
        {
            return mapLong("blocktime");
        }

        public int blocktransactions()
        {
            return mapInt("blocktransactions");
        }

        public int totaltransactions()
        {
            return mapInt("totaltransactions");
        }

        public String alerts()
        {
            return mapStr("alerts");
        }
    }
}
