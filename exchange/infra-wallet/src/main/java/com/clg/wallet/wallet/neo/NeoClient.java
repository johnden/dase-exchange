package com.clg.wallet.wallet.neo;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clg.wallet.bean.ClientBean;
import com.clg.wallet.bean.ResultDTO;
import com.clg.wallet.enums.ResultCode;
import com.clg.wallet.help.JsonRpcClient;
import com.clg.wallet.utils.BigDecimalUtils;
import com.clg.wallet.utils.GsonUtils;
import com.clg.wallet.utils.IntegerUtils;
import com.clg.wallet.utils.StringParser;
import com.clg.wallet.wallet.neo.bean.AccountState;
import com.clg.wallet.wallet.neo.bean.Address;
import com.clg.wallet.wallet.neo.bean.ApplicationLog;
import com.clg.wallet.wallet.neo.bean.Balance;
import com.clg.wallet.wallet.neo.bean.Block;
import com.clg.wallet.wallet.neo.bean.ItemTx;
import com.clg.wallet.wallet.neo.bean.NeoTx;
import com.clg.wallet.wallet.neo.bean.Notifications;
import com.clg.wallet.wallet.neo.bean.Peer;
import com.clg.wallet.wallet.neo.bean.State;
import com.clg.wallet.wallet.neo.bean.TxVout;
import com.clg.wallet.wallet.neo.bean.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Utils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

public class NeoClient implements Neo
{
    private static JsonRpcClient client ;
    private ClientBean mClientBean;
    private static Map<String, Integer> decimalMap = new HashMap();

    public NeoClient(ClientBean clientBean)
    {
        this.mClientBean = clientBean;
    }

    public ResultDTO dumpPrivKey(String address)
    {
        if (validateAddress(address)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "dumpprivkey", new String[] { address }, String.class));
        }
        throw new RuntimeException("dumpPrivKey地址格式错误");
    }

    public boolean validateAddress(String address)
    {
        if (address.startsWith("0x")) {
            address = address.substring(2);
        }
        if ((!StringUtils.isEmpty(address)) && (address.length() == 34) && (address.startsWith("A"))) {
            return true;
        }
        return false;
    }

    public boolean validateAssertId(String assertId)
    {
        if (assertId.startsWith("0x")) {
            assertId = assertId.substring(2);
        }
        if ((!StringUtils.isEmpty(assertId)) && ((assertId.length() == 64) || (assertId.length() == 40))) {
            return true;
        }
        return false;
    }

    public ResultDTO getAccountState(String address)
    {
        if (validateAddress(address)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getaccountstate", new String[] { address }, AccountState.class));
        }
        throw new RuntimeException("getAccountState地址格式错误");
    }

    public ResultDTO getBalance(String assetId)
    {
        if (validateAssertId(assetId)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getbalance", new String[] { assetId }, Balance.class));
        }
        throw new RuntimeException("getBalance资产id格式错误");
    }

    public ResultDTO getTokenBalance(String assetId, String address)
    {
        if (validateAssertId(assetId))
        {
            JSONArray jsonArray = new JSONArray();
            jsonArray.add(assetId);
            jsonArray.add("balanceOf");
            JSONObject hash160 = new JSONObject();
            hash160.put("typeType", "Hash160");
            hash160.put("value", addressToHash(address));
            JSONArray jsonArray1 = new JSONArray();
            jsonArray1.add(hash160);
            jsonArray.add(jsonArray1);
            JSONObject balanceObj = client.invoke(this.mClientBean, "invokefunction", jsonArray, JSONObject.class);
            String value = balanceObj.getJSONArray("stack").getJSONObject(0).getString("value");
            BigDecimal balance = BigDecimal.ZERO;
            if (StringUtils.isEmpty(value)) {
                return new ResultDTO().setResult(balance);
            }
            BigDecimal balanceDec = new BigDecimal(BigDecimalUtils.byteArrayToBig(value));
            if (balanceDec.compareTo(BigDecimal.ZERO) > 0) {
                balance = balanceDec.divide(BigDecimal.TEN.pow(((Integer)getTokenDecimals(assetId).getResult()).intValue()));
            }
            return new ResultDTO().setResult(balance);
        }
        throw new RuntimeException("getBalance资产id格式错误");
    }

    public ResultDTO changeBalance(BigInteger balanceBigInt, int decimals)
    {
        BigDecimal balance = new BigDecimal(balanceBigInt);
        if ((null != balance) && (balance.compareTo(BigDecimal.ZERO) > 0))
        {
            balance = balance.divide(BigDecimal.TEN.pow(decimals));
            balance = BigDecimalUtils.formatBigDecimal(balance, 2);
        }
        return new ResultDTO().setResult(balance);
    }

    public String addressToHash(String address)
    {
        byte[] code = Base58.decodeChecked(address);
        String hash = "0x" + StringParser.bytesToHexString(Utils.reverseBytes(code));
        hash = hash.substring(0, hash.length() - 2);
        return hash;
    }

    public String hashToAddress(String hash)
    {
        hash = "17" + hash;
        byte[] bytes = StringParser.hexStrToByteArray(hash);
        String check = StringParser.bytesToHexString(Sha256Hash.hashTwice(bytes)).substring(0, 8);
        hash = hash + check;
        return Base58.encode(StringParser.hexStrToByteArray(hash));
    }

    public ResultDTO getTokenDecimals(String assetId)
    {
        if (null != decimalMap.get(assetId)) {
            return new ResultDTO().setResult(decimalMap.get(assetId));
        }
        if (validateAssertId(assetId))
        {
            JSONArray jsonArray = new JSONArray();
            jsonArray.add(assetId);
            jsonArray.add("decimals");
            JSONArray jsonArray1 = new JSONArray();
            jsonArray.add(jsonArray1);
            JSONObject balanceObj = client.invoke(this.mClientBean, "invokefunction", jsonArray, JSONObject.class);
            String value = balanceObj.getJSONArray("stack").getJSONObject(0).getString("value");
            decimalMap.put(assetId, Integer.valueOf(value));
            return new ResultDTO().setResult(Integer.valueOf(value));
        }
        throw new RuntimeException("getBalance资产id格式错误");
    }

    public ResultDTO getBestBlockHash()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getbestblockhash", new String[] { "" }, String.class));
    }

    public ResultDTO getJsonBlock(String hashOrNumber)
    {
        if (IntegerUtils.isInt(hashOrNumber)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new Integer[] { Integer.valueOf(hashOrNumber), Integer.valueOf(1) }, Block.class));
        }
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new String[] { hashOrNumber, "1" }, Block.class));
    }

    public ResultDTO getRawBlock(String hashOrNumber)
    {
        if (IntegerUtils.isInt(hashOrNumber)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new Integer[] { Integer.valueOf(hashOrNumber) }, String.class));
        }
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new String[] { hashOrNumber }, String.class));
    }

    public ResultDTO getApplicationLog(String txid)
    {
        ApplicationLog applicationLog = client.invoke(this.mClientBean, "getapplicationlog", new String[] { txid }, ApplicationLog.class);
        if ((StringUtils.isEmpty(applicationLog.getVmstate())) || (applicationLog.getVmstate().contains("FAULT"))) {
            return new ResultDTO().setStatusCode(ResultCode.TYPE_MP5_ERROR.getCode());
        }
        ArrayList<Notifications> notifications = applicationLog.getNotifications();
        ArrayList<ItemTx> itemList = new ArrayList();
        for (Notifications item : notifications) {
            if ("Array".equalsIgnoreCase(item.getState().getType()))
            {
                ArrayList<Type> typeList = item.getState().getValue();
                if ((!CollectionUtils.isEmpty(typeList)) && (4 == typeList.size()))
                {
                    String value = (typeList.get(0)).getValue();
                    if (!"7472616e73666572".equalsIgnoreCase(value)) {
                        return new ResultDTO().setStatusCode(ResultCode.TYPE_ERROR.getCode());
                    }
                    String from = hashToAddress((typeList.get(1)).getValue());
                    String to = hashToAddress((typeList.get(2)).getValue());
                    Type type = typeList.get(3);
                    String typeInfo = type.getType();
                    String amount = "";
                    BigDecimal balanceDec = BigDecimal.ZERO;
                    if ("ByteArray".equalsIgnoreCase(typeInfo)) {
                        balanceDec = new BigDecimal(BigDecimalUtils.byteArrayToBig(type.getValue()));
                    } else {
                        balanceDec = new BigDecimal(type.getValue());
                    }
                    ItemTx itemTx = new ItemTx().setAmount(balanceDec).setTxid(txid).setFromAddress(from).setToAddress(to).setAssertId(item.getContract());
                    itemList.add(itemTx);
                }
            }
        }
        if (!CollectionUtils.isEmpty(itemList)) {
            return new ResultDTO().setResult(itemList);
        }
        return new ResultDTO().setStatusCode(ResultCode.TYPE_MP5_ERROR.getCode());
    }

    public ResultDTO getBlockCount()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblockcount", new String[] { "" }, Long.class));
    }

    public ResultDTO getBlockHash(Long number)
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblockhash", new String[] { "" }, String.class));
    }

    public ResultDTO getConnectionCount()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getconnectioncount", new String[] { "" }, Integer.class));
    }

    public ResultDTO getNewAddress()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getnewaddress", new String[] { "" }, String.class));
    }

    public ResultDTO getRawMempool()
    {
        JSONArray jsonArray = client.invoke(this.mClientBean, "getrawmempool", new String[] { "" }, JSONArray.class);
        if ((null == jsonArray) || (jsonArray.isEmpty())) {
            return new ResultDTO().setResult(new ArrayList());
        }
        return new ResultDTO().setResult(GsonUtils.convertList(jsonArray.toJSONString(), String.class));
    }

    public ResultDTO getJsonTransaction(String txid)
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getrawtransaction", new String[] { txid, "1" }, NeoTx.class));
    }

    public ResultDTO getRawTransaction(String txid)
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getrawtransaction", new String[] { txid }, String.class));
    }

    public ResultDTO getTxOut(String txId, Integer n)
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "gettxout", new String[] { txId, "" + n }, TxVout.class));
    }

    public ResultDTO getPeers()
    {
        JSONArray jsonArray = client.invoke(this.mClientBean, "getpeers", new String[] { "" }, JSONArray.class);
        if ((null == jsonArray) || (jsonArray.isEmpty())) {
            return new ResultDTO().setResult(new ArrayList());
        }
        return new ResultDTO().setResult(GsonUtils.convertList(jsonArray.toJSONString(), Peer.class));
    }

    public ResultDTO listAddress()
    {
        JSONArray jsonArray = client.invoke(this.mClientBean, "listaddress", new String[] { "" }, JSONArray.class);
        if ((null == jsonArray) || (jsonArray.isEmpty())) {
            return new ResultDTO().setResult(new ArrayList());
        }
        return new ResultDTO().setResult(GsonUtils.convertList(jsonArray.toJSONString(), Address.class));
    }

    public ResultDTO sendFrom(String assertId, String from, String to, BigDecimal value, BigDecimal fee)
    {
        value = BigDecimalUtils.formatBigDecimal(value);
        fee = BigDecimalUtils.formatBigDecimal(fee);
        if (isToken(assertId) ? checkTokenBalance(assertId, from, value) : checkBalance(assertId, value))
        {
            String[] array = null;
            if ((null != fee) && (fee.compareTo(BigDecimal.ZERO) > 0)) {
                array = new String[] { assertId, from, to, value + "", fee + "" };
            } else {
                array = new String[] { assertId, from, to, value + "" };
            }
            TxVout sendfrom = client.invoke(this.mClientBean, "sendfrom", array, TxVout.class);
            return new ResultDTO().setResult(sendfrom);
        }
        return new ResultDTO().setStatusCode(ResultCode.NOT_ENOUGH.getCode());
    }

    public ResultDTO sendRawTransaction(String raw)
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "sendrawtransaction", new String[] { raw }, Boolean.class));
    }

    public ResultDTO sendToAddress(String assertId, String to, BigDecimal value, BigDecimal fee, String changeAddress)
    {
        value = BigDecimalUtils.formatBigDecimal(value);
        fee = BigDecimalUtils.formatBigDecimal(fee);
        if ((isToken(assertId)) && (!StringUtils.isEmpty(changeAddress)) && (!checkTokenBalance(assertId, changeAddress, value))) {
            return new ResultDTO().setStatusCode(ResultCode.NOT_ENOUGH.getCode());
        }
        if ((!isToken(assertId)) && (!checkBalance(assertId, value))) {
            return new ResultDTO().setStatusCode(ResultCode.NOT_ENOUGH.getCode());
        }
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(assertId);
        jsonArray.add(to);
        jsonArray.add("" + value);
        if ((null != fee) && (fee.compareTo(BigDecimal.ZERO) > 0)) {
            jsonArray.add("" + fee.toString());
        }
        if (!StringUtils.isEmpty(changeAddress)) {
            jsonArray.add(changeAddress);
        }
        NeoTx sendfrom = client.invoke(this.mClientBean, "sendtoaddress", jsonArray, NeoTx.class);
        return new ResultDTO().setResult(sendfrom);
    }

    public boolean checkTokenBalance(String assertId, String address, BigDecimal value)
    {
        ResultDTO result = getTokenBalance(assertId, address);
        Balance balance = (Balance)result.getResult();
        BigDecimal confirmed = balance.getConfirmed();
        if ((null != confirmed) && (null != value) && (confirmed.compareTo(value) >= 0)) {
            return true;
        }
        return false;
    }

    public boolean checkBalance(String assertId, BigDecimal value)
    {
        ResultDTO result = getBalance(assertId);
        Balance balance = (Balance)result.getResult();
        BigDecimal confirmed = balance.getConfirmed();
        if ((null != confirmed) && (null != value) && (confirmed.compareTo(value) >= 0)) {
            return true;
        }
        return false;
    }

    public boolean isToken(String assertId)
    {
        assertId = assertId.startsWith("0x") ? assertId.substring(2) : assertId;
        return assertId.length() == 40;
    }

    public static void main(String[] args) {}
}
