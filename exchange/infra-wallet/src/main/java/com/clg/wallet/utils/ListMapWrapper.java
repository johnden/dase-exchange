package com.clg.wallet.utils;

import java.util.*;

public abstract class ListMapWrapper<X> extends AbstractList<X>
{
    public List<Map> list;

    public ListMapWrapper(List<Map> list)
    {
        this.list = list;
    }

    protected abstract X wrap(Map paramMap);

    public X get(int index)
    {
        return (X)wrap((Map)this.list.get(index));
    }

    public int size()
    {
        return this.list.size();
    }
}
