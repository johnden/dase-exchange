package com.clg.wallet.utils;

import java.util.*;
import java.io.*;

public class CrippledJavaScriptParser
{
    private static Keyword[] keywords;

    private static boolean isDigit(final char ch) {
        return ch >= '0' && ch <= '9';
    }

    private static boolean isIdStart(final char ch) {
        return ch == '_' || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    private static boolean isId(final char ch) {
        return isDigit(ch) || ch == '_' || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    private static Object parseJSString(final StringParser jsString, final char delim) {
        final StringBuilder b = new StringBuilder();
        while (!jsString.isEmpty()) {
            final char sc = jsString.poll();
            if (sc == '\\') {
                final char cc = jsString.poll();
                switch (cc) {
                    case 'b': {
                        b.append('\b');
                        continue;
                    }
                    default: {
                        b.append(cc);
                        continue;
                    }
                    case 'f': {
                        b.append('\f');
                        continue;
                    }
                    case 'n': {
                        b.append('\n');
                        continue;
                    }
                    case 'r': {
                        b.append('\r');
                        continue;
                    }
                    case 't': {
                        b.append('\t');
                        continue;
                    }
                    case 'u': {
                        try {
                            final char ec = (char)Integer.parseInt(jsString.peek(4), 16);
                            b.append(ec);
                            jsString.forward(4);
                        }
                        catch (NumberFormatException var6) {
                            b.append("\\u");
                        }
                        continue;
                    }
                }
            }
            else {
                if (sc == delim) {
                    break;
                }
                b.append(sc);
            }
        }
        return b.toString();
    }

    private static List parseJSArray(final StringParser jsArray) {
        final ArrayList rv = new ArrayList();
        jsArray.trim();
        if (jsArray.peek() == ']') {
            jsArray.forward(1);
            return rv;
        }
        while (!jsArray.isEmpty()) {
            rv.add(parseJSExpr(jsArray));
            jsArray.trim();
            if (!jsArray.isEmpty()) {
                final char ch = jsArray.poll();
                if (ch == ']') {
                    return rv;
                }
                if (ch != ',') {
                    throw new RuntimeException(jsArray.toString());
                }
                jsArray.trim();
            }
        }
        return rv;
    }

    private static String parseId(final StringParser jsId) {
        final StringBuilder b = new StringBuilder();
        b.append(jsId.poll());
        char ch;
        while (isId(ch = jsId.peek())) {
            b.append(ch);
            jsId.forward(1);
        }
        return b.toString();
    }

    private static HashMap parseJSHash(final StringParser jsHash) {
        final LinkedHashMap rv = new LinkedHashMap();
        jsHash.trim();
        if (jsHash.peek() == '}') {
            jsHash.forward(1);
            return rv;
        }
        while (!jsHash.isEmpty()) {
            Object key;
            if (isIdStart(jsHash.peek())) {
                key = parseId(jsHash);
            }
            else {
                key = parseJSExpr(jsHash);
            }
            jsHash.trim();
            if (jsHash.isEmpty()) {
                throw new IllegalArgumentException();
            }
            if (jsHash.peek() != ':') {
                throw new RuntimeException(jsHash.toString());
            }
            jsHash.forward(1);
            jsHash.trim();
            final Object value = parseJSExpr(jsHash);
            jsHash.trim();
            if (!jsHash.isEmpty()) {
                final char ch = jsHash.poll();
                if (ch == '}') {
                    rv.put(key, value);
                    return rv;
                }
                if (ch != ',') {
                    throw new RuntimeException(jsHash.toString());
                }
                jsHash.trim();
            }
            rv.put(key, value);
        }
        return rv;
    }

    public static Object parseJSExpr(final StringParser jsExpr) {
        if (jsExpr.isEmpty()) {
            throw new IllegalArgumentException();
        }
        jsExpr.trim();
        final char start = jsExpr.poll();
        if (start == '[') {
            return parseJSArray(jsExpr);
        }
        if (start == '{') {
            return parseJSHash(jsExpr);
        }
        if (start == '\'' || start == '\"') {
            return parseJSString(jsExpr, start);
        }
        if (isDigit(start) || start == '-' || start == '+') {
            final StringBuilder b = new StringBuilder();
            if (start != '+') {
                b.append(start);
            }
            char psc = '\0';
            boolean exp = false;
            boolean dot = false;
            while (!jsExpr.isEmpty()) {
                final char sc = jsExpr.peek();
                if (!isDigit(sc)) {
                    if (sc == 'E' || sc == 'e') {
                        if (exp) {
                            throw new NumberFormatException(b.toString() + jsExpr.toString());
                        }
                        exp = true;
                    }
                    else if (sc == '.') {
                        if (dot || exp) {
                            throw new NumberFormatException(b.toString() + jsExpr.toString());
                        }
                        dot = true;
                    }
                    else {
                        if (sc != '-' && sc != '+') {
                            break;
                        }
                        if (psc != 'E') {
                            if (psc != 'e') {
                                break;
                            }
                        }
                    }
                }
                b.append(sc);
                jsExpr.forward(1);
                psc = sc;
            }
            return (!dot && !exp) ? ((double)Long.parseLong(b.toString())) : Double.parseDouble(b.toString());
        }
        final Keyword[] var7 = CrippledJavaScriptParser.keywords;
        final int var8 = var7.length;
        int var9 = 0;
        while (var9 < var8) {
            final Keyword keyword = var7[var9];
            final int keywordlen = keyword.keywordFromSecond.length();
            if (start == keyword.firstChar && jsExpr.peek(keywordlen).equals(keyword.keywordFromSecond)) {
                if (jsExpr.length() == keyword.keywordFromSecond.length()) {
                    jsExpr.forward(keyword.keywordFromSecond.length());
                    return keyword.value;
                }
                if (!isId(jsExpr.charAt(keyword.keywordFromSecond.length()))) {
                    jsExpr.forward(keyword.keywordFromSecond.length());
                    jsExpr.trim();
                    return keyword.value;
                }
                throw new IllegalArgumentException(jsExpr.toString());
            }
            else {
                ++var9;
            }
        }
        if (start != 'n' || !jsExpr.peek("ew Date(".length()).equals("ew Date(")) {
            throw new UnsupportedOperationException("Unparsable javascript expression: \"" + start + jsExpr + "\"");
        }
        jsExpr.forward("ew Date(".length());
        final Number date = (Number)parseJSExpr(jsExpr);
        jsExpr.trim();
        if (jsExpr.poll() != ')') {
            throw new RuntimeException("Invalid date");
        }
        return new Date(date.longValue());
    }

    public static Object parseJSExpr(final String jsExpr) {
        return parseJSExpr(new StringParser(jsExpr));
    }

    public static LinkedHashMap<String, Object> parseJSVars(final String javascript) {
        try {
            final BufferedReader r = new BufferedReader(new StringReader(javascript));
            final LinkedHashMap rv = new LinkedHashMap();
            String l;
            while ((l = r.readLine()) != null) {
                l = l.trim();
                if (!l.isEmpty() && l.startsWith("var")) {
                    l = l.substring(3).trim();
                    final int i = l.indexOf(61);
                    if (i == -1) {
                        continue;
                    }
                    final String varName = l.substring(0, i).trim();
                    String expr = l.substring(i + 1).trim();
                    if (expr.endsWith(";")) {
                        expr = expr.substring(0, expr.length() - 1).trim();
                    }
                    rv.put(varName, parseJSExpr(expr));
                }
            }
            return (LinkedHashMap<String, Object>)rv;
        }
        catch (IOException var7) {
            throw new RuntimeException(var7);
        }
    }

    static {
        CrippledJavaScriptParser.keywords = new Keyword[] { new Keyword("null", null), new Keyword("true", Boolean.TRUE), new Keyword("false", Boolean.FALSE) };
    }

    private static class Keyword
    {
        public final String keyword;
        public final Object value;
        public final char firstChar;
        public final String keywordFromSecond;

        public Keyword(final String keyword, final Object value) {
            this.keyword = keyword;
            this.value = value;
            this.firstChar = keyword.charAt(0);
            this.keywordFromSecond = keyword.substring(1);
        }
    }
}
