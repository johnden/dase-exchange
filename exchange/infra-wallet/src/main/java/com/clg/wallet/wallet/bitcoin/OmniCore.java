package com.clg.wallet.wallet.bitcoin;

public interface OmniCore extends Bitcoin
{
    public static final int PROPERTYID_USDT = 31;

    OmniInfo omni_getinfo()
            throws BitcoinException;

    OmniBalance omni_getbalance(String paramString, int paramInt)
            throws BitcoinException;

    OmniTransaction omni_gettransaction(String paramString)
            throws BitcoinException;

    String omni_send(String paramString1, String paramString2, int paramInt, double paramDouble)
            throws BitcoinException;

    public static abstract interface OmniInfo
    {
        int omnicoreversion_int();

        String omnicoreversion();

        String mastercoreversion();

        String bitcoincoreversion();

        String commitinfo();

        int block();

        long blocktime();

        int blocktransactions();

        int totaltransactions();

        String alerts();
    }

    public static abstract interface OmniBalance
    {
        double balance();

        double reserved();
    }

    public static abstract interface OmniTransaction
    {
        String txid();

        String sendingaddress();

        String referenceaddress();

        boolean ismine();

        int confirmations();

        double amount();

        double fee();

        long blocktime();

        boolean valid();

        int positioninblock();

        int version();

        int type_int();

        String type();
    }
}
