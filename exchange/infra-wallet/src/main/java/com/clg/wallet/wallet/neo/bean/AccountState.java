package com.clg.wallet.wallet.neo.bean;

import java.util.*;

public class AccountState
{
    private int version;
    private String script_hash;
    private boolean frozen;
    private List<AssertBalance> balances;

    public AccountState setVersion(int version)
    {
        this.version = version;return this;
    }

    public AccountState setScript_hash(String script_hash)
    {
        this.script_hash = script_hash;return this;
    }

    public AccountState setFrozen(boolean frozen)
    {
        this.frozen = frozen;return this;
    }

    public AccountState setBalances(List<AssertBalance> balances)
    {
        this.balances = balances;return this;
    }


    public int getVersion()
    {
        return this.version;
    }

    public String getScript_hash()
    {
        return this.script_hash;
    }

    public boolean isFrozen()
    {
        return this.frozen;
    }

    public List<AssertBalance> getBalances()
    {
        return this.balances;
    }
}
