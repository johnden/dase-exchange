package com.clg.wallet.bean;

import java.math.*;

public class TxData
{
    private String formAddress;
    private String toAddress;
    private String toPublickey;
    private String priKey;
    private String pass;
    private String tag;
    private BigDecimal balance;
    private String assertId;

    public TxData setFormAddress(String formAddress)
    {
        this.formAddress = formAddress;return this;
    }

    public TxData setToAddress(String toAddress)
    {
        this.toAddress = toAddress;return this;
    }

    public TxData setToPublickey(String toPublickey)
    {
        this.toPublickey = toPublickey;return this;
    }

    public TxData setPriKey(String priKey)
    {
        this.priKey = priKey;return this;
    }

    public TxData setPass(String pass)
    {
        this.pass = pass;return this;
    }

    public TxData setTag(String tag)
    {
        this.tag = tag;return this;
    }

    public TxData setBalance(BigDecimal balance)
    {
        this.balance = balance;return this;
    }

    public TxData setAssertId(String assertId)
    {
        this.assertId = assertId;return this;
    }

    public String getFormAddress()
    {
        return this.formAddress;
    }

    public String getToAddress()
    {
        return this.toAddress;
    }

    public String getToPublickey()
    {
        return this.toPublickey;
    }

    public String getPriKey()
    {
        return this.priKey;
    }

    public String getPass()
    {
        return this.pass;
    }

    public String getTag()
    {
        return this.tag;
    }

    public BigDecimal getBalance()
    {
        return this.balance;
    }

    public String getAssertId()
    {
        return this.assertId;
    }
}
