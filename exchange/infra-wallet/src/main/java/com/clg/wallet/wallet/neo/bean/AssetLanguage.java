package com.clg.wallet.wallet.neo.bean;

public class AssetLanguage
{
    private String lang;
    private String name;

    public AssetLanguage setLang(String lang)
    {
        this.lang = lang;return this;
    }

    public AssetLanguage setName(String name)
    {
        this.name = name;return this;
    }


    public String getLang()
    {
        return this.lang;
    }

    public String getName()
    {
        return this.name;
    }
}
