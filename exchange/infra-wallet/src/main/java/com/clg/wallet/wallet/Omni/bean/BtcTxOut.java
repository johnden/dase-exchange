package com.clg.wallet.wallet.Omni.bean;

import java.math.*;

public class BtcTxOut
{
    private BigDecimal value;
    private Integer n;
    private BtcScriptPubKey scriptPubKey;

    public BtcTxOut setValue(BigDecimal value)
    {
        this.value = value;return this;
    }

    public BtcTxOut setN(Integer n)
    {
        this.n = n;return this;
    }

    public BtcTxOut setScriptPubKey(BtcScriptPubKey scriptPubKey)
    {
        this.scriptPubKey = scriptPubKey;return this;
    }



    public BigDecimal getValue()
    {
        return this.value;
    }

    public Integer getN()
    {
        return this.n;
    }

    public BtcScriptPubKey getScriptPubKey()
    {
        return this.scriptPubKey;
    }
}
