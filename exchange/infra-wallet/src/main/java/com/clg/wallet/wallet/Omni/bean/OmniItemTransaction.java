package com.clg.wallet.wallet.Omni.bean;

import java.math.*;

public class OmniItemTransaction
{

    public String txid;
    public String fee;
    public String sendingaddress;
    public String referenceaddress;
    public boolean ismine;
    public int version;
    public int type_int;
    public String type;
    public int propertyid;
    public boolean divisible;
    public BigDecimal amount;
    public boolean valid;
    public String blockhash;
    public int blocktime;
    public int positioninblock;
    public int block;
    public int confirmations;

    public OmniItemTransaction setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public OmniItemTransaction setFee(String fee)
    {
        this.fee = fee;return this;
    }

    public OmniItemTransaction setSendingaddress(String sendingaddress)
    {
        this.sendingaddress = sendingaddress;return this;
    }

    public OmniItemTransaction setReferenceaddress(String referenceaddress)
    {
        this.referenceaddress = referenceaddress;return this;
    }

    public OmniItemTransaction setIsmine(boolean ismine)
    {
        this.ismine = ismine;return this;
    }

    public OmniItemTransaction setVersion(int version)
    {
        this.version = version;return this;
    }

    public OmniItemTransaction setType_int(int type_int)
    {
        this.type_int = type_int;return this;
    }

    public OmniItemTransaction setType(String type)
    {
        this.type = type;return this;
    }

    public OmniItemTransaction setPropertyid(int propertyid)
    {
        this.propertyid = propertyid;return this;
    }

    public OmniItemTransaction setDivisible(boolean divisible)
    {
        this.divisible = divisible;return this;
    }

    public OmniItemTransaction setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public OmniItemTransaction setValid(boolean valid)
    {
        this.valid = valid;return this;
    }

    public OmniItemTransaction setBlockhash(String blockhash)
    {
        this.blockhash = blockhash;return this;
    }

    public OmniItemTransaction setBlocktime(int blocktime)
    {
        this.blocktime = blocktime;return this;
    }

    public OmniItemTransaction setPositioninblock(int positioninblock)
    {
        this.positioninblock = positioninblock;return this;
    }

    public OmniItemTransaction setBlock(int block)
    {
        this.block = block;return this;
    }

    public OmniItemTransaction setConfirmations(int confirmations)
    {
        this.confirmations = confirmations;return this;
    }



    public String getTxid()
    {
        return this.txid;
    }

    public String getFee()
    {
        return this.fee;
    }

    public String getSendingaddress()
    {
        return this.sendingaddress;
    }

    public String getReferenceaddress()
    {
        return this.referenceaddress;
    }

    public boolean isIsmine()
    {
        return this.ismine;
    }

    public int getVersion()
    {
        return this.version;
    }

    public int getType_int()
    {
        return this.type_int;
    }

    public String getType()
    {
        return this.type;
    }

    public int getPropertyid()
    {
        return this.propertyid;
    }

    public boolean isDivisible()
    {
        return this.divisible;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public boolean isValid()
    {
        return this.valid;
    }

    public String getBlockhash()
    {
        return this.blockhash;
    }

    public int getBlocktime()
    {
        return this.blocktime;
    }

    public int getPositioninblock()
    {
        return this.positioninblock;
    }

    public int getBlock()
    {
        return this.block;
    }

    public int getConfirmations()
    {
        return this.confirmations;
    }
}
