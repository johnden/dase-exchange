package com.clg.wallet.wallet.bitshare;

public class BitSharesException extends Exception
{
    public BitSharesException() {}

    public BitSharesException(String msg)
    {
        super(msg);
    }

    public BitSharesException(Throwable cause)
    {
        super(cause);
    }

    public BitSharesException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
