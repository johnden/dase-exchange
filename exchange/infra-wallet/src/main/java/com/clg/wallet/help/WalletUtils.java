package com.clg.wallet.help;

import java.security.*;
import java.io.*;
import org.web3j.crypto.*;
import com.clg.wallet.utils.*;
import java.time.format.*;
import java.time.*;
import org.springframework.util.*;
import org.web3j.utils.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;

public class WalletUtils
{
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String generateFullNewWalletFile(String password, File destinationDirectory)
            throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, CipherException, IOException
    {
        return generateNewWalletFile(password, destinationDirectory, true);
    }

    public static String generateLightNewWalletFile(String password, File destinationDirectory)
            throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, CipherException, IOException
    {
        return generateNewWalletFile(password, destinationDirectory, false);
    }

    public static String generateNewWalletFile(String password, File destinationDirectory, boolean useFullScrypt)
            throws CipherException, IOException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException
    {
        ECKeyPair ecKeyPair = Keys.createEcKeyPair();
        return generateWalletFile(password, ecKeyPair, destinationDirectory, useFullScrypt);
    }

    public static String generateWalletFile(String password, ECKeyPair ecKeyPair, File destinationDirectory, boolean useFullScrypt)
            throws CipherException, IOException
    {
        WalletFile walletFile;
        if (useFullScrypt) {
            walletFile = Wallet.createStandard(password, ecKeyPair);
        } else {
            walletFile = Wallet.createLight(password, ecKeyPair);
        }
        String fileName = getWalletFileName(walletFile);
        File destination = new File(destinationDirectory, fileName);
        objectMapper.writeValue(destination, walletFile);
        return fileName;
    }

    public static Credentials loadCredentials(String password, String source)
            throws IOException, CipherException
    {
        WalletFile walletFile = null;
        if (StringParser.isJson(source))
        {
            walletFile = objectMapper.readValue(source, WalletFile.class);
            return Credentials.create(Wallet.decrypt(password, walletFile));
        }
        return loadCredentials(password, new File(source));
    }

    public static Credentials loadCredentials(String password, File source)
            throws IOException, CipherException
    {
        WalletFile walletFile = objectMapper.readValue(source, WalletFile.class);
        return Credentials.create(Wallet.decrypt(password, walletFile));
    }

    private static String getWalletFileName(WalletFile walletFile)
    {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("'UTC--'yyyy-MM-dd'T'HH-mm-ss.nVV'--'");
        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        return now.format(format) + walletFile.getAddress() + ".json";
    }

    public static String getDefaultKeyDirectory(String type)
    {
        return getDefaultKeyDirectory(System.getProperty("os.name"), type);
    }

    static String getDefaultKeyDirectory(String osName1, String type)
    {
        String osName = osName1.toLowerCase();
        if (osName.startsWith("mac")) {
            return String.format("%s%sdata%s" + type, new Object[] { System.getProperty("user.home"), File.separator, File.separator });
        }
        if (osName.contains("linux")) {
            return String.format("%s%sdata%s" + type, new Object[] { System.getProperty("user.home"), File.separator, File.separator });
        }
        return osName.startsWith("win") ? String.format("%s%" + type, new Object[] { System.getenv("APPDATA"), File.separator }) : String.format("%s%s." + type, new Object[] { System.getProperty("user.home"), File.separator });
    }

    public static String getTestnetKeyDirectory()
    {
        return getTestnetKeyDirectory("Ethereum");
    }

    public static String getMainnetKeyDirectory()
    {
        return getMainnetKeyDirectory("Ethereum");
    }

    public static String getTestnetKeyDirectory(String type)
    {
        if (StringUtils.isEmpty(type)) {
            type = "Ethereum";
        }
        return String.format("%s%stestnet%s", new Object[] { getDefaultKeyDirectory(type), File.separator, File.separator });
    }

    public static String getMainnetKeyDirectory(String type)
    {
        if (StringUtils.isEmpty(type)) {
            type = "Ethereum";
        }
        return String.format("%s%s", new Object[] { getDefaultKeyDirectory(type), File.separator });
    }

    public static boolean isValidPrivateKey(String privateKey)
    {
        String cleanPrivateKey = Numeric.cleanHexPrefix(privateKey);
        return cleanPrivateKey.length() == 64;
    }

    public static boolean isValidAddress(String input)
    {
        String cleanInput = Numeric.cleanHexPrefix(input);
        try
        {
            Numeric.toBigIntNoPrefix(cleanInput);
        }
        catch (NumberFormatException var3)
        {
            return false;
        }
        return cleanInput.length() == 40;
    }

    static
    {
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
    }
}
