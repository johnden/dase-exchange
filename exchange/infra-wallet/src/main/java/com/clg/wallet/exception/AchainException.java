package com.clg.wallet.exception;

public class AchainException extends RuntimeException {


    public AchainException() {}

    public AchainException(String message)
    {
        super(message);
    }

    public AchainException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public AchainException(Throwable cause)
    {
        super(cause);
    }

    protected AchainException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}