package com.clg.wallet.newclient;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clg.wallet.bean.ClientBean;
import com.clg.wallet.bean.ResultDTO;
import com.clg.wallet.bean.TxData;
import com.clg.wallet.enums.ResultCode;
import com.clg.wallet.help.JsonRpcClient;
import com.clg.wallet.utils.BigDecimalUtils;
import com.clg.wallet.utils.GsonUtils;
import com.clg.wallet.utils.HttpRequestUtil;
import com.clg.wallet.utils.IntegerUtils;
import com.clg.wallet.utils.StringParser;
import com.clg.wallet.wallet.neo.bean.AccountState;
import com.clg.wallet.wallet.neo.bean.Address;
import com.clg.wallet.wallet.neo.bean.ApplicationLog;
import com.clg.wallet.wallet.neo.bean.Balance;
import com.clg.wallet.wallet.neo.bean.Block;
import com.clg.wallet.wallet.neo.bean.ItemTx;
import com.clg.wallet.wallet.neo.bean.NeoTx;
import com.clg.wallet.wallet.neo.bean.Notifications;
import com.clg.wallet.wallet.neo.bean.Peer;
import com.clg.wallet.wallet.neo.bean.State;
import com.clg.wallet.wallet.neo.bean.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Utils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

public class NeoNewClient extends NormalClient
{
    private static Map<String, Integer> decimalMap = new HashMap();
    private String neoUrl = "https://neoscan.io/api/main_net/v1/get_height";
    private static JsonRpcClient client = JsonRpcClient.getInstance();

    public NeoNewClient(ClientBean clientBean)
    {
        super(clientBean);
    }

    public ResultDTO getNewAddress()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getnewaddress", new String[] { "" }, String.class));
    }

    public ResultDTO getNewAddress(String tag)
    {
        return null;
    }

    public ResultDTO getBalance()
    {
        throw new RuntimeException("neo不包含此方法");
    }

    public ResultDTO getBalance(String assetId)
    {
        if (validateAssertId(assetId)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getbalance", new String[] { assetId }, Balance.class));
        }
        throw new RuntimeException("getBalance资产id格式错误");
    }

    public ResultDTO getBalanceByAccount(String address)
    {
        return null;
    }

    public ResultDTO getTokenBalance(String assetId, String address)
    {
        if (validateAssertId(assetId))
        {
            JSONArray jsonArray = new JSONArray();
            jsonArray.add(assetId);
            jsonArray.add("balanceOf");
            JSONObject hash160 = new JSONObject();
            hash160.put("typeType", "Hash160");
            hash160.put("value", addressToHash(address));
            JSONArray jsonArray1 = new JSONArray();
            jsonArray1.add(hash160);
            jsonArray.add(jsonArray1);
            JSONObject balanceObj = client.invoke(this.mClientBean, "invokefunction", jsonArray, JSONObject.class);
            String value = balanceObj.getJSONArray("stack").getJSONObject(0).getString("value");
            BigDecimal balance = BigDecimal.ZERO;
            if (StringUtils.isEmpty(value)) {
                return new ResultDTO().setResult(balance);
            }
            BigDecimal balanceDec = new BigDecimal(BigDecimalUtils.byteArrayToBig(value));
            if (balanceDec.compareTo(BigDecimal.ZERO) > 0) {
                balance = balanceDec.divide(BigDecimal.TEN.pow(((Integer)getTokenDecimals(assetId).getResult()).intValue()));
            }
            return new ResultDTO().setResult(balance);
        }
        throw new RuntimeException("getBalance资产id格式错误");
    }

    public ResultDTO getListAddress()
    {
        JSONArray jsonArray = client.invoke(this.mClientBean, "listaddress", new String[] { "" }, JSONArray.class);
        if ((null == jsonArray) || (jsonArray.isEmpty())) {
            return new ResultDTO().setResult(new ArrayList());
        }
        return new ResultDTO().setResult(GsonUtils.convertList(jsonArray.toJSONString(), Address.class));
    }

    public ResultDTO getPeers()
    {
        JSONArray jsonArray = client.invoke(this.mClientBean, "getpeers", new String[] { "" }, JSONArray.class);
        if ((null == jsonArray) || (jsonArray.isEmpty())) {
            return new ResultDTO().setResult(new ArrayList());
        }
        return new ResultDTO().setResult(GsonUtils.convertList(jsonArray.toJSONString(), Peer.class));
    }

    public ResultDTO getBlockCount()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblockcount", new String[] { "" }, Long.class));
    }

    public ResultDTO getBlockHash(long blockNumber)
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblockhash", new String[] { "" }, String.class));
    }

    public ResultDTO getConnectionCount()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getconnectioncount", new String[] { "" }, Integer.class));
    }

    public ResultDTO dumpPrivKey(String address)
    {
        boolean isRight = ((Boolean)validateAddress(address).getResult()).booleanValue();
        if (isRight) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "dumpprivkey", new String[] { address }, String.class));
        }
        throw new RuntimeException("dumpPrivKey地址格式错误");
    }

    public ResultDTO validateAddress(String address)
    {
        if (address.startsWith("0x")) {
            address = address.substring(2);
        }
        if ((!StringUtils.isEmpty(address)) && (address.length() == 34) && (address.startsWith("A"))) {
            return new ResultDTO().setResult(Boolean.valueOf(true));
        }
        return new ResultDTO().setResult(Boolean.valueOf(false));
    }

    public ResultDTO sendNormal(TxData tx)
    {
        throw new RuntimeException("neo不包含此方法");
    }

    public ResultDTO sendNormalToken(TxData tx)
    {
        return sendToAddress(tx.getAssertId(), tx.getToAddress(), tx.getBalance(), BigDecimal.ZERO, tx.getFormAddress());
    }

    public ResultDTO sendToAddress(String assertId, String to, BigDecimal value, BigDecimal fee, String changeAddress)
    {
        if ((null == value) || (value.compareTo(BigDecimal.ZERO) <= 0)) {
            throw new RuntimeException("转账金额为空或小于0");
        }
        value = BigDecimalUtils.formatBigDecimal(value);
        fee = BigDecimalUtils.formatBigDecimal(fee);
        if ((isToken(assertId)) && (!StringUtils.isEmpty(changeAddress)) && (!checkTokenBalance(assertId, changeAddress, value))) {
            return new ResultDTO().setStatusCode(ResultCode.NOT_ENOUGH.getCode());
        }
        if ((!isToken(assertId)) && (!checkBalance(assertId, value))) {
            return new ResultDTO().setStatusCode(ResultCode.NOT_ENOUGH.getCode());
        }
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(assertId);
        jsonArray.add(to);
        jsonArray.add("" + value);
        if ((null != fee) && (fee.compareTo(BigDecimal.ZERO) > 0)) {
            jsonArray.add("" + fee.toString());
        }
        if (!StringUtils.isEmpty(changeAddress)) {
            jsonArray.add(changeAddress);
        }
        NeoTx sendfrom = client.invoke(this.mClientBean, "sendtoaddress", jsonArray, NeoTx.class);
        return new ResultDTO().setResult(sendfrom);
    }

    public boolean checkTokenBalance(String assertId, String address, BigDecimal value)
    {
        ResultDTO result = getTokenBalance(assertId, address);
        Balance balance = (Balance)result.getResult();
        BigDecimal confirmed = balance.getConfirmed();
        if ((null != confirmed) && (null != value) && (confirmed.compareTo(value) >= 0)) {
            return true;
        }
        return false;
    }

    public boolean checkBalance(String assertId, BigDecimal value)
    {
        ResultDTO result = getBalance(assertId);
        Balance balance = (Balance)result.getResult();
        BigDecimal confirmed = balance.getConfirmed();
        if ((null != confirmed) && (null != value) && (confirmed.compareTo(value) >= 0)) {
            return true;
        }
        return false;
    }

    public boolean isToken(String assertId)
    {
        assertId = assertId.startsWith("0x") ? assertId.substring(2) : assertId;
        return assertId.length() == 40;
    }

    public ResultDTO getTxBlockNumber(String hashOrNumber)
    {
        if (IntegerUtils.isInt(hashOrNumber)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new Integer[] { Integer.valueOf(hashOrNumber), Integer.valueOf(1) }, Block.class));
        }
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new String[] { hashOrNumber, "1" }, Block.class));
    }

    public ResultDTO getBlockByNumber(String txIdOrNumber)
    {
        if (IntegerUtils.isInt(txIdOrNumber)) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new Integer[] { Integer.valueOf(txIdOrNumber), Integer.valueOf(1) }, Block.class));
        }
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getblock", new String[] { txIdOrNumber, "1" }, Block.class));
    }

    public ResultDTO getAddrFromAcount(String account)
    {
        return null;
    }

    public boolean validateAssertId(String assertId)
    {
        if (assertId.startsWith("0x")) {
            assertId = assertId.substring(2);
        }
        if ((!StringUtils.isEmpty(assertId)) && ((assertId.length() == 64) || (assertId.length() == 40))) {
            return true;
        }
        return false;
    }

    public ResultDTO getAccountState(String address)
    {
        boolean isRight = ((Boolean)validateAddress(address).getResult()).booleanValue();
        if (isRight) {
            return new ResultDTO().setResult(client.invoke(this.mClientBean, "getaccountstate", new String[] { address }, AccountState.class));
        }
        throw new RuntimeException("getAccountState地址格式错误");
    }

    public ResultDTO getTokenDecimals(String assetId)
    {
        if (null != decimalMap.get(assetId)) {
            return new ResultDTO().setResult(decimalMap.get(assetId));
        }
        if (validateAssertId(assetId))
        {
            JSONArray jsonArray = new JSONArray();
            jsonArray.add(assetId);
            jsonArray.add("decimals");
            JSONArray jsonArray1 = new JSONArray();
            jsonArray.add(jsonArray1);
            JSONObject balanceObj = client.invoke(this.mClientBean, "invokefunction", jsonArray, JSONObject.class);
            String value = balanceObj.getJSONArray("stack").getJSONObject(0).getString("value");
            decimalMap.put(assetId, Integer.valueOf(value));
            return new ResultDTO().setResult(Integer.valueOf(value));
        }
        throw new RuntimeException("getBalance资产id格式错误");
    }

    public ResultDTO getBestBlockHash()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getbestblockhash", new String[] { "" }, String.class));
    }

    public ResultDTO getPendingTx()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getconnectioncount", new String[] { "" }, Integer.class));
    }

    public ResultDTO getInfo()
    {
        return getBlockCount();
    }

    public ResultDTO getTransactionConfirmed(String txid)
    {
        return new ResultDTO().setResult(Integer.valueOf(1));
    }

    public ResultDTO getTransactionFee(String txid)
    {
        return new ResultDTO().setResult(BigDecimal.ZERO);
    }

    public ResultDTO getTransaction(String txid)
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "getrawtransaction", new String[] { txid, "1" }, NeoTx.class));
    }

    public String hashToAddress(String hash)
    {
        hash = "17" + hash;
        byte[] bytes = StringParser.hexStrToByteArray(hash);
        String check = StringParser.bytesToHexString(Sha256Hash.hashTwice(bytes)).substring(0, 8);
        hash = hash + check;
        return Base58.encode(StringParser.hexStrToByteArray(hash));
    }

    public ResultDTO getExplorerBlockNumber(String key)
    {
        JSONObject blockNumberObj = HttpRequestUtil.getJson(this.neoUrl, JSONObject.class);
        Integer blockCount = blockNumberObj.getInteger("height");
        if (null != blockCount) {
            return new ResultDTO().setResult(blockCount);
        }
        return new ResultDTO().setResult(Integer.valueOf(0)).setStatusCode(ResultCode.EXPLORER_ERROR.getCode());
    }

    public String addressToHash(String address)
    {
        byte[] code = Base58.decodeChecked(address);
        String hash = "0x" + StringParser.bytesToHexString(Utils.reverseBytes(code));
        hash = hash.substring(0, hash.length() - 2);
        return hash;
    }

    public ResultDTO getApplicationLog(String txid)
    {
        ApplicationLog applicationLog = client.invoke(this.mClientBean, "getapplicationlog", new String[] { txid }, ApplicationLog.class);
        if ((StringUtils.isEmpty(applicationLog.getVmstate())) || (applicationLog.getVmstate().contains("FAULT"))) {
            return new ResultDTO().setStatusCode(ResultCode.TYPE_MP5_ERROR.getCode());
        }
        ArrayList<Notifications> notifications = applicationLog.getNotifications();
        ArrayList<ItemTx> itemList = new ArrayList();
        for (Notifications item : notifications) {
            if ("Array".equalsIgnoreCase(item.getState().getType()))
            {
                ArrayList<Type> typeList = item.getState().getValue();
                if ((!CollectionUtils.isEmpty(typeList)) && (4 == typeList.size()))
                {
                    String value = ((Type)typeList.get(0)).getValue();
                    if (!"7472616e73666572".equalsIgnoreCase(value)) {
                        return new ResultDTO().setStatusCode(ResultCode.TYPE_ERROR.getCode());
                    }
                    String from = hashToAddress((typeList.get(1)).getValue());
                    String to = hashToAddress((typeList.get(2)).getValue());
                    Type type = typeList.get(3);
                    String typeInfo = type.getType();
                    String amount = "";
                    BigDecimal balanceDec = BigDecimal.ZERO;
                    if ("ByteArray".equalsIgnoreCase(typeInfo)) {
                        balanceDec = new BigDecimal(BigDecimalUtils.byteArrayToBig(type.getValue()));
                    } else {
                        balanceDec = new BigDecimal(type.getValue());
                    }
                    ItemTx itemTx = new ItemTx().setAmount(balanceDec).setTxid(txid).setFromAddress(from).setToAddress(to).setAssertId(item.getContract());
                    itemList.add(itemTx);
                }
            }
        }
        if (!CollectionUtils.isEmpty(itemList)) {
            return new ResultDTO().setResult(itemList);
        }
        return new ResultDTO().setStatusCode(ResultCode.TYPE_MP5_ERROR.getCode());
    }

    public static void main(String[] args)
    {
        NeoNewClient neoNewClient = new NeoNewClient(null);
        ResultDTO explorerBlockNumber = neoNewClient.getExplorerBlockNumber("");
    }
}
