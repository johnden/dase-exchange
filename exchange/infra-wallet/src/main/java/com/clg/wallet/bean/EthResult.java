package com.clg.wallet.bean;

public class EthResult
{

    private boolean isSuccess;
    private String txid;
    private String info;
    private int code;

    public EthResult setSuccess(boolean isSuccess)
    {
        this.isSuccess = isSuccess;return this;
    }

    public EthResult setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public EthResult setInfo(String info)
    {
        this.info = info;return this;
    }

    public EthResult setCode(int code)
    {
        this.code = code;return this;
    }

    public boolean isSuccess()
    {
        return this.isSuccess;
    }

    public String getTxid()
    {
        return this.txid;
    }

    public String getInfo()
    {
        return this.info;
    }

    public int getCode()
    {
        return this.code;
    }
}
