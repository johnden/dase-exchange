package com.clg.wallet.wallet.neo.bean;

public class Script
{
    private String invocation;
    private String verification;

    public Script setInvocation(String invocation)
    {
        this.invocation = invocation;return this;
    }

    public Script setVerification(String verification)
    {
        this.verification = verification;return this;
    }



    public String getInvocation()
    {
        return this.invocation;
    }

    public String getVerification()
    {
        return this.verification;
    }
}
