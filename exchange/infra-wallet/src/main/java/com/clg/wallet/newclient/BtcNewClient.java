package com.clg.wallet.newclient;

import org.springframework.util.*;
import java.math.*;
import com.clg.wallet.enums.*;
import com.clg.wallet.bean.*;
import com.clg.wallet.wallet.bitcoin.*;
import com.alibaba.fastjson.*;
import com.clg.wallet.utils.*;
import org.slf4j.*;

public class BtcNewClient extends NormalClient
{
    private static final Logger LOG = LoggerFactory.getLogger(BtcNewClient.class);
    private String btcUrl = "https://blockchain.info/latestblock";
    private String dogeUrl = "https://dogechain.info/chain/Dogecoin/q/getblockcount";

    public BtcNewClient(ClientBean clientBean)
    {
        super(clientBean);
    }

    public ResultDTO getNewAddress()
    {
        return getNewAddress("");
    }

    public ResultDTO getNewAddress(String tag)
    {
        String address = "";
        try
        {
            Bitcoin client = ClientInstance.getBitCoinClient(this.mClientBean);
            address = client.getNewAddress("TRADE_" + tag);
        }
        catch (Exception var6)
        {
            LOG.error(var6.getMessage());
        }
        return new ResultDTO().setResult(address);
    }

    public ResultDTO getBalance()
    {
        return getBalance(null);
    }

    public ResultDTO getBalance(String address)
    {
        try
        {
            Bitcoin client = ClientInstance.getBitCoinClient(this.mClientBean);
            BigDecimal balance = StringUtils.isEmpty(address) ? BigDecimal.valueOf(client.getBalance()) : BigDecimal.valueOf(client.getBalance(address));
            return new ResultDTO().setResult(balance);
        }
        catch (Exception var4)
        {
            LOG.error(var4.getMessage());
        }
        return new ResultDTO().setStatusCode(ResultCode.NOT_ENOUGH.getCode()).setResult(new BigDecimal("-99"));
    }

    public ResultDTO getBalanceByAccount(String address)
    {
        return null;
    }

    public ResultDTO getTokenBalance(String assertId, String address)
    {
        return null;
    }

    public ResultDTO getListAddress()
    {
        return null;
    }

    public ResultDTO getPeers()
    {
        return null;
    }

    public ResultDTO getBlockCount()
    {
        try
        {
            int blockCount = ClientInstance.getBitCoinClient(this.mClientBean).getBlockCount();
            return new ResultDTO().setResult(Integer.valueOf(blockCount));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return new ResultDTO().setStatusCode(ResultCode.SERVER_ERROR.getCode());
    }

    public ResultDTO getBlockHash(long blockNumber)
    {
        return null;
    }

    public ResultDTO getConnectionCount()
    {
        return null;
    }

    public ResultDTO dumpPrivKey(String address)
    {
        return null;
    }

    public ResultDTO validateAddress(String address)
    {
        boolean isRight = false;
        try
        {
            Bitcoin client = ClientInstance.getBitCoinClient(this.mClientBean);
            Bitcoin.AddressValidationResult validate = client.validateAddress(address);
            if (!validate.isValid()) {
                throw new Exception(address + "不是一个有效的钱包地址");
            }
            isRight = true;
        }
        catch (Exception var5)
        {
            LOG.error(var5.getMessage());
        }
        return new ResultDTO().setResult(Boolean.valueOf(isRight));
    }

    public ResultDTO sendNormal(TxData tx)
    {
        Bitcoin client = null;
        try
        {
            client = ClientInstance.getBitCoinClient(this.mClientBean);
            if (!StringUtils.isEmpty(tx.getPass())) {
                client.walletPassPhrase(tx.getPass(), Long.valueOf(10000L));
            }
            String sender = client.sendToAddress(tx.getToAddress(), tx.getBalance().doubleValue());
            if ((sender != null) && (sender.indexOf("status") != -1))
            {
                LOG.error("Send transaction error:" + sender);
                throw new Exception("Send transaction error:" + sender);
            }
            return new ResultDTO().setResult(sender);
        }
        catch (BitcoinRPCException e)
        {
            String errorResponse = e.getErrorResponse();
            if (StringParser.isJson(errorResponse)) {
                return getErrorResult(errorResponse);
            }
            throw new RuntimeException(e.getMessage());
        }
        catch (Exception var7)
        {
            LOG.error(var7.getMessage());
            throw new RuntimeException(var7.getMessage());
        }
        finally
        {
            try
            {
                client.walletLock();
            }
            catch (BitcoinException e)
            {
                e.printStackTrace();
            }
        }
    }

    public ResultDTO sendNormalToken(TxData tx)
    {
        return null;
    }

    public ResultDTO getTxBlockNumber(String txid)
    {
        return null;
    }

    public ResultDTO getBlockByNumber(String txIdOrNumber)
    {
        return null;
    }

    public ResultDTO getAddrFromAcount(String account)
    {
        return null;
    }

    public boolean validateAssertId(String assertId)
    {
        return false;
    }

    public ResultDTO getAccountState(String address)
    {
        return null;
    }

    public ResultDTO getTokenDecimals(String assetId)
    {
        return null;
    }

    public ResultDTO getBestBlockHash()
    {
        return null;
    }

    public ResultDTO getPendingTx()
    {
        return null;
    }

    public ResultDTO getInfo()
    {
        try
        {
            Bitcoin info = ClientInstance.getBitCoinClient(this.mClientBean);
            return new ResultDTO().setResult(info);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public ResultDTO getTransactionConfirmed(String txid)
    {
        try
        {
            Bitcoin client = ClientInstance.getBitCoinClient(this.mClientBean);
            return new ResultDTO().setResult(Integer.valueOf(client.getTransaction(txid).confirmations()));
        }
        catch (Exception var4)
        {
            LOG.error(var4.getMessage());
            throw new RuntimeException(var4.getMessage());
        }
    }

    public ResultDTO getTransactionFee(String txid)
    {
        return new ResultDTO().setResult(BigDecimal.ZERO);
    }

    public ResultDTO getTransaction(String txid)
    {
        return null;
    }

    public ResultDTO changePassword(String oldPass, String newPass)
    {
        Bitcoin client = null;
        try
        {
            client = ClientInstance.getBitCoinClient(this.mClientBean);
            client.changePass(oldPass, newPass);
            return new ResultDTO().setResult(Boolean.valueOf(true));
        }
        catch (BitcoinRPCException e)
        {
            String errorResponse = e.getErrorResponse();
            if (StringParser.isJson(errorResponse)) {
                return getErrorResult(errorResponse);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return new ResultDTO().setResult(Boolean.valueOf(false));
    }

    private ResultDTO getErrorResult(String error)
    {
        JSONObject parse = (JSONObject)JSONObject.parse(error);
        String code = parse.getJSONObject("error").getString("code");
        String message = parse.getJSONObject("error").getString("message");
        return new ResultDTO().setStatusCode(Integer.valueOf(code).intValue()).setResultDesc(message);
    }

    public ResultDTO getExplorerBlockNumber(String key)
    {
        int blockNumber = 0;
        if ("btc".equalsIgnoreCase(this.mClientBean.getName()))
        {
            String url = this.btcUrl + (StringUtils.isEmpty(key) ? "" : key);
            JSONObject lastBlockInfo = (JSONObject)HttpRequestUtil.getJson(url, JSONObject.class);
            if (null != lastBlockInfo) {
                blockNumber = lastBlockInfo.getInteger("height").intValue();
            }
        }
        else if ("goge".equalsIgnoreCase(this.mClientBean.getName()))
        {
            String url = this.dogeUrl + (StringUtils.isEmpty(key) ? "" : key);
            blockNumber = ((Integer)HttpRequestUtil.getJson(url, Integer.class)).intValue();
        }
        return new ResultDTO().setResult(Integer.valueOf(blockNumber));
    }

    public static void main(String[] args)
    {
        ClientBean clientBean = new ClientBean();
        clientBean.setRpcIp("127.0.0.1");
        clientBean.setRpcPort("16174");
        clientBean.setCoinType("goge");
        clientBean.setRpcUser("hsharerpc");
        clientBean.setRpcPwd("12345678");
        clientBean.setName("goge");

        BtcNewClient btcNewClient = new BtcNewClient(clientBean);
        ResultDTO explorerBlockNumber = btcNewClient.getExplorerBlockNumber("");
    }
}
