package com.clg.wallet.wallet.bitshare;

import java.nio.charset.*;
import java.io.*;
import java.util.logging.*;
import java.net.*;
import javax.net.ssl.*;
import java.util.*;
import com.clg.wallet.utils.*;
import java.math.*;

public class BitSharesJSONRPCClient implements BitShares
{
    private static final Logger logger;
    public final URL rpcURL;
    private URL noAuthURL;
    private String authStr;
    private HostnameVerifier hostnameVerifier;
    private SSLSocketFactory sslSocketFactory;
    private int connectTimeout;
    public static final Charset QUERY_CHARSET;

    public BitSharesJSONRPCClient(final String rpcUrl) throws MalformedURLException {
        this(new URL(rpcUrl));
    }

    public BitSharesJSONRPCClient(final URL rpc) {
        this.hostnameVerifier = null;
        this.sslSocketFactory = null;
        this.connectTimeout = 0;
        this.rpcURL = rpc;
        try {
            this.noAuthURL = new URI(rpc.getProtocol(), null, rpc.getHost(), rpc.getPort(), rpc.getPath(), rpc.getQuery(), null).toURL();
        }
        catch (MalformedURLException var3) {
            throw new IllegalArgumentException(rpc.toString(), var3);
        }
        catch (URISyntaxException var4) {
            throw new IllegalArgumentException(rpc.toString(), var4);
        }
        this.authStr = ((rpc.getUserInfo() == null) ? null : String.valueOf(Base64Coder.encode(rpc.getUserInfo().getBytes(Charset.forName("ISO8859-1")))));
    }

    public HostnameVerifier getHostnameVerifier() {
        return this.hostnameVerifier;
    }

    public void setHostnameVerifier(final HostnameVerifier hostnameVerifier) {
        this.hostnameVerifier = hostnameVerifier;
    }

    public SSLSocketFactory getSslSocketFactory() {
        return this.sslSocketFactory;
    }

    public void setSslSocketFactory(final SSLSocketFactory sslSocketFactory) {
        this.sslSocketFactory = sslSocketFactory;
    }

    public void setConnectTimeout(final int timeout) {
        if (timeout < 0) {
            throw new IllegalArgumentException("timeout can not be negative");
        }
        this.connectTimeout = timeout;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    public byte[] prepareRequest(final String method, final Object... params) {
        return JSON.stringify(new LinkedHashMap() {
            {
                this.put("method", method);
                this.put("params", params);
                this.put("id", "1");
            }
        }).getBytes(BitSharesJSONRPCClient.QUERY_CHARSET);
    }

    private static byte[] loadStream(final InputStream in, final boolean close) throws IOException {
        final ByteArrayOutputStream o = new ByteArrayOutputStream();
        final byte[] buffer = new byte[1024];
        while (true) {
            final int nr = in.read(buffer);
            if (nr == -1) {
                return o.toByteArray();
            }
            if (nr == 0) {
                throw new IOException("Read timed out");
            }
            o.write(buffer, 0, nr);
        }
    }

    public Object loadResponse(final InputStream in, final Object expectedID, final boolean close) throws IOException, BitSharesException {
        Object var6;
        try {
            final String r = new String(loadStream(in, close), BitSharesJSONRPCClient.QUERY_CHARSET);
            BitSharesJSONRPCClient.logger.log(Level.FINE, "BitShare JSON-RPC response:\n{0}", r);
            try {
                final Map response = (Map)JSON.parse(r);
                if (!expectedID.equals(response.get("id"))) {
                    throw new BitSharesException("Wrong response ID (expected: " + String.valueOf(expectedID) + ", response: " + response.get("id") + ")");
                }
                if (response.get("error") != null) {
                    throw new BitSharesException(JSON.stringify(response.get("error")));
                }
                var6 = response.get("result");
            }
            catch (ClassCastException var7) {
                throw new BitSharesException("Invalid server response format (data: \"" + r + "\")");
            }
        }
        finally {
            if (close) {
                in.close();
            }
        }
        return var6;
    }

    public Object query(final String method, final Object... o) throws BitSharesException {
        try {
            final HttpURLConnection conn = (HttpURLConnection)this.noAuthURL.openConnection();
            if (this.connectTimeout != 0) {
                conn.setConnectTimeout(this.connectTimeout);
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);
            if (conn instanceof HttpsURLConnection) {
                if (this.hostnameVerifier != null) {
                    ((HttpsURLConnection)conn).setHostnameVerifier(this.hostnameVerifier);
                }
                if (this.sslSocketFactory != null) {
                    ((HttpsURLConnection)conn).setSSLSocketFactory(this.sslSocketFactory);
                }
            }
            conn.setRequestProperty("Authorization", "Basic " + this.authStr);
            final byte[] r = this.prepareRequest(method, o);
            BitSharesJSONRPCClient.logger.log(Level.FINE, "BitShare JSON-RPC request:\n{0}", new String(r, BitSharesJSONRPCClient.QUERY_CHARSET));
            conn.getOutputStream().write(r);
            conn.getOutputStream().close();
            final int responseCode = conn.getResponseCode();
            if (responseCode != 200) {
                throw new BitSharesException("RPC Query Failed (method: " + method + ", params: " + Arrays.deepToString(o) + ", response header: " + responseCode + " " + conn.getResponseMessage() + ", response: " + new String(loadStream(conn.getErrorStream(), true)));
            }
            return this.loadResponse(conn.getInputStream(), 1.0, true);
        }
        catch (IOException var6) {
            throw new BitSharesException("RPC Query Failed (method: " + method + ", params: " + Arrays.deepToString(o) + ")", var6);
        }
    }

    @Override
    public boolean is_locked() throws BitSharesException {
        return (boolean)this.query("is_locked", new Object[0]);
    }

    @Override
    public boolean unlock(final String password) throws BitSharesException {
        return this.query("unlock", password) == null;
    }

    @Override
    public TransferResult transfer(final String from, final String to, final String amount, final String asset_symbol, final String memo, final boolean broadcast, final String amount_to_fee, final String symbol_to_fee) throws BitSharesException {
        return new TransferResultMapWrapper((Map)this.query("transfer", from, to, amount, asset_symbol, memo, broadcast, amount_to_fee, symbol_to_fee));
    }

    @Override
    public List<Transaction> get_account_history(final String name, final int limit) throws BitSharesException {
        return new TransactionListMapWrapper((List<Map>)this.query("get_account_history", name, limit));
    }

    @Override
    public AccountInfo get_account(final String nameOrId) throws BitSharesException {
        return new AccountInfoMapWrapper((Map)this.query("get_account", nameOrId));
    }

    @Override
    public DynamicGlobalProperties get_dynamic_global_properties() throws BitSharesException {
        return new DynamicGlobalPropertiesMapWrapper((Map)this.query("get_dynamic_global_properties", new Object[0]));
    }

    @Override
    public List<AccountBalances> list_account_balances(final String nameOrId) throws BitSharesException {
        return new AccountBalancesListMapWrapper((List<Map>)this.query("list_account_balances", nameOrId));
    }

    static {
        logger = Logger.getLogger(BitSharesJSONRPCClient.class.getCanonicalName());
        QUERY_CHARSET = Charset.forName("ISO8859-1");
    }

    private class AccountBalancesListMapWrapper extends ListMapWrapper<AccountBalances>
    {
        public AccountBalancesListMapWrapper(final List<Map> var1) {
            super(var1);
        }

        @Override
        protected AccountBalances wrap(final Map m) {
            return new AccountBalances() {
                @Override
                public String asset_id() {
                    return MapWrapper.mapStr(m, "asset_id");
                }

                @Override
                public BigInteger amount() {
                    return BigInteger.valueOf(Long.valueOf(MapWrapper.mapStr(m, "amount")));
                }

                @Override
                public String toString() {
                    return m.toString();
                }
            };
        }
    }

    private class DynamicGlobalPropertiesMapWrapper extends MapWrapper implements DynamicGlobalProperties
    {
        public DynamicGlobalPropertiesMapWrapper(final Map m) {
            super(m);
        }

        @Override
        public Long head_block_number() {
            return Long.valueOf(this.mapStr("head_block_number"));
        }

        @Override
        public Long last_irreversible_block_num() {
            return Long.valueOf(this.mapStr("last_irreversible_block_num"));
        }
    }

    private class AccountInfoMapWrapper extends MapWrapper implements AccountInfo
    {
        public AccountInfoMapWrapper(final Map m) {
            super(m);
        }

        @Override
        public String id() {
            return this.mapStr("id");
        }

        @Override
        public String name() {
            return this.mapStr("name");
        }
    }

    private class TransactionListMapWrapper extends ListMapWrapper<Transaction>
    {
        public TransactionListMapWrapper(final List<Map> var1) {
            super(var1);
        }

        @Override
        protected Transaction wrap(final Map m) {
            return new Transaction() {
                @Override
                public String memo() {
                    return MapWrapper.mapStr(m, "memo");
                }

                @Override
                public String description() {
                    return MapWrapper.mapStr(m, "description");
                }

                @Override
                public String from() {
                    return this.description().contains("Create WcgAccount") ? null :  MapWrapper.mapStr((Map)((List)((Map)m.get("op")).get("op")).get(1), "from");
                }

                @Override
                public String to() {
                    return this.description().contains("Create WcgAccount") ? null : MapWrapper.mapStr((Map)((List)((Map)m.get("op")).get("op")).get(1), "to");
                }

                @Override
                public BigInteger amount() {
                    return this.description().contains("Create WcgAccount") ? null : BigInteger.valueOf(Long.valueOf(MapWrapper.mapStr((Map)((Map)((List)((Map)m.get("op")).get("op")).get(1)).get("amount"), "amount")).longValue());
                }

                @Override
                public String asset_id() {
                    return this.description().contains("Create WcgAccount") ? null : MapWrapper.mapStr((Map)((Map)((List)((Map)m.get("op")).get("op")).get(1)).get("amount"), "asset_id");
                }

                @Override
                public Long block_num() {
                    return Long.valueOf(MapWrapper.mapStr((Map) m.get("op"), "block_num"));
                }

                @Override
                public Long virtual_op() {
                    return Long.valueOf(MapWrapper.mapStr((Map)m.get("op"), "virtual_op"));
                }

                @Override
                public String id() {
                    return MapWrapper.mapStr((Map)m.get("op"), "id");
                }

                @Override
                public String toString() {
                    return m.toString();
                }
            };
        }
    }

    private class TransferResultMapWrapper extends MapWrapper implements TransferResult
    {
        public TransferResultMapWrapper(final Map m) {
            super(m);
        }

        @Override
        public Long ref_block_num() {
            return Long.valueOf(this.mapStr("ref_block_num"));
        }
    }
}
