package com.clg.wallet.utils;

import com.alibaba.fastjson.*;

public class StringParser
{

    private String string;
    int index;

    public int length()
    {
        return this.string.length() - this.index;
    }

    public StringParser(String string)
    {
        this.string = string;
        this.index = 0;
    }

    public void forward(int chars)
    {
        this.index += chars;
    }

    public char poll()
    {
        char c = this.string.charAt(this.index);
        forward(1);
        return c;
    }

    public String poll(int length)
    {
        String str = this.string.substring(this.index, length + this.index);
        forward(length);
        return str;
    }

    private void commit()
    {
        this.string = this.string.substring(this.index);
        this.index = 0;
    }

    public String pollBeforeSkipDelim(String s)
    {
        commit();
        int i = this.string.indexOf(s);
        if (i == -1) {
            throw new RuntimeException("\"" + s + "\" not found in \"" + this.string + "\"");
        }
        String rv = this.string.substring(0, i);
        forward(i + s.length());
        return rv;
    }

    public char peek()
    {
        return this.string.charAt(this.index);
    }

    public String peek(int length)
    {
        return this.string.substring(this.index, length + this.index);
    }

    public String trim()
    {
        commit();
        return this.string = this.string.trim();
    }

    public char charAt(int pos)
    {
        return this.string.charAt(pos + this.index);
    }

    public boolean isEmpty()
    {
        return this.string.length() <= this.index;
    }

    public String toString()
    {
        commit();
        return this.string;
    }

    public static String bytesToHexString(byte[] src)
    {
        StringBuilder stringBuilder = new StringBuilder("");
        if ((src == null) || (src.length <= 0)) {
            return null;
        }
        for (int i = 0; i < src.length; i++)
        {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    public static byte[] hexStrToByteArray(String str)
    {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return new byte[0];
        }
        byte[] byteArray = new byte[str.length() / 2];
        for (int i = 0; i < byteArray.length; i++)
        {
            String subStr = str.substring(2 * i, 2 * i + 2);
            byteArray[i] = ((byte)Integer.parseInt(subStr, 16));
        }
        return byteArray;
    }

    public static int byteArrayToInt(byte[] b)
    {
        return b[3] & 0xFF | (b[2] & 0xFF) << 8 | (b[1] & 0xFF) << 16 | (b[0] & 0xFF) << 24;
    }

    public static boolean isJson(String content)
    {
        try
        {
            JSONObject jsonStr = JSONObject.parseObject(content);
            return true;
        }
        catch (Exception e) {}
        return false;
    }
}
