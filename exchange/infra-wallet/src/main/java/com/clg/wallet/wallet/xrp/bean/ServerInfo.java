package com.clg.wallet.wallet.xrp.bean;

public class ServerInfo
{
    private boolean status;
    private Long seq;
    private String complete_ledgers;

    public ServerInfo setStatus(boolean status)
    {
        this.status = status;return this;
    }

    public ServerInfo setSeq(Long seq)
    {
        this.seq = seq;return this;
    }

    public ServerInfo setComplete_ledgers(String complete_ledgers)
    {
        this.complete_ledgers = complete_ledgers;return this;
    }


    public boolean isStatus()
    {
        return this.status;
    }

    public Long getSeq()
    {
        return this.seq;
    }

    public String getComplete_ledgers()
    {
        return this.complete_ledgers;
    }
}
