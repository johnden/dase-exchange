package com.clg.wallet.enums;

public interface CoinName
{
    public static final String BTC = "btc";
    public static final String DOGE = "goge";
    public static final String LTC = "ltc";
    public static final String ETH = "eth";
    public static final String HSR = "hsr";
    public static final String ACT = "act";
    public static final String ETC = "etc";
    public static final String NEO = "neo";
    public static final String XRP = "xrp";
    public static final String WCG = "wcg";
}
