package com.clg.wallet.newclient;

import java.util.*;
import com.clg.wallet.bean.*;
import com.clg.wallet.wallet.Omni.*;

public class ClientFactory
{
    static HashMap<String, Client> hashMap = new HashMap();

    public static Client getClient(ClientBean clientBean)
    {
        String coinType = clientBean.getCoinType();
        if (null != hashMap.get(coinType)) {
            return (Client)hashMap.get(coinType);
        }
        Client client = null;
        if ((coinType.equalsIgnoreCase("eth")) ||
                (coinType.equalsIgnoreCase("ethToken")) ||
                (coinType.equalsIgnoreCase("etz")) ||
                (coinType.equalsIgnoreCase("etc"))) {
            client = new EthNewClient(clientBean);
        } else if ((coinType.equalsIgnoreCase("act")) ||
                (coinType.equalsIgnoreCase("actToken"))) {
            client = new ActNewClient(clientBean);
        } else if ((coinType.equalsIgnoreCase("neo")) ||
                (coinType.equalsIgnoreCase("neoToken"))) {
            client = new NeoNewClient(clientBean);
        } else if (coinType.equalsIgnoreCase("btc")) {
            client = new OmniNewClient(clientBean);
        } else if (coinType.equalsIgnoreCase("xrp")) {
            client = new XrpNewClient(clientBean);
        } else if (coinType.equalsIgnoreCase("wcg")) {
            client = new WcgNewClient(clientBean);
        } else if (coinType.equalsIgnoreCase("btcToken")) {
            client = new OmniNewClient(clientBean);
        } else {
            throw new RuntimeException("暂时未开放改币种");
        }
        return client;
    }
}
