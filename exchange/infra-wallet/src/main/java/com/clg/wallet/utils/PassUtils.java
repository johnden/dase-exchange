package com.clg.wallet.utils;

import java.util.*;

public class PassUtils
{
    public static String key = "qiqiliuo";
    public static int contentLength = 8;

    public static String encodePass(String pass)
    {
        String uuid = pass.substring(0, contentLength);
        String time = ShareCodeUtil.idToCode(pass.substring(contentLength));
        return uuid + reverse(time).toLowerCase();
    }

    public static String getPass()
    {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        return uuid.substring(0, contentLength) + System.currentTimeMillis();
    }

    public static String decodePass(String content)
    {
        return content.substring(0, contentLength) + ShareCodeUtil.codeToId(reverse(content.substring(contentLength)));
    }

    public static String reverse(String str)
    {
        return new StringBuilder(str).reverse().toString();
    }

    public static void main(String[] args)
    {
        System.out.println(getPass());
    }
}
