package com.clg.wallet.wallet.neo.bean;

import java.util.*;

public class Block
{
    private String hash;
    private Long size;
    private Integer version;
    private String previousblockhash;
    private String merkleroot;
    private Long time;
    private Long index;
    private String nonce;
    private String nextconsensus;
    private Script script;
    private List<NeoTx> tx;
    private Long confirmations;
    private String nextblockhash;

    public Block setHash(String hash)
    {
        this.hash = hash;return this;
    }

    public Block setSize(Long size)
    {
        this.size = size;return this;
    }

    public Block setVersion(Integer version)
    {
        this.version = version;return this;
    }

    public Block setPreviousblockhash(String previousblockhash)
    {
        this.previousblockhash = previousblockhash;return this;
    }

    public Block setMerkleroot(String merkleroot)
    {
        this.merkleroot = merkleroot;return this;
    }

    public Block setTime(Long time)
    {
        this.time = time;return this;
    }

    public Block setIndex(Long index)
    {
        this.index = index;return this;
    }

    public Block setNonce(String nonce)
    {
        this.nonce = nonce;return this;
    }

    public Block setNextconsensus(String nextconsensus)
    {
        this.nextconsensus = nextconsensus;return this;
    }

    public Block setScript(Script script)
    {
        this.script = script;return this;
    }

    public Block setTx(List<NeoTx> tx)
    {
        this.tx = tx;return this;
    }

    public Block setConfirmations(Long confirmations)
    {
        this.confirmations = confirmations;return this;
    }

    public Block setNextblockhash(String nextblockhash)
    {
        this.nextblockhash = nextblockhash;return this;
    }



    public String getHash()
    {
        return this.hash;
    }

    public Long getSize()
    {
        return this.size;
    }

    public Integer getVersion()
    {
        return this.version;
    }

    public String getPreviousblockhash()
    {
        return this.previousblockhash;
    }

    public String getMerkleroot()
    {
        return this.merkleroot;
    }

    public Long getTime()
    {
        return this.time;
    }

    public Long getIndex()
    {
        return this.index;
    }

    public String getNonce()
    {
        return this.nonce;
    }

    public String getNextconsensus()
    {
        return this.nextconsensus;
    }

    public Script getScript()
    {
        return this.script;
    }

    public List<NeoTx> getTx()
    {
        return this.tx;
    }

    public Long getConfirmations()
    {
        return this.confirmations;
    }

    public String getNextblockhash()
    {
        return this.nextblockhash;
    }
}
