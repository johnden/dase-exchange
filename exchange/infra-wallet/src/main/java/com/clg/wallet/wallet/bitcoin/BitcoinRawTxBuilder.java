package com.clg.wallet.wallet.bitcoin;

import java.util.*;

public class BitcoinRawTxBuilder
{

    public final Bitcoin bitcoin;
    public LinkedHashSet<Bitcoin.TxInput> inputs = new LinkedHashSet();
    public List<Bitcoin.TxOutput> outputs = new ArrayList();
    private HashMap<String, Bitcoin.RawTransaction> txCache = new HashMap();

    public BitcoinRawTxBuilder(Bitcoin bitcoin)
    {
        this.bitcoin = bitcoin;
    }

    public BitcoinRawTxBuilder in(Bitcoin.TxInput in)
    {
        this.inputs.add(new Input(in.txid(), in.vout()));
        return this;
    }

    public BitcoinRawTxBuilder in(String txid, int vout)
    {
        in(new Bitcoin.BasicTxInput(txid, vout));
        return this;
    }

    public BitcoinRawTxBuilder out(String address, double amount)
    {
        if (amount <= 0.0D) {
            return this;
        }
        this.outputs.add(new Bitcoin.BasicTxOutput(address, amount));
        return this;
    }

    public BitcoinRawTxBuilder in(double value)
            throws BitcoinException
    {
        return in(value, 6);
    }

    public BitcoinRawTxBuilder in(double value, int minConf)
            throws BitcoinException
    {
        final List<Bitcoin.Unspent> unspent = this.bitcoin.listUnspent(minConf);
        double v = value;
        for (final Bitcoin.Unspent o : unspent) {
            if (!this.inputs.contains(new Input(o))) {
                this.in(o);
                v = BitcoinUtil.normalizeAmount(v - o.amount());
            }
            if (v < 0.0) {
                break;
            }
        }
        if (v > 0.0) {
            throw new BitcoinException("Not enough bitcoins (" + v + "/" + value + ")");
        }
        return this;
    }

    private Bitcoin.RawTransaction tx(String txId)
            throws BitcoinException
    {
        Bitcoin.RawTransaction tx = this.txCache.get(txId);
        if (tx != null) {
            return tx;
        }
        tx = this.bitcoin.getRawTransaction(txId);
        this.txCache.put(txId, tx);
        return tx;
    }

    public BitcoinRawTxBuilder outChange(String address)
            throws BitcoinException
    {
        return outChange(address, 0.0D);
    }

    public BitcoinRawTxBuilder outChange(String address, double fee)
            throws BitcoinException
    {
        double is = 0.0;
        for (final Bitcoin.TxInput i : this.inputs) {
            is = BitcoinUtil.normalizeAmount(is + this.tx(i.txid()).vOut().get(i.vout()).value());
        }
        double os = fee;
        for (final Bitcoin.TxOutput o : this.outputs) {
            os = BitcoinUtil.normalizeAmount(os + o.amount());
        }
        if (os < is) {
            this.out(address, BitcoinUtil.normalizeAmount(is - os));
        }
        return this;
    }

    public String create()
            throws BitcoinException
    {
        return this.bitcoin.createRawTransaction(new ArrayList<>(this.inputs), this.outputs);
    }

    public String sign()
            throws BitcoinException
    {
        return this.bitcoin.signRawTransaction(create());
    }

    public String send()
            throws BitcoinException
    {
        return this.bitcoin.sendRawTransaction(sign());
    }
    private class Input extends Bitcoin.BasicTxInput
    {
        public Input(final String txid, final int vout) {
            super(txid, vout);
        }

        public Input(Bitcoin.TxInput copy)
        {
            this(copy.txid(), copy.vout());
        }
    }
}
