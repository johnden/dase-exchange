package com.clg.wallet.newclient;

import com.clg.wallet.bean.*;

public interface Client
{
     String getName();

     ResultDTO getNewAddress();

     ResultDTO getNewAddress(String paramString);

     ResultDTO getBalance();

     ResultDTO getBalance(String paramString);

     ResultDTO getBalanceByAccount(String paramString);

     ResultDTO getTokenBalance(String paramString1, String paramString2);

     ResultDTO getListAddress();

     ResultDTO getPeers();

     ResultDTO getBlockCount();

     ResultDTO getBlockHash(long paramLong);

     ResultDTO getConnectionCount();

     ResultDTO dumpPrivKey(String paramString);

     ResultDTO validateAddress(String paramString);

     ResultDTO sendNormal(TxData paramTxData);

     ResultDTO sendNormalToken(TxData paramTxData);

     ResultDTO getTxBlockNumber(String paramString);

     ResultDTO getBlockByNumber(String paramString);

     ResultDTO getAddrFromAcount(String paramString);

     boolean validateAssertId(String paramString);

     ResultDTO getAccountState(String paramString);

     ResultDTO getTokenDecimals(String paramString);

     ResultDTO getBestBlockHash();

     ResultDTO getPendingTx();

     ResultDTO getInfo();

     ResultDTO getTransactionConfirmed(String paramString);

     ResultDTO getTransactionFee(String paramString);

     ResultDTO getTransaction(String paramString);

     ResultDTO changePassword(String paramString1, String paramString2);

     ResultDTO getExplorerBlockNumber(String paramString);
}
