package com.clg.wallet.wallet.bitcoin;

import java.util.*;

public interface Bitcoin
{
    void addNode(final String p0, final AddNoteCmd p1) throws BitcoinException;

    String createRawTransaction(final List<TxInput> p0, final List<TxOutput> p1) throws BitcoinException;

    RawTransaction decodeRawTransaction(final String p0) throws BitcoinException;

    String dumpPrivKey(final String p0) throws BitcoinException;

    String getAccount(final String p0) throws BitcoinException;

    String getAccountAddress(final String p0) throws BitcoinException;

    List<String> getAddressesByAccount(final String p0) throws BitcoinException;

    double getBalance() throws BitcoinException;

    double getBalance(final String p0) throws BitcoinException;

    double getBalance(final String p0, final int p1) throws BitcoinException;

    Block getBlock(final String p0) throws BitcoinException;

    int getBlockCount() throws BitcoinException;

    String getBlockHash(final int p0) throws BitcoinException;

    int getConnectionCount() throws BitcoinException;

    double getDifficulty() throws BitcoinException;

    boolean getGenerate() throws BitcoinException;

    double getHashesPerSec() throws BitcoinException;

    Info getNetworkinfo() throws BitcoinException;

    Info getInfo() throws BitcoinException;

    MiningInfo getMiningInfo() throws BitcoinException;

    String getNewAddress() throws BitcoinException;

    String getNewAddress(final String p0) throws BitcoinException;

    PeerInfo getPeerInfo() throws BitcoinException;

    String getRawTransactionHex(final String p0) throws BitcoinException;

    RawTransaction getRawTransaction(final String p0) throws BitcoinException;

    double getReceivedByAccount(final String p0) throws BitcoinException;

    double getReceivedByAccount(final String p0, final int p1) throws BitcoinException;

    double getReceivedByAddress(final String p0) throws BitcoinException;

    double getReceivedByAddress(final String p0, final int p1) throws BitcoinException;

    RawTransaction getTransaction(final String p0) throws BitcoinException;

    TxOutSetInfo getTxOutSetInfo() throws BitcoinException;

    Work getWork() throws BitcoinException;

    void importPrivKey(final String p0) throws BitcoinException;

    void importPrivKey(final String p0, final String p1) throws BitcoinException;

    void importPrivKey(final String p0, final String p1, final boolean p2) throws BitcoinException;

    Map<String, Number> listAccounts() throws BitcoinException;

    Map<String, Number> listAccounts(final int p0) throws BitcoinException;

    List<ReceivedAddress> listReceivedByAccount() throws BitcoinException;

    List<ReceivedAddress> listReceivedByAccount(final int p0) throws BitcoinException;

    List<ReceivedAddress> listReceivedByAccount(final int p0, final boolean p1) throws BitcoinException;

    List<ReceivedAddress> listReceivedByAddress() throws BitcoinException;

    List<ReceivedAddress> listReceivedByAddress(final int p0) throws BitcoinException;

    List<ReceivedAddress> listReceivedByAddress(final int p0, final boolean p1) throws BitcoinException;

    TransactionsSinceBlock listSinceBlock() throws BitcoinException;

    TransactionsSinceBlock listSinceBlock(final String p0) throws BitcoinException;

    TransactionsSinceBlock listSinceBlock(final String p0, final int p1) throws BitcoinException;

    List<Transaction> listTransactions() throws BitcoinException;

    List<Transaction> listTransactions(final String p0) throws BitcoinException;

    List<Transaction> listTransactions(final String p0, final int p1) throws BitcoinException;

    List<Transaction> listTransactions(final String p0, final int p1, final int p2) throws BitcoinException;

    List<Unspent> listUnspent() throws BitcoinException;

    List<Unspent> listUnspent(final int p0) throws BitcoinException;

    List<Unspent> listUnspent(final int p0, final int p1) throws BitcoinException;

    List<Unspent> listUnspent(final int p0, final int p1, final String... p2) throws BitcoinException;

    String sendFrom(final String p0, final String p1, final double p2) throws BitcoinException;

    String sendFrom(final String p0, final String p1, final double p2, final int p3) throws BitcoinException;

    String sendFrom(final String p0, final String p1, final double p2, final int p3, final String p4) throws BitcoinException;

    String sendFrom(final String p0, final String p1, final double p2, final int p3, final String p4, final String p5) throws BitcoinException;

    String sendMany(final String p0, final List<TxOutput> p1) throws BitcoinException;

    String sendMany(final String p0, final List<TxOutput> p1, final int p2) throws BitcoinException;

    String sendMany(final String p0, final List<TxOutput> p1, final int p2, final String p3) throws BitcoinException;

    String sendRawTransaction(final String p0) throws BitcoinException;

    String sendToAddress(final String p0, final double p1) throws BitcoinException;

    String sendToAddress(final String p0, final double p1, final String p2) throws BitcoinException;

    String sendToAddress(final String p0, final double p1, final String p2, final String p3) throws BitcoinException;

    String signMessage(final String p0, final String p1) throws BitcoinException;

    String signRawTransaction(final String p0) throws BitcoinException;

    void walletLock() throws BitcoinException;

    void walletPassPhrase(final String p0, final Long p1) throws BitcoinException;

    void changePass(final String p0, final String p1) throws BitcoinException;

    void stop() throws BitcoinException;

    AddressValidationResult validateAddress(final String p0) throws BitcoinException;

    boolean verifyMessage(final String p0, final String p1, final String p2) throws BitcoinException;

    public static class BasicTxOutput implements TxOutput
    {
        public String address;
        public double amount;

        public BasicTxOutput(final String address, final double amount) {
            this.address = address;
            this.amount = amount;
        }

        @Override
        public String address() {
            return this.address;
        }

        @Override
        public double amount() {
            return this.amount;
        }
    }

    public static class BasicTxInput implements TxInput
    {
        public String txid;
        public int vout;

        public BasicTxInput(final String txid, final int vout) {
            this.txid = txid;
            this.vout = vout;
        }
        @Override
        public String txid() {
            return this.txid;
        }

        @Override
        public int vout() {
            return this.vout;
        }
    }

    public enum AddNoteCmd
    {
        add,
        remove,
        onetry;
    }

    public interface TxInput
    {
        String txid();

        int vout();
    }

    public interface TxOutput
    {
        String address();

        double amount();
    }

    public interface Block
    {
        String hash();

        int confirmations();

        int size();

        int height();

        int version();

        String merkleRoot();

        List<String> tx();

        Date time();

        long nonce();

        String bits();

        double difficulty();

        String previousHash();

        String nextHash();

        Block previous() throws BitcoinException;

        Block next() throws BitcoinException;
    }

    public interface Info
    {
        int version();

        int protocolversion();

        int walletversion();

        double balance();

        int blocks();

        int timeoffset();

        int connections();

        String proxy();

        double difficulty();

        boolean testnet();

        int keypoololdest();

        int keypoolsize();

        int unlocked_until();

        double paytxfee();

        double relayfee();

        String errors();
    }

    public interface MiningInfo
    {
        int blocks();

        int currentblocksize();

        int currentblocktx();

        double difficulty();

        String errors();

        int genproclimit();

        double networkhashps();

        int pooledtx();

        boolean testnet();

        String chain();

        boolean generate();
    }

    public interface PeerInfo
    {
        String addr();

        String services();

        int lastsend();

        int lastrecv();

        int bytessent();

        int bytesrecv();

        int blocksrequested();

        Date conntime();

        int version();

        String subver();

        boolean inbound();

        int startingheight();

        int banscore();
    }

    public interface RawTransaction
    {
        String hex();

        String txId();

        int version();

        long lockTime();

        List<In> vIn();

        List<Out> vOut();

        String blockHash();

        int confirmations();

        Date time();

        Date blocktime();

        public interface In extends TxInput
        {
            Map<String, Object> scriptSig();

            long sequence();

            RawTransaction getTransaction();

            Out getTransactionOutput();
        }

        public interface Out
        {
            double value();

            int n();

            ScriptPubKey scriptPubKey();

            TxInput toInput();

            RawTransaction transaction();

            public interface ScriptPubKey
            {
                String asm();

                String hex();

                int reqSigs();

                String type();

                List<String> addresses();
            }
        }
    }

    public interface TxOutSetInfo
    {
        int height();

        String bestBlock();

        int transactions();

        int txOuts();

        int bytesSerialized();

        String hashSerialized();

        double totalAmount();
    }

    public interface Work
    {
        String midstate();

        String data();

        String hash1();

        String target();
    }

    public interface ReceivedAddress
    {
        String address();

        String account();

        double amount();

        int confirmations();
    }

    public interface Transaction
    {
        String account();

        String address();

        String category();

        double amount();

        double fee();

        int confirmations();

        String blockHash();

        int blockIndex();

        Date blockTime();

        String txId();

        Date time();

        Date timeReceived();

        String comment();

        String commentTo();

        RawTransaction raw();
    }

    public interface TransactionsSinceBlock
    {
        List<Transaction> transactions();

        String lastBlock();
    }

    public interface Unspent extends TxInput, TxOutput
    {
        String txid();

        int vout();

        String address();

        String account();

        String scriptPubKey();

        double amount();

        int confirmations();
    }

    public interface AddressValidationResult
    {
        boolean isValid();

        String address();

        boolean isMine();

        boolean isScript();

        String pubKey();

        boolean isCompressed();

        String account();
    }
}
