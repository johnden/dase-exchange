package com.clg.wallet.wallet.neo.bean;

public class AssertBalance
{
    private String asset;
    private double value;

    public AssertBalance setAsset(String asset)
    {
        this.asset = asset;return this;
    }

    public AssertBalance setValue(double value)
    {
        this.value = value;return this;
    }


    public String getAsset()
    {
        return this.asset;
    }

    public double getValue()
    {
        return this.value;
    }
}
