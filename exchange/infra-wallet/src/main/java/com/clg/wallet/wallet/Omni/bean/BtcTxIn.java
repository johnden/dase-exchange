package com.clg.wallet.wallet.Omni.bean;

import java.util.*;

public class BtcTxIn
{
    private Map<String, Object> scriptSig;
    private long sequence;
    private String txid;
    private Integer vout;

    public BtcTxIn setScriptSig(Map<String, Object> scriptSig)
    {
        this.scriptSig = scriptSig;return this;
    }

    public BtcTxIn setSequence(long sequence)
    {
        this.sequence = sequence;return this;
    }

    public BtcTxIn setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public BtcTxIn setVout(Integer vout)
    {
        this.vout = vout;return this;
    }


    public Map<String, Object> getScriptSig()
    {
        return this.scriptSig;
    }

    public long getSequence()
    {
        return this.sequence;
    }

    public String getTxid()
    {
        return this.txid;
    }

    public Integer getVout()
    {
        return this.vout;
    }
}
