package com.clg.wallet.bean;

import java.math.*;

public class EThTransactionBean
{
    private String fromAddress;
    private String fromUserKeystorePath;
    private String fromUserKeystore;
    private String fromUserPass;
    private String feeAddress;
    private String feeUserKeystorePath;
    private String feeUserKeystore;
    private String feeUserPass;
    private double minTokenEth;
    private boolean isAllTransaction;
    private BigDecimal balance;
    private BigDecimal tokenBalance;
    private String contractAddress;
    private String contractAmountUnit;
    private String toAddress;
    private boolean isFee;
    private Long addPrice;

    public EThTransactionBean setFromAddress(String fromAddress)
    {
        this.fromAddress = fromAddress;return this;
    }

    public EThTransactionBean setFromUserKeystorePath(String fromUserKeystorePath)
    {
        this.fromUserKeystorePath = fromUserKeystorePath;return this;
    }

    public EThTransactionBean setFromUserKeystore(String fromUserKeystore)
    {
        this.fromUserKeystore = fromUserKeystore;return this;
    }

    public EThTransactionBean setFromUserPass(String fromUserPass)
    {
        this.fromUserPass = fromUserPass;return this;
    }

    public EThTransactionBean setFeeAddress(String feeAddress)
    {
        this.feeAddress = feeAddress;return this;
    }

    public EThTransactionBean setFeeUserKeystorePath(String feeUserKeystorePath)
    {
        this.feeUserKeystorePath = feeUserKeystorePath;return this;
    }

    public EThTransactionBean setFeeUserKeystore(String feeUserKeystore)
    {
        this.feeUserKeystore = feeUserKeystore;return this;
    }

    public EThTransactionBean setFeeUserPass(String feeUserPass)
    {
        this.feeUserPass = feeUserPass;return this;
    }

    public EThTransactionBean setMinTokenEth(double minTokenEth)
    {
        this.minTokenEth = minTokenEth;return this;
    }

    public EThTransactionBean setAllTransaction(boolean isAllTransaction)
    {
        this.isAllTransaction = isAllTransaction;return this;
    }

    public EThTransactionBean setBalance(BigDecimal balance)
    {
        this.balance = balance;return this;
    }

    public EThTransactionBean setTokenBalance(BigDecimal tokenBalance)
    {
        this.tokenBalance = tokenBalance;return this;
    }

    public EThTransactionBean setContractAddress(String contractAddress)
    {
        this.contractAddress = contractAddress;return this;
    }

    public EThTransactionBean setContractAmountUnit(String contractAmountUnit)
    {
        this.contractAmountUnit = contractAmountUnit;return this;
    }

    public EThTransactionBean setToAddress(String toAddress)
    {
        this.toAddress = toAddress;return this;
    }

    public EThTransactionBean setFee(boolean isFee)
    {
        this.isFee = isFee;return this;
    }

    public EThTransactionBean setAddPrice(Long addPrice)
    {
        this.addPrice = addPrice;return this;
    }


    public String getFromAddress()
    {
        return this.fromAddress;
    }

    public String getFromUserKeystorePath()
    {
        return this.fromUserKeystorePath;
    }

    public String getFromUserKeystore()
    {
        return this.fromUserKeystore;
    }

    public String getFromUserPass()
    {
        return this.fromUserPass;
    }

    public String getFeeAddress()
    {
        return this.feeAddress;
    }

    public String getFeeUserKeystorePath()
    {
        return this.feeUserKeystorePath;
    }

    public String getFeeUserKeystore()
    {
        return this.feeUserKeystore;
    }

    public String getFeeUserPass()
    {
        return this.feeUserPass;
    }

    public double getMinTokenEth()
    {
        return this.minTokenEth;
    }

    public boolean isAllTransaction()
    {
        return this.isAllTransaction;
    }

    public BigDecimal getBalance()
    {
        return this.balance;
    }

    public BigDecimal getTokenBalance()
    {
        return this.tokenBalance;
    }

    public String getContractAddress()
    {
        return this.contractAddress;
    }

    public String getContractAmountUnit()
    {
        return this.contractAmountUnit;
    }

    public String getToAddress()
    {
        return this.toAddress;
    }

    public boolean isFee()
    {
        return this.isFee;
    }

    public Long getAddPrice()
    {
        return this.addPrice;
    }
}
