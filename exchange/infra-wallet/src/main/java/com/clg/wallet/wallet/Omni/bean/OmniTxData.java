package com.clg.wallet.wallet.Omni.bean;

import java.util.*;

public class OmniTxData
{
    List<OmniTxItem> receiveList;
    List<OmniTxItem> sendList;
    String blockHash;

    public OmniTxData setReceiveList(List<OmniTxItem> receiveList)
    {
        this.receiveList = receiveList;return this;
    }

    public OmniTxData setSendList(List<OmniTxItem> sendList)
    {
        this.sendList = sendList;return this;
    }

    public OmniTxData setBlockHash(String blockHash)
    {
        this.blockHash = blockHash;return this;
    }



    public List<OmniTxItem> getReceiveList()
    {
        return this.receiveList;
    }

    public List<OmniTxItem> getSendList()
    {
        return this.sendList;
    }

    public String getBlockHash()
    {
        return this.blockHash;
    }
}
