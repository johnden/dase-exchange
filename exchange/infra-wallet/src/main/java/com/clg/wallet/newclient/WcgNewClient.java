package com.clg.wallet.newclient;

import org.springframework.util.*;
import com.clg.wallet.bean.*;
import java.math.*;
import com.clg.wallet.wallet.wcg.bean.*;
import com.alibaba.fastjson.*;
import java.net.*;
import java.io.*;
import java.util.*;
import com.clg.wallet.utils.*;

public class WcgNewClient extends NormalClient
{
    private String url = "";

    public WcgNewClient(ClientBean clientBean)
    {
        super(clientBean);
        this.url = ("http://" + this.mClientBean.getRpcIp() + ":" + this.mClientBean.getRpcPort() + "/wcg?requestType=");
    }

    public ResultDTO getBalance(String address)
    {
        WcgAccount account = (WcgAccount)getAccountState(address).getResult();
        BigDecimal balance = BigDecimalUtils.realBalance(account.getBalanceNQT(), 8);
        return new ResultDTO().setResult(balance);
    }

    public ResultDTO validateAddress(String address)
    {
        if ((!StringUtils.isEmpty(address)) && (
                (address.toLowerCase().startsWith("wcg")) ||
                        (address.toLowerCase().startsWith("nxt"))))
        {
            String[] split = address.split("-");
            String replace = address.replace("-", "");
            return new ResultDTO().setResult(Boolean.valueOf((split.length == 5) && (replace.length() == 20)));
        }
        return new ResultDTO().setResult(Boolean.valueOf(false));
    }

    public ResultDTO getBlockCount()
    {
        String method = "getBlockchainStatus";
        JSONObject result = getResult(JSONObject.class, method, "");
        String blockNumber = result.getString("numberOfBlocks").split("\\.")[0];
        return new ResultDTO().setResult(Integer.valueOf(blockNumber));
    }

    public ResultDTO sendNormal(TxData tx)
    {
        String pri = tx.getPriKey();
        BigInteger balance = BigDecimalUtils.chainBalance(tx.getBalance().toString(), 8).toBigInteger();
        Map<String, String> params = new HashMap();
        params.put("amountNQT", balance + "");
        params.put("recipient", tx.getToAddress());
        params.put("message", tx.getTag());
        params.put("secretPhrase", pri);
        params.put("deadline", "60");
        params.put("feeNQT", "1000000");
        JSONObject result = postResult("sendMoney", params);
        return new ResultDTO().setResult(result.getString("transaction"));
    }

    public ResultDTO getTransactionFee(String txid)
    {
        return new ResultDTO().setResult(new BigDecimal(0.01D));
    }

    public ResultDTO getTxBlockNumber(String txid)
    {
        String condition = "transaction=" + txid;
        JSONObject result = getResult(JSONObject.class, "getTransaction", condition);
        String blockNumber = result.getString("height").split("\\.")[0];
        return new ResultDTO().setResult(Integer.valueOf(blockNumber));
    }

    public ResultDTO getBlockByNumber(String txIdOrNumber)
    {
        List<WcgTx> wcgTxList = new ArrayList();
        String method = "getBlock";
        String condition = "height=" + txIdOrNumber + "&includeTransactions=true&includeExecutedPhased=true";
        JSONObject result = getResult(JSONObject.class, method, condition);
        if (null != result)
        {
            JSONArray txs = result.getJSONArray("transactions");
            if ((null != txs) && (txs.size() > 0)) {
                for (int i = 0; i < txs.size(); i++)
                {
                    WcgTx wcgTx = new WcgTx();
                    JSONObject jsonObject = txs.getJSONObject(i);
                    if ((null != jsonObject) && (!jsonObject.getBoolean("phased").booleanValue()))
                    {
                        String balanceStr = jsonObject.getString("amountNQT");
                        String hash = jsonObject.getString("transaction");
                        BigDecimal balance = BigDecimalUtils.realBalance(balanceStr, 8);
                        JSONObject attachment = jsonObject.getJSONObject("attachment");
                        boolean encry = isEncry(attachment);
                        String message = attachment.getString("message");
                        if (encry) {
                            message = hash;
                        }
                        String fromAddress = jsonObject.getString("senderRS");
                        String toAddress = jsonObject.getString("recipientRS");
                        if (!StringUtils.isEmpty(toAddress))
                        {
                            wcgTx.setAmountNQT(balance).setMessage(message).setFromAddress(fromAddress).setToAddress(toAddress).setTxId(hash).setEncry(encry);
                            wcgTxList.add(wcgTx);
                        }
                    }
                }
            }
        }
        return new ResultDTO().setResult(wcgTxList);
    }

    private boolean isEncry(JSONObject attachment)
    {
        String message = attachment.getString("message");
        if (!StringUtils.isEmpty(message)) {
            return false;
        }
        JSONObject encryptedMessage = attachment.getJSONObject("encryptedMessage");
        if (null != encryptedMessage) {
            return true;
        }
        return false;
    }

    public String decry(String hash, String pri)
    {
        if (pri.contains(" ")) {
            try
            {
                pri = URLEncoder.encode(pri, "utf-8");
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                throw new RuntimeException("私钥转码失败");
            }
        }
        String condition = "transaction=" + hash + "&secretPhrase=" + pri;
        JSONObject message = getResult(JSONObject.class, "readMessage", condition);
        return message.getString("decryptedMessage");
    }

    public ResultDTO getAccountState(String address)
    {
        WcgAccount result = getResult(WcgAccount.class, "getAccount", "account=" + address);
        return new ResultDTO().setResult(result);
    }

    public ResultDTO getBestBlockHash()
    {
        String method = "getBlockchainStatus";
        JSONObject result = getResult(JSONObject.class, method, "");
        String lastBlock = result.getString("lastBlock");
        return new ResultDTO().setResult(Integer.valueOf(lastBlock));
    }

    public ResultDTO getInfo()
    {
        JSONObject blockChain = getResult(JSONObject.class, "getMyInfo", "");
        return new ResultDTO().setResult(blockChain);
    }

    public ResultDTO getTransactionConfirmed(String txid)
    {
        Integer commit = Integer.valueOf(0);
        ResultDTO transaction = getTransaction(txid);
        JSONObject jsonObject = transaction.toJSONObj();
        if (null != jsonObject) {
            commit = jsonObject.getInteger("confirmations");
        }
        return new ResultDTO().setResult(commit);
    }

    public boolean isFailed(String txId, int confirmed)
    {
        JSONObject jsonObject = getTransaction(txId).toJSONObj();
        Integer height = Integer.valueOf(jsonObject.getString("height").split("\\.")[0]);
        List<WcgTx> wcgTxList = (List)getBlockByNumber(height + "").getResult();
        for (WcgTx tx : wcgTxList) {
            if ((tx.getTxId().equalsIgnoreCase(txId)) &&
                    (null != jsonObject))
            {
                int nowConfirmations = jsonObject.getInteger("confirmations").intValue();
                Boolean phased = jsonObject.getBoolean("phased");
                return (nowConfirmations <= confirmed) || (phased.booleanValue());
            }
        }
        return true;
    }

    public ResultDTO getTransaction(String txid)
    {
        String mUrl = this.url + "getTransaction&transaction=" + txid;
        JSONObject json = HttpRequestUtil.getJson(mUrl, JSONObject.class);
        return new ResultDTO().setResult(json);
    }

    private <T> T getResult(Class<T> t, String method, String codition)
    {
        String mUrl = this.url + method + "&" + codition;
        return (T)HttpRequestUtil.getJson(mUrl, t);
    }

    private JSONObject postResult(String method, Map<String, String> param)
    {
        String mUrl = this.url + method;
        String post = HttpRequestUtil.post(mUrl, param);
        return (JSONObject)JSONObject.parse(post);
    }

    public static void main(String[] args)
    {
        String pri = "burn gotten unknown destroy brown expect chain nation nobody between seek about";
        ClientBean clientBean = new ClientBean();
        clientBean.setRpcPort("2082");
        clientBean.setRpcIp("47.52.241.253");
        WcgNewClient wcgNewClient = new WcgNewClient(clientBean);

        String decry = wcgNewClient.decry("17596521492139766319", pri);
        System.out.println(decry);
    }

    private static void getTx()
    {
        String address = "WCG-7T7B-5QEY-V8GS-D33VW";
        ClientBean clientBean = new ClientBean();
        clientBean.setRpcPort("2082");
        clientBean.setRpcIp("47.52.241.253");
        WcgNewClient wcgNewClient = new WcgNewClient(clientBean);
        for (int i = 498443; i < 498448; i++)
        {
            System.out.println("区块高度:" + i);
            List<WcgTx> result = (List)wcgNewClient.getBlockByNumber(i + "").getResult();
            System.out.println("当前区块的交易数量:" + result.size());
            for (WcgTx tx : result)
            {
                System.out.println("区块id:" + tx.getTxId());
                if (tx.getToAddress().equalsIgnoreCase(address))
                {
                    String message = tx.getMessage();
                    System.out.println("message:" + message);
                }
            }
        }
    }
}
