package com.clg.wallet.wallet.neo.bean;

import java.util.*;

public class NeoTx
{

    private String txid;
    private Integer size;
    private String type;
    private Integer version;
    private List<Object> attributes;
    private List<TxVin> vin;
    private List<TxVout> vout;
    private Double sys_fee;
    private Double net_fee;
    private List<Script> scripts;
    private List<TxVin> claims;
    private Long nonce;
    private String script;
    private String gas;
    private Long confirmations;
    private Long blocktime;

    public NeoTx setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public NeoTx setSize(Integer size)
    {
        this.size = size;return this;
    }

    public NeoTx setType(String type)
    {
        this.type = type;return this;
    }

    public NeoTx setVersion(Integer version)
    {
        this.version = version;return this;
    }

    public NeoTx setAttributes(List<Object> attributes)
    {
        this.attributes = attributes;return this;
    }

    public NeoTx setVin(List<TxVin> vin)
    {
        this.vin = vin;return this;
    }

    public NeoTx setVout(List<TxVout> vout)
    {
        this.vout = vout;return this;
    }

    public NeoTx setSys_fee(Double sys_fee)
    {
        this.sys_fee = sys_fee;return this;
    }

    public NeoTx setNet_fee(Double net_fee)
    {
        this.net_fee = net_fee;return this;
    }

    public NeoTx setScripts(List<Script> scripts)
    {
        this.scripts = scripts;return this;
    }

    public NeoTx setClaims(List<TxVin> claims)
    {
        this.claims = claims;return this;
    }

    public NeoTx setNonce(Long nonce)
    {
        this.nonce = nonce;return this;
    }

    public NeoTx setScript(String script)
    {
        this.script = script;return this;
    }

    public NeoTx setGas(String gas)
    {
        this.gas = gas;return this;
    }

    public NeoTx setConfirmations(Long confirmations)
    {
        this.confirmations = confirmations;return this;
    }

    public NeoTx setBlocktime(Long blocktime)
    {
        this.blocktime = blocktime;return this;
    }


    public String getTxid()
    {
        return this.txid;
    }

    public Integer getSize()
    {
        return this.size;
    }

    public String getType()
    {
        return this.type;
    }

    public Integer getVersion()
    {
        return this.version;
    }

    public List<Object> getAttributes()
    {
        return this.attributes;
    }

    public List<TxVin> getVin()
    {
        return this.vin;
    }

    public List<TxVout> getVout()
    {
        return this.vout;
    }

    public Double getSys_fee()
    {
        return this.sys_fee;
    }

    public Double getNet_fee()
    {
        return this.net_fee;
    }

    public List<Script> getScripts()
    {
        return this.scripts;
    }

    public List<TxVin> getClaims()
    {
        return this.claims;
    }

    public Long getNonce()
    {
        return this.nonce;
    }

    public String getScript()
    {
        return this.script;
    }

    public String getGas()
    {
        return this.gas;
    }

    public Long getConfirmations()
    {
        return this.confirmations;
    }

    public Long getBlocktime()
    {
        return this.blocktime;
    }
}
