package com.clg.wallet.wallet.bitcoin;

public abstract class ConfirmedPaymentListener extends SimpleBitcoinPaymentListener
{
    public int minConf = 1;

    public ConfirmedPaymentListener()
    {
        this.minConf = 1;
    }

    public void transaction(Bitcoin.Transaction transaction)
    {
        if (transaction.confirmations() >= this.minConf) {
            confirmed(transaction);
        }
    }

    public abstract void confirmed(Bitcoin.Transaction paramTransaction);
}
