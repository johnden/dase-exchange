package com.clg.wallet.wallet.wcg.bean;

import java.math.*;

public class WcgTx
{
    private String message;
    private String txId;
    private String fromAddress;
    private String toAddress;
    private BigDecimal amountNQT;
    private boolean encry;

    public WcgTx setMessage(String message)
    {
        this.message = message;return this;
    }

    public WcgTx setTxId(String txId)
    {
        this.txId = txId;return this;
    }

    public WcgTx setFromAddress(String fromAddress)
    {
        this.fromAddress = fromAddress;return this;
    }

    public WcgTx setToAddress(String toAddress)
    {
        this.toAddress = toAddress;return this;
    }

    public WcgTx setAmountNQT(BigDecimal amountNQT)
    {
        this.amountNQT = amountNQT;return this;
    }

    public WcgTx setEncry(boolean encry)
    {
        this.encry = encry;return this;
    }


    public String getMessage()
    {
        return this.message;
    }

    public String getTxId()
    {
        return this.txId;
    }

    public String getFromAddress()
    {
        return this.fromAddress;
    }

    public String getToAddress()
    {
        return this.toAddress;
    }

    public BigDecimal getAmountNQT()
    {
        return this.amountNQT;
    }

    public boolean isEncry()
    {
        return this.encry;
    }
}
