package com.clg.wallet;

import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.*;


@SpringBootApplication(scanBasePackages={"com.clg"})
public class WalletApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(WalletApplication.class, args);
    }
}
