package com.clg.wallet.wallet.neo.bean;

import java.util.*;

public class Peer
{
    private List<PeerInfo> unconnected;
    private List<PeerInfo> bad;
    private List<PeerInfo> connected;

    public Peer setUnconnected(List<PeerInfo> unconnected)
    {
        this.unconnected = unconnected;return this;
    }

    public Peer setBad(List<PeerInfo> bad)
    {
        this.bad = bad;return this;
    }

    public Peer setConnected(List<PeerInfo> connected)
    {
        this.connected = connected;return this;
    }



    public List<PeerInfo> getUnconnected()
    {
        return this.unconnected;
    }

    public List<PeerInfo> getBad()
    {
        return this.bad;
    }

    public List<PeerInfo> getConnected()
    {
        return this.connected;
    }
}
