package com.clg.wallet.utils;

public class TaskStringUtils
{
    public static boolean isAllEmpty(String... stringArr)
    {
        if ((null != stringArr) && (stringArr.length > 0))
        {
            for (String str : stringArr) {
                if ((str == null) || ("".equals(str))) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
}
