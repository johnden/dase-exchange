package com.clg.wallet.wallet.xrp.bean;

import java.math.*;

public class Tx
{
    private String hash;
    private String fromAddress;
    private String toAddress;
    private BigDecimal Balance;
    private String transactionType;
    private String memo;
    private String status;
    private boolean validated;
    private Long lastLedgerSequence;
    private String transactionResult;
    private boolean isSuccess;
    private Long blockNumber;

    public Tx setHash(String hash)
    {
        this.hash = hash;return this;
    }

    public Tx setFromAddress(String fromAddress)
    {
        this.fromAddress = fromAddress;return this;
    }

    public Tx setToAddress(String toAddress)
    {
        this.toAddress = toAddress;return this;
    }

    public Tx setBalance(BigDecimal Balance)
    {
        this.Balance = Balance;return this;
    }

    public Tx setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;return this;
    }

    public Tx setMemo(String memo)
    {
        this.memo = memo;return this;
    }

    public Tx setStatus(String status)
    {
        this.status = status;return this;
    }

    public Tx setValidated(boolean validated)
    {
        this.validated = validated;return this;
    }

    public Tx setLastLedgerSequence(Long lastLedgerSequence)
    {
        this.lastLedgerSequence = lastLedgerSequence;return this;
    }

    public Tx setTransactionResult(String transactionResult)
    {
        this.transactionResult = transactionResult;return this;
    }

    public Tx setSuccess(boolean isSuccess)
    {
        this.isSuccess = isSuccess;return this;
    }

    public Tx setBlockNumber(Long blockNumber)
    {
        this.blockNumber = blockNumber;return this;
    }


    public String getHash()
    {
        return this.hash;
    }

    public String getFromAddress()
    {
        return this.fromAddress;
    }

    public String getToAddress()
    {
        return this.toAddress;
    }

    public BigDecimal getBalance()
    {
        return this.Balance;
    }

    public String getTransactionType()
    {
        return this.transactionType;
    }

    public String getMemo()
    {
        return this.memo;
    }

    public String getStatus()
    {
        return this.status;
    }

    public boolean isValidated()
    {
        return this.validated;
    }

    public Long getLastLedgerSequence()
    {
        return this.lastLedgerSequence;
    }

    public String getTransactionResult()
    {
        return this.transactionResult;
    }

    public boolean isSuccess()
    {
        return this.isSuccess;
    }

    public Long getBlockNumber()
    {
        return this.blockNumber;
    }
}
