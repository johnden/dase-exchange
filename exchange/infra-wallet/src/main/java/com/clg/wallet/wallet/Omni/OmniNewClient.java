package com.clg.wallet.wallet.Omni;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clg.wallet.bean.ClientBean;
import com.clg.wallet.bean.ResultDTO;
import com.clg.wallet.bean.TxData;
import com.clg.wallet.enums.ResultCode;
import com.clg.wallet.help.JsonRpcClient;
import com.clg.wallet.newclient.NormalClient;
import com.clg.wallet.utils.BigDecimalUtils;
import com.clg.wallet.utils.HttpRequestUtil;
import com.clg.wallet.wallet.Omni.bean.BtcBlock;
import com.clg.wallet.wallet.Omni.bean.BtcTransaction;
import com.clg.wallet.wallet.Omni.bean.BtcTxIn;
import com.clg.wallet.wallet.Omni.bean.BtcTxItem;
import com.clg.wallet.wallet.Omni.bean.BtcTxOut;
import com.clg.wallet.wallet.Omni.bean.OmniBalance;
import com.clg.wallet.wallet.Omni.bean.OmniItemTransaction;
import com.clg.wallet.wallet.Omni.bean.OmniTxItem;
import com.clg.wallet.wallet.Omni.bean.ValidateAddress;
import com.googlecode.jsonrpc4j.JsonRpcClientException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import jdk.nashorn.api.scripting.JSObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

public class OmniNewClient extends NormalClient
{
    private static JsonRpcClient jsonRpcClient ;
    private static final Logger LOG = LoggerFactory.getLogger(OmniNewClient.class);
    private static BigDecimal fee = new BigDecimal(1.0E-4D);
    private static BigDecimal dust = new BigDecimal(6.0E-6D);
    private String btcUrl = "https://blockchain.info/latestblock";
    private String dogeUrl = "https://dogechain.info/chain/Dogecoin/q/getblockcount";

    public OmniNewClient(final ClientBean clientBean) {
        super(clientBean);
    }

    @Override
    public ResultDTO getNewAddress() {
        return this.getNewAddress("");
    }

    @Override
    public ResultDTO getNewAddress(final String tag) {
        final String address = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "getnewaddress", String.class, tag);
        return new ResultDTO().setResult(address);
    }

    @Override
    public ResultDTO getBalance() {
        return this.getBalance(null);
    }

    @Override
    public ResultDTO getBalance(final String address) {
        BigDecimal balance;
        if (StringUtils.isEmpty(address)) {
            balance = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "getbalance", BigDecimal.class, new Object[0]);
        }
        else {
            balance = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "getbalance", BigDecimal.class, address);
        }
        return new ResultDTO().setResult(balance);
    }

    @Override
    public ResultDTO getTransactionFee(final String txid) {
        BigDecimal fee = BigDecimal.ZERO;
        BigDecimal inBalance = BigDecimal.ZERO;
        BigDecimal outBalance = BigDecimal.ZERO;
        final BtcTransaction btcTransaction = this.getTransaction(txid).toObj(BtcTransaction.class);
        final List<BtcTxIn> vin = btcTransaction.getVin();
        if (!CollectionUtils.isEmpty(vin)) {
            for (final BtcTxIn item : vin) {
                final Integer vout = item.getVout();
                final String vinTxid = item.getTxid();
                final BtcTransaction vinTransaction = this.getTransaction(vinTxid).toObj(BtcTransaction.class);
                inBalance = inBalance.add(vinTransaction.getVout().get(vout).getValue());
            }
        }
        final List<BtcTxOut> vout2 = btcTransaction.getVout();
        for (final BtcTxOut item2 : vout2) {
            final BigDecimal value = item2.getValue();
            outBalance = outBalance.add(value);
        }
        fee = inBalance.subtract(outBalance);
        return new ResultDTO().setResult((fee.compareTo(BigDecimal.ZERO) > 0) ? BigDecimal.ZERO : fee);
    }

    @Override
    public ResultDTO getBlockCount() {
        final int blockCount = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "getblockcount", Integer.class, new Object[0]);
        return new ResultDTO().setResult(blockCount);
    }

    @Override
    public ResultDTO getTokenBalance(final String assertId, final String address) {
        final OmniBalance balance = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "omni_getbalance", OmniBalance.class, address, Integer.valueOf(assertId));
        return new ResultDTO().setResult(balance.getBalance());
    }

    public ResultDTO getWalletTokenBalance(final String assertId) {
        BigDecimal balance = BigDecimal.ZERO;
        final List<OmniItemTransaction> omniItemTransactions = this.omniListTransactions();
        if (!CollectionUtils.isEmpty(omniItemTransactions)) {
            for (final OmniItemTransaction item : omniItemTransactions) {
                if (Integer.valueOf(assertId) == item.propertyid) {
                    balance = balance.add(item.amount);
                }
            }
        }
        return new ResultDTO().setResult(balance);
    }

    @Override
    public ResultDTO validateAddress(final String address) {
        final ValidateAddress validateaddress = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "validateaddress", ValidateAddress.class, address);
        final boolean isRight = null == validateaddress || StringUtils.isEmpty(validateaddress.getAddress());
        return new ResultDTO().setResult(isRight);
    }

    public ResultDTO listSinceBlock(final String blockHash) {
        final JSONObject jsonObject = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "listsinceblock", JSONObject.class, blockHash);
        final List<OmniTxItem> omniTxItems = JSONObject.parseArray(JSONObject.toJSONString(jsonObject.getJSONArray("transactions")), (Class)OmniTxItem.class);
        return new ResultDTO().setResult(omniTxItems);
    }

    public ResultDTO listUnspent(final int confirm) {
        final JSONArray jsonObject = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "listunspent", JSONArray.class, confirm);
        final List<BtcTxItem> omniTxItems = JSONObject.parseArray(JSONObject.toJSONString(jsonObject), (Class)BtcTxItem.class);
        return new ResultDTO().setResult(omniTxItems);
    }

    private boolean unlockWallet(final String pass) {
        try {
            OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "walletpassphrase", JSONObject.class, pass, 10000L);
            return true;
        }
        catch (JsonRpcClientException e) {
            if (e.toString().contains("The wallet passphrase entered was incorrect")) {
                return false;
            }
            throw e;
        }
    }

    private BtcTxItem getFeeBtcTx() {
        BtcTxItem feeItem = null;
        final ResultDTO walletTokenBalance = this.listUnspent(6);
        final List<BtcTxItem> omniTxItems = (List<BtcTxItem>)walletTokenBalance.getResult();
        for (final BtcTxItem item : omniTxItems) {
            if (item.amount.compareTo(OmniNewClient.fee.add(OmniNewClient.dust)) > 0) {
                feeItem = item;
                break;
            }
        }
        return feeItem;
    }

    @Override
    public ResultDTO sendNormalToken(final TxData tx) {
        final BtcTxItem feeBtcTx = this.getFeeBtcTx();
        if (null == feeBtcTx) {
            return new ResultDTO().setStatusCode(ResultCode.FEE_NOTENOUGH_ERROR.getCode());
        }
        final String opReturn = jsonRpcClient.newInvoke(this.mClientBean, "omni_createpayload_simplesend", String.class, Integer.valueOf(tx.getAssertId()), BigDecimalUtils.formatBigDecimal(tx.getBalance(), 8).toString());
        final JSONArray txRaws = new JSONArray();
        final JSONObject txItem = new JSONObject();
        txRaws.add(txItem);
        txItem.put("txid", feeBtcTx.txid);
        txItem.put("vout", feeBtcTx.vout);
        final JSONObject sendObj = new JSONObject();
        String raw = jsonRpcClient.newInvoke(this.mClientBean, "createrawtransaction", String.class, txRaws, sendObj);
        raw = jsonRpcClient.newInvoke(this.mClientBean, "omni_createrawtx_opreturn", String.class, raw, opReturn);
        raw = jsonRpcClient.newInvoke(this.mClientBean, "omni_createrawtx_reference", String.class, raw, tx.getToAddress());
        txItem.put("scriptPubKey", feeBtcTx.getScriptPubKey());
        txItem.put("value", feeBtcTx.getAmount());
        raw = jsonRpcClient.newInvoke(this.mClientBean, "omni_createrawtx_change", String.class, raw, txRaws, tx.getToAddress(), 1.0E-4);
        if (!this.unlockWallet(tx.getPass())) {
            return new ResultDTO().setStatusCode(ResultCode.PASS_ERROR.getCode());
        }
        return new ResultDTO().setResult(this.signTx(raw));
    }

    public String signTx(String raw) {
        final JSONObject signInfo = jsonRpcClient.newInvoke(this.mClientBean, "signrawtransaction", JSONObject.class, raw);
        raw = signInfo.getString("hex");
        final Boolean complete = signInfo.getBoolean("complete");
        if (complete) {
            return jsonRpcClient.newInvoke(this.mClientBean, "sendrawtransaction", String.class, new Object[0]);
        }
        throw new RuntimeException("签名失败，暂时不支持多重签名");
    }

    @Override
    public ResultDTO getBlockByNumber(final String txIdOrNumber) {
        JSONObject jsonObject = null;
        if ("hsr".equalsIgnoreCase(this.mClientBean.getName())) {
            jsonObject = jsonRpcClient.newInvoke(this.mClientBean, "getblockbynumber", JSONObject.class, Integer.valueOf(txIdOrNumber));
        }
        else {
            final String hash = jsonRpcClient.newInvoke(this.mClientBean, "getblockhash", String.class, Integer.valueOf(txIdOrNumber));
            jsonObject = jsonRpcClient.newInvoke(this.mClientBean, "getblock", JSONObject.class, hash);
        }
        final BtcBlock btcBlock = (BtcBlock)JSONObject.toJavaObject(jsonObject, (Class)BtcBlock.class);
        return new ResultDTO().setResult(btcBlock);
    }

    @Override
    public ResultDTO sendNormal(final TxData tx) {
        if (!StringUtils.isEmpty(tx.getPass()) && !this.unlockWallet(tx.getPass())) {
            return new ResultDTO().setStatusCode(ResultCode.PASS_ERROR.getCode());
        }
        final String sender = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "sendtoaddress", String.class, tx.getBalance().doubleValue());
        if (!StringUtils.isEmpty(tx.getPass())) {
            OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "walletlock", JSObject.class, new Object[0]);
        }
        return new ResultDTO().setResult(sender);
    }

    public List<OmniItemTransaction> omniListTransactions() {
        final JSONArray omniListTransactions = OmniNewClient.jsonRpcClient.newInvoke(this.mClientBean, "omni_listtransactions", JSONArray.class, new Object[0]);
        final List<OmniItemTransaction> omniItemTransactions = JSONObject.parseArray(JSONObject.toJSONString(omniListTransactions), (Class)OmniItemTransaction.class);
        return omniItemTransactions;
    }

    @Override
    public ResultDTO getInfo() {
        final String method = "hsr".equalsIgnoreCase(this.mClientBean.getName()) ? "getinfo" : "getnetworkinfo";
        final JSONObject info = jsonRpcClient.newInvoke(this.mClientBean, method, JSONObject.class, new Object[0]);
        return new ResultDTO().setResult(info);
    }

    @Override
    public ResultDTO getTransaction(final String txid) {
        BtcTransaction btcTransaction = null;
        try {
            final JSONObject jsonObject = jsonRpcClient.newInvoke(this.mClientBean, "getrawtransaction", JSONObject.class, txid, 1);
            btcTransaction = JSONObject.toJavaObject(jsonObject, BtcTransaction.class);
        }
        catch (Exception e) {
            if (e.toString().contains("Invalid or non-wallet transaction id")) {
                LOG.info("更当前钱包无关");
            }
        }
        return new ResultDTO().setResult(btcTransaction);
    }

    @Override
    public ResultDTO getExplorerBlockNumber(final String key) {
        int blockNumber = 0;
        if ("btc".equalsIgnoreCase(this.mClientBean.getName())) {
            final String url = this.btcUrl + (StringUtils.isEmpty(key) ? "" : key);
            final JSONObject lastBlockInfo = HttpRequestUtil.getJson(url, JSONObject.class);
            if (null != lastBlockInfo) {
                blockNumber = lastBlockInfo.getInteger("height");
            }
        }
        else if ("goge".equalsIgnoreCase(this.mClientBean.getName())) {
            final String url = this.dogeUrl + (StringUtils.isEmpty(key) ? "" : key);
            blockNumber = HttpRequestUtil.getJson(url, Integer.class);
        }
        return new ResultDTO().setResult(blockNumber);
    }

    @Override
    public ResultDTO getTransactionConfirmed(final String txid) {
        final BtcTransaction transaction = this.getTransaction(txid).toObj(BtcTransaction.class);
        return new ResultDTO().setResult(transaction.getConfirmations());
    }

    public static void main(final String[] args) {
        final ClientBean clientBean = new ClientBean();
        clientBean.setRpcPwd("dsaisdssadsa45wssddsflswqekk43kldsd2");
        clientBean.setRpcUser("bhaobtc");
        clientBean.setRpcIp("47.52.250.189");
        clientBean.setRpcPort("8889");
        clientBean.setName("btc");
        final OmniNewClient omniNewClient = new OmniNewClient(clientBean);
        final ResultDTO balance = omniNewClient.getBalance("111");
        System.out.println(balance);
        final ResultDTO info = omniNewClient.getInfo();
        System.out.println(info);
    }
}
