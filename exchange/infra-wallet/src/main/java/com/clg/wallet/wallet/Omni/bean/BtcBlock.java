package com.clg.wallet.wallet.Omni.bean;

import java.util.*;

public class BtcBlock
{

    private String hash;
    private int confirmations;
    private int size;
    private int height;
    private int version;
    private String merkleRoot;
    private List<String> tx;
    private Date time;
    private long nonce;
    private String bits;
    private double difficulty;
    private String previousHash;
    private String nextHash;

    public BtcBlock setHash(String hash)
    {
        this.hash = hash;return this;
    }

    public BtcBlock setConfirmations(int confirmations)
    {
        this.confirmations = confirmations;return this;
    }

    public BtcBlock setSize(int size)
    {
        this.size = size;return this;
    }

    public BtcBlock setHeight(int height)
    {
        this.height = height;return this;
    }

    public BtcBlock setVersion(int version)
    {
        this.version = version;return this;
    }

    public BtcBlock setMerkleRoot(String merkleRoot)
    {
        this.merkleRoot = merkleRoot;return this;
    }

    public BtcBlock setTx(List<String> tx)
    {
        this.tx = tx;return this;
    }

    public BtcBlock setTime(Date time)
    {
        this.time = time;return this;
    }

    public BtcBlock setNonce(long nonce)
    {
        this.nonce = nonce;return this;
    }

    public BtcBlock setBits(String bits)
    {
        this.bits = bits;return this;
    }

    public BtcBlock setDifficulty(double difficulty)
    {
        this.difficulty = difficulty;return this;
    }

    public BtcBlock setPreviousHash(String previousHash)
    {
        this.previousHash = previousHash;return this;
    }

    public BtcBlock setNextHash(String nextHash)
    {
        this.nextHash = nextHash;return this;
    }


    public String getHash()
    {
        return this.hash;
    }

    public int getConfirmations()
    {
        return this.confirmations;
    }

    public int getSize()
    {
        return this.size;
    }

    public int getHeight()
    {
        return this.height;
    }

    public int getVersion()
    {
        return this.version;
    }

    public String getMerkleRoot()
    {
        return this.merkleRoot;
    }

    public List<String> getTx()
    {
        return this.tx;
    }

    public Date getTime()
    {
        return this.time;
    }

    public long getNonce()
    {
        return this.nonce;
    }

    public String getBits()
    {
        return this.bits;
    }

    public double getDifficulty()
    {
        return this.difficulty;
    }

    public String getPreviousHash()
    {
        return this.previousHash;
    }

    public String getNextHash()
    {
        return this.nextHash;
    }
}
