package com.clg.wallet.wallet.bitcoin;

public class BitcoinRPCException extends BitcoinException
{
    private String errorResponse;

    public BitcoinRPCException() {}

    public BitcoinRPCException(String msg)
    {
        super(msg);
    }

    public BitcoinRPCException(Throwable cause)
    {
        super(cause);
    }

    public BitcoinRPCException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public String getErrorResponse()
    {
        return this.errorResponse;
    }

    public void setErrorResponse(String errorResponse)
    {
        this.errorResponse = errorResponse;
    }
}
