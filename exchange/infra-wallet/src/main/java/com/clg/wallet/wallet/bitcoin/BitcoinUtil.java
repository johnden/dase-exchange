package com.clg.wallet.wallet.bitcoin;

public class BitcoinUtil
{
    public static double normalizeAmount(double amount)
    {
        return (0.5D + amount / 1.0E-8D) * 1.0E-8D;
    }
}
