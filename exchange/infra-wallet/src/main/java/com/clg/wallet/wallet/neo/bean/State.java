package com.clg.wallet.wallet.neo.bean;

import java.util.*;

public class State
{
    private String type;
    private ArrayList<Type> value;

    public State setType(String type)
    {
        this.type = type;return this;
    }

    public State setValue(ArrayList<Type> value)
    {
        this.value = value;return this;
    }


    public String getType()
    {
        return this.type;
    }

    public ArrayList<Type> getValue()
    {
        return this.value;
    }
}
