package com.clg.wallet.wallet.Omni.bean;

import java.math.*;
import java.util.*;

public class BtcTransaction
{
    private BigDecimal amount;
    private BigDecimal fee;
    private String hex;
    private String txid;
    private int version;
    private long locktime;
    private List<BtcTxIn> vin;
    private List<BtcTxOut> vout;
    private String blockhash;
    private int confirmations;
    private Date time;
    private Date blocktime;

    public BtcTransaction setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public BtcTransaction setFee(BigDecimal fee)
    {
        this.fee = fee;return this;
    }

    public BtcTransaction setHex(String hex)
    {
        this.hex = hex;return this;
    }

    public BtcTransaction setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public BtcTransaction setVersion(int version)
    {
        this.version = version;return this;
    }

    public BtcTransaction setLocktime(long locktime)
    {
        this.locktime = locktime;return this;
    }

    public BtcTransaction setVin(List<BtcTxIn> vin)
    {
        this.vin = vin;return this;
    }

    public BtcTransaction setVout(List<BtcTxOut> vout)
    {
        this.vout = vout;return this;
    }

    public BtcTransaction setBlockhash(String blockhash)
    {
        this.blockhash = blockhash;return this;
    }

    public BtcTransaction setConfirmations(int confirmations)
    {
        this.confirmations = confirmations;return this;
    }

    public BtcTransaction setTime(Date time)
    {
        this.time = time;return this;
    }

    public BtcTransaction setBlocktime(Date blocktime)
    {
        this.blocktime = blocktime;return this;
    }


    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public BigDecimal getFee()
    {
        return this.fee;
    }

    public String getHex()
    {
        return this.hex;
    }

    public String getTxid()
    {
        return this.txid;
    }

    public int getVersion()
    {
        return this.version;
    }

    public long getLocktime()
    {
        return this.locktime;
    }

    public List<BtcTxIn> getVin()
    {
        return this.vin;
    }

    public List<BtcTxOut> getVout()
    {
        return this.vout;
    }

    public String getBlockhash()
    {
        return this.blockhash;
    }

    public int getConfirmations()
    {
        return this.confirmations;
    }

    public Date getTime()
    {
        return this.time;
    }

    public Date getBlocktime()
    {
        return this.blocktime;
    }
}
