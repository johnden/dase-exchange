package com.clg.wallet.wallet.neo.bean;

import java.util.*;

public class ApplicationLog
{
    private String txid;
    private String vmstate;
    private double gas_consumed;
    private Object stack;
    private ArrayList<Notifications> notifications;

    public ApplicationLog setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public ApplicationLog setVmstate(String vmstate)
    {
        this.vmstate = vmstate;return this;
    }

    public ApplicationLog setGas_consumed(double gas_consumed)
    {
        this.gas_consumed = gas_consumed;return this;
    }

    public ApplicationLog setStack(Object stack)
    {
        this.stack = stack;return this;
    }

    public ApplicationLog setNotifications(ArrayList<Notifications> notifications)
    {
        this.notifications = notifications;return this;
    }


    public String getTxid()
    {
        return this.txid;
    }

    public String getVmstate()
    {
        return this.vmstate;
    }

    public double getGas_consumed()
    {
        return this.gas_consumed;
    }

    public Object getStack()
    {
        return this.stack;
    }

    public ArrayList<Notifications> getNotifications()
    {
        return this.notifications;
    }
}
