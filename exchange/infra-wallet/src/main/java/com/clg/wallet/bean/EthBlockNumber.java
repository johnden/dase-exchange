package com.clg.wallet.bean;

public class EthBlockNumber
{
    private String jsonrpc;
    private int id;
    private String result;

    public EthBlockNumber setJsonrpc(String jsonrpc)
    {
        this.jsonrpc = jsonrpc;return this;
    }

    public EthBlockNumber setId(int id)
    {
        this.id = id;return this;
    }

    public EthBlockNumber setResult(String result)
    {
        this.result = result;return this;
    }


    public String getJsonrpc()
    {
        return this.jsonrpc;
    }

    public int getId()
    {
        return this.id;
    }

    public String getResult()
    {
        return this.result;
    }
}
