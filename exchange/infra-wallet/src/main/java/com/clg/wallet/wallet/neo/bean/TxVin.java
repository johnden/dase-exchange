package com.clg.wallet.wallet.neo.bean;

public class TxVin
{
    private String txid;
    private Integer vout;

    public TxVin setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public TxVin setVout(Integer vout)
    {
        this.vout = vout;return this;
    }


    public String getTxid()
    {
        return this.txid;
    }

    public Integer getVout()
    {
        return this.vout;
    }
}
