package com.clg.wallet.wallet.bitcoin;

public class SimpleBitcoinPaymentListener implements BitcoinPaymentListener
{
    public void block(String blockHash) {}

    public void transaction(Bitcoin.Transaction transaction) {}
}
