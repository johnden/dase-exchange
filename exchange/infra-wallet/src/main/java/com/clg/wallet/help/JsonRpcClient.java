package com.clg.wallet.help;

import com.clg.wallet.bean.*;
import com.clg.wallet.newclient.*;
import com.googlecode.jsonrpc4j.*;
import com.clg.wallet.exception.*;
import org.springframework.util.*;
import com.clg.wallet.utils.*;
import com.clg.wallet.wallet.Omni.*;

public class JsonRpcClient
{
    private static JsonRpcClient client = null;

    public static JsonRpcClient getInstance()
    {
        if (null == client) {
            synchronized (JsonRpcClient.class)
            {
                if (null == client) {
                    client = new JsonRpcClient();
                }
            }
        }
        return client;
    }

    public <T> T invoke(ClientBean clientBean, String method, Object params, Class<T> clazz)
    {
        try
        {
            return (T)ClientInstance.getJsonRpcClient(clientBean).invoke(method, params, clazz);
        }
        catch (JsonRpcClientException jsonRpcClientException)
        {
            throw jsonRpcClientException;
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            throw new AchainException(throwable.getMessage());
        }
    }

    public <T> T newInvoke(ClientBean clientBean, String method, Class<T> clazz, Object... params)
    {
        try
        {
            JSONRPCClient jsonRpcClient = ClientInstance.getNewOmni(clientBean);
            Object query;
            if (StringUtils.isEmpty(params)) {
                query = jsonRpcClient.query(method, new Object[0]);
            } else {
                query = jsonRpcClient.query(method, params);
            }
            return (T)GsonUtils.convertObj(GsonUtils.toJson(query), clazz);
        }
        catch (JsonRpcClientException jsonRpcClientException)
        {
            throw jsonRpcClientException;
        }
        catch (Throwable throwable)
        {
            System.out.println("errorrorreweweweweew:" + clientBean.toString());
            throwable.printStackTrace();
            throw new AchainException(throwable.getMessage());
        }
    }
}
