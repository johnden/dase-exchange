package com.clg.wallet.utils;

import java.util.regex.*;

public class IntegerUtils
{
    public static boolean isInt(String string)
    {
        if (string == null) {
            return false;
        }
        String regEx1 = "[\\-|\\+]?\\d+";

        Pattern p = Pattern.compile(regEx1);
        Matcher m = p.matcher(string);
        if (m.matches()) {
            return true;
        }
        return false;
    }
}
