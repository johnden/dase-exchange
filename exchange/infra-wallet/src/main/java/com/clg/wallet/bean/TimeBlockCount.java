package com.clg.wallet.bean;

public class TimeBlockCount
{
    private Long blockCount;
    private Long time;

    public TimeBlockCount setBlockCount(Long blockCount)
    {
        this.blockCount = blockCount;return this;
    }

    public TimeBlockCount setTime(Long time)
    {
        this.time = time;return this;
    }

    public Long getBlockCount()
    {
        return this.blockCount;
    }

    public Long getTime()
    {
        return this.time;
    }
}
