package com.clg.wallet.wallet.Omni.bean;

import java.math.*;

public class BtcTxItem
{
    public String txid;
    public int vout;
    public String address;
    public String account;
    public String scriptPubKey;
    public BigDecimal amount;
    public int confirmations;
    public boolean spendable;
    public boolean solvable;

    public BtcTxItem setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public BtcTxItem setVout(int vout)
    {
        this.vout = vout;return this;
    }

    public BtcTxItem setAddress(String address)
    {
        this.address = address;return this;
    }

    public BtcTxItem setAccount(String account)
    {
        this.account = account;return this;
    }

    public BtcTxItem setScriptPubKey(String scriptPubKey)
    {
        this.scriptPubKey = scriptPubKey;return this;
    }

    public BtcTxItem setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public BtcTxItem setConfirmations(int confirmations)
    {
        this.confirmations = confirmations;return this;
    }

    public BtcTxItem setSpendable(boolean spendable)
    {
        this.spendable = spendable;return this;
    }

    public BtcTxItem setSolvable(boolean solvable)
    {
        this.solvable = solvable;return this;
    }


    public String getTxid()
    {
        return this.txid;
    }

    public int getVout()
    {
        return this.vout;
    }

    public String getAddress()
    {
        return this.address;
    }

    public String getAccount()
    {
        return this.account;
    }

    public String getScriptPubKey()
    {
        return this.scriptPubKey;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public int getConfirmations()
    {
        return this.confirmations;
    }

    public boolean isSpendable()
    {
        return this.spendable;
    }

    public boolean isSolvable()
    {
        return this.solvable;
    }
}
