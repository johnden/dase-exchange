package com.clg.wallet.bean;

import com.clg.wallet.utils.*;

public class ResultData
{

    public boolean toInteger()
    {
        return IntegerUtils.isInt(toString());
    }
}
