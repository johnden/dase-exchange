package com.clg.wallet.wallet.neo.bean;

public class Address
{

    private String address;
    private boolean haskey;
    private String label;
    private boolean watchonly;

    public Address setAddress(String address)
    {
        this.address = address;return this;
    }

    public Address setHaskey(boolean haskey)
    {
        this.haskey = haskey;return this;
    }

    public Address setLabel(String label)
    {
        this.label = label;return this;
    }

    public Address setWatchonly(boolean watchonly)
    {
        this.watchonly = watchonly;return this;
    }

    public String getAddress()
    {
        return this.address;
    }

    public boolean isHaskey()
    {
        return this.haskey;
    }

    public String getLabel()
    {
        return this.label;
    }

    public boolean isWatchonly()
    {
        return this.watchonly;
    }
}
