package com.clg.wallet.wallet.wcg.bean;

public class WcgAccount
{
    private String unconfirmedBalanceNQT;
    private String accountRS;
    private String forgedBalanceNQT;
    private String balanceNQT;
    private String publicKey;
    private int requestProcessingTime;
    private String account;

    public WcgAccount setUnconfirmedBalanceNQT(String unconfirmedBalanceNQT)
    {
        this.unconfirmedBalanceNQT = unconfirmedBalanceNQT;return this;
    }

    public WcgAccount setAccountRS(String accountRS)
    {
        this.accountRS = accountRS;return this;
    }

    public WcgAccount setForgedBalanceNQT(String forgedBalanceNQT)
    {
        this.forgedBalanceNQT = forgedBalanceNQT;return this;
    }

    public WcgAccount setBalanceNQT(String balanceNQT)
    {
        this.balanceNQT = balanceNQT;return this;
    }

    public WcgAccount setPublicKey(String publicKey)
    {
        this.publicKey = publicKey;return this;
    }

    public WcgAccount setRequestProcessingTime(int requestProcessingTime)
    {
        this.requestProcessingTime = requestProcessingTime;return this;
    }

    public WcgAccount setAccount(String account)
    {
        this.account = account;return this;
    }

    public String getUnconfirmedBalanceNQT()
    {
        return this.unconfirmedBalanceNQT;
    }

    public String getAccountRS()
    {
        return this.accountRS;
    }

    public String getForgedBalanceNQT()
    {
        return this.forgedBalanceNQT;
    }

    public String getBalanceNQT()
    {
        return this.balanceNQT;
    }

    public String getPublicKey()
    {
        return this.publicKey;
    }

    public int getRequestProcessingTime()
    {
        return this.requestProcessingTime;
    }

    public String getAccount()
    {
        return this.account;
    }
}
