package com.clg.wallet.utils;

import com.google.gson.reflect.*;
import org.springframework.util.*;
import java.util.*;
import com.google.gson.*;

public class GsonUtils
{
    static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    public static <T> List<T> convertList(String json, TypeToken<List<T>> token)
    {
        if (StringUtils.isEmpty(json)) {
            return new ArrayList();
        }
        new TypeToken() {};
        return (List)gson.fromJson(json, token.getType());
    }

    public static <T> List<T> convertList(String json, Class<T> cls)
    {
        if (StringUtils.isEmpty(json)) {
            return new ArrayList();
        }
        TypeToken<List<T>> typeToken = new TypeToken() {};
        return (List)gson.fromJson(json, typeToken.getType());
    }

    public static <T> T convertObj(String json, Class<T> cls)
    {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        return (T)gson.fromJson(json, cls);
    }

    public static String toJson(Object obj)
    {
        if (obj == null) {
            return "";
        }
        return gson.toJson(obj);
    }

    public static String getJsonObjectAsString(JsonObject jsonObject, String name)
    {
        if ((jsonObject == null) || (StringUtils.isEmpty(name))) {
            return null;
        }
        JsonElement jsonElement = jsonObject.get(name);
        return jsonElement == null ? null : jsonElement.getAsString();
    }

    public static JsonObject getJsonObjectChild(JsonObject jsonObject, String name)
    {
        if ((jsonObject == null) || (StringUtils.isEmpty(name))) {
            return null;
        }
        JsonElement jsonElement = jsonObject.get(name);
        return jsonElement == null ? null : jsonElement.getAsJsonObject();
    }

    public static boolean getJsonObjectAsBoolean(JsonObject jsonObject, String name)
    {
        if ((jsonObject == null) || (StringUtils.isEmpty(name))) {
            return false;
        }
        JsonElement jsonElement = jsonObject.get(name);
        return jsonElement == null ? false : jsonElement.getAsBoolean();
    }
}
