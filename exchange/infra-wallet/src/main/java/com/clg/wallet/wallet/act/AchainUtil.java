package com.clg.wallet.wallet.act;

import com.clg.wallet.help.*;
import com.clg.wallet.bean.*;
import com.alibaba.fastjson.*;
import org.springframework.util.*;
import java.util.*;

public class AchainUtil
{
    private static JsonRpcClient client;

    public static JSONObject getInfo(ClientBean clientBean)
    {
        return client.invoke(clientBean, "info", new String[] { "" }, JSONObject.class);
    }

    public static Long getBlockCount(ClientBean clientBean)
    {
        Long result = client.invoke(clientBean, "blockchain_get_block_count", new String[] { "" }, Long.class);
        return result;
    }

    public static JSONObject getBlock(ClientBean clientBean, String blocknum)
    {
        JSONObject result = client.invoke(clientBean, "blockchain_get_block", new String[] { blocknum }, JSONObject.class);
        return result;
    }

    public static JSONObject getPrettyTransaction(ClientBean clientBean, String transactionId)
    {
        JSONObject result = client.invoke(clientBean, "blockchain_get_pretty_transaction", new String[] { transactionId }, JSONObject.class);
        return result;
    }

    public static JSONObject getPrettyContractTransaction(ClientBean clientBean, String resultTrxId)
    {
        JSONObject result = client.invoke(clientBean, "blockchain_get_pretty_contract_transaction", new String[] { resultTrxId }, JSONObject.class);
        return result;
    }

    public static JSONArray getEvents(ClientBean clientBean, String blockNum, String resultTrxId)
    {
        JSONArray result = client.invoke(clientBean, "blockchain_get_events", new String[] { blockNum, resultTrxId }, JSONArray.class);
        return result;
    }

    public static JSONArray getTransaction(ClientBean clientBean, String transactionId)
    {
        JSONArray result = client.invoke(clientBean, "blockchain_get_transaction", new String[] { transactionId }, JSONArray.class);
        return result;
    }

    public static JSONObject walletTransferToAddress(ClientBean clientBean, String amount, String fromAccountName, String toAddress)
    {
        JSONObject result = client.invoke(clientBean, "wallet_transfer_to_address", new String[] { amount, "ACT", fromAccountName, toAddress }, JSONObject.class);
        return result;
    }

    public static JSONObject callContract(ClientBean clientBean, String contractId, String fromAccountName, String transferInfo)
    {
        JSONObject result = client.invoke(clientBean, "call_contract", new String[] { contractId, fromAccountName, "transfer_to", transferInfo, "ACT", "1" }, JSONObject.class);
        return result;
    }

    public static JSONObject getContractResult(ClientBean clientBean, String entryId)
    {
        JSONObject result = client.invoke(clientBean, "blockchain_get_contract_result", new String[] { entryId }, JSONObject.class);
        return result;
    }

    public static Object createWallet(ClientBean clientBean, String walletName, String password)
    {
        Object result = client.invoke(clientBean, "wallet_create", new String[] { walletName, password }, Object.class);
        return result;
    }

    public static String createAccount(ClientBean clientBean, String accountName)
    {
        String result = client.invoke(clientBean, "wallet_account_create", new String[] { accountName }, String.class);
        return result;
    }

    public static void unlockWallet(ClientBean clientBean, String timeout, String password)
    {
        client.invoke(clientBean, "wallet_unlock", new String[] { timeout, password }, Object.class);
    }

    public static void openWallet(ClientBean clientBean, String walletName)
    {
        client.invoke(clientBean, "wallet_open", new String[] { walletName }, Object.class);
    }

    public static void closeWallet(ClientBean clientBean)
    {
        client.invoke(clientBean, "wallet_close", new String[] { "" }, Object.class);
    }

    public static String walletGetAccountPublicAddress(ClientBean clientBean, String acount)
    {
        String result = client.invoke(clientBean, "wallet_get_account_public_address", new String[] { acount }, String.class);
        if ((!StringUtils.isEmpty(result)) && (result.length() > 32)) {
            return result.substring(0, result.length() - 32);
        }
        return result;
    }

    public static JSONArray getBalanceByAccountName(ClientBean clientBean, String account)
    {
        return client.invoke(clientBean, "wallet_account_balance", new String[] { account }, JSONArray.class);
    }

    public static JSONArray getBalanceIdsByAccountName(ClientBean clientBean, String account)
    {
        return client.invoke(clientBean, "wallet_account_balance_ids", new String[] { account }, JSONArray.class);
    }

    public static JSONObject getBalanceByBalanceId(ClientBean clientBean, String balanceId)
    {
        return client.invoke(clientBean, "blockchain_get_balance", new String[] { balanceId }, JSONObject.class);
    }

    public static String getPrivateKey(ClientBean clientBean, String address)
    {
        String privateKey = client.invoke(clientBean, "wallet_dump_private_key", new String[] { address }, String.class);
        return privateKey;
    }

    public static JSONArray getBalances(ClientBean clientBean, String publicKey)
    {
        JSONArray balances = client.invoke(clientBean, "blockchain_list_key_balances", new String[] { publicKey }, JSONArray.class);
        return balances;
    }

    public static JSONArray listAccounts(ClientBean clientBean)
    {
        return client.invoke(clientBean, "wallet_list_my_accounts", new String[] { "" }, JSONArray.class);
    }

    public static JSONArray getContractBalance(ClientBean clientBean, String contractId)
    {
        return client.invoke(clientBean, "get_contract_balance", new String[] { contractId }, JSONArray.class);
    }

    public static boolean check(ClientBean clientBean)
    {
        Map<String, Object> serverInfo = getInfo(clientBean);
        int blockAge = ((Integer)serverInfo.get("blockchain_head_block_age")).intValue();
        int connNum = ((Integer)serverInfo.get("network_num_connections")).intValue();
        System.out.println("achain server status: blockage:" + blockAge + ", connectNum: " + connNum);
        if (blockAge > 10)
        {
            System.out.println("服务器正在同步数据");
            return false;
        }
        return true;
    }
}
