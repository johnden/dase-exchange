package com.clg.wallet.wallet.bitcoin;

import java.util.*;
import org.slf4j.*;

public class BitcoinAcceptor
{
    private static final Logger  LOG = LoggerFactory.getLogger((Class)BitcoinAcceptor.class);;
    public final Bitcoin bitcoin;
    int monitorDepth;
    private final LinkedHashSet<BitcoinPaymentListener> listeners;
    private HashSet<String> seen;
    private String lastBlock;
    private String monitorBlock;
    public static final int DefaultCount = 6;

    public BitcoinAcceptor(final Bitcoin bitcoin, final String lastBlock, final int monitorDepth) {
        this.listeners = new LinkedHashSet<BitcoinPaymentListener>();
        this.seen = new HashSet<String>();
        this.monitorBlock = null;
        this.bitcoin = bitcoin;
        this.lastBlock = lastBlock;
        this.monitorDepth = monitorDepth;
    }

    public BitcoinAcceptor(final Bitcoin bitcoin) {
        this(bitcoin, null, 6);
    }

    public BitcoinAcceptor(final Bitcoin bitcoin, final String lastBlock, final int monitorDepth, final BitcoinPaymentListener listener) {
        this(bitcoin, lastBlock, monitorDepth);
        this.listeners.add(listener);
    }

    public BitcoinAcceptor(final Bitcoin bitcoin, final BitcoinPaymentListener listener) {
        this(bitcoin, null, 6);
        this.listeners.add(listener);
    }

    public String getAccountAddress(final String account) throws BitcoinException {
        final List<String> a = this.bitcoin.getAddressesByAccount(account);
        return a.isEmpty() ? this.bitcoin.getNewAddress(account) : a.get(0);
    }

    public synchronized String getLastBlock() {
        return this.lastBlock;
    }

    public synchronized void setLastBlock(final String lastBlock) throws BitcoinException {
        if (this.lastBlock != null) {
            throw new IllegalStateException("lastBlock already set");
        }
        this.lastBlock = lastBlock;
        this.updateMonitorBlock();
    }

    public synchronized BitcoinPaymentListener[] getListeners() {
        return this.listeners.toArray(new BitcoinPaymentListener[0]);
    }

    public synchronized void addListener(final BitcoinPaymentListener listener) {
        this.listeners.add(listener);
    }

    public synchronized void removeListener(final BitcoinPaymentListener listener) {
        this.listeners.remove(listener);
    }

    private void updateMonitorBlock() throws BitcoinException {
        this.monitorBlock = this.lastBlock;
        for (int i = 0; i < this.monitorDepth && this.monitorBlock != null; ++i) {
            final Bitcoin.Block b = this.bitcoin.getBlock(this.monitorBlock);
            this.monitorBlock = ((b == null) ? null : b.previousHash());
        }
    }

    public synchronized void checkPayments() throws BitcoinException {
        final Bitcoin.TransactionsSinceBlock transactions = (this.monitorBlock == null) ? this.bitcoin.listSinceBlock() : this.bitcoin.listSinceBlock(this.monitorBlock);
        if (null != transactions) {
            for (final Bitcoin.Transaction transaction : transactions.transactions()) {
                if ("receive".equals(transaction.category())) {
                    if (!this.seen.add(transaction.txId())) {
                        continue;
                    }
                    for (final BitcoinPaymentListener listener : this.listeners) {
                        try {
                            listener.transaction(transaction);
                        }
                        catch (Exception ex) {
                            LOG.error(ex.toString());
                        }
                    }
                }
            }
            if (!transactions.lastBlock().equals(this.lastBlock)) {
                this.seen.clear();
                this.lastBlock = transactions.lastBlock();
                this.updateMonitorBlock();
                for (final BitcoinPaymentListener listener2 : this.listeners) {
                    try {
                        listener2.block(this.lastBlock);
                    }
                    catch (Exception ex2) {
                        LOG.error(ex2.toString());
                    }
                }
            }
        }
    }
}
