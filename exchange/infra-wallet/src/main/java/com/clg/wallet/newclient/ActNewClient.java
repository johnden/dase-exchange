package com.clg.wallet.newclient;

import com.clg.wallet.help.*;
import java.math.*;
import com.clg.wallet.bean.*;
import com.clg.wallet.exception.*;
import com.alibaba.fastjson.*;
import com.clg.wallet.enums.*;
import com.clg.wallet.wallet.act.*;
import java.util.*;
import org.slf4j.*;

public class ActNewClient extends NormalClient
{
    private static JsonRpcClient client ;
    private static final Logger LOG = LoggerFactory.getLogger(ActNewClient.class);
    public static int BLOCK_NOT_FOUNT = 55536;

    public ActNewClient(ClientBean clientBean)
    {
        super(clientBean);
    }

    public ResultDTO getNewAddress()
    {
        return null;
    }

    public ResultDTO getNewAddress(String tag)
    {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String address = tag + uuid;
        return new ResultDTO().setResult(address);
    }

    public ResultDTO getBalance()
    {
        return null;
    }

    public ResultDTO getBalance(String publicKey)
    {
        JSONArray balanceInfo = AchainUtil.getBalances(this.mClientBean, publicKey);
        String balance = balanceInfo.getJSONArray(0).getJSONObject(1).getString("balance");
        double realBalance = Double.valueOf(balance).doubleValue() / 100000.0D;
        return new ResultDTO().setResult(BigDecimal.valueOf(realBalance));
    }

    public ResultDTO getBalanceByAccount(String account)
    {
        JSONArray balanceInfo = AchainUtil.getBalanceByAccountName(this.mClientBean, account);
        String balance = balanceInfo.getJSONArray(0).getJSONArray(1).getJSONArray(0).getString(1);
        double realBalance = Double.valueOf(balance).doubleValue() / 100000.0D;
        return new ResultDTO().setResult(BigDecimal.valueOf(realBalance));
    }

    public ResultDTO getTokenBalance(String assertId, String address)
    {
        JSONArray contractBalance = AchainUtil.getContractBalance(this.mClientBean, assertId);
        return new ResultDTO().setResult(contractBalance);
    }

    public ResultDTO getListAddress()
    {
        JSONArray accounts = (JSONArray)client.invoke(this.mClientBean, "wallet_list_my_accounts", new String[] { "" }, JSONArray.class);
        return new ResultDTO().setResult(accounts);
    }

    public ResultDTO getPeers()
    {
        return null;
    }

    public ResultDTO getBlockCount()
    {
        Long blockCount = AchainUtil.getBlockCount(this.mClientBean);
        return new ResultDTO().setResult(blockCount);
    }

    public ResultDTO getBlockHash(long blockNumber)
    {
        return null;
    }

    public ResultDTO getConnectionCount()
    {
        return null;
    }

    public ResultDTO dumpPrivKey(String address)
    {
        return null;
    }

    public ResultDTO validateAddress(String address)
    {
        return null;
    }

    public ResultDTO sendNormal(TxData tx)
    {
        BigDecimal accountAmount = (BigDecimal)getBalance(tx.getFormAddress()).getResult();
        BigDecimal value = tx.getBalance().setScale(5);
        if (accountAmount.compareTo(value.add(BigDecimal.ONE)) > 0)
        {
            AchainUtil.unlockWallet(this.mClientBean, "10", this.mClientBean.getWalletPass());

            String trueAmount = String.valueOf(value);
            JSONObject result = AchainUtil.walletTransferToAddress(this.mClientBean, trueAmount, tx.getFormAddress(), tx.getToAddress());

            String entryId = result.getString("entry_id");
            return new ResultDTO().setResult(entryId);
        }
        throw new AchainException("账户ACT余额不足");
    }

    public ResultDTO sendNormalToken(TxData tx)
    {
        BigDecimal value = tx.getBalance().setScale(5);

        BigDecimal accountAmount = (BigDecimal)getBalanceByAccount(tx.getFormAddress()).getResult();
        if (accountAmount.compareTo(BigDecimal.ONE) < 0) {
            throw new AchainException("账户ACT余额不足");
        }
        String transactionId = null;

        AchainUtil.unlockWallet(this.mClientBean, "10", this.mClientBean.getWalletPass());
        String toAddrAndAmount = tx.getToAddress() + "|" + String.valueOf(value);

        JSONObject callContractResult = AchainUtil.callContract(this.mClientBean, tx.getAssertId(), tx.getFormAddress(), toAddrAndAmount);
        transactionId = callContractResult.getString("entry_id");
        return new ResultDTO().setResult(transactionId);
    }

    public ResultDTO getPrettyTransaction(String txId)
    {
        JSONObject tx = AchainUtil.getPrettyTransaction(this.mClientBean, txId);
        return new ResultDTO().setResult(tx);
    }

    public ResultDTO getPrettyContractTransaction(String txId)
    {
        JSONObject tx = AchainUtil.getPrettyContractTransaction(this.mClientBean, txId);
        return new ResultDTO().setResult(tx);
    }

    public ResultDTO getTxBlockNumber(String number)
    {
        JSONObject block = AchainUtil.getBlock(this.mClientBean, number);
        return new ResultDTO().setResult(block);
    }

    public ResultDTO getBlockByNumber(String txIdOrNumber)
    {
        int blockNumber = 0;
        try
        {
            JSONArray transaction = AchainUtil.getTransaction(this.mClientBean, txIdOrNumber);
            blockNumber = Integer.valueOf(transaction.getJSONObject(1).getJSONObject("chain_location").getString("block_num")).intValue();
        }
        catch (AchainException e)
        {
            if (e.getMessage().contains("Block not found")) {
                return new ResultDTO().setStatusCode(BLOCK_NOT_FOUNT);
            }
        }
        catch (Exception e)
        {
            LOG.error(e.toString());
        }
        return new ResultDTO().setResult(Integer.valueOf(blockNumber));
    }

    public ResultDTO getAddrFromAcount(String account)
    {
        AchainUtil.openWallet(this.mClientBean, "hwang");
        JSONArray balanceInfo = AchainUtil.listAccounts(this.mClientBean);
        for (int i = 0; i < balanceInfo.size(); i++)
        {
            String name = balanceInfo.getJSONObject(i).getString("name");
            if (account.equalsIgnoreCase(name)) {
                return new ResultDTO().setResult(AchainUtil.walletGetAccountPublicAddress(this.mClientBean, name));
            }
        }
        return new ResultDTO().setStatusCode(ResultCode.OTHER_ERROR.getCode());
    }

    public boolean validateAssertId(String assertId)
    {
        return false;
    }

    public ResultDTO getAccountState(String address)
    {
        return null;
    }

    public ResultDTO getTokenDecimals(String assetId)
    {
        return null;
    }

    public ResultDTO getBestBlockHash()
    {
        return null;
    }

    public ResultDTO getPendingTx()
    {
        return null;
    }

    public ResultDTO getInfo()
    {
        return new ResultDTO().setResult(client.invoke(this.mClientBean, "info", new String[] { "" }, JSONObject.class));
    }

    public ResultDTO getTransactionConfirmed(String txid)
    {
        Long currentBlockNumber = AchainUtil.getBlockCount(this.mClientBean);
        Long txBlockNumber = Long.valueOf("" + getBlockByNumber(txid).getResult());
        if (txBlockNumber.longValue() == AchainCore.BLOCK_NOT_FOUNT) {
            return new ResultDTO().setResult(Integer.valueOf(AchainCore.BLOCK_NOT_FOUNT));
        }
        return new ResultDTO().setResult(Long.valueOf(currentBlockNumber.longValue() - txBlockNumber.longValue()));
    }

    public ResultDTO getTransactionFee(String txid)
    {
        return new ResultDTO().setResult(BigDecimal.ZERO);
    }

    public ResultDTO getTransaction(String txid)
    {
        JSONArray transaction = AchainUtil.getTransaction(this.mClientBean, txid);
        return new ResultDTO().setResult(transaction);
    }

    public static boolean checkCallContractResult(ClientBean clientBean, String entryId)
    {
        JSONObject contractInfo = null;
        try
        {
            contractInfo = AchainUtil.getContractResult(clientBean, entryId);
        }
        catch (AchainException e)
        {
            LOG.info(e.toString());
            return false;
        }
        if (null == contractInfo) {
            return false;
        }
        String blockNum = contractInfo.getString("block_num");
        String resultTrxId = contractInfo.getString("trx_id");

        JSONArray events = AchainUtil.getEvents(clientBean, blockNum, resultTrxId);

        String eventType = events.getJSONObject(0).getString("event_type");
        String eventParam = events.getJSONObject(0).getString("event_param");
        if ("transfer_to_success".equals(eventType))
        {
            String[] params = eventParam.split(",");
            String[] fromAccount = params[0].split(":");
            String[] toAccount = params[1].split(":");
            return true;
        }
        return false;
    }

    public static boolean check(ClientBean clientBean)
    {
        Map<String, Object> serverInfo = AchainUtil.getInfo(clientBean);
        int blockAge = ((Integer)serverInfo.get("blockchain_head_block_age")).intValue();
        int connNum = ((Integer)serverInfo.get("network_num_connections")).intValue();
        System.out.println("achain server status: blockage:" + blockAge + ", connectNum: " + connNum);
        if (blockAge > 10)
        {
            System.out.println("服务器正在同步数据");
            return false;
        }
        return true;
    }
}
