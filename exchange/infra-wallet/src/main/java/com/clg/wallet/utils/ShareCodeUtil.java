package com.clg.wallet.utils;

public class ShareCodeUtil
{
    private static final char[] r = { 'F', 'L', 'G', 'W', '5', 'X', 'C', '3', '9', 'Z', 'M', '6', '7', 'Y', 'R', 'T', '2', 'H', 'S', '8', 'D', 'V', 'E', 'J', '4', 'K', 'Q', 'P', 'U', 'A', 'N', 'B' };
    private static final int binLen = r.length;
    private static final long startNumber = 1000L;

    public static String idToCode(long id, long costomStartNumber)
    {
        if (costomStartNumber < 0L) {
            costomStartNumber = 1000L;
        }
        id += costomStartNumber;
        char[] buf = new char[32];
        int charPos = 32;
        while (id / binLen > 0L)
        {
            int ind = (int)(id % binLen);

            buf[(--charPos)] = r[ind];
            id /= binLen;
        }
        buf[(--charPos)] = r[((int)(id % binLen))];

        String str = new String(buf, charPos, 32 - charPos);
        return str.toUpperCase();
    }

    public static String idToCode(long idL)
    {
        return idToCode(idL, -1L);
    }

    public static String idToCode(String id)
    {
        long idL = Long.parseLong(id);
        return idToCode(idL, -1L);
    }

    public static String idToCode(String id, long costomStartNumber)
    {
        long idL = Long.parseLong(id);
        return idToCode(idL, costomStartNumber);
    }

    public static long codeToId(String code)
    {
        code = code.toUpperCase();
        char[] chs = code.toCharArray();
        long res = 0L;
        for (int i = 0; i < chs.length; i++)
        {
            int ind = 0;
            for (int j = 0; j < binLen; j++) {
                if (chs[i] == r[j])
                {
                    ind = j;
                    break;
                }
            }
            if (i > 0) {
                res = res * binLen + ind;
            } else {
                res = ind;
            }
        }
        res -= 1000L;
        return res;
    }

    public static void main(String[] args) {}

    public static String bytesToHexString(byte[] src)
    {
        StringBuilder stringBuilder = new StringBuilder("");
        if ((src == null) || (src.length <= 0)) {
            return null;
        }
        for (int i = 0; i < src.length; i++)
        {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }
}
