package com.clg.wallet.wallet.qtum;

import java.util.logging.*;
import java.net.*;
import java.util.*;
import java.math.*;
import com.clg.wallet.wallet.bitcoin.*;
import com.clg.wallet.utils.*;

public class QtumJSONRPCClient extends BitcoinJSONRPCClient implements Qtum
{

    private static final Logger logger = Logger.getLogger(QtumJSONRPCClient.class.getCanonicalName());

    public QtumJSONRPCClient(String rpcUrl)
            throws MalformedURLException
    {
        super(rpcUrl);
    }

    public QtumJSONRPCClient(URL rpc)
    {
        super(rpc);
    }

    public double qtum_getbalance(String token, String address, int precision)
            throws BitcoinException
    {
        String hexAddress = (String)query("gethexaddress", new Object[] { address });
        String contractData = "70a08231" + toContractData(hexAddress);
        Map result = (Map)query("callcontract", new Object[] { token, contractData });
        Map execResult = (Map)result.get("executionResult");
        double balance = BigDecimal.valueOf(hexToDec((String)execResult.get("output"))).divide(BigDecimal.TEN.pow(precision)).doubleValue();
        return balance;
    }

    public Qtum.QtumSender qtum_send(String token, String fromaddress, String toaddress, double amount, int precision)
            throws BitcoinException
    {
        String amountHex = BigDecimal.valueOf(amount).multiply(BigDecimal.TEN.pow(precision)).toBigInteger().toString(16);
        String toaddressHex = (String)query("gethexaddress", new Object[] { toaddress });
        String contractData = "a9059cbb" + toContractData(toaddressHex) + toContractData(amountHex);
        return new QtumSenderMapWrapper((Map)query("sendtocontract", new Object[] { token, contractData, Integer.valueOf(0), Integer.valueOf(100000), Double.valueOf(4.0E-7D), fromaddress }));
    }

    private String toContractData(String data)
    {
        String res = String.format("%64s", new Object[] { data });
        res = res.replaceAll("\\s", "0");
        return res;
    }

    private long hexToDec(String hexStr)
    {
        long dec_num = Long.parseLong(hexStr, 16);
        return dec_num;
    }

    private class QtumSenderMapWrapper
            extends MapWrapper
            implements Qtum.QtumSender
    {
        public QtumSenderMapWrapper(Map m)
        {
            super(m);
        }

        public String txid()
        {
            return mapStr("txid");
        }

        public String sender()
        {
            return mapStr("sender");
        }

        public String hash160()
        {
            return mapStr("hash160");
        }
    }
}
