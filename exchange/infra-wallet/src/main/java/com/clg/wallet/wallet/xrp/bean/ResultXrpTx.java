package com.clg.wallet.wallet.xrp.bean;

import java.util.*;

public class ResultXrpTx
{

    private List<Tx> txList;
    private Long blockNumber;

    public ResultXrpTx setTxList(List<Tx> txList)
    {
        this.txList = txList;return this;
    }

    public ResultXrpTx setBlockNumber(Long blockNumber)
    {
        this.blockNumber = blockNumber;return this;
    }

    public List<Tx> getTxList()
    {
        return this.txList;
    }

    public Long getBlockNumber()
    {
        return this.blockNumber;
    }
}
