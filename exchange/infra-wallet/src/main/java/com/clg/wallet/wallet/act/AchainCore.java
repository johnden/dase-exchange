package com.clg.wallet.wallet.act;

import com.clg.wallet.bean.*;
import java.math.*;
import com.clg.wallet.exception.*;
import com.alibaba.fastjson.*;
import java.util.*;
import org.slf4j.*;

public class AchainCore
{
    private static final Logger LOG = LoggerFactory.getLogger(AchainCore.class);
    public static int BLOCK_NOT_FOUNT = 55536;

    public static BigDecimal getBalance(ClientBean clientBean, String publicKey)
    {
        JSONArray balanceInfo = AchainUtil.getBalances(clientBean, publicKey);
        String balance = balanceInfo.getJSONArray(0).getJSONObject(1).getString("balance");
        double realBalance = Double.valueOf(balance).doubleValue() / 100000.0D;
        return BigDecimal.valueOf(realBalance);
    }

    public static BigDecimal getBalanceByAccountName(ClientBean clientBean, String accountName)
    {
        JSONArray balanceInfo = AchainUtil.getBalanceByAccountName(clientBean, accountName);
        String balance = balanceInfo.getJSONArray(0).getJSONArray(1).getJSONArray(0).getString(1);
        double realBalance = Double.valueOf(balance).doubleValue() / 100000.0D;
        return BigDecimal.valueOf(realBalance);
    }

    public static String getAddrFromAcount(ClientBean clientBean, String account)
    {
        AchainUtil.openWallet(clientBean, "hwang");
        JSONArray balanceInfo = AchainUtil.listAccounts(clientBean);
        for (int i = 0; i < balanceInfo.size(); i++)
        {
            String name = balanceInfo.getJSONObject(i).getString("name");
            if (account.equalsIgnoreCase(name)) {
                return AchainUtil.walletGetAccountPublicAddress(clientBean, name);
            }
        }
        return "";
    }

    public static int getCurrentBlockNumber(ClientBean clientBean, String id)
    {
        int blockNumber = 0;
        try
        {
            JSONArray transaction = AchainUtil.getTransaction(clientBean, id);
            return Integer.valueOf(transaction.getJSONObject(1).getJSONObject("chain_location").getString("block_num")).intValue();
        }
        catch (AchainException e)
        {
            if (e.getMessage().contains("Block not found")) {
                return BLOCK_NOT_FOUNT;
            }
        }
        catch (Exception e)
        {
            LOG.error(e.toString());
            return blockNumber;
        }
        return blockNumber;
    }

    public static boolean checkCallContractResult(ClientBean clientBean, String entryId)
    {
        JSONObject contractInfo = null;
        try
        {
            contractInfo = AchainUtil.getContractResult(clientBean, entryId);
        }
        catch (AchainException e)
        {
            LOG.info(e.toString());
            return false;
        }
        if (null == contractInfo) {
            return false;
        }
        String blockNum = contractInfo.getString("block_num");
        String resultTrxId = contractInfo.getString("trx_id");

        JSONArray events = AchainUtil.getEvents(clientBean, blockNum, resultTrxId);

        String eventType = events.getJSONObject(0).getString("event_type");
        String eventParam = events.getJSONObject(0).getString("event_param");
        if ("transfer_to_success".equals(eventType))
        {
            String[] params = eventParam.split(",");
            String[] fromAccount = params[0].split(":");
            String[] toAccount = params[1].split(":");
            return true;
        }
        return false;
    }

    public static String createAddress(String addr)
    {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        return addr + uuid;
    }
}
