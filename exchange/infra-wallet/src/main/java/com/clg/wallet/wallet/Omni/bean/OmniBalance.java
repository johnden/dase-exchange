package com.clg.wallet.wallet.Omni.bean;

import java.math.*;

public class OmniBalance
{
    private BigDecimal balance;
    private BigDecimal reserved;

    public OmniBalance setBalance(BigDecimal balance)
    {
        this.balance = balance;return this;
    }

    public OmniBalance setReserved(BigDecimal reserved)
    {
        this.reserved = reserved;return this;
    }



    public BigDecimal getBalance()
    {
        return this.balance;
    }

    public BigDecimal getReserved()
    {
        return this.reserved;
    }
}
