package com.clg.wallet.exception;

public class HttpClientException extends BaseException
{

    public HttpClientException(String message)
    {
        super(message);
    }

    public HttpClientException(String errorCode, String message)
    {
        super(errorCode, message);
    }
}
