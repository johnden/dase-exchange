package com.clg.wallet.wallet.bitcoin;

public interface BitcoinPaymentListener
{
    void block(String paramString);

    void transaction(Bitcoin.Transaction paramTransaction);
}
