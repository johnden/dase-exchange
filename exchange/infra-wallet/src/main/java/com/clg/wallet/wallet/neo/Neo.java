package com.clg.wallet.wallet.neo;

import com.clg.wallet.bean.*;
import java.math.*;

public interface Neo
{
    ResultDTO dumpPrivKey(String paramString);

    boolean validateAddress(String paramString);

    boolean validateAssertId(String paramString);

    ResultDTO getAccountState(String paramString);

    ResultDTO getBalance(String paramString);

    ResultDTO getTokenBalance(String paramString1, String paramString2);

    ResultDTO changeBalance(BigInteger paramBigInteger, int paramInt);

    ResultDTO getTokenDecimals(String paramString);

    ResultDTO getBestBlockHash();

    ResultDTO getJsonBlock(String paramString);

    ResultDTO getRawBlock(String paramString);

    ResultDTO getApplicationLog(String paramString);

    ResultDTO getBlockCount();

    ResultDTO getBlockHash(Long paramLong);

    ResultDTO getConnectionCount();

    ResultDTO getNewAddress();

    ResultDTO getRawMempool();

    ResultDTO getJsonTransaction(String paramString);

    ResultDTO getRawTransaction(String paramString);

    ResultDTO getTxOut(String paramString, Integer paramInteger);

    ResultDTO getPeers();

    ResultDTO listAddress();

    ResultDTO sendFrom(String paramString1, String paramString2, String paramString3, BigDecimal paramBigDecimal1, BigDecimal paramBigDecimal2);

    ResultDTO sendRawTransaction(String paramString);

    ResultDTO sendToAddress(String paramString1, String paramString2, BigDecimal paramBigDecimal1, BigDecimal paramBigDecimal2, String paramString3);

    boolean checkBalance(String paramString, BigDecimal paramBigDecimal);

    boolean checkTokenBalance(String paramString1, String paramString2, BigDecimal paramBigDecimal);

    String hashToAddress(String paramString);

    String addressToHash(String paramString);
}
