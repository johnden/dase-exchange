package com.clg.wallet.wallet.neo.bean;

public class Type
{
    private String type;
    private String value;

    public Type setType(String type)
    {
        this.type = type;return this;
    }

    public Type setValue(String value)
    {
        this.value = value;return this;
    }


    public String getType()
    {
        return this.type;
    }

    public String getValue()
    {
        return this.value;
    }
}
