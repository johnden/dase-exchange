package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.WalletCollectTask;

/**
 * <p>
 * 钱包归集任务 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface WalletCollectTaskMapper extends BaseMapper<WalletCollectTask> {

}
