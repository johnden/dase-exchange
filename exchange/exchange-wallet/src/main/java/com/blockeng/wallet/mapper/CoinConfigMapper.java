package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.CoinConfig;

/**
 * <p>
 * 币种钱包配置 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinConfigMapper extends BaseMapper<CoinConfig> {

}
