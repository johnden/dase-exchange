package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-07
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 获取用户所有邀请记录
     * @param userId
     * @return
     */
    List<User> getInviteList(@Param("user_id") Long userId);

    /**
     * 获取用户所有邀请总数
     * @param userId
     * @return
     */
    int getInviteCount(@Param("user_id") Long userId);

    /**
     * 获取用户订单总数
     * @param userId
     * @return
     */
    int getOrderCountByUserId(@Param("user_id") Long userId);
}
