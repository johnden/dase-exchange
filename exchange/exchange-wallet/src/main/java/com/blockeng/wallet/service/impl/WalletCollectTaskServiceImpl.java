package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.WalletCollectTask;
import com.blockeng.wallet.mapper.WalletCollectTaskMapper;
import com.blockeng.wallet.service.WalletCollectTaskService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包归集任务 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class WalletCollectTaskServiceImpl extends ServiceImpl<WalletCollectTaskMapper, WalletCollectTask> implements WalletCollectTaskService {

}
