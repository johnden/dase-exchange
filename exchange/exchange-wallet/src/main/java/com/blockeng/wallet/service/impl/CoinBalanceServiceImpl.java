package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.CoinBalance;
import com.blockeng.wallet.mapper.CoinBalanceMapper;
import com.blockeng.wallet.service.CoinBalanceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 币种余额 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class CoinBalanceServiceImpl extends ServiceImpl<CoinBalanceMapper, CoinBalance> implements CoinBalanceService {

}
