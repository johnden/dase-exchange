package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.AdminAddress;
import com.blockeng.wallet.mapper.AdminAddressMapper;
import com.blockeng.wallet.service.AdminAddressService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包归集提币地址管理 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class AdminAddressServiceImpl extends ServiceImpl<AdminAddressMapper, AdminAddress> implements AdminAddressService {

}
