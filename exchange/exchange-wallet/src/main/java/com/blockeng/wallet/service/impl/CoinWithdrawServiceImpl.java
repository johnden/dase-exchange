package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.CoinWithdraw;
import com.blockeng.wallet.mapper.CoinWithdrawMapper;
import com.blockeng.wallet.service.CoinWithdrawService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数字货币提现记录 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class CoinWithdrawServiceImpl extends ServiceImpl<CoinWithdrawMapper, CoinWithdraw> implements CoinWithdrawService {

}
