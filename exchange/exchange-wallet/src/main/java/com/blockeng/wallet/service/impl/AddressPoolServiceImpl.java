package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.AddressPool;
import com.blockeng.wallet.mapper.AddressPoolMapper;
import com.blockeng.wallet.service.AddressPoolService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包地址池 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class AddressPoolServiceImpl extends ServiceImpl<AddressPoolMapper, AddressPool> implements AddressPoolService {

}
