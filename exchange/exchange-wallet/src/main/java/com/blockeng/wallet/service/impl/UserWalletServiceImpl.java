package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.dto.CoinRechargeDto;
import com.blockeng.wallet.entity.Account;
import com.blockeng.wallet.entity.CoinRecharge;
import com.blockeng.wallet.entity.UserWallet;
import com.blockeng.wallet.entity.WalletCoinRecharge;
import com.blockeng.wallet.mapper.UserWalletMapper;
import com.blockeng.wallet.response.BaseResponse;
import com.blockeng.wallet.response.ResponseCode;
import com.blockeng.wallet.response.ResponseResultUtil;
import com.blockeng.wallet.service.AccountService;
import com.blockeng.wallet.service.CoinRechargeService;
import com.blockeng.wallet.service.UserWalletService;
import com.blockeng.wallet.service.WalletCoinRechargeService;
import com.blockeng.wallet.util.GsonUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 用户提币地址 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class UserWalletServiceImpl extends ServiceImpl<UserWalletMapper, UserWallet> implements UserWalletService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CoinRechargeService coinRechargeService;

    @Autowired
    private WalletCoinRechargeService walletCoinRechargeService;

    /**
     * 账户充值
     *
     * @param coinRechargeDto
     * @return
     */
    public BaseResponse updateRecharge(CoinRechargeDto coinRechargeDto) {
        String address = coinRechargeDto.getAddress();
        String txId = coinRechargeDto.getTxId();

        QueryWrapper<Account> accountWrapper = new QueryWrapper<>();
        accountWrapper.eq("rec_addr", address);
        Account account = accountService.selectOne(accountWrapper);
        if (StringUtils.isEmpty(account)) {
            return new ResponseResultUtil().error(ResponseCode.OPERATE_FAIL.getCode(), "不存在该用户地址");
        }
        QueryWrapper<CoinRecharge> coinRechargeWrapper = new QueryWrapper<>();
        coinRechargeWrapper.eq("address", address).and(wrapper -> wrapper.eq("txid", txId));
        CoinRecharge coinRecharge = coinRechargeService.selectOne(coinRechargeWrapper);
        if (StringUtils.isEmpty(coinRecharge)) {
            QueryWrapper<WalletCoinRecharge> walletCoinRechargeWrapper = new QueryWrapper<>();
            walletCoinRechargeWrapper.eq("address", address).and(wrapper -> wrapper.eq("txid", txId));
            WalletCoinRecharge walletCoinRecharge = walletCoinRechargeService.selectOne(walletCoinRechargeWrapper);
            if (StringUtils.isEmpty(walletCoinRecharge)) {
                return new ResponseResultUtil().error(ResponseCode.OPERATE_FAIL.getCode(), "钱包未充值成功");
            }
            rabbitTemplate.convertAndSend("finance.recharge.success", GsonUtil.toJson(walletCoinRecharge));
            return new ResponseResultUtil().success();
        } else {
            return new ResponseResultUtil().error(ResponseCode.OPERATE_FAIL.getCode(), "用户充值已经成功，无需再充值");
        }
    }
}
