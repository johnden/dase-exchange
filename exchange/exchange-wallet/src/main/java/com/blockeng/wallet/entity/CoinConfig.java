package com.blockeng.wallet.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 币种钱包配置
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("coin_config")
public class CoinConfig extends Model<CoinConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * 币种ID(对应coin表ID)
     */
    private Long id;
    /**
     * 币种名称
     */
    private String name;
    /**
     * 币种类型：btc-比特币系列；eth-以太坊；ethToken-以太坊代币；etc-以太经典；
     */
    @TableField("coin_type")
    private String coinType;
    /**
     * 钱包最低留存的币
     */
    @TableField("credit_limit")
    private BigDecimal creditLimit;
    /**
     * 是否自动提币
     */
    @TableField("auto_draw")
    private Integer autoDraw;
    /**
     * 自动提币的最高额度
     */
    @TableField("auto_draw_limit")
    private BigDecimal autoDrawLimit;
    /**
     * 当触发改状态的时候,开始归集
     */
    @TableField("credit_max_limit")
    private BigDecimal creditMaxLimit;
    /**
     * rpc服务ip
     */
    @TableField("rpc_ip")
    private String rpcIp;
    /**
     * rpc服务port
     */
    @TableField("rpc_port")
    private String rpcPort;
    /**
     * rpc用户
     */
    @TableField("rpc_user")
    private String rpcUser;
    /**
     * rpc密码
     */
    @TableField("rpc_pwd")
    private String rpcPwd;
    /**
     * 最后一个区块
     */
    @TableField("last_block")
    private String lastBlock;
    /**
     * 钱包用户名
     */
    @TableField("wallet_user")
    private String walletUser;
    /**
     * 钱包密码
     */
    @TableField("wallet_pass")
    private String walletPass;
    /**
     * 代币合约地址
     */
    @TableField("contract_address")
    private String contractAddress;
    /**
     * context
     */
    private String context;
    /**
     * 最低确认数
     */
    @TableField("min_confirm")
    private Integer minConfirm;
    /**
     * 定时任务
     */
    private String task;
    /**
     * 是否可用0不可用,1可用
     */
    private Integer status;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
