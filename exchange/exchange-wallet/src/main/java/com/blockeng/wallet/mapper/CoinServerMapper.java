package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.CoinServer;

/**
 * <p>
 * 钱包服务器监控信息 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinServerMapper extends BaseMapper<CoinServer> {

}
