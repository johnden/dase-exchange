package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.dto.CoinRechargeDto;
import com.blockeng.wallet.entity.UserWallet;

/**
 * <p>
 * 用户提币地址 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface UserWalletService extends IService<UserWallet> {

    /**
     * 账户充值
     * @param coinRechargeDto
     * @return
     */
    Object updateRecharge(CoinRechargeDto coinRechargeDto);
}
