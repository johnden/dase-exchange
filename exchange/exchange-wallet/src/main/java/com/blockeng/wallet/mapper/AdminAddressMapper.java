package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.AdminAddress;

/**
 * <p>
 * 钱包归集提币地址管理 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface AdminAddressMapper extends BaseMapper<AdminAddress> {

}
