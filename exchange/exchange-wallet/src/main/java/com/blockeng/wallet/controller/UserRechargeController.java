package com.blockeng.wallet.controller;

import com.blockeng.wallet.dto.CoinRechargeDto;
import com.blockeng.wallet.response.ResponseCode;
import com.blockeng.wallet.response.ResponseResultUtil;
import com.blockeng.wallet.service.UserWalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月19日
 * @修改记录
 */
@RestController
@RequestMapping("wallet")
@Api(value = "用户钱包", description = "钱包服务器")
public class UserRechargeController {
    @Autowired
    private UserWalletService userWalletService;

    @RequestMapping(value = "/updateRecharge", method = RequestMethod.POST)
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")}, value = "用户充值", httpMethod = "POST")
    public Object updateRecharge(@RequestBody CoinRechargeDto coinRechargeDto) {
        if (!StringUtils.isEmpty(coinRechargeDto)) {
            if (!StringUtils.isEmpty(coinRechargeDto.getAddress()) && !StringUtils.isEmpty(coinRechargeDto.getTxId())) {
                return userWalletService.updateRecharge(coinRechargeDto);
            } else {
                return new ResponseResultUtil().error(ResponseCode.OPERATE_FAIL.getCode(),"地址或订单号不能为空");
            }
        } else {
            return new ResponseResultUtil().error(ResponseCode.OPERATE_FAIL.getCode(),"参数不能为空");
        }
    }
}
