package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 数字货币充值记录
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wallet_coin_recharge")
public class WalletCoinRecharge extends Model<WalletCoinRecharge> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 币种名称
     */
    @TableField("coin_name")
    private String coinName;
    /**
     * 币种类型
     */
    @TableField("coin_type")
    private String coinType;
    /**
     * 钱包地址
     */
    private String address;
    /**
     * 充值确认数
     */
    private Integer confirm;
    /**
     * 状态：0-待入帐；1-充值失败，2到账失败，3到账成功；
     */
    private Integer status;
    /**
     * 交易id
     */
    private String txid;
    /**
     * 实际到账
     */
    private BigDecimal amount;
    /**
     * 修改时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
