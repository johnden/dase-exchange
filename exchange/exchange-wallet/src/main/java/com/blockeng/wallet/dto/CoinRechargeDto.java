package com.blockeng.wallet.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月19日
 * @修改记录
 */
@Data
@Accessors(chain = true)
public class CoinRechargeDto {
    /**
     * 充值地址
     */
    @NotEmpty(message = "充值地址")
    private String address;

    /**
     * 订单id
     */
    @NotEmpty(message = "充值的订单id")
    private String txId;
}
