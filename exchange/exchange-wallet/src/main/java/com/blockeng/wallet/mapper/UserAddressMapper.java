package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.UserAddress;

/**
 * <p>
 * 用户钱包地址信息 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}
