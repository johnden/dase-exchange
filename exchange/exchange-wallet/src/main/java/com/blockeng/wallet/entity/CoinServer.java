package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包服务器监控信息
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("coin_server")
public class CoinServer extends Model<CoinServer> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 钱包服务器ip
     */
    @TableField("rpc_ip")
    private String rpcIp;
    /**
     * 钱包服务器ip
     */
    @TableField("rpc_port")
    private String rpcPort;
    /**
     * 服务是否运行 0:正常,1:停止
     */
    private Integer running;
    /**
     * 钱包服务器区块高度
     */
    @TableField("wallet_number")
    private Long walletNumber;
    /**
     * 钱包名称
     */
    @TableField("coin_name")
    private String coinName;
    /**
     * 备注信息
     */
    private String mark;
    /**
     * 真实区块高度
     */
    @TableField("real_number")
    private Long realNumber;
    /**
     * 修改时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
