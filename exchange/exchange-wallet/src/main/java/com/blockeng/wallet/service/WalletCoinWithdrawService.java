package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.CoinWithdraw;

/**
 * <p>
 * 数字货币提现记录 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface WalletCoinWithdrawService extends IService<CoinWithdraw> {

}
