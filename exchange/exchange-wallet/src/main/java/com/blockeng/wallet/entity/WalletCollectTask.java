package com.blockeng.wallet.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包归集任务
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wallet_collect_task")
public class WalletCollectTask extends Model<WalletCollectTask> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 币种ID
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 币种类型
     */
    @TableField("coin_type")
    private String coinType;
    /**
     * 币种名称
     */
    @TableField("coin_name")
    private String coinName;
    /**
     * 来自哪个用户
     */
    @TableField("user_id")
    private Long userId;
    /**
     * txid
     */
    private String txid;
    /**
     * 归集数量
     */
    private BigDecimal amount;
    /**
     * 链上手续费
     */
    @TableField("chain_fee")
    private BigDecimal chainFee;
    /**
     * 备注
     */
    private String mark;
    /**
     * 是否处理
     */
    private Integer status;
    /**
     * 来自哪个地址
     */
    @TableField("from_address")
    private String fromAddress;
    /**
     * 转到哪里
     */
    @TableField("to_address")
    private String toAddress;
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
