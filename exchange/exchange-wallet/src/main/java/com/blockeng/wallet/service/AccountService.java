package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.Account;

/**
 * <p>
 * 资金账户 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface AccountService extends IService<Account> {

}
