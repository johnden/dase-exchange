package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.AddressPool;

/**
 * <p>
 * 钱包地址池 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface AddressPoolService extends IService<AddressPool> {

}
