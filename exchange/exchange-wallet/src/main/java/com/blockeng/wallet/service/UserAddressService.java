package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.UserAddress;

/**
 * <p>
 * 用户钱包地址信息 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface UserAddressService extends IService<UserAddress> {

}
