package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.Account;

/**
 * <p>
 * 资金账户 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface AccountMapper extends BaseMapper<Account> {

}
