package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包归集提币地址管理
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("admin_address")
public class AdminAddress extends Model<AdminAddress> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 币种Id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * eth keystore
     */
    private String keystore;
    /**
     * eth账号密码
     */
    private String pwd;
    /**
     * 地址
     */
    private String address;
    /**
     * 1:归账(冷钱包地址),2:打款,3:手续费
     */
    private Integer status;
    /**
     * 类型
     */
    @TableField("coin_type")
    private String coinType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
