package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户钱包地址信息
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_address")
public class UserAddress extends Model<UserAddress> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 币种ID
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 地址
     */
    private String address;
    /**
     * keystore
     */
    private String keystore;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 更新时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;
    /**
     * 错误数据id
     */
    private Long markid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
