package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.CoinConfig;
import com.blockeng.wallet.mapper.CoinConfigMapper;
import com.blockeng.wallet.service.CoinConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 币种钱包配置 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class CoinConfigServiceImpl extends ServiceImpl<CoinConfigMapper, CoinConfig> implements CoinConfigService {

}
