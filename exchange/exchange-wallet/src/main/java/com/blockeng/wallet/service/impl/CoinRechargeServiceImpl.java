package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.CoinRecharge;
import com.blockeng.wallet.mapper.CoinRechargeMapper;
import com.blockeng.wallet.service.CoinRechargeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数字货币充值记录 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class CoinRechargeServiceImpl extends ServiceImpl<CoinRechargeMapper, CoinRecharge> implements CoinRechargeService {

}
