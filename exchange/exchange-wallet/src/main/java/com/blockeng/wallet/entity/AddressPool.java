package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包地址池
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("address_pool")
public class AddressPool extends Model<AddressPool> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 币种ID
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 地址
     */
    private String address;
    /**
     * keystore
     */
    private String keystore;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 地址类型
     */
    @TableField("coin_type")
    private String coinType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
