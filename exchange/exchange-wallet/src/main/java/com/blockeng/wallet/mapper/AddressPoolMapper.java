package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.AddressPool;

/**
 * <p>
 * 钱包地址池 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface AddressPoolMapper extends BaseMapper<AddressPool> {

}
