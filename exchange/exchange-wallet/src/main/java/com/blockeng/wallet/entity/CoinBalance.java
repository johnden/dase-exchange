package com.blockeng.wallet.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 币种余额
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("coin_balance")
public class CoinBalance extends Model<CoinBalance> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 币种ID
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 币种名称
     */
    @TableField("coin_name")
    private String coinName;
    /**
     * 系统余额（根据充值提币计算）
     */
    @TableField("system_balance")
    private BigDecimal systemBalance;
    /**
     * 币种类型
     */
    @TableField("coin_type")
    private String coinType;
    /**
     * 归集账户余额
     */
    @TableField("collect_account_balance")
    private BigDecimal collectAccountBalance;
    /**
     * 钱包账户余额
     */
    @TableField("loan_account_balance")
    private BigDecimal loanAccountBalance;
    /**
     * 手续费账户余额(eth转账需要手续费)
     */
    @TableField("fee_account_balance")
    private BigDecimal feeAccountBalance;
    /**
     * 充值账户余额(一般xrp,bts这些需要)
     */
    @TableField("recharge_account_balance")
    private BigDecimal rechargeAccountBalance;
    /**
     * 更新时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
