package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.CoinBalance;

/**
 * <p>
 * 币种余额 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinBalanceService extends IService<CoinBalance> {

}
