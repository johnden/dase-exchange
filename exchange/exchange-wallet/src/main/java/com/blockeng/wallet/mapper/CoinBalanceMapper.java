package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.CoinBalance;

/**
 * <p>
 * 币种余额 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinBalanceMapper extends BaseMapper<CoinBalance> {

}
