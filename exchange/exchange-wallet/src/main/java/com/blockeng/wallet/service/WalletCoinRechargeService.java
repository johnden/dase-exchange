package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.WalletCoinRecharge;

/**
 * <p>
 * 数字货币充值记录 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface WalletCoinRechargeService extends IService<WalletCoinRecharge> {

}
