package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.User;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-07
 */
public interface UserService extends IService<User> {

}
