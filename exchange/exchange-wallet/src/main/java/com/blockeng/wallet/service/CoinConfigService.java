package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.CoinConfig;

/**
 * <p>
 * 币种钱包配置 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinConfigService extends IService<CoinConfig> {

}
