package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.CoinServer;

/**
 * <p>
 * 钱包服务器监控信息 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinServerService extends IService<CoinServer> {

}
