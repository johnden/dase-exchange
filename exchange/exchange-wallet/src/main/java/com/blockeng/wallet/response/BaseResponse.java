package com.blockeng.wallet.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BaseResponse {
    /**
     * @description 响应码
     */
    private int code;

    /**
     * @description 响应消息
     */
    private String message;

    /**
     * @description 数据
     */
    private Object data;

    public BaseResponse(int code, String message, Object data) {
        super();
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
