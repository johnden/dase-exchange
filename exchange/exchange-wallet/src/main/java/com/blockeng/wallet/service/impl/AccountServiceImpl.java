package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.Account;
import com.blockeng.wallet.mapper.AccountMapper;
import com.blockeng.wallet.service.AccountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金账户 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

}
