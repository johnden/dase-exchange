package com.blockeng.wallet.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.wallet.entity.Account;
import com.blockeng.wallet.entity.User;
import com.blockeng.wallet.response.BaseResponse;
import com.blockeng.wallet.response.ResponseResultUtil;
import com.blockeng.wallet.service.AccountService;
import com.blockeng.wallet.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月05日
 * @修改记录
 */
@RestController
@RequestMapping("account")
@Api(value = "钱包服务器", description = "用户管理")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private UserService userService;



    @RequestMapping(value = "initialAccount", method = RequestMethod.GET)
    @ApiOperation("初始化币帐户")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "user_id", value = "用户编号", required = true, dataType = "Long"),
            @ApiImplicitParam(paramType = "query", name = "coin_id", value = "币编号", required = true, dataType = "Long"),
            @ApiImplicitParam(paramType = "query", name = "begin_date", value = "用户注册时间", required = true, dataType = "String")
    })
    public BaseResponse initialAccount(@RequestParam(value = "user_id", defaultValue = "0") Long userId, @RequestParam("coin_id") Long coinId, @RequestParam(value = "begin_date", defaultValue = "") String begin_date) {
        if (userId > 0) {
            QueryWrapper<Account> accountWrapper = new QueryWrapper<>();
            accountWrapper.eq("user_id", userId).and(wrapper -> wrapper.eq("coin_id", coinId));
            Account accountOne = accountService.selectOne(accountWrapper);
            if (StringUtils.isEmpty(accountOne)) {
                Account account = new Account()
                        .setUserId(userId)
                        .setStatus(1)
                        .setCoinId(coinId)
                        .setWithdrawalsAmount(BigDecimal.ZERO)
                        .setBalanceAmount(BigDecimal.ZERO)
                        .setFreezeAmount(BigDecimal.ZERO)
                        .setRechargeAmount(BigDecimal.ZERO)
                        .setNetValue(BigDecimal.ZERO)
                        .setLockMargin(BigDecimal.ZERO)
                        .setFloatProfit(BigDecimal.ZERO)
                        .setTotalProfit(BigDecimal.ZERO)
                        .setRecAddr("")
                        .setVersion(0L);
                accountService.insert(account);
            }

        } else {
            List<User> users;
            if (!StringUtils.isEmpty(begin_date)) {
                QueryWrapper<User> userWrapper = new QueryWrapper<>();
                userWrapper.gt("created", begin_date);
                users = userService.selectList(userWrapper);
            } else {
                QueryWrapper<User> userWrapper = new QueryWrapper<>();
                userWrapper.gt("id", 0);
                users = userService.selectList(userWrapper);
            }
            for (User user : users) {
                QueryWrapper<Account> accountWrapper = new QueryWrapper<>();
                accountWrapper.eq("user_id", user.getId()).and(wrapper -> wrapper.eq("coin_id", coinId));
                Account accountOne = accountService.selectOne(accountWrapper);
                if (StringUtils.isEmpty(accountOne)) {
                    Account account = new Account()
                            .setUserId(user.getId())
                            .setStatus(1)
                            .setCoinId(coinId)
                            .setWithdrawalsAmount(BigDecimal.ZERO)
                            .setBalanceAmount(BigDecimal.ZERO)
                            .setFreezeAmount(BigDecimal.ZERO)
                            .setRechargeAmount(BigDecimal.ZERO)
                            .setNetValue(BigDecimal.ZERO)
                            .setLockMargin(BigDecimal.ZERO)
                            .setFloatProfit(BigDecimal.ZERO)
                            .setTotalProfit(BigDecimal.ZERO)
                            .setRecAddr("")
                            .setVersion(0L);
                    accountService.insert(account);
                }
            }
        }
        return new ResponseResultUtil().success();
    }
}
