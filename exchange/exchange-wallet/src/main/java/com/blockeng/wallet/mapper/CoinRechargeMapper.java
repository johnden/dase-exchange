package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.CoinRecharge;

/**
 * <p>
 * 数字货币充值记录 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinRechargeMapper extends BaseMapper<CoinRecharge> {

}
