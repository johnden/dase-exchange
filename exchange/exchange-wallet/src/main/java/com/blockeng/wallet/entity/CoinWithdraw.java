package com.blockeng.wallet.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数字货币提现记录
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("coin_withdraw")
public class CoinWithdraw extends Model<CoinWithdraw> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 币种名称
     */
    @TableField("coin_name")
    private String coinName;
    /**
     * 币种类型
     */
    @TableField("coin_type")
    private String coinType;
    /**
     * 钱包地址
     */
    private String address;
    /**
     * 交易id
     */
    private String txid;
    /**
     * 提现量
     */
    private BigDecimal num;
    /**
     * 手续费()
     */
    private BigDecimal fee;
    /**
     * 实际提现
     */
    private BigDecimal mum;
    /**
     * 0站内1其他
     */
    private Boolean type;
    /**
     * 链上手续费花费
     */
    @TableField("chain_fee")
    private BigDecimal chainFee;
    /**
     * 区块高度
     */
    @TableField("block_num")
    private Integer blockNum;
    /**
     * 钱包提币备注备注
     */
    @TableField("wallet_mark")
    private String walletMark;
    /**
     * 后台审核人员提币备注备注
     */
    private String remark;
    /**
     * 当前审核级数
     */
    private Integer step;
    /**
     * 状态：0-审核中；1-成功；2-拒绝；3-撤销；4-审核通过；5-打币中；
     */
    private Boolean status;
    /**
     * 审核时间
     */
    @TableField("audit_time")
    private LocalDateTime auditTime;
    /**
     * 修改时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
