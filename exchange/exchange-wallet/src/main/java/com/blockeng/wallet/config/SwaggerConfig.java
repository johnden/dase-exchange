package com.blockeng.wallet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年02月28日
 * @修改记录
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     *  创建API应用
     *  apiInfo() 增加API相关信息
     *  通过select()函数返回一个ApiSelectorBuilder实例,用来控制哪些接口暴露给Swagger来展现，
     *  本例采用指定扫描的包路径来定义指定要建立API的目录。
     * @return
     */
    @Bean
    public Docket createRestApi()
    {
        return  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.blockeng.wallet.controller"))
                .paths(PathSelectors.any())
                .build();
    }


    /**
     *  创建该API的基本信息（这些基本信息会展现在文档页面中）
     *  访问地址：http://项目实际地址/swagger-ui.html
     * @return
     */
    public ApiInfo apiInfo()
    {
        return new ApiInfoBuilder()
                .title("exchange-wallet钱包系统")
                .description("exchange-wallet钱包系统")
                .termsOfServiceUrl("www.bhao.vip")
                .contact(new Contact("链云（广州）区块链科技有限公司","www.bhao.vip","hnlshzx@163.com"))
                .version("1.0.0")
                .build();
    }
}
