package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.CoinWithdraw;

/**
 * <p>
 * 数字货币提现记录 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface CoinWithdrawMapper extends BaseMapper<CoinWithdraw> {

}
