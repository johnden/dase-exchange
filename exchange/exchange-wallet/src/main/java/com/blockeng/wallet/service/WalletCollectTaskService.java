package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.WalletCollectTask;

/**
 * <p>
 * 钱包归集任务 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface WalletCollectTaskService extends IService<WalletCollectTask> {

}
