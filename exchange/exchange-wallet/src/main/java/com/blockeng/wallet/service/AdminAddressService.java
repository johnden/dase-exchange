package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.wallet.entity.AdminAddress;

/**
 * <p>
 * 钱包归集提币地址管理 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface AdminAddressService extends IService<AdminAddress> {

}
