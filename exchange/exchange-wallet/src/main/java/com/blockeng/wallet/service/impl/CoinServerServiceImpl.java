package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.CoinServer;
import com.blockeng.wallet.mapper.CoinServerMapper;
import com.blockeng.wallet.service.CoinServerService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包服务器监控信息 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class CoinServerServiceImpl extends ServiceImpl<CoinServerMapper, CoinServer> implements CoinServerService {

}
