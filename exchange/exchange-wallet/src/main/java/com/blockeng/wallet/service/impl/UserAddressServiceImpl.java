package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.wallet.entity.UserAddress;
import com.blockeng.wallet.mapper.UserAddressMapper;
import com.blockeng.wallet.service.UserAddressService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户钱包地址信息 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements UserAddressService {

}
