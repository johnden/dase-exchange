package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.wallet.entity.UserWallet;

/**
 * <p>
 * 用户提币地址 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-19
 */
public interface UserWalletMapper extends BaseMapper<UserWallet> {

}
