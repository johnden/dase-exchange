package com.blockeng.wallet.help;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import com.blockeng.wallet.utils.*;
import org.springframework.util.*;
import java.util.*;
import com.clg.wallet.bean.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.newclient.*;
import org.slf4j.*;

@Component
public class ClientInfo
{
    private static final Logger LOG = LoggerFactory.getLogger(ClientInfo.class);
    HashMap<String, List<CoinConfig>> typeCoinMap = new HashMap();
    HashMap<Long, CoinConfig> idCoinMap = new HashMap();
    Map<String, Map<String, CoinConfig>> typeContractAddrMap = new HashMap();
    Map<String, Map<Long, CoinConfig>> typeContractIdMap = new HashMap();
    Map<Long, String> netIpMap = new HashMap();
    @Autowired
    private AdminAddressService adminAddressService;
    @Autowired
    private CoinConfigService coinConfigService;
    private List<CoinConfig> allCoinList;
    private long oldTime;
    private long INTERVAL_TIME = 60000L;
    @Autowired
    private DESUtil desUtil;
    @Autowired
    private IpUtils ipUtils;

    public String getCoinNetIp(Long id)
    {
        return this.netIpMap.get(id);
    }

    public List<CoinConfig> getCoinConfigFormType(String type)
    {
        return getCoin(Long.valueOf(0L), type);
    }

    public HashMap<String, List<CoinConfig>> getTypeCoinMap()
    {
        initCoinData();
        return this.typeCoinMap;
    }

    public CoinConfig getCoinConfigFormId(Long coinId)
    {
        List<CoinConfig> coin = getCoin(coinId, null);
        return coin.get(0);
    }

    private List<CoinConfig> getCoin(Long coinId, String type)
    {
        initCoinData();
        if (coinId.longValue() > 0L)
        {
            List<CoinConfig> coinList = new ArrayList();
            coinList.add(this.idCoinMap.get(coinId));
            return coinList;
        }
        return this.typeCoinMap.get(type.toLowerCase());
    }

    public synchronized void initCoinData(boolean sysn)
    {
        long nowTime = System.currentTimeMillis();
        if ((!sysn) && (!CollectionUtils.isEmpty(this.allCoinList)) && (nowTime - this.oldTime <= this.INTERVAL_TIME)) {
            return;
        }
        this.oldTime = nowTime;
        List<CoinConfig> nowCoinList = this.coinConfigService.selectAllCoin();
        if (CollectionUtils.isEmpty(nowCoinList))
        {
            LOG.error("币种信息为空");
            return;
        }
        if (compareList(nowCoinList, this.allCoinList)) {
            return;
        }
        this.allCoinList = nowCoinList;
        this.typeContractAddrMap.clear();
        this.netIpMap.clear();
        this.idCoinMap.clear();
        this.typeCoinMap.clear();

        String ip = this.ipUtils.getNetIp();
        LOG.info("公网ip:" + ip);
        for (CoinConfig item : this.allCoinList)
        {
            String rpcIp = item.getRpcIp();
            String rpcPwd = item.getRpcPwd();
            if (!this.ipUtils.isIP(rpcIp))
            {
                LOG.error("ip网络格式错误");
            }
            else if (StringUtils.isEmpty(item.getTask()))
            {
                LOG.error("task不能为空或者格式错误");
            }
            else
            {
                String type = item.getCoinType();
                if (StringUtils.isEmpty(type))
                {
                    LOG.error("币种类型错误");
                }
                else
                {
                    this.netIpMap.put(item.getId(), item.getRpcIp());
                    if ((!StringUtils.isEmpty(ip)) && (ip.equalsIgnoreCase(item.getRpcIp())))
                    {
                        LOG.info("更改网络ip");
                        item.setRpcIp("127.0.0.1");
                    }
                    if (!StringUtils.isEmpty(rpcPwd)) {
                        item.setRpcPwd(this.desUtil.decrypt(rpcPwd));
                    }
                    if (!StringUtils.isEmpty(item.getWalletPass())) {
                        item.setWalletPass(this.desUtil.decrypt(item.getWalletPass()));
                    }
                    this.idCoinMap.put(item.getId(), item);

                    type = type.toLowerCase();
                    if (!this.typeCoinMap.containsKey(type)) {
                        this.typeCoinMap.put(type, new ArrayList());
                    }
                    this.typeCoinMap.get(type).add(item);
                }
            }
        }
    }

    public synchronized void initCoinData()
    {
        initCoinData(false);
    }

    public ClientBean getClientInfoFromType(String type)
    {
        List<CoinConfig> coinList = getCoin(Long.valueOf(0L), type);
        if (!CollectionUtils.isEmpty(coinList)) {
            return getClientBean(coinList.get(0));
        }
        return null;
    }

    public ClientBean getClientBean(CoinConfig coinConfig)
    {
        ClientBean clientBean = new ClientBean();

        clientBean.setId(coinConfig.getId());
        clientBean.setName(coinConfig.getName());
        clientBean.setCoinType(coinConfig.getCoinType());
        clientBean.setCreditLimit(coinConfig.getCreditLimit());
        clientBean.setRpcIp(coinConfig.getRpcIp());
        clientBean.setRpcPort(coinConfig.getRpcPort());
        clientBean.setRpcUser(coinConfig.getRpcUser());
        clientBean.setRpcPwd(coinConfig.getRpcPwd());
        clientBean.setLastBlock(coinConfig.getLastBlock());
        clientBean.setWalletUser(coinConfig.getWalletUser());
        clientBean.setContractAddress(coinConfig.getContractAddress());
        clientBean.setMinConfirm(coinConfig.getMinConfirm());
        clientBean.setWalletPass(coinConfig.getWalletPass());
        clientBean.setContext(coinConfig.getContext());
        clientBean.setTask(coinConfig.getTask());
        clientBean.setStatus(coinConfig.getStatus());

        return clientBean;
    }

    public ClientBean getClientInfoFromId(Long coinId)
    {
        List<CoinConfig> coinList = getCoin(coinId, null);
        if (!CollectionUtils.isEmpty(coinList)) {
            return getClientBean(coinList.get(0));
        }
        return null;
    }

    private boolean compareList(List<CoinConfig> nowList, List<CoinConfig> oldList)
    {
        if (nowList == oldList) {
            return true;
        }
        if (CollectionUtils.isEmpty(nowList)) {
            return false;
        }
        if (CollectionUtils.isEmpty(oldList)) {
            return false;
        }
        if (nowList.size() != oldList.size()) {
            return false;
        }
        for (int i = 0; i < nowList.size(); i++)
        {
            CoinConfig nowCoinConfig = nowList.get(i);
            CoinConfig oldCoinConfig = oldList.get(i);
            if (!nowCoinConfig.toString().equalsIgnoreCase(oldCoinConfig.toString())) {
                return false;
            }
        }
        return true;
    }

    public Map<String, CoinConfig> getTokenList(String type)
    {
        type = type.toLowerCase();
        Map<String, CoinConfig> contractCoinMap = this.typeContractAddrMap.get(type);
        if (CollectionUtils.isEmpty(contractCoinMap))
        {
            contractCoinMap = new HashMap();
            List<CoinConfig> configList = getCoinConfigFormType(type);
            if (!CollectionUtils.isEmpty(configList))
            {
                for (CoinConfig coin : configList)
                {
                    CoinConfig coinConfig = contractCoinMap.get(coin.getContractAddress());
                    if ((null == coinConfig) && (!StringUtils.isEmpty(coin.getContractAddress())))
                    {
                        String contractAddress = coin.getContractAddress();
                        if (!coin.getContractAddress().startsWith("0x")) {
                            contractAddress = "0x" + contractAddress;
                        }
                        contractCoinMap.put(contractAddress.toLowerCase(), coin);
                    }
                }
                this.typeContractAddrMap.put(type, contractCoinMap);
            }
        }
        return contractCoinMap;
    }

    public Map<Long, CoinConfig> getTokenIdList(String type)
    {
        type = type.toLowerCase();
        Map<Long, CoinConfig> contractCoinMap = this.typeContractIdMap.get(type);
        if (CollectionUtils.isEmpty(contractCoinMap))
        {
            contractCoinMap = new HashMap();
            List<CoinConfig> configList = getCoinConfigFormType(type);
            if (!CollectionUtils.isEmpty(configList))
            {
                for (CoinConfig coin : configList)
                {
                    CoinConfig coinConfig = contractCoinMap.get(coin.getContractAddress());
                    if ((null == coinConfig) && (coin.getId().longValue() > 0L)) {
                        contractCoinMap.put(coin.getId(), coin);
                    }
                }
                this.typeContractIdMap.put(type, contractCoinMap);
            }
        }
        return contractCoinMap;
    }

    public EThTransactionBean getEthTransactionBean(CoinConfig coin, CoinWithdraw item)
    {
        String type = coin.getCoinType().toLowerCase();
        type = type.replace("Token", "");
        AdminAddress payAdminAddress = this.adminAddressService.queryAdminAccount(type, 2);
        AdminAddress feeAdminAddress = this.adminAddressService.queryAdminAccount(type, 3);
        EThTransactionBean eThTransactionBean = new EThTransactionBean();
        eThTransactionBean
                .setFromAddress(payAdminAddress.getAddress())
                .setFromUserKeystore(this.desUtil.decrypt(payAdminAddress.getKeystore()))
                .setFromUserPass(this.desUtil.decrypt(payAdminAddress.getPwd()))
                .setToAddress(item.getAddress().trim())
                .setBalance(item.getMum())
                .setTokenBalance(item.getMum())
                .setContractAddress(coin.getContractAddress());
        if (null != feeAdminAddress) {
            eThTransactionBean.setFeeAddress(feeAdminAddress.getAddress()).setFeeUserKeystorePath(this.desUtil.decrypt(feeAdminAddress.getKeystore())).setFeeUserPass(this.desUtil.decrypt(feeAdminAddress.getPwd()));
        }
        return eThTransactionBean;
    }

    public Client getClientFromType(String type)
    {
        type = type.toLowerCase();
        if (StringUtils.isEmpty(type)) {
            throw new RuntimeException("获取客户端的" + type + "不能为空");
        }
        ClientBean clientType = getClientInfoFromType(type);
        if (null == clientType) {
            throw new RuntimeException("获取客户端的信息不能为空");
        }
        Client client = ClientFactory.getClient(clientType);
        if (null == client) {
            throw new RuntimeException("获取客户端的实体不能为空");
        }
        return client;
    }

    public Client getClientFromId(Long coinId)
    {
        if ((null == coinId) || (coinId.longValue() <= 0L)) {
            throw new RuntimeException("获取客户端的失败不能,coinid不能为零或者为空");
        }
        ClientBean clientType = getClientInfoFromId(coinId);
        if (null == clientType) {
            throw new RuntimeException("获取客户端的信息不能为空");
        }
        Client client = ClientFactory.getClient(clientType);
        if (null == client) {
            throw new RuntimeException("获取客户端的实体不能为空");
        }
        return client;
    }
}
