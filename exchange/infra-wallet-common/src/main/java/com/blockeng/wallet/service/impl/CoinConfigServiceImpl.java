package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.utils.*;
import org.springframework.beans.factory.annotation.*;
import java.io.*;
import java.util.*;
import org.springframework.util.*;
import com.baomidou.mybatisplus.mapper.*;
import com.blockeng.wallet.dto.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.enums.*;
import com.blockeng.wallet.exception.*;
import com.clg.wallet.newclient.*;
import com.clg.wallet.bean.*;

@Service
@Transactional
public class CoinConfigServiceImpl extends ServiceImpl<CoinConfigMapper, CoinConfig> implements CoinConfigService
{
    @Autowired
    private DESUtil desUtil;

    public String lastBlock(Long id)
    {
        CoinConfig coinConfig = (CoinConfig)((CoinConfigMapper)this.baseMapper).selectById(id);
        if (null != coinConfig) {
            return coinConfig.getLastBlock();
        }
        return "0";
    }

    public List<CoinConfig> selectCoinFromType(String type)
    {
        EntityWrapper<CoinConfig> ew = new EntityWrapper();
        ew.eq("status", Integer.valueOf(1));
        if (!StringUtils.isEmpty(type)) {
            ew.eq("coin_type", type);
        }
        return super.selectList(ew);
    }

    public List<CoinConfig> selectAllCoin()
    {
        EntityWrapper<CoinConfig> ew = new EntityWrapper();
        ew.eq("status", Integer.valueOf(1));
        return super.selectList(ew);
    }

    public CoinConfig selectCoinFromId(Long coinId)
    {
        return (CoinConfig)super.selectById(coinId);
    }

    public boolean updateCoinLastblock(String lastBlock, Long id)
    {
        return super.updateById(new CoinConfig().setId(id).setLastBlock(lastBlock));
    }

    public WalletResultDTO updateCoinPass(String newPass, String oldPass, CoinConfig coinConfig)
            throws CoinException
    {
        Client client = ClientFactory.getClient(getClientBean(coinConfig));
        super.updateById(new CoinConfig().setId(coinConfig.getId()).setWalletPass(this.desUtil.encrypt(newPass)));
        ResultDTO resultDTO = client.changePassword(oldPass, newPass);
        if (resultDTO.getStatusCode() == ResultCode.SUCCESS.getCode()) {
            return WalletResultDTO.successResult();
        }
        throw new CoinException("修改密码失败,code=" + resultDTO.getStatusCode() + ",原因:" + resultDTO.getResultDesc());
    }
    public ClientBean getClientBean(CoinConfig coinConfig)
    {
        ClientBean clientBean = new ClientBean();

        clientBean.setId(coinConfig.getId());
        clientBean.setName(coinConfig.getName());
        clientBean.setCoinType(coinConfig.getCoinType());
        clientBean.setCreditLimit(coinConfig.getCreditLimit());
        clientBean.setRpcIp(coinConfig.getRpcIp());
        clientBean.setRpcPort(coinConfig.getRpcPort());
        clientBean.setRpcUser(coinConfig.getRpcUser());
        clientBean.setRpcPwd(coinConfig.getRpcPwd());
        clientBean.setLastBlock(coinConfig.getLastBlock());
        clientBean.setWalletUser(coinConfig.getWalletUser());
        clientBean.setContractAddress(coinConfig.getContractAddress());
        clientBean.setMinConfirm(coinConfig.getMinConfirm());
        clientBean.setWalletPass(coinConfig.getWalletPass());
        clientBean.setContext(coinConfig.getContext());
        clientBean.setTask(coinConfig.getTask());
        clientBean.setStatus(coinConfig.getStatus());

        return clientBean;
    }
}
