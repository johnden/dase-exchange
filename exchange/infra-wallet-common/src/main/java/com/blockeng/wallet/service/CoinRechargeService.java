package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import java.util.*;
import java.math.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.bean.*;

public interface CoinRechargeService extends IService<CoinRecharge>
{
    List getByTxid(String paramString);

    boolean addRecord(String paramString1, String paramString2, Long paramLong, BigDecimal paramBigDecimal, int paramInt, CoinConfig paramCoinConfig);

    boolean addCollectTask(ClientBean paramClientBean, CoinRecharge paramCoinRecharge, String paramString);

    boolean updateInsertWallet(Long paramLong, int paramInt);

    boolean updateInsertWalletCommit(Long paramLong, int paramInt);

    List<CoinRecharge> getNotDealInWallet();

    List<CoinRecharge> getNotDealInWalletByType(String paramString);
}
