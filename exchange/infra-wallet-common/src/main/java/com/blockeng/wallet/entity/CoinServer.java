package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.util.*;
import java.io.*;

@TableName("coin_server")
public class CoinServer extends Model<CoinServer>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("rpc_port")
    private String rpcPort;
    @TableField("rpc_ip")
    private String rpcIp;
    private int running;
    @TableField("wallet_number")
    private Integer walletNumber;
    @TableField("real_number")
    private Integer realNumber;
    @TableField("coin_name")
    private String coinName;
    private String mark;
    @TableField("last_update_time")
    private Date lastUpdateTime;
    private Date created;

    public CoinServer setId(Long id)
    {
        this.id = id;return this;
    }

    public CoinServer setRpcPort(String rpcPort)
    {
        this.rpcPort = rpcPort;return this;
    }

    public CoinServer setRpcIp(String rpcIp)
    {
        this.rpcIp = rpcIp;return this;
    }

    public CoinServer setRunning(int running)
    {
        this.running = running;return this;
    }

    public CoinServer setWalletNumber(Integer walletNumber)
    {
        this.walletNumber = walletNumber;return this;
    }

    public CoinServer setRealNumber(Integer realNumber)
    {
        this.realNumber = realNumber;return this;
    }

    public CoinServer setCoinName(String coinName)
    {
        this.coinName = coinName;return this;
    }

    public CoinServer setMark(String mark)
    {
        this.mark = mark;return this;
    }

    public CoinServer setLastUpdateTime(Date lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;return this;
    }

    public CoinServer setCreated(Date created)
    {
        this.created = created;return this;
    }

    public Long getId()
    {
        return this.id;
    }

    public String getRpcPort()
    {
        return this.rpcPort;
    }

    public String getRpcIp()
    {
        return this.rpcIp;
    }

    public int getRunning()
    {
        return this.running;
    }

    public Integer getWalletNumber()
    {
        return this.walletNumber;
    }

    public Integer getRealNumber()
    {
        return this.realNumber;
    }

    public String getCoinName()
    {
        return this.coinName;
    }

    public String getMark()
    {
        return this.mark;
    }

    public Date getLastUpdateTime()
    {
        return this.lastUpdateTime;
    }

    public Date getCreated()
    {
        return this.created;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }
}
