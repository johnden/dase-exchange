package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import org.springframework.amqp.rabbit.core.*;
import com.clg.wallet.bean.*;
import com.clg.wallet.newclient.*;
import java.math.*;
import org.springframework.util.*;
import com.blockeng.wallet.entity.*;
import com.blockeng.wallet.enums.*;
import com.clg.wallet.utils.*;
import java.util.*;
import com.baomidou.mybatisplus.mapper.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinRechargeServiceImpl extends ServiceImpl<CoinRechargeMapper, CoinRecharge> implements CoinRechargeService
{

    private static final Logger LOG = LoggerFactory.getLogger(CoinRechargeServiceImpl.class);
    @Autowired
    private AdminAddressService adminAddressService;
    @Autowired
    private WalletCollectTaskService walletCollectTaskService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public boolean addCollectTask(ClientBean clientBean, CoinRecharge coinRecharge, String type)
    {
        BigDecimal mum = coinRecharge.getAmount();
        if ("etz".equalsIgnoreCase(coinRecharge.getCoinName()))
        {
            EthNewClient client = (EthNewClient)ClientFactory.getClient(clientBean);
            mum = mum.subtract(client.getEthFee());
            if (BigDecimal.ZERO.compareTo(mum) >= 0)
            {
                LOG.info("归账余额不足,无需归账,userid:" + coinRecharge.getUserId());
                return true;
            }
        }
        WalletCollectTask task = new WalletCollectTask();
        task.setCoinId(coinRecharge.getCoinId())
                .setUserId(coinRecharge.getUserId())
                .setCoinName(coinRecharge.getCoinName())
                .setFromAddress(coinRecharge.getAddress())
                .setCoinType(type);
        AdminAddress payAdminAddress = this.adminAddressService.queryAdminAccount(clientBean.getCoinType(), 2);
        if (!StringUtils.isEmpty(payAdminAddress.getAddress())) {
            task.setToAddress(payAdminAddress.getAddress());
        } else {
            return false;
        }
        task.setAmount(mum);
        return this.walletCollectTaskService.insert(task);
    }

    public boolean addRecord(String hash, String to, Long userId, BigDecimal money, int confirm, CoinConfig coin)
    {
        List txList = getByTxid(hash);
        if ((null == txList) || (txList.size() <= 0))
        {
            LOG.info("查询到一条充值记录,交易id:" + hash + " 币种名称:" + coin.getName());
            CoinRecharge inRecord = new CoinRecharge();
            inRecord.setUserId(userId);
            inRecord.setAddress(to);
            inRecord.setAmount(money);
            inRecord.setTxid(hash);
            inRecord.setCoinType(coin.getCoinType());
            inRecord.setCoinId(coin.getId());
            inRecord.setCoinName(coin.getName());
            inRecord.setConfirm(Integer.valueOf(confirm));
            if (super.insert(inRecord))
            {
                this.rabbitTemplate.convertAndSend(MessageChannel.COIN_RECHARGE_MSG.getName(), GsonUtils.toJson(inRecord));
                return true;
            }
            return false;
        }
        LOG.info("不能重复充值,交易id:" + hash);
        return false;
    }

    public List getByTxid(String txid)
    {
        EntityWrapper<CoinRecharge> ew = new EntityWrapper();
        ew.eq("txid", txid);
        return super.selectList(ew);
    }

    public boolean updateInsertWallet(Long id, int status)
    {
        return super.updateById(new CoinRecharge().setId(id).setStatus(Integer.valueOf(status)));
    }

    public boolean updateInsertWalletCommit(Long id, int confirm)
    {
        return super.updateById(new CoinRecharge().setId(id).setConfirm(Integer.valueOf(confirm)));
    }

    public List<CoinRecharge> getNotDealInWallet()
    {
        EntityWrapper<CoinRecharge> ew = new EntityWrapper();
        ew.ne("confirm", Integer.valueOf(1));
        ew.ne("txid", "");
        ew.ne("status", Integer.valueOf(-1));
        return super.selectList(ew);
    }

    public List<CoinRecharge> getNotDealInWalletByType(String coinType)
    {
        EntityWrapper<CoinRecharge> ew = new EntityWrapper();
        ew.ne("confirm", Integer.valueOf(1));
        ew.ne("txid", "");
        ew.ne("status", Integer.valueOf(-1));
        ew.eq("coin_type", coinType);
        return super.selectList(ew);
    }
}
