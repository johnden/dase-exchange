package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.math.*;
import java.util.*;
import java.io.*;

@TableName("coin_recharge")
public class CoinRecharge extends Model<CoinRecharge>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("user_id")
    private Long userId;
    @TableField("coin_id")
    private Long coinId;
    @TableField("coin_name")
    private String coinName;
    @TableField("coin_type")
    private String coinType;
    private String address;
    private Integer confirm;
    private Integer status;
    private String txid;
    private BigDecimal amount;
    @TableField("last_update_time")
    private Date lastUpdateTime;
    private Date created;

    public CoinRecharge setId(Long id)
    {
        this.id = id;return this;
    }

    public CoinRecharge setUserId(Long userId)
    {
        this.userId = userId;return this;
    }

    public CoinRecharge setCoinId(Long coinId)
    {
        this.coinId = coinId;return this;
    }

    public CoinRecharge setCoinName(String coinName)
    {
        this.coinName = coinName;return this;
    }

    public CoinRecharge setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }

    public CoinRecharge setAddress(String address)
    {
        this.address = address;return this;
    }

    public CoinRecharge setConfirm(Integer confirm)
    {
        this.confirm = confirm;return this;
    }

    public CoinRecharge setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public CoinRecharge setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public CoinRecharge setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public CoinRecharge setLastUpdateTime(Date lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;return this;
    }

    public CoinRecharge setCreated(Date created)
    {
        this.created = created;return this;
    }

    public Long getId()
    {
        return this.id;
    }

    public Long getUserId()
    {
        return this.userId;
    }

    public Long getCoinId()
    {
        return this.coinId;
    }

    public String getCoinName()
    {
        return this.coinName;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    public String getAddress()
    {
        return this.address;
    }

    public Integer getConfirm()
    {
        return this.confirm;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public String getTxid()
    {
        return this.txid;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public Date getLastUpdateTime()
    {
        return this.lastUpdateTime;
    }

    public Date getCreated()
    {
        return this.created;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }
}
