package com.blockeng.wallet.utils;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;

@Component
public class DESUtil
{
    @Autowired
    private ReadProperties readProperties;

    public String decrypt(String data)
    {
        return DecryptCoder.decrypt(data, this.readProperties.aesKey);
    }

    public String encrypt(String data)
    {
        return EncryptCoder.encrypt(data, this.readProperties.aesKey);
    }

    public static void main(String[] args)
    {
        String ltcdecrypt = DecryptCoder.decrypt("5B1EDAC20E4D5C87CF7B56F5ED792BCEEF4546BA09C95325038D2687BC26EDC88E25A73B68609993", "J7F2pdanWjkct7an");
        System.out.println(ltcdecrypt);
        ltcdecrypt = EncryptCoder.encrypt("12345678", "!y@l#d$p%l^a&n*t");
        System.out.println(ltcdecrypt);

        ltcdecrypt = EncryptCoder.encrypt("sarasdf2443234dfggd67576sdassd1223", "!y@l#d$p%l^a&n*t");
        System.out.println(ltcdecrypt);
    }
}
