package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.help.*;
import org.springframework.beans.factory.annotation.*;
import java.math.*;
import java.util.*;
import com.baomidou.mybatisplus.mapper.*;
import org.springframework.util.*;
import com.clg.wallet.newclient.*;
import com.blockeng.wallet.entity.*;
import org.slf4j.*;

@Service
@Transactional
public class WalletCollectTaskServiceImpl extends ServiceImpl<WalletCollectTaskMapper, WalletCollectTask> implements WalletCollectTaskService
{
    private static final Logger LOG = LoggerFactory.getLogger(WalletCollectTaskServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;

    public boolean updateTaskStatus(Long id, String txid, String mark, int status)
    {
        return super.updateById(new WalletCollectTask().setId(id).setTxid(txid).setMark(mark).setStatus(Integer.valueOf(status)));
    }

    public boolean updateTaskStatus(Long id, String txid, String mark, int status, BigDecimal chainFee)
    {
        return super.updateById(new WalletCollectTask().setId(id).setTxid(txid).setMark(mark).setStatus(Integer.valueOf(status)).setChainFee(chainFee));
    }

    public List<WalletCollectTask> getTodoTask(String type)
    {
        EntityWrapper<WalletCollectTask> ew = new EntityWrapper();
        ew.eq("status", Integer.valueOf(0));
        ew.eq("coin_type", type);
        return super.selectList(ew);
    }

    public List<WalletCollectTask> queryNoFeeList(Long id, String type)
    {
        EntityWrapper<WalletCollectTask> ew = new EntityWrapper();
        if ((null != id) && (id.longValue() > 0L)) {
            ew.eq("coin_id", id);
        }
        if (!StringUtils.isEmpty(type)) {
            ew.eq("coin_type", type);
        }
        ew.andNew().isNull("chain_fee").or("chain_fee", new Object[] { "" });
        ew.andNew().isNotNull("txid");
        return super.selectList(ew);
    }

    public boolean addCollectTask(BigDecimal amount, Long userId, Long coinId, String fromAddress, String toAddress)
    {
        return addCollectTask(amount, userId, coinId, fromAddress, toAddress, 0);
    }

    public boolean addCollectTask(BigDecimal amount, Long userId, Long coinId, String fromAddress, String toAddress, int status)
    {
        CoinConfig coin = this.clientInfo.getCoinConfigFormId(coinId);
        if ("eth".equalsIgnoreCase(coin.getCoinType()))
        {
            EthNewClient client = (EthNewClient)this.clientInfo.getClientFromId(coinId);
            amount = amount.subtract(client.getEthFee());
            if (BigDecimal.ZERO.compareTo(amount) >= 0)
            {
                LOG.info("归集金额不足,无需归账,userid:" + userId);
                return true;
            }
        }
        WalletCollectTask walletCollectTask = new WalletCollectTask();
        walletCollectTask
                .setAmount(amount)
                .setUserId(userId)
                .setCoinName(coin.getName())
                .setCoinId(coinId)
                .setCoinType(coin.getCoinType())
                .setFromAddress(fromAddress)
                .setToAddress(toAddress)
                .setStatus(Integer.valueOf(status));
        return super.insert(walletCollectTask);
    }
}
