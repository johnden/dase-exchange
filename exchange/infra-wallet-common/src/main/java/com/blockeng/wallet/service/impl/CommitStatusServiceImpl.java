package com.blockeng.wallet.service.impl;

import org.springframework.transaction.annotation.*;
import org.springframework.stereotype.*;
import com.blockeng.wallet.help.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import org.springframework.util.*;
import com.clg.wallet.enums.*;
import java.math.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.wallet.act.*;
import com.clg.wallet.newclient.*;
import com.clg.wallet.bean.*;
import java.util.*;
import org.slf4j.*;

@Transactional
@Service
public class CommitStatusServiceImpl implements CommitStatusService
{
    private static final Logger LOG = LoggerFactory.getLogger(CommitStatusServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private CoinRechargeService coinRechargeService;
    @Autowired
    private CoinWithdrawService coinWithdrawService;
    @Autowired
    private WalletCollectTaskService walletCollectTaskService;
    private Client client;

    public void commitEth()
    {
        updateEthOrEtc("eth");
    }

    public void commitEtc()
    {
        updateEthOrEtc("etc");
    }

    private void updateEthOrEtc(String type)
    {
        Client client = this.clientInfo.getClientFromType(type);

        List<CoinWithdraw> withdrawList = this.coinWithdrawService.queryNoFeeList(Long.valueOf(0L), type);
        if ("eth".equalsIgnoreCase(type)) {
            withdrawList.addAll(this.coinWithdrawService.queryNoFeeList(Long.valueOf(0L), "ethToken"));
        }
        if (!CollectionUtils.isEmpty(withdrawList)) {
            for (final CoinWithdraw withdraw : withdrawList) {
                final String txid = withdraw.getTxid();
                if (!StringUtils.isEmpty(txid) && !txid.equalsIgnoreCase("0000000000000000000000000000000000000000000")) {
                    try {
                        final ResultDTO resultDTO = client.getTransactionFee(txid);
                        if (resultDTO.getStatusCode() != ResultCode.SUCCESS.getCode()) {
                            continue;
                        }
                        final BigDecimal fee = resultDTO.toBigDecimal();
                        this.coinWithdrawService.updateById(withdraw.setChainFee(fee));
                    }
                    catch (NoSuchElementException e) {
                        e.printStackTrace();
                        CommitStatusServiceImpl.LOG.error("没有找到相应的充值记录,txid:" + txid);
                    }
                    catch (Exception e2) {
                        CommitStatusServiceImpl.LOG.error("未查询到相应的区块信息,txid:" + txid);
                        e2.printStackTrace();
                    }
                }
            }
        }
        final List<WalletCollectTask> collectTask = this.walletCollectTaskService.queryNoFeeList(0L, type);
        if ("eth".equalsIgnoreCase(type)) {
            collectTask.addAll(this.walletCollectTaskService.queryNoFeeList(0L, "ethToken"));
        }
        if (!CollectionUtils.isEmpty(collectTask)) {
            for (final WalletCollectTask walletCollectTask : collectTask) {
                final String txid2 = walletCollectTask.getTxid();
                if (!StringUtils.isEmpty(txid2)) {
                    final ResultDTO resultDTO2 = client.getTransactionFee(txid2);
                    if (resultDTO2.getStatusCode() != ResultCode.SUCCESS.getCode()) {
                        continue;
                    }
                    final BigDecimal fee2 = resultDTO2.toBigDecimal();
                    this.walletCollectTaskService.updateById(walletCollectTask.setChainFee(fee2));
                }
            }
        }
    }

    public void commitAct()
    {
        List<CoinRecharge> rechargeList = getRechargeList("act", "actToken");
        if (!CollectionUtils.isEmpty(rechargeList)) {
            for (CoinRecharge recharge : rechargeList)
            {
                CoinConfig coin = this.clientInfo.getCoinConfigFormId(recharge.getCoinId());
                commitStatus(recharge, coin);
            }
        }
    }

    public void commitBtc()
    {
        List<CoinWithdraw> withdrawList = this.coinWithdrawService.queryNoFeeList(Long.valueOf(0L), "btc");
        if (!CollectionUtils.isEmpty(withdrawList)) {
            for (CoinWithdraw coinWithdraw : withdrawList) {}
        }
        final List<CoinRecharge> rechargeList = this.getRechargeList("btc");
        if (!CollectionUtils.isEmpty(rechargeList)) {
            for (final CoinRecharge recharge : rechargeList) {
                final CoinConfig coin = this.clientInfo.getCoinConfigFormId(recharge.getCoinId());
                this.commitStatus(recharge, coin);
            }
        }
    }

    public void commitNeo()
    {
        List<CoinRecharge> rechargeList = getRechargeList("neo", "neoToken");
        if (!CollectionUtils.isEmpty(rechargeList)) {
            for (CoinRecharge recharge : rechargeList)
            {
                CoinConfig coin = this.clientInfo.getCoinConfigFormId(recharge.getCoinId());
                commitStatus(recharge, coin);
            }
        }
    }

    private void commitStatus(CoinRecharge userInWallet, CoinConfig coin)
    {
        this.client = ClientFactory.getClient(getClientBean(coin));
        int commit = this.client.getTransactionConfirmed(userInWallet.getTxid()).toInteger();
        if (AchainCore.BLOCK_NOT_FOUNT == commit) {
            this.coinRechargeService.updateInsertWallet(userInWallet.getId(), -1);
        } else {
            checkIsSuccess(commit, userInWallet, coin);
        }
    }
    public ClientBean getClientBean(CoinConfig coinConfig)
    {
        ClientBean clientBean = new ClientBean();

        clientBean.setId(coinConfig.getId());
        clientBean.setName(coinConfig.getName());
        clientBean.setCoinType(coinConfig.getCoinType());
        clientBean.setCreditLimit(coinConfig.getCreditLimit());
        clientBean.setRpcIp(coinConfig.getRpcIp());
        clientBean.setRpcPort(coinConfig.getRpcPort());
        clientBean.setRpcUser(coinConfig.getRpcUser());
        clientBean.setRpcPwd(coinConfig.getRpcPwd());
        clientBean.setLastBlock(coinConfig.getLastBlock());
        clientBean.setWalletUser(coinConfig.getWalletUser());
        clientBean.setContractAddress(coinConfig.getContractAddress());
        clientBean.setMinConfirm(coinConfig.getMinConfirm());
        clientBean.setWalletPass(coinConfig.getWalletPass());
        clientBean.setContext(coinConfig.getContext());
        clientBean.setTask(coinConfig.getTask());
        clientBean.setStatus(coinConfig.getStatus());

        return clientBean;
    }
    private void checkIsSuccess(int commit, CoinRecharge userInWallet, CoinConfig coin)
    {
        int needConfirmed = coin.getMinConfirm().intValue() - commit;
        if (needConfirmed > 0)
        {
            this.coinRechargeService.updateInsertWalletCommit(userInWallet.getId(), -needConfirmed);
        }
        else
        {
            if (("eth".equalsIgnoreCase(coin.getCoinType())) ||
                    ("ethToken".equalsIgnoreCase(coin.getCoinType())))
            {
                boolean failed = ((EthNewClient)this.client).isFailed(userInWallet.getTxid()).toBoolean();
                if (failed)
                {
                    this.coinRechargeService.updateInsertWallet(userInWallet.getId(), -1);
                    return;
                }
            }
            this.coinRechargeService.updateInsertWalletCommit(userInWallet.getId(), 1);
            if (("eth".equalsIgnoreCase(coin.getCoinType())) ||
                    ("ethToken".equalsIgnoreCase(coin.getCoinType()))) {
                addTask(userInWallet, "eth", coin.getCoinType());
            } else if ("etc".equalsIgnoreCase(coin.getCoinType())) {
                addTask(userInWallet, "etc", coin.getCoinType());
            } else if (!"btc".equalsIgnoreCase(coin.getCoinType())) {
                if (!"neo".equalsIgnoreCase(coin.getCoinType())) {
                    if (!"xrp".equalsIgnoreCase(coin.getCoinType())) {}
                }
            }
        }
    }

    private void addTask(CoinRecharge userInWallet, String coinTypeEtc, String type)
    {
        ClientBean clientBean = this.clientInfo.getClientInfoFromType(coinTypeEtc);
        if (this.coinRechargeService.addCollectTask(clientBean, userInWallet, type)) {
            LOG.info("增加归集任务成功");
        } else {
            LOG.info("增加归集任务失败");
        }
    }

    private List<CoinRecharge> getRechargeList(String type)
    {
        return getRechargeList(type, null);
    }

    private List<CoinRecharge> getRechargeList(String type, String typeToken)
    {
        if (!StringUtils.isEmpty(type))
        {
            List<CoinRecharge> list = this.coinRechargeService.getNotDealInWalletByType(type);
            if (!StringUtils.isEmpty(typeToken))
            {
                List<CoinRecharge> tokenList = this.coinRechargeService.getNotDealInWalletByType(typeToken);
                list.addAll(tokenList);
            }
            return list;
        }
        return new ArrayList();
    }
}
