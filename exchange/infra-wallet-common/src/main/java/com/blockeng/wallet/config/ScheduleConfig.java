package com.blockeng.wallet.config;

import org.springframework.scheduling.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.config.*;
import java.util.concurrent.*;

@Configuration
public class ScheduleConfig implements SchedulingConfigurer
{
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar)
    {
        taskRegistrar.setScheduler(Executors.newScheduledThreadPool(40));
    }
}
