package com.blockeng.wallet.dto;

import java.math.*;

public class CoinRechargeMessageDTO
{
    private Long userId;
    private String coinName;
    private BigDecimal amount;

    public CoinRechargeMessageDTO setUserId(Long userId)
    {
        this.userId = userId;return this;
    }

    public CoinRechargeMessageDTO setCoinName(String coinName)
    {
        this.coinName = coinName;return this;
    }

    public CoinRechargeMessageDTO setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public Long getUserId()
    {
        return this.userId;
    }

    public String getCoinName()
    {
        return this.coinName;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public CoinRechargeMessageDTO(Long userId, String coinName, BigDecimal amount)
    {
        this.userId = userId;
        this.coinName = coinName;
        this.amount = amount;
    }

    public CoinRechargeMessageDTO() {}
}
