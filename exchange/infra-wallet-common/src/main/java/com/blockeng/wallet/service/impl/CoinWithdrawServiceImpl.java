package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.entity.*;
import com.blockeng.wallet.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.springframework.amqp.rabbit.core.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.util.*;
import java.util.*;
import com.baomidou.mybatisplus.mapper.*;
import com.blockeng.wallet.enums.*;
import com.blockeng.wallet.dto.*;
import com.clg.wallet.utils.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinWithdrawServiceImpl extends ServiceImpl<CoinWithdrawMapper, CoinWithdraw> implements CoinWithdrawService
{
    private static final Logger LOG = LoggerFactory.getLogger(CoinWithdrawServiceImpl.class);
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private CoinWithdrawService coinWithdrawService;

    public boolean updateTx(CoinWithdraw item)
    {
        CoinWithdraw coinWithdraw = new CoinWithdraw().setId(item.getId()).setWalletMark(item.getWalletMark()).setStatus(item.getStatus()).setLastUpdateTime(new Date());
        if (!StringUtils.isEmpty(item.getTxid())) {
            coinWithdraw.setTxid(item.getTxid());
        }
        return super.insertOrUpdate(coinWithdraw);
    }

    public List<CoinWithdraw> queryOutList()
    {
        return queryOutList(null);
    }

    public List<CoinWithdraw> queryOutList(String type)
    {
        EntityWrapper<CoinWithdraw> ew = new EntityWrapper();
        ew.eq("status", Integer.valueOf(4));
        if (!StringUtils.isEmpty(type)) {
            ew.eq("coin_type", type);
        }
        ew.andNew().isNull("txid").or().eq("txid", "");
        return super.selectList(ew);
    }

    public List<CoinWithdraw> queryNoFeeList(Long id, String type)
    {
        EntityWrapper<CoinWithdraw> ew = new EntityWrapper();
        if ((null != id) && (id.longValue() > 0L)) {
            ew.eq("coin_id", id);
        }
        if (!StringUtils.isEmpty(type)) {
            ew.eq("coin_type", type);
        }
        ew.andNew().isNull("chain_fee").or("chain_fee", new Object[] { "" });
        ew.andNew().isNotNull("txid").ne("txid", "");
        return super.selectList(ew);
    }

    public void updateDrawInfo(CoinWithdraw withdraw)
    {
        int status = withdraw.getStatus().intValue();
        if (status == 5)
        {
            this.rabbitTemplate.convertAndSend(MessageChannel.COIN_WITHDRAW_MSG.getName(), WalletResultDTO.successResult(withdraw).toJson());
            LOG.info("打币成功,txid:" + withdraw.getTxid());
        }
        else
        {
            LOG.info("打币成功,coinId:" + withdraw.getId());
            this.rabbitTemplate.convertAndSend(MessageChannel.COIN_WITHDRAW_MSG.getName(), WalletResultDTO.errorResult(WalletResultCode.WITH_DRAW_FAILED.getCode(), withdraw).toJson());
        }
        this.coinWithdrawService.updateTx(withdraw);
    }

    public void withDraw(String message, String type)
    {
        CoinWithdraw coinWithdraw = (CoinWithdraw)GsonUtils.convertObj(message, CoinWithdraw.class);
        if (null == coinWithdraw)
        {
            LOG.error("打币失败....");
            return;
        }
        try
        {
            if (!"eth".equalsIgnoreCase(type)) {
                if (!"act".equalsIgnoreCase(type)) {
                    if (!"btc".equalsIgnoreCase(type)) {
                        if (!"eth".equalsIgnoreCase(type)) {
                            if (!"etc".equalsIgnoreCase(type)) {
                                if (!"neo".equalsIgnoreCase(type)) {
                                    if (!"wcg".equalsIgnoreCase(type)) {
                                        if (!"xrp".equalsIgnoreCase(type)) {}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return;
        }
        catch (Exception e)
        {
            updateDrawInfo(coinWithdraw.setStatus(Integer.valueOf(6)).setWalletMark("提币异常,请检查是否打币成功:" + e.getMessage()));
            e.printStackTrace();
        }
    }
}
