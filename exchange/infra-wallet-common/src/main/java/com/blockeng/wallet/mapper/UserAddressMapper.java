package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.mapper.*;
import com.blockeng.wallet.entity.*;

public interface UserAddressMapper extends BaseMapper<UserAddress>
{
}
