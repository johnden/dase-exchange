package com.blockeng.wallet.mapper;

import com.baomidou.mybatisplus.mapper.*;
import com.blockeng.wallet.entity.*;

public interface CoinConfigMapper extends BaseMapper<CoinConfig>
{
}
