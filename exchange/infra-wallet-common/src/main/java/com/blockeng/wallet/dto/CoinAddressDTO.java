package com.blockeng.wallet.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CoinAddressDTO
{
    @NotEmpty(message="币种id")
    private String coinId;
    @NotNull(message="用户ID不能为空")
    private Long userId;

    public CoinAddressDTO setCoinId(String coinId)
    {
        this.coinId = coinId;return this;
    }

    public CoinAddressDTO setUserId(Long userId)
    {
        this.userId = userId;return this;
    }

    public String getCoinId()
    {
        return this.coinId;
    }

    public Long getUserId()
    {
        return this.userId;
    }
}
