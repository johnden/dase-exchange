package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.math.*;
import java.util.*;
import java.io.*;

@TableName("wallet_collect_task")
public class WalletCollectTask extends Model<WalletCollectTask>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("coin_id")
    private Long coinId;
    @TableField("coin_type")
    private String coinType;
    @TableField("coin_name")
    private String coinName;
    @TableField("user_id")
    private Long userId;
    private String txid;
    private BigDecimal amount;
    private BigDecimal chainFee;
    private String mark;
    private Integer status;
    @TableField("from_address")
    private String fromAddress;
    @TableField("to_address")
    private String toAddress;
    @TableField("last_update_time")
    private Date lastUpdateTime;
    private Date created;

    public WalletCollectTask setId(Long id)
    {
        this.id = id;return this;
    }

    public WalletCollectTask setCoinId(Long coinId)
    {
        this.coinId = coinId;return this;
    }

    public WalletCollectTask setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }

    public WalletCollectTask setCoinName(String coinName)
    {
        this.coinName = coinName;return this;
    }

    public WalletCollectTask setUserId(Long userId)
    {
        this.userId = userId;return this;
    }

    public WalletCollectTask setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public WalletCollectTask setAmount(BigDecimal amount)
    {
        this.amount = amount;return this;
    }

    public WalletCollectTask setChainFee(BigDecimal chainFee)
    {
        this.chainFee = chainFee;return this;
    }

    public WalletCollectTask setMark(String mark)
    {
        this.mark = mark;return this;
    }

    public WalletCollectTask setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public WalletCollectTask setFromAddress(String fromAddress)
    {
        this.fromAddress = fromAddress;return this;
    }

    public WalletCollectTask setToAddress(String toAddress)
    {
        this.toAddress = toAddress;return this;
    }

    public WalletCollectTask setLastUpdateTime(Date lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;return this;
    }

    public WalletCollectTask setCreated(Date created)
    {
        this.created = created;return this;
    }

    public Long getId()
    {
        return this.id;
    }

    public Long getCoinId()
    {
        return this.coinId;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    public String getCoinName()
    {
        return this.coinName;
    }

    public Long getUserId()
    {
        return this.userId;
    }

    public String getTxid()
    {
        return this.txid;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public BigDecimal getChainFee()
    {
        return this.chainFee;
    }

    public String getMark()
    {
        return this.mark;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public String getFromAddress()
    {
        return this.fromAddress;
    }

    public String getToAddress()
    {
        return this.toAddress;
    }

    public Date getLastUpdateTime()
    {
        return this.lastUpdateTime;
    }

    public Date getCreated()
    {
        return this.created;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }
}
