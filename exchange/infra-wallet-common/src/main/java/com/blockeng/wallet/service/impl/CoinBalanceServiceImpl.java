package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.help.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import com.clg.wallet.bean.*;
import com.clg.wallet.newclient.*;
import java.util.*;
import java.math.*;
import com.clg.wallet.wallet.neo.bean.*;
import org.springframework.util.*;
import com.blockeng.wallet.entity.*;
import com.baomidou.mybatisplus.mapper.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinBalanceServiceImpl extends ServiceImpl<CoinBalanceMapper, CoinBalance> implements CoinBalanceService
{
    private static final Logger LOG = LoggerFactory.getLogger(CoinBalanceServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    private boolean sendFeeMsg = false;
    @Autowired
    private AdminAddressService adminAddressService;

    public void checkBalance(String type)
    {
        ClientBean clientBean = this.clientInfo.getClientInfoFromType(type);
        Client client;
        ClientBean mainClientBean;
        if (null != clientBean)
        {
            client = ClientFactory.getClient(clientBean);
            saveBalance(client, clientBean);
            String typeToken = type + "Token";
            HashMap<String, List<CoinConfig>> typeCoinMap = this.clientInfo.getTypeCoinMap();
            List<CoinConfig> coinConfigList = (List)typeCoinMap.get(typeToken);
            if (!CollectionUtils.isEmpty(coinConfigList))
            {
                mainClientBean = this.clientInfo.getClientInfoFromType(type);
                for (CoinConfig coin : coinConfigList) {
                    saveTokenBalance(client, mainClientBean, coin);
                }
            }
        }
        else
        {
            LOG.error("未找到当前币种");
        }
    }

    private void saveBalance(Client client, ClientBean clientBean)
    {
        BigDecimal collectBalance = BigDecimal.ZERO;
        BigDecimal loanBalance = BigDecimal.ZERO;
        BigDecimal feeBalance = BigDecimal.ZERO;
        BigDecimal rechargeBalance = BigDecimal.ZERO;
        if (clientBean.getCoinType().equalsIgnoreCase("btc"))
        {
            loanBalance = client.getBalance().toBigDecimal();
        }
        else if (clientBean.getCoinType().equalsIgnoreCase("neo"))
        {
            Balance balance = (Balance)client.getBalance(clientBean.getContractAddress()).getResult();
            loanBalance = balance.getBalance();
        }
        else
        {
            AdminAddress loadAddress = this.adminAddressService.queryAdminAccount(clientBean.getId(), 2);
            if ((null != loadAddress) && (!StringUtils.isEmpty(loadAddress.getAddress()))) {
                loanBalance = client.getBalance(loadAddress.getAddress()).toBigDecimal();
            }
        }
        AdminAddress collectAddress = this.adminAddressService.queryAdminAccount(clientBean.getId(), 1);
        if ((null != collectAddress) && (!StringUtils.isEmpty(collectAddress.getAddress()))) {
            collectBalance = client.getBalance(collectAddress.getAddress()).toBigDecimal();
        }
        AdminAddress feeAddress = this.adminAddressService.queryAdminAccount(clientBean.getId(), 3);
        if ((clientBean.getCoinType().equalsIgnoreCase("eth")) && (null != feeAddress) && (!StringUtils.isEmpty(feeAddress.getAddress())))
        {
            feeBalance = client.getBalance(feeAddress.getAddress()).toBigDecimal();
            if ((feeBalance.compareTo(BigDecimal.valueOf(0.5D)) < 0) && (!this.sendFeeMsg)) {
                this.sendFeeMsg = true;
            } else if (feeBalance.compareTo(BigDecimal.valueOf(0.5D)) >= 0) {
                this.sendFeeMsg = false;
            }
        }
        AdminAddress rechargeAddress = this.adminAddressService.queryAdminAccount(clientBean.getId(), 4);
        if ((null != rechargeAddress) && (!StringUtils.isEmpty(rechargeAddress.getAddress()))) {
            rechargeBalance = client.getBalance(rechargeAddress.getAddress()).toBigDecimal();
        }
        insertBalance(clientBean, feeBalance, collectBalance, loanBalance, rechargeBalance);
    }

    private void saveTokenBalance(Client client, ClientBean mainClientBean, CoinConfig coin)
    {
        BigDecimal collectBalance = BigDecimal.ZERO;
        BigDecimal loanBalance = BigDecimal.ZERO;

        AdminAddress homeAddress = this.adminAddressService.queryAdminAccount(mainClientBean.getId(), 1);
        if ((null != homeAddress) && (!StringUtils.isEmpty(homeAddress.getAddress()))) {
            collectBalance = client.getTokenBalance(coin.getContractAddress(), homeAddress.getAddress()).toBigDecimal();
        }
        AdminAddress loadAddress = this.adminAddressService.queryAdminAccount(mainClientBean.getId(), 2);
        if ((null != loadAddress) && (!StringUtils.isEmpty(loadAddress.getAddress()))) {
            collectBalance = client.getTokenBalance(coin.getContractAddress(), loadAddress.getAddress()).toBigDecimal();
        }
        insertBalance(getClientBean(coin), null, BigDecimal.ZERO, collectBalance, loanBalance);
    }
    public ClientBean getClientBean(CoinConfig coinConfig)
    {
        ClientBean clientBean = new ClientBean();

        clientBean.setId(coinConfig.getId());
        clientBean.setName(coinConfig.getName());
        clientBean.setCoinType(coinConfig.getCoinType());
        clientBean.setCreditLimit(coinConfig.getCreditLimit());
        clientBean.setRpcIp(coinConfig.getRpcIp());
        clientBean.setRpcPort(coinConfig.getRpcPort());
        clientBean.setRpcUser(coinConfig.getRpcUser());
        clientBean.setRpcPwd(coinConfig.getRpcPwd());
        clientBean.setLastBlock(coinConfig.getLastBlock());
        clientBean.setWalletUser(coinConfig.getWalletUser());
        clientBean.setContractAddress(coinConfig.getContractAddress());
        clientBean.setMinConfirm(coinConfig.getMinConfirm());
        clientBean.setWalletPass(coinConfig.getWalletPass());
        clientBean.setContext(coinConfig.getContext());
        clientBean.setTask(coinConfig.getTask());
        clientBean.setStatus(coinConfig.getStatus());

        return clientBean;
    }
    private void insertBalance(ClientBean clientBean, BigDecimal rechargeBalance, BigDecimal feeBalance, BigDecimal collectBalance, BigDecimal loadBalance)
    {
        CoinBalance coinBalance = new CoinBalance();
        coinBalance.setCoinId(clientBean.getId())
                .setCoinName(clientBean.getName())
                .setRechargeAccountBalance(rechargeBalance)
                .setFeeAccountBalance(feeBalance)
                .setCollectAccountBalance(collectBalance)
                .setLoanAccountBalance(loadBalance)
                .setCoinType(clientBean.getCoinType());
        Long id = selectByCoinId(clientBean.getId());
        if (id.longValue() > 0L) {
            coinBalance.setId(id);
        }
        if (super.insertOrUpdate(coinBalance)) {
            LOG.info("更新实时账户余额成功");
        } else {
            LOG.info("更新实时账户余额失败,币种名称:" + clientBean.getName());
        }
    }

    public Long selectByCoinId(Long coinId)
    {
        EntityWrapper<CoinBalance> ew = new EntityWrapper();
        ew.eq("coin_id", coinId);
        CoinBalance coinBalance = (CoinBalance)super.selectOne(ew);
        if (null != coinBalance) {
            return coinBalance.getId();
        }
        return Long.valueOf(0L);
    }
}
