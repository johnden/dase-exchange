package com.blockeng.wallet.help;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import com.blockeng.wallet.utils.*;
import com.clg.wallet.newclient.*;
import java.math.*;
import org.springframework.util.*;
import com.clg.wallet.bean.*;
import com.blockeng.wallet.entity.*;
import org.slf4j.*;

@Component
public class ETHClient
{

    private static final Logger LOG = LoggerFactory.getLogger(ETHClient.class);
    @Autowired
    private ClientInfo actClientInfo;
    @Autowired
    private AdminAddressService adminAddressService;
    @Autowired
    private DESUtil desUtil;

    public EthResult sendEthTokenToCenter(UserAddress userEth, String addr, CoinConfig coin)
            throws Exception
    {
        EthNewClient client = (EthNewClient)ClientFactory.getClient(this.actClientInfo.getClientInfoFromType("etz"));
        BigDecimal tokenBalance = client.getTokenBalance(coin.getContractAddress(), userEth.getAddress()).toBigDecimal();
        if (tokenBalance.compareTo(BigDecimal.valueOf(0L)) > 0)
        {
            if (null == addr)
            {
                AdminAddress backAddress = this.adminAddressService.queryAdminAccount("etz", 1);
                if (null != backAddress) {
                    addr = backAddress.getAddress();
                }
            }
            EThTransactionBean eThTransactionBean = new EThTransactionBean();
            eThTransactionBean
                    .setTokenBalance(tokenBalance)
                    .setFromAddress(userEth.getAddress())
                    .setToAddress(addr)
                    .setAllTransaction(true)
                    .setFromUserKeystore(this.desUtil.decrypt(userEth.getKeystore()))
                    .setContractAddress(coin.getContractAddress())
                    .setFromUserPass(this.desUtil.decrypt(userEth.getPwd()));
            EthResult ethResult = client.sentEthToken(eThTransactionBean);
            if (ethResult.isSuccess()) {
                return ethResult;
            }
            if (ethResult.getCode() == -2)
            {
                AdminAddress feeAdminAddress = this.adminAddressService.queryAdminAccount("etz", 3);
                if ((null != feeAdminAddress) && (!StringUtils.isEmpty(feeAdminAddress.getPwd())) && (!StringUtils.isEmpty(feeAdminAddress.getKeystore())))
                {
                    eThTransactionBean.setFromAddress(feeAdminAddress.getAddress()).setFromUserPass(this.desUtil.decrypt(feeAdminAddress.getPwd())).setFromUserKeystore(this.desUtil.decrypt(feeAdminAddress.getKeystore())).setAllTransaction(false).setBalance(client.getEthTokenFee()).setToAddress(userEth.getAddress());
                    ethResult = client.sentEth(eThTransactionBean);
                    return ethResult.setCode(-2).setSuccess(false);
                }
                throw new RuntimeException("手续费账户为空，或者没有keystore或者password");
            }
            return ethResult;
        }
        LOG.info("数量为0无需归集" + userEth.getAddress());
        return new EthResult().setCode(-3).setSuccess(false).setInfo("数量为0无需归集" + userEth.getAddress());
    }

    public EthResult sendEthToCenter(ClientBean clientBean, UserAddress userEth, WalletCollectTask task)
            throws Exception
    {
        EthNewClient client = (EthNewClient)ClientFactory.getClient(clientBean);
        if (StringUtils.isEmpty(task.getToAddress())) {
            throw new RuntimeException("归账地址不能为空");
        }
        EThTransactionBean eThTransactionBean = new EThTransactionBean();
        eThTransactionBean.setFromAddress(userEth.getAddress())
                .setFromUserPass(this.desUtil.decrypt(userEth.getPwd()))
                .setFromUserKeystore(this.desUtil.decrypt(userEth.getKeystore()))
                .setAllTransaction(true)
                .setBalance(task.getAmount())
                .setToAddress(task.getToAddress());
        return client.sentEth(eThTransactionBean);
    }
}
