package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;
import java.math.*;
import java.util.*;

public interface WalletCollectTaskService extends IService<WalletCollectTask>
{
    boolean updateTaskStatus(Long paramLong, String paramString1, String paramString2, int paramInt);

    boolean updateTaskStatus(Long paramLong, String paramString1, String paramString2, int paramInt, BigDecimal paramBigDecimal);

    List<WalletCollectTask> getTodoTask(String paramString);

    List<WalletCollectTask> queryNoFeeList(Long paramLong, String paramString);

    boolean addCollectTask(BigDecimal paramBigDecimal, Long paramLong1, Long paramLong2, String paramString1, String paramString2);

    boolean addCollectTask(BigDecimal paramBigDecimal, Long paramLong1, Long paramLong2, String paramString1, String paramString2, int paramInt);
}
