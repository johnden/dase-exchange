package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;

public interface CoinServerService extends IService<CoinServer>
{

    void checkServer(String paramString);

    CoinServer selectCount(String paramString);
}
