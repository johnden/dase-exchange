package com.blockeng.wallet.enums;

public enum AdminAddressType
{
    ADMIN_ADDRESS_TYPE_COLLECT(1),  ADMIN_ADDRESS_TYPE_LOAD(2),  ADMIN_ADDRESS_TYPE_FEE(3),  ADMIN_ADDRESS_TYPE_RECHARGE(4);

    private int type;

    private AdminAddressType(int type)
    {
        this.type = type;
    }

    public int getType()
    {
        return this.type;
    }

    public void setType(int type)
    {
        this.type = type;
    }
}
