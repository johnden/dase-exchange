package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;

public interface CoinBalanceService extends IService<CoinBalance>
{
    void checkBalance(String paramString);
}
