package com.blockeng.wallet.config;

public interface Constant
{
    public static final int TASK_OPEN = 1;
    public static final String ALL_JOB = "all_job";
    public static final int INSERT_SUCCESS = 1;
    public static final int ADMIN_ADDRESS_TYPE_COLLECT = 1;
    public static final int ADMIN_ADDRESS_TYPE_PAY = 2;
    public static final int ADMIN_ADDRESS_TYPE_FEE = 3;
    public static final int ADMIN_ADDRESS_TYPE_RECHARGE = 4;
    public static final int PAY_SUCCESS_STATUS = 5;
    public static final int PAY_FAILED = 6;
    public static final int PAY_FAILED_REJECT = 2;
    public static final String PAY_SUCCESS_INFO = "打款成功";
    public static final String PAY_NOT_ENOUGH_INFO = "余额不足";
    public static final String PAY_ACCOUNT_NOT_NULL_INFO = "打款账户不能为空";
    public static final String PAY_ACCOUNT_PRI_INFO = "打款私钥不能为空";
    public static final String PAY_FAILED_INFO = "打款失败";
    public static final String RECHARGE_ACCOUNT_NOT_NULL = "充值主账户不能为空";
    public static final String FEATURES_RECHARGE = "recharge";
    public static final String FEATURES_COLLECT = "collect";
    public static final String FEATURES_DRAW = "draw";
    public static final String FEATURES_OTHER = "other";
    public static final String FEATURES_ADDRESS = "address";
    public static final String NEO_CONTRACT = "c56f33fc6ecfcd0c225c4ab356fee59390af8560be0e930faebe74a6daff7c9b";
    public static final String MOCK_TXID = "0000000000000000000000000000000000000000000";
}
