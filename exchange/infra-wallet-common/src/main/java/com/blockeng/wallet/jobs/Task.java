package com.blockeng.wallet.jobs;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import org.springframework.scheduling.config.*;
import org.springframework.scheduling.*;
import java.util.*;
import org.springframework.scheduling.support.*;
import org.slf4j.*;

@Component
public class Task
{
    private static final Logger LOG = LoggerFactory.getLogger(Task.class);;
    @Autowired
    private CoinServerService coinServerService;
    @Autowired
    private CoinBalanceService coinBalanceService;
    @Autowired
    private FeeService feeService;
    
    public void service(final ScheduledTaskRegistrar taskRegistrar, final String type) {
        taskRegistrar.addTriggerTask(() -> this.otherJob(type), triggerContext -> {
            final String cron = "0/59 * * * * ? ";
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }
    
    private void otherJob(final String type) {
        try {
            this.coinServerService.checkServer(type);
            this.coinBalanceService.checkBalance(type);
        }
        catch (Exception e) {
            LOG.info(e.getMessage());
        }
    }
}
