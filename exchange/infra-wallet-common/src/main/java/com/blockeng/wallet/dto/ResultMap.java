package com.blockeng.wallet.dto;

import java.util.*;

public class ResultMap extends HashMap<String, Object>
{
    private static final long serialVersionUID = -4970973511892646114L;
    public static final int RETURN_RESULT_SUCCESSFUL = 0;
    public static final int RETURN_RESULT_ERROR = 1;
    public static final String RESULT = "data";
    public static final String STATUSCODE = "errcode";
    public static final String RESULT_DESC = "errmsg";

    public ResultMap()
    {
        this(0, null, null);
    }

    public ResultMap(Object result)
    {
        put("data", result);
    }

    public ResultMap(int statusCode, Object result, String desc)
    {
        put("errcode", Integer.valueOf(statusCode));
        put("data", result);
        put("errmsg", desc);
    }

    public static ResultMap getSuccessfulResult(Object result, String desc)
    {
        return new ResultMap(0, result, desc);
    }

    public static ResultMap getSuccessfulResult(Object result)
    {
        return getSuccessfulResult(result, null);
    }

    public static ResultMap getSuccessfulResult(String desc)
    {
        return getSuccessfulResult(null, desc);
    }

    public static ResultMap getSuccessfulResult(Boolean falg, String desc)
    {
        return getSuccessfulResult(desc, null);
    }

    public static ResultMap getSuccessfulResult()
    {
        return getSuccessfulResult(null, null);
    }

    public static ResultMap getFailureResult(int statusCode, Object result, String desc)
    {
        return new ResultMap(statusCode, result, desc);
    }

    public static ResultMap getFailureResult(int statusCode, Object result)
    {
        return getFailureResult(statusCode, result, null);
    }

    public static ResultMap getFailureResult(int statusCode, String desc)
    {
        return getFailureResult(statusCode, null, desc);
    }

    public static ResultMap getFailureResult(int statusCode)
    {
        return getFailureResult(statusCode, null, null);
    }

    public static ResultMap getFailureResult(Object result, String desc)
    {
        return getFailureResult(1, result, desc);
    }

    public static ResultMap getFailureResult(Object result)
    {
        return getFailureResult(result, null);
    }

    public static ResultMap getFailureResult(String desc)
    {
        return getFailureResult(null, desc);
    }

    public static ResultMap getFailureResult()
    {
        return getFailureResult(null, null);
    }

    public int getStatusCode()
    {
        Object status = get("errcode");
        return status == null ? 1 : 0;
    }
}
