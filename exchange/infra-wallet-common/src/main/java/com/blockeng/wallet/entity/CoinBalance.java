package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.math.*;
import java.util.*;
import java.io.*;

@TableName("coin_balance")
public class CoinBalance extends Model<CoinBalance>
{

    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("coin_id")
    private Long coinId;
    @TableField("coin_name")
    private String coinName;
    @TableField("system_balance")
    private BigDecimal systemBalance;
    @TableField("coin_type")
    private String coinType;
    @TableField("collect_account_balance")
    private BigDecimal collectAccountBalance;
    @TableField("loan_account_balance")
    private BigDecimal loanAccountBalance;
    @TableField("fee_account_balance")
    private BigDecimal feeAccountBalance;
    @TableField("recharge_account_balance")
    private BigDecimal rechargeAccountBalance;
    @TableField("last_update_time")
    private Date lastUpdateTime;
    private Date created;

    public CoinBalance setId(Long id)
    {
        this.id = id;return this;
    }

    public CoinBalance setCoinId(Long coinId)
    {
        this.coinId = coinId;return this;
    }

    public CoinBalance setCoinName(String coinName)
    {
        this.coinName = coinName;return this;
    }

    public CoinBalance setSystemBalance(BigDecimal systemBalance)
    {
        this.systemBalance = systemBalance;return this;
    }

    public CoinBalance setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }

    public CoinBalance setCollectAccountBalance(BigDecimal collectAccountBalance)
    {
        this.collectAccountBalance = collectAccountBalance;return this;
    }

    public CoinBalance setLoanAccountBalance(BigDecimal loanAccountBalance)
    {
        this.loanAccountBalance = loanAccountBalance;return this;
    }

    public CoinBalance setFeeAccountBalance(BigDecimal feeAccountBalance)
    {
        this.feeAccountBalance = feeAccountBalance;return this;
    }

    public CoinBalance setRechargeAccountBalance(BigDecimal rechargeAccountBalance)
    {
        this.rechargeAccountBalance = rechargeAccountBalance;return this;
    }

    public CoinBalance setLastUpdateTime(Date lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;return this;
    }

    public CoinBalance setCreated(Date created)
    {
        this.created = created;return this;
    }

    public Long getId()
    {
        return this.id;
    }

    public Long getCoinId()
    {
        return this.coinId;
    }

    public String getCoinName()
    {
        return this.coinName;
    }

    public BigDecimal getSystemBalance()
    {
        return this.systemBalance;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    public BigDecimal getCollectAccountBalance()
    {
        return this.collectAccountBalance;
    }

    public BigDecimal getLoanAccountBalance()
    {
        return this.loanAccountBalance;
    }

    public BigDecimal getFeeAccountBalance()
    {
        return this.feeAccountBalance;
    }

    public BigDecimal getRechargeAccountBalance()
    {
        return this.rechargeAccountBalance;
    }

    public Date getLastUpdateTime()
    {
        return this.lastUpdateTime;
    }

    public Date getCreated()
    {
        return this.created;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }
}
