package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.util.*;
import java.io.*;

@TableName("user_address")
public class UserAddress extends Model<UserAddress>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("user_id")
    private Long userId;
    @TableField("coin_id")
    private Long coinId;
    private String address;
    private String keystore;
    private String pwd;
    @TableField("last_update_time")
    private Date lastUpdateTime;
    private Date created;

    public UserAddress setId(Long id)
    {
        this.id = id;return this;
    }

    public UserAddress setUserId(Long userId)
    {
        this.userId = userId;return this;
    }

    public UserAddress setCoinId(Long coinId)
    {
        this.coinId = coinId;return this;
    }

    public UserAddress setAddress(String address)
    {
        this.address = address;return this;
    }

    public UserAddress setKeystore(String keystore)
    {
        this.keystore = keystore;return this;
    }

    public UserAddress setPwd(String pwd)
    {
        this.pwd = pwd;return this;
    }

    public UserAddress setLastUpdateTime(Date lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;return this;
    }

    public UserAddress setCreated(Date created)
    {
        this.created = created;return this;
    }

    public UserAddress(Long userId, Long coinId, String address, String keystore, String pwd)
    {
        this.userId = userId;
        this.coinId = coinId;
        this.address = address;
        this.keystore = keystore;
        this.pwd = pwd;
    }

    public Long getId()
    {
        return this.id;
    }

    public Long getUserId()
    {
        return this.userId;
    }

    public Long getCoinId()
    {
        return this.coinId;
    }

    public String getAddress()
    {
        return this.address;
    }

    public String getKeystore()
    {
        return this.keystore;
    }

    public String getPwd()
    {
        return this.pwd;
    }

    public Date getLastUpdateTime()
    {
        return this.lastUpdateTime;
    }

    public Date getCreated()
    {
        return this.created;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }

    public UserAddress() {}
}
