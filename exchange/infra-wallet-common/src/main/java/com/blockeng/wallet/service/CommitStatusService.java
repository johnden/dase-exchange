package com.blockeng.wallet.service;

public interface CommitStatusService
{
    void commitEth();
    
    void commitAct();
    
    void commitBtc();
    
    void commitEtc();
    
    void commitNeo();
}
