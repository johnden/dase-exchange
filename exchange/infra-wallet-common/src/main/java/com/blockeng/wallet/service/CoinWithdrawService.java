package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;
import java.util.*;

public interface CoinWithdrawService extends IService<CoinWithdraw>
{
    boolean updateTx(CoinWithdraw paramCoinWithdraw);

    List<CoinWithdraw> queryOutList(String paramString);

    List<CoinWithdraw> queryNoFeeList(Long paramLong, String paramString);

    void updateDrawInfo(CoinWithdraw paramCoinWithdraw);

    void withDraw(String paramString1, String paramString2);
}
