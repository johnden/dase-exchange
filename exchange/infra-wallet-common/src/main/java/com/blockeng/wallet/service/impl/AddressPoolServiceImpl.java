package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.entity.*;
import com.blockeng.wallet.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.utils.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.util.*;
import com.baomidou.mybatisplus.mapper.*;
import com.clg.wallet.bean.*;
import org.slf4j.*;

@Service
@Transactional
public class AddressPoolServiceImpl extends ServiceImpl<AddressPoolMapper, AddressPool> implements AddressPoolService
{
    private static final Logger LOG = LoggerFactory.getLogger(UserAddressServiceImpl.class);
    @Autowired
    private DESUtil desUtil;

    public int selectAddressCount(long coinId, String type)
    {
        EntityWrapper<AddressPool> ew = new EntityWrapper();
        if (coinId > 0L) {
            ew.eq("coin_id", Long.valueOf(coinId));
        }
        if (!StringUtils.isEmpty(type)) {
            ew.eq("coin_type", type);
        }
        return super.selectCount(ew);
    }

    public AddressPool selectAddress(Long id)
    {
        EntityWrapper<AddressPool> ew = new EntityWrapper();
        ew.eq("coin_id", id);
        return (AddressPool)super.selectOne(ew);
    }

    public boolean insertEthAddress(AddressBean addressBean, String type, long id)
    {
        AddressPool addressPool = new AddressPool();
        addressPool.setCoinType(type)
                .setCoinId(Long.valueOf(id))
                .setAddress(addressBean.getAddress());
        if (!StringUtils.isEmpty(addressBean.getPwd())) {
            addressPool.setPwd(this.desUtil.encrypt(addressBean.getPwd()));
        }
        if (!StringUtils.isEmpty(addressBean.getKeystore())) {
            addressPool.setKeystore(this.desUtil.encrypt(addressBean.getKeystore()));
        }
        return super.insert(addressPool);
    }
}
