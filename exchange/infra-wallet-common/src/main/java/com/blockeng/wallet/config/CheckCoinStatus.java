package com.blockeng.wallet.config;

import org.springframework.stereotype.*;
import com.blockeng.wallet.utils.*;
import org.springframework.beans.factory.annotation.*;

@Component
public class CheckCoinStatus
{
    @Autowired
    private ReadProperties readProperties;

    public boolean rechargeIsRunning()
    {
        return this.readProperties.recharge == 1;
    }

    public boolean collectIsRunning()
    {
        return this.readProperties.collect == 1;
    }

    public boolean drawIsRunning()
    {
        return this.readProperties.draw == 1;
    }

    public boolean otherIsRunning()
    {
        return this.readProperties.other == 1;
    }

    private boolean addressIsRunning()
    {
        return this.readProperties.address == 1;
    }

    public boolean allIsRunning()
    {
        return this.readProperties.allJob == 1;
    }

    public boolean featuresIsOpen(String feature)
    {
        if (feature.startsWith("collect")) {
            return collectIsRunning();
        }
        if (feature.startsWith("draw")) {
            return drawIsRunning();
        }
        if (feature.startsWith("other")) {
            return otherIsRunning();
        }
        if (feature.startsWith("recharge")) {
            return rechargeIsRunning();
        }
        if (feature.startsWith("address")) {
            return addressIsRunning();
        }
        return false;
    }
}
