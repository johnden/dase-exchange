package com.blockeng.wallet.web;

import com.blockeng.wallet.service.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.help.*;
import springfox.documentation.annotations.*;
import org.springframework.util.*;
import com.blockeng.wallet.dto.*;
import com.blockeng.wallet.exception.*;
import com.blockeng.wallet.entity.*;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;

@RestController
@RequestMapping({ "/coinConfig" })
public class CoinConfigController
{

    @Autowired
    private CoinConfigService coinConfigService;
    @Autowired
    private ClientInfo clientInfo;

    @PostMapping({"/changePass"})
    @ResponseBody
    @ApiOperation(value="order-001 更改钱包密码", notes="更改钱包密码", httpMethod="POST")
    @ApiImplicitParams({@io.swagger.annotations.ApiImplicitParam(name="更改钱包密码,新密码", value="newPass", required=true, dataType="String"), @io.swagger.annotations.ApiImplicitParam(name="������������,������", value="oldPass", required=true, dataType="String"), @io.swagger.annotations.ApiImplicitParam(name="����Id", value="coinId", required=true, dataType="Long")})
    WalletResultDTO changePass(@ApiIgnore String newPass, @ApiIgnore String oldPass, @ApiIgnore Long coinId)
    {
        if ((StringUtils.isEmpty(newPass)) || (StringUtils.isEmpty(oldPass)) || (null == coinId) || (coinId.longValue() <= 0L)) {
            return new WalletResultDTO().setStatusCode(WalletResultCode.USER_NOT_LONG.getCode());
        }
        CoinConfig coinConfig = this.clientInfo.getCoinConfigFormId(coinId);
        if (null == coinConfig) {
            return new WalletResultDTO().setStatusCode(WalletResultCode.NOT_EXIST_COIN_ID.getCode());
        }
        try
        {
            return this.coinConfigService.updateCoinPass(newPass, oldPass, coinConfig);
        }
        catch (CoinException e)
        {
            return new WalletResultDTO().setStatusCode(WalletResultCode.SUCCESS.getCode()).setResultDesc(e.getMessage());
        }
    }
}
