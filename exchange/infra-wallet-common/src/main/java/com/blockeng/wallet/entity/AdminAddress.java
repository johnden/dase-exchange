package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.io.*;

@TableName("admin_address")
public class AdminAddress extends Model<AdminAddress>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("coin_id")
    private Long coinId;
    private String keystore;
    private String pwd;
    private String address;
    private Integer status;
    @TableField("coin_type")
    private String coinType;

    public AdminAddress setId(Long id)
    {
        this.id = id;return this;
    }

    public AdminAddress setCoinId(Long coinId)
    {
        this.coinId = coinId;return this;
    }

    public AdminAddress setKeystore(String keystore)
    {
        this.keystore = keystore;return this;
    }

    public AdminAddress setPwd(String pwd)
    {
        this.pwd = pwd;return this;
    }

    public AdminAddress setAddress(String address)
    {
        this.address = address;return this;
    }

    public AdminAddress setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public AdminAddress setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }

    public Long getId()
    {
        return this.id;
    }

    public Long getCoinId()
    {
        return this.coinId;
    }

    public String getKeystore()
    {
        return this.keystore;
    }

    public String getPwd()
    {
        return this.pwd;
    }

    public String getAddress()
    {
        return this.address;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }
}
