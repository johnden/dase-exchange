package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;
import com.blockeng.wallet.dto.*;

public interface UserAddressService extends IService<UserAddress>
{

    UserAddress getByCoinIdAndAddr(String paramString, Long paramLong);

    UserAddress getByCoinIdAndUserId(Long paramLong1, Long paramLong2);

    int selectCount(String paramString, long paramLong);

    String getAddress(CoinAddressDTO paramCoinAddressDTO);
}
