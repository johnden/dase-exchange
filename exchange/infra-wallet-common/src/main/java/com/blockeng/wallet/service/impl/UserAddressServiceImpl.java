package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.help.*;
import com.blockeng.wallet.service.*;
import com.baomidou.mybatisplus.mapper.*;
import java.io.*;
import com.blockeng.wallet.dto.*;
import org.springframework.util.*;
import com.blockeng.wallet.utils.*;
import java.util.*;
import com.blockeng.wallet.entity.*;
import org.slf4j.*;

@Service
@Transactional
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements UserAddressService
{
    private static final Logger log = LoggerFactory.getLogger(UserAddressServiceImpl.class);
    @Autowired
    private CoinConfigService coinConfigService;
    @Autowired
    private UserAddressService userAddressService;
    @Autowired
    private AdminAddressService adminAddressService;
    @Autowired
    private ClientInfo clientInfo;
    @Autowired
    private AddressPoolService addressPoolService;

    public UserAddress getByCoinIdAndAddr(String to, Long coinId)
    {
        UserAddress userAddress = new UserAddress().setAddress(to);
        if ((null != coinId) && (coinId.longValue() > 0L)) {
            userAddress.setCoinId(coinId);
        }
        try
        {
            this.baseMapper.selectOne(userAddress);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return this.baseMapper.selectOne(userAddress);
    }

    public UserAddress getByCoinIdAndUserId(Long userId, Long coinId)
    {
        UserAddress userAddress = new UserAddress();
        if ((null != coinId) && (coinId.longValue() > 0L)) {
            userAddress.setCoinId(coinId);
        }
        if ((null != userId) && (userId.longValue() > 0L)) {
            userAddress.setUserId(userId);
        }
        return this.baseMapper.selectOne(userAddress);
    }

    public int selectCount(String to, long coidId)
    {
        EntityWrapper<UserAddress> ew = new EntityWrapper();
        ew.eq("address", to);
        if (coidId > 0L) {
            ew.eq("coin_id", Long.valueOf(coidId));
        }
        return this.baseMapper.selectCount(ew).intValue();
    }

    public synchronized String getAddress(CoinAddressDTO getAddressDTO)
    {
        Long userId = getAddressDTO.getUserId();
        CoinConfig coin = this.coinConfigService.selectById(getAddressDTO.getCoinId());
        if (null == coin) {
            return WalletResultDTO.errorResult(WalletResultCode.NOT_EXIST_COIN_ID.getCode(), WalletResultCode.NOT_EXIST_COIN_ID.getMessage()).toJson();
        }
        EntityWrapper<UserAddress> wrapper = new EntityWrapper();
        wrapper.eq("user_id", userId)
                .eq("coin_id", coin.getId());
        UserAddress userAddress = this.userAddressService.selectOne(wrapper);
        if (userAddress != null) {
            return WalletResultDTO.successResult(userAddress.getAddress()).toJson();
        }
        EntityWrapper<AdminAddress> addressWrapper = new EntityWrapper();
        addressWrapper.eq("coin_id", coin.getId()).eq("status", Integer.valueOf(4));
        List<AdminAddress> adminAddressList = this.adminAddressService.selectList(addressWrapper);
        if (!CollectionUtils.isEmpty(adminAddressList))
        {
            AdminAddress adminAddress;
            if (adminAddressList.size() == 1)
            {
                adminAddress = adminAddressList.get(0);
            }
            else
            {
                int index = new Random().nextInt(adminAddressList.size());
                adminAddress = adminAddressList.get(index);
            }
            String address = adminAddress.getAddress() + "|" + AddressCodeUtil.idToCode(getAddressDTO.getUserId().longValue());
            UserAddress newAddress = new UserAddress(userId, coin.getId(), address, null, null);
            this.baseMapper.insert(newAddress);
            return WalletResultDTO.successResult(address).toJson();
        }
        if (coin.getCoinType().contains("Token"))
        {
            CoinConfig mainCoin = this.clientInfo.getCoinConfigFormType(coin.getCoinType().replace("Token", "")).get(0);
            wrapper = new EntityWrapper();
            wrapper.eq("user_id", userId)
                    .eq("coin_id", mainCoin.getId());
            userAddress = (UserAddress)super.selectOne(wrapper);
            if (userAddress != null)
            {
                UserAddress newAddress = new UserAddress(userId, coin.getId(), userAddress.getAddress(), userAddress.getKeystore(), userAddress.getPwd());
                this.baseMapper.insert(newAddress);
                return WalletResultDTO.successResult(newAddress.getAddress()).toJson();
            }
            AddressPool addressPool = this.addressPoolService.selectAddress(mainCoin.getId());
            if (addressPool != null)
            {
                userAddress = new UserAddress(userId, mainCoin.getId(), addressPool.getAddress(), addressPool.getKeystore(), addressPool.getPwd());
                this.baseMapper.insert(userAddress);

                userAddress = new UserAddress(userId, coin.getId(), addressPool.getAddress(), addressPool.getKeystore(), addressPool.getPwd());
                this.baseMapper.insert(userAddress);

                this.addressPoolService.deleteById(addressPool.getId());
                return WalletResultDTO.successResult(addressPool.getAddress()).toJson();
            }
            return WalletResultDTO.errorResult(WalletResultCode.NOT_FIND_WALLET_ADDRESS.getCode(), WalletResultCode.NOT_FIND_WALLET_ADDRESS.getMessage()).toJson();
        }
        AddressPool addressPool = this.addressPoolService.selectAddress(coin.getId());
        if (addressPool != null)
        {
            userAddress = new UserAddress(userId, coin.getId(), addressPool.getAddress(), addressPool.getKeystore(), addressPool.getPwd());
            this.baseMapper.insert(userAddress);
            this.addressPoolService.deleteById(addressPool.getId());
            return WalletResultDTO.successResult(addressPool.getAddress()).toJson();
        }
        return WalletResultDTO.errorResult(WalletResultCode.NOT_FIND_WALLET_ADDRESS.getCode(), WalletResultCode.NOT_FIND_WALLET_ADDRESS.getMessage()).toJson();
    }

    private AddressPool getAddressPool(Long coinId)
    {
        AddressPool addressPool = this.addressPoolService.selectAddress(coinId);
        this.addressPoolService.deleteById(addressPool.getId());
        return addressPool;
    }
}
