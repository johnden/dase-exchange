package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.entity.*;
import com.blockeng.wallet.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.help.*;

@Service
@Transactional
public class AdminAddressServiceImpl extends ServiceImpl<AdminAddressMapper, AdminAddress> implements AdminAddressService
{
    private ClientInfo clientInfo;

    public AdminAddress queryAdminAccount(String type, int status)
    {
        return this.baseMapper.selectOne(new AdminAddress().setCoinType(type).setStatus(Integer.valueOf(status)));
    }

    public AdminAddress queryAdminAccount(Long coinId, int status)
    {
        return this.baseMapper.selectOne(new AdminAddress().setCoinId(coinId).setStatus(Integer.valueOf(status)));
    }
}
