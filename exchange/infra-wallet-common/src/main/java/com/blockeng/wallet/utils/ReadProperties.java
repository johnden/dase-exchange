package com.blockeng.wallet.utils;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;

@Component
public class ReadProperties
{

    @Value("${task.isOpen.allJob}")
    public int allJob;
    @Value("${task.isOpen.recharge}")
    public int recharge;
    @Value("${task.isOpen.collect}")
    public int collect;
    @Value("${task.isOpen.draw}")
    public int draw;
    @Value("${task.isOpen.other}")
    public int other;
    @Value("${task.isOpen.address}")
    public int address;
    @Value("${ip.local.limit}")
    public int localLimit;
    @Value("${task.ip.url}")
    public String ipUrl;
    @Value("${plant.aes.key}")
    public String aesKey;
    @Value("${recharge.count}")
    public int maxCount;
}
