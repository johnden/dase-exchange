package com.blockeng.wallet.utils;

import javax.crypto.spec.*;
import java.security.spec.*;
import java.security.*;
import javax.crypto.*;

public class EncryptCoder
{


    private static final String DES = "DES";

    public static byte[] encrypt(byte[] src, byte[] key)
            throws Exception
    {
        SecureRandom sr = new SecureRandom();

        DESKeySpec dks = new DESKeySpec(key);

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey securekey = keyFactory.generateSecret(dks);

        Cipher cipher = Cipher.getInstance("DES");

        cipher.init(1, securekey, sr);

        return cipher.doFinal(src);
    }

    public static final String encrypt(String password, String key)
    {
        try
        {
            return byte2String(encrypt(password.getBytes(), key.getBytes()));
        }
        catch (Exception localException) {}
        return null;
    }

    public static String byte2String(byte[] b)
    {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++)
        {
            stmp = Integer.toHexString(b[n] & 0xFF);
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs.toUpperCase();
    }

    public static String DataEncrypt(String str, byte[] key)
    {
        String encrypt = null;
        try
        {
            byte[] ret = encrypt(str.getBytes("UTF-8"), key);
            encrypt = new String(Base64.encode(ret));
        }
        catch (Exception e)
        {
            System.out.print(e);
            encrypt = str;
        }
        return encrypt;
    }

    public static void main(String[] args)
    {
        String encryptString = encrypt("123456", "!q@i#q$i%l^i&u*()-");
        System.out.println("加密后:" + encryptString);
    }
}
