package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.io.*;

@TableName("address_pool")
public class AddressPool extends Model<AddressPool>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("coin_id")
    private Long coinId;
    private String address;
    private String keystore;
    private String pwd;
    private String coinType;

    public AddressPool setId(Long id)
    {
        this.id = id;return this;
    }

    public AddressPool setCoinId(Long coinId)
    {
        this.coinId = coinId;return this;
    }

    public AddressPool setAddress(String address)
    {
        this.address = address;return this;
    }

    public AddressPool setKeystore(String keystore)
    {
        this.keystore = keystore;return this;
    }

    public AddressPool setPwd(String pwd)
    {
        this.pwd = pwd;return this;
    }

    public AddressPool setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }


    public Long getId()
    {
        return this.id;
    }

    public Long getCoinId()
    {
        return this.coinId;
    }

    public String getAddress()
    {
        return this.address;
    }

    public String getKeystore()
    {
        return this.keystore;
    }

    public String getPwd()
    {
        return this.pwd;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }
}
