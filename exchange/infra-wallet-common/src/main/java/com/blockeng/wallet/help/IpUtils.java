package com.blockeng.wallet.help;

import org.springframework.stereotype.*;
import com.blockeng.wallet.utils.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.util.*;
import com.alibaba.fastjson.*;
import com.clg.wallet.utils.*;
import java.util.regex.*;

@Component
public class IpUtils
{
    @Autowired
    private ReadProperties readProperties;
    String netIp = "";

    public String getNetIp()
    {
        if (!StringUtils.isEmpty(this.netIp)) {
            return this.netIp;
        }
        if ((this.readProperties.localLimit == 1) &&
                (StringUtils.isEmpty(this.netIp)) &&
                (!StringUtils.isEmpty(this.readProperties.ipUrl)))
        {
            JSONObject json = (JSONObject)HttpRequestUtil.getJson(this.readProperties.ipUrl, JSONObject.class);
            if (null != json)
            {
                this.netIp = json.getString("query");
            }
            else
            {
                json = (JSONObject)HttpRequestUtil.getJson("https://ipapi.co/json", JSONObject.class);
                if (null != json) {
                    this.netIp = json.getString("ip");
                }
            }
        }
        return this.netIp;
    }

    public boolean isIP(String addr)
    {
        if (StringUtils.isEmpty(addr)) {
            return false;
        }
        if ((addr.length() < 7) || (addr.length() > 15) || ("".equals(addr))) {
            return false;
        }
        String rexp = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";

        Pattern pat = Pattern.compile(rexp);

        Matcher mat = pat.matcher(addr);

        boolean ipAddress = mat.find();

        return ipAddress;
    }
}
