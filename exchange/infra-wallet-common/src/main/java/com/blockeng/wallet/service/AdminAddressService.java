package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;

public interface AdminAddressService extends IService<AdminAddress>
{
    AdminAddress queryAdminAccount(String paramString, int paramInt);

    AdminAddress queryAdminAccount(Long paramLong, int paramInt);
}
