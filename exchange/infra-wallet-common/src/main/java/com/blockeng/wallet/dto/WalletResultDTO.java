package com.blockeng.wallet.dto;

import io.swagger.annotations.ApiModelProperty;
import com.clg.wallet.utils.*;

public class WalletResultDTO
{
    @ApiModelProperty(example="响应代码")
    private int statusCode;
    @ApiModelProperty(example="结果描述")
    private String resultDesc;
    @ApiModelProperty(example="响应数据对象")
    private Object result;

    public WalletResultDTO setStatusCode(int statusCode)
    {
        this.statusCode = statusCode;return this;
    }

    public WalletResultDTO setResultDesc(String resultDesc)
    {
        this.resultDesc = resultDesc;return this;
    }

    public WalletResultDTO setResult(Object result)
    {
        this.result = result;return this;
    }

    public int getStatusCode()
    {
        return this.statusCode;
    }

    public String getResultDesc()
    {
        return this.resultDesc;
    }

    public Object getResult()
    {
        return this.result;
    }

    public WalletResultDTO(int statusCode, String resultDesc)
    {
        this.statusCode = statusCode;
        this.resultDesc = resultDesc;
    }

    public WalletResultDTO(int statusCode, Object result)
    {
        this.statusCode = statusCode;
        this.result = result;
    }

    public WalletResultDTO(int statusCode, String resultDesc, Object result)
    {
        this.statusCode = statusCode;
        this.resultDesc = resultDesc;
        this.result = result;
    }

    private WalletResultDTO(WalletResultCode resultCode)
    {
        this(resultCode.getCode(), resultCode.getMessage());
    }

    private WalletResultDTO(WalletResultCode resultCode, Object data)
    {
        this(resultCode.getCode(), resultCode.getMessage(), data);
    }

    public static WalletResultDTO successResult()
    {
        return new WalletResultDTO(WalletResultCode.SUCCESS);
    }

    public static WalletResultDTO successResult(Object data)
    {
        return new WalletResultDTO(WalletResultCode.SUCCESS, data);
    }

    public static WalletResultDTO errorResult(WalletResultCode resultCode)
    {
        return new WalletResultDTO(resultCode);
    }

    public static WalletResultDTO errorResult(int code, String message)
    {
        return new WalletResultDTO(code, message);
    }

    public static WalletResultDTO errorResult(int code, String message, Object result)
    {
        return new WalletResultDTO(code, message, result);
    }

    public static WalletResultDTO errorResult(int code, Object result)
    {
        return new WalletResultDTO(code, result);
    }

    public String toJson()
    {
        return GsonUtils.toJson(this);
    }

    public WalletResultDTO() {}
}
