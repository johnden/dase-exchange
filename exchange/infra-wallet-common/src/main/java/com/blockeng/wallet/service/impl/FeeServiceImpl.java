package com.blockeng.wallet.service.impl;

import org.springframework.transaction.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import com.blockeng.wallet.service.*;
import com.blockeng.wallet.help.*;
import com.blockeng.wallet.entity.*;
import org.springframework.util.*;
import com.clg.wallet.newclient.*;
import java.util.*;
import java.math.*;
import org.slf4j.*;

@Transactional
@Service
public class FeeServiceImpl implements FeeService
{
    private static final Logger LOG = LoggerFactory.getLogger(FeeServiceImpl.class);
    @Autowired
    private CoinWithdrawService coinWithdrawService;
    @Autowired
    private WalletCollectTaskService walletCollectTaskService;
    @Autowired
    private ClientInfo clientInfo;

    public void queryChainFee(Long id, String type)
    {
        List<CoinWithdraw> withdrawList = this.coinWithdrawService.queryNoFeeList(id, type);
        List<WalletCollectTask> walletCollectTaskList = this.walletCollectTaskService.queryNoFeeList(id, type);
        saveWithDrawFee(withdrawList, walletCollectTaskList);
    }

    private void saveWithDrawFee(List<CoinWithdraw> withdraws, List<WalletCollectTask> collectTaskList)
    {
        Client client = null;
        if (!CollectionUtils.isEmpty(withdraws)) {
            for (CoinWithdraw item : withdraws)
            {
                if (null == client) {
                    client = this.clientInfo.getClientFromId(item.getCoinId());
                }
                if (null != client)
                {
                    String txid = item.getTxid();
                    if (!StringUtils.isEmpty(txid))
                    {
                        BigDecimal feeBalance = client.getTransactionFee(txid).toBigDecimal();
                        this.coinWithdrawService.updateById(item.setChainFee(feeBalance));
                    }
                }
            }
        } else if (!CollectionUtils.isEmpty(collectTaskList)) {
            for (WalletCollectTask item : collectTaskList)
            {
                if (null == client) {
                    client = this.clientInfo.getClientFromId(item.getCoinId());
                }
                if (null != client)
                {
                    String txid = item.getTxid();
                    if (!StringUtils.isEmpty(txid))
                    {
                        BigDecimal feeBalance = client.getTransactionFee(txid).toBigDecimal();
                        this.walletCollectTaskService.updateById(item.setChainFee(feeBalance));
                    }
                }
            }
        } else {
            LOG.error("当前无需要查询手续费");
        }
    }
}
