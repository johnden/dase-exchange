package com.blockeng.wallet.utils;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import java.io.*;
import okhttp3.*;

@Component
public class SmsApi
{

    public static OkHttpClient client = new OkHttpClient();
    private static String url;
    private static String userid;
    private static String account;
    private static String password;

    @Value("${sms.url}")
    public void setUrl(String url)
    {
        url = url;
    }

    @Value("${sms.userid}")
    public void setUserid(String userid)
    {
        userid = userid;
    }

    @Value("${sms.account}")
    public void setAccount(String account)
    {
        account = account;
    }

    @Value("${sms.password}")
    public void setPassword(String password)
    {
        password = password;
    }

    public static String sendTo(String phone, String text)
    {
        RequestBody body = new FormBody.Builder().add("userid", userid).add("account", account).add("password", password).add("mobile", phone).add("content", text).add("action", "send").build();

        Request request = new Request.Builder().url(url).post(body).build();
        Response response = null;
        try
        {
            response = client.newCall(request).execute();
            return response.body().string();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static String querySendDetails()
    {
        RequestBody body = new FormBody.Builder().add("userid", userid).add("account", account).add("password", password).add("action", "overage").build();

        Request request = new Request.Builder().url(url).post(body).build();
        Response response = null;
        try
        {
            response = client.newCall(request).execute();
            return response.body().string();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
