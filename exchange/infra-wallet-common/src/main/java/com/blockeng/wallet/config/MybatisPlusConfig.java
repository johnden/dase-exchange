package com.blockeng.wallet.config;

import org.mybatis.spring.annotation.*;
import com.baomidou.mybatisplus.plugins.*;
import org.springframework.context.annotation.*;
import com.baomidou.mybatisplus.incrementer.*;
import com.baomidou.mybatisplus.mapper.*;

@Configuration
@MapperScan({ "com.blockeng.wallet.mapper*" })
public class MybatisPlusConfig
{
    @Bean
    public PaginationInterceptor paginationInterceptor()
    {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();

        paginationInterceptor.setLocalPage(true);
        return paginationInterceptor;
    }

    @Bean
    public IKeyGenerator keyGenerator()
    {
        return new H2KeyGenerator();
    }

    @Bean
    public ISqlInjector sqlInjector()
    {
        return new LogicSqlInjector();
    }
}
