package com.blockeng.wallet.utils;

import javax.crypto.spec.*;
import java.security.spec.*;
import java.security.*;
import javax.crypto.*;

public class DecryptCoder
{

    private static final String DES = "DES";

    public static byte[] decrypt(byte[] src, byte[] key)
            throws Exception
    {
        SecureRandom sr = new SecureRandom();

        DESKeySpec dks = new DESKeySpec(key);

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey securekey = keyFactory.generateSecret(dks);

        Cipher cipher = Cipher.getInstance("DES");

        cipher.init(2, securekey, sr);

        return cipher.doFinal(src);
    }

    public static final String decrypt(String data, String key)
    {
        try
        {
            return new String(decrypt(String2byte(data.getBytes()), key.getBytes()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] String2byte(byte[] b)
    {
        if (b.length % 2 != 0) {
            throw new IllegalArgumentException("长度不是偶数");
        }
        byte[] b2 = new byte[b.length / 2];
        for (int n = 0; n < b.length; n += 2)
        {
            String item = new String(b, n, 2);
            b2[(n / 2)] = ((byte)Integer.parseInt(item, 16));
        }
        return b2;
    }

    public static String DataDecrypt(String str, byte[] key)
    {
        String decrypt = null;
        try
        {
            byte[] ret = decrypt(Base64.decode(str), key);
            decrypt = new String(ret, "UTF-8");
        }
        catch (Exception e)
        {
            System.out.print(e);
            decrypt = str;
        }
        return decrypt;
    }
}
