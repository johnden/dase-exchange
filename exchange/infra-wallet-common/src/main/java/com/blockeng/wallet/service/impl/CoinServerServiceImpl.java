package com.blockeng.wallet.service.impl;

import com.baomidou.mybatisplus.service.impl.*;
import com.blockeng.wallet.mapper.*;
import com.blockeng.wallet.service.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import com.blockeng.wallet.help.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.util.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.enums.*;
import java.util.*;
import com.clg.wallet.newclient.*;
import com.clg.wallet.bean.*;
import com.baomidou.mybatisplus.mapper.*;
import org.slf4j.*;

@Service
@Transactional
public class CoinServerServiceImpl extends ServiceImpl<CoinServerMapper, CoinServer> implements CoinServerService
{

    private static final Logger LOG = LoggerFactory.getLogger(CoinServerServiceImpl.class);
    @Autowired
    private ClientInfo clientInfo;
    @Value("${eth.web.apikey}")
    private String ethApikey;

    public void checkServer(String type)
    {
        HashMap<String, List<CoinConfig>> typeCoinMap = this.clientInfo.getTypeCoinMap();
        if ((!CollectionUtils.isEmpty(typeCoinMap)) && (!StringUtils.isEmpty(type)))
        {
            List<CoinConfig> coinList = (List)typeCoinMap.get(type);
            checkItem(coinList);
        }
    }

    private void checkItem(List<CoinConfig> coinConfigs)
    {
        if (CollectionUtils.isEmpty(coinConfigs))
        {
            LOG.info("没有查询到需要服务的类");
            return;
        }
        for (CoinConfig coin : coinConfigs)
        {
            Client client = ClientFactory.getClient(getClientBean(coin));
            if (null == client)
            {
                LOG.error("获取钱包客户端失败");
            }
            else
            {
                ResultDTO result = client.getInfo();
                CoinServer newCoinServer = null;
                String mark = "服务器正常";
                if ((null != result) && (result.getStatusCode() == ResultCode.SUCCESS.getCode()))
                {
                    Integer blockNumber = Integer.valueOf(client.getBlockCount().toInteger());
                    int realBlockHeight = 0;
                    if ("eth".equalsIgnoreCase(coin.getCoinType())) {
                        realBlockHeight = ((EthNewClient)client).getExplorerBlockNumber(this.ethApikey).toInteger();
                    }
                    if (realBlockHeight - blockNumber.intValue() > 10) {
                        mark = "钱包服务器严重落后于真实高度,或者等待钱包同步完成";
                    }
                    newCoinServer = new CoinServer().setRpcIp(this.clientInfo.getCoinNetIp(coin.getId())).setRpcPort(coin.getRpcPort()).setRealNumber(Integer.valueOf(realBlockHeight)).setWalletNumber(blockNumber).setRunning(0).setMark(mark);
                }
                else
                {
                    newCoinServer = new CoinServer().setRpcIp(this.clientInfo.getCoinNetIp(coin.getId())).setRpcPort(coin.getRpcPort()).setRealNumber(Integer.valueOf(0)).setWalletNumber(Integer.valueOf(0)).setRunning(1).setMark("��������������,������������������");
                }
                CoinServer coinServer = selectCount(coin.getName());
                newCoinServer.setCoinName(coin.getName());
                if (null != coinServer)
                {
                    newCoinServer.setId(coinServer.getId());
                    super.updateById(newCoinServer);
                }
                else
                {
                    super.insert(newCoinServer);
                }
            }
        }
    }
    public ClientBean getClientBean(CoinConfig coinConfig)
    {
        ClientBean clientBean = new ClientBean();

        clientBean.setId(coinConfig.getId());
        clientBean.setName(coinConfig.getName());
        clientBean.setCoinType(coinConfig.getCoinType());
        clientBean.setCreditLimit(coinConfig.getCreditLimit());
        clientBean.setRpcIp(coinConfig.getRpcIp());
        clientBean.setRpcPort(coinConfig.getRpcPort());
        clientBean.setRpcUser(coinConfig.getRpcUser());
        clientBean.setRpcPwd(coinConfig.getRpcPwd());
        clientBean.setLastBlock(coinConfig.getLastBlock());
        clientBean.setWalletUser(coinConfig.getWalletUser());
        clientBean.setContractAddress(coinConfig.getContractAddress());
        clientBean.setMinConfirm(coinConfig.getMinConfirm());
        clientBean.setWalletPass(coinConfig.getWalletPass());
        clientBean.setContext(coinConfig.getContext());
        clientBean.setTask(coinConfig.getTask());
        clientBean.setStatus(coinConfig.getStatus());

        return clientBean;
    }
    public CoinServer selectCount(String name)
    {
        EntityWrapper<CoinServer> ew = new EntityWrapper();
        ew.eq("coin_name", name);
        return (CoinServer)super.selectOne(ew);
    }
}
