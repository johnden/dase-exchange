package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.math.*;
import java.util.*;
import java.io.*;

@TableName("coin_withdraw")
public class CoinWithdraw extends Model<CoinWithdraw>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    @TableField("user_id")
    private Long userId;
    @TableField("coin_id")
    private Long coinId;
    @TableField("coin_name")
    private String coinName;
    @TableField("coin_type")
    private String coinType;
    private String address;
    private Integer status;
    private String txid;
    private BigDecimal num;
    private BigDecimal fee;
    private BigDecimal mum;
    private Integer type;
    private BigDecimal chainFee;
    @TableField("block_num")
    private Integer blockNum;
    private String remark;
    private String walletMark;
    @TableField("last_update_time")
    private Date lastUpdateTime;
    private Date created;

    public CoinWithdraw setId(Long id)
    {
        this.id = id;return this;
    }

    public CoinWithdraw setUserId(Long userId)
    {
        this.userId = userId;return this;
    }

    public CoinWithdraw setCoinId(Long coinId)
    {
        this.coinId = coinId;return this;
    }

    public CoinWithdraw setCoinName(String coinName)
    {
        this.coinName = coinName;return this;
    }

    public CoinWithdraw setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }

    public CoinWithdraw setAddress(String address)
    {
        this.address = address;return this;
    }

    public CoinWithdraw setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public CoinWithdraw setTxid(String txid)
    {
        this.txid = txid;return this;
    }

    public CoinWithdraw setNum(BigDecimal num)
    {
        this.num = num;return this;
    }

    public CoinWithdraw setFee(BigDecimal fee)
    {
        this.fee = fee;return this;
    }

    public CoinWithdraw setMum(BigDecimal mum)
    {
        this.mum = mum;return this;
    }

    public CoinWithdraw setType(Integer type)
    {
        this.type = type;return this;
    }

    public CoinWithdraw setChainFee(BigDecimal chainFee)
    {
        this.chainFee = chainFee;return this;
    }

    public CoinWithdraw setBlockNum(Integer blockNum)
    {
        this.blockNum = blockNum;return this;
    }

    public CoinWithdraw setRemark(String remark)
    {
        this.remark = remark;return this;
    }

    public CoinWithdraw setWalletMark(String walletMark)
    {
        this.walletMark = walletMark;return this;
    }

    public CoinWithdraw setLastUpdateTime(Date lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;return this;
    }

    public CoinWithdraw setCreated(Date created)
    {
        this.created = created;return this;
    }

    public Long getId()
    {
        return this.id;
    }

    public Long getUserId()
    {
        return this.userId;
    }

    public Long getCoinId()
    {
        return this.coinId;
    }

    public String getCoinName()
    {
        return this.coinName;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    public String getAddress()
    {
        return this.address;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public String getTxid()
    {
        return this.txid;
    }

    public BigDecimal getNum()
    {
        return this.num;
    }

    public BigDecimal getFee()
    {
        return this.fee;
    }

    public BigDecimal getMum()
    {
        return this.mum;
    }

    public Integer getType()
    {
        return this.type;
    }

    public BigDecimal getChainFee()
    {
        return this.chainFee;
    }

    public Integer getBlockNum()
    {
        return this.blockNum;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public String getWalletMark()
    {
        return this.walletMark;
    }

    public Date getLastUpdateTime()
    {
        return this.lastUpdateTime;
    }

    public Date getCreated()
    {
        return this.created;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }
}
