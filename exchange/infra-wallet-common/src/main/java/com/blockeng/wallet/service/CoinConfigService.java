package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;
import java.util.*;
import com.blockeng.wallet.dto.*;
import com.blockeng.wallet.exception.*;

public interface CoinConfigService extends IService<CoinConfig>
{
    String lastBlock(Long paramLong);

    List<CoinConfig> selectCoinFromType(String paramString);

    List<CoinConfig> selectAllCoin();

    CoinConfig selectCoinFromId(Long paramLong);

    boolean updateCoinLastblock(String paramString, Long paramLong);

    WalletResultDTO updateCoinPass(String paramString1, String paramString2, CoinConfig paramCoinConfig)
            throws CoinException;
}
