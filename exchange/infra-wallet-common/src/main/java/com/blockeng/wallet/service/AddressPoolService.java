package com.blockeng.wallet.service;

import com.baomidou.mybatisplus.service.*;
import com.blockeng.wallet.entity.*;
import com.clg.wallet.bean.*;

public interface AddressPoolService extends IService<AddressPool>
{
    boolean insertEthAddress(AddressBean paramAddressBean, String paramString, long paramLong);

    int selectAddressCount(long paramLong, String paramString);

    AddressPool selectAddress(Long paramLong);
}
