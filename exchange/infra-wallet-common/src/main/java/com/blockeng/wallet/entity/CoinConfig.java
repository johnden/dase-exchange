package com.blockeng.wallet.entity;

import com.baomidou.mybatisplus.activerecord.*;
import com.baomidou.mybatisplus.enums.*;
import com.baomidou.mybatisplus.annotations.*;
import java.math.*;
import java.io.*;

@TableName("coin_config")
public class CoinConfig extends Model<CoinConfig>
{
    private static final long serialVersionUID = 1L;
    @TableId(value="id", type=IdType.ID_WORKER)
    private Long id;
    private String name;
    @TableField("coin_type")
    private String coinType;
    @TableField("credit_limit")
    private BigDecimal creditLimit;
    @TableField("credit_max_limit")
    private BigDecimal creditMaxLimit;
    @TableField("rpc_ip")
    private String rpcIp;
    @TableField("rpc_port")
    private String rpcPort;
    @TableField("rpc_user")
    private String rpcUser;
    @TableField("rpc_pwd")
    private String rpcPwd;
    @TableField("last_block")
    private String lastBlock;
    @TableField("wallet_user")
    private String walletUser;
    @TableField("contract_address")
    private String contractAddress;
    @TableField("min_confirm")
    private Integer minConfirm;
    @TableField("wallet_pass")
    private String walletPass;
    @TableField("context")
    private String context;
    private String task;
    private Integer status;

    public CoinConfig setId(Long id)
    {
        this.id = id;return this;
    }

    public CoinConfig setName(String name)
    {
        this.name = name;return this;
    }

    public CoinConfig setCoinType(String coinType)
    {
        this.coinType = coinType;return this;
    }

    public CoinConfig setCreditLimit(BigDecimal creditLimit)
    {
        this.creditLimit = creditLimit;return this;
    }

    public CoinConfig setCreditMaxLimit(BigDecimal creditMaxLimit)
    {
        this.creditMaxLimit = creditMaxLimit;return this;
    }

    public CoinConfig setRpcIp(String rpcIp)
    {
        this.rpcIp = rpcIp;return this;
    }

    public CoinConfig setRpcPort(String rpcPort)
    {
        this.rpcPort = rpcPort;return this;
    }

    public CoinConfig setRpcUser(String rpcUser)
    {
        this.rpcUser = rpcUser;return this;
    }

    public CoinConfig setRpcPwd(String rpcPwd)
    {
        this.rpcPwd = rpcPwd;return this;
    }

    public CoinConfig setLastBlock(String lastBlock)
    {
        this.lastBlock = lastBlock;return this;
    }

    public CoinConfig setWalletUser(String walletUser)
    {
        this.walletUser = walletUser;return this;
    }

    public CoinConfig setContractAddress(String contractAddress)
    {
        this.contractAddress = contractAddress;return this;
    }

    public CoinConfig setMinConfirm(Integer minConfirm)
    {
        this.minConfirm = minConfirm;return this;
    }

    public CoinConfig setWalletPass(String walletPass)
    {
        this.walletPass = walletPass;return this;
    }

    public CoinConfig setContext(String context)
    {
        this.context = context;return this;
    }

    public CoinConfig setTask(String task)
    {
        this.task = task;return this;
    }

    public CoinConfig setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public Long getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getCoinType()
    {
        return this.coinType;
    }

    public BigDecimal getCreditLimit()
    {
        return this.creditLimit;
    }

    public BigDecimal getCreditMaxLimit()
    {
        return this.creditMaxLimit;
    }

    public String getRpcIp()
    {
        return this.rpcIp;
    }

    public String getRpcPort()
    {
        return this.rpcPort;
    }

    public String getRpcUser()
    {
        return this.rpcUser;
    }

    public String getRpcPwd()
    {
        return this.rpcPwd;
    }

    public String getLastBlock()
    {
        return this.lastBlock;
    }

    public String getWalletUser()
    {
        return this.walletUser;
    }

    public String getContractAddress()
    {
        return this.contractAddress;
    }

    public Integer getMinConfirm()
    {
        return this.minConfirm;
    }

    public String getWalletPass()
    {
        return this.walletPass;
    }

    public String getContext()
    {
        return this.context;
    }

    public String getTask()
    {
        return this.task;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    protected Serializable pkVal()
    {
        return this.id;
    }

}
