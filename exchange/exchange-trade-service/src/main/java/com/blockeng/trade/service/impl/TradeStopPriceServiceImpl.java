package com.blockeng.trade.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.trade.entity.TradeStopPrice;
import com.blockeng.trade.mapper.TradeStopPriceMapper;
import com.blockeng.trade.service.TradeStopPriceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class TradeStopPriceServiceImpl extends ServiceImpl<TradeStopPriceMapper, TradeStopPrice> implements TradeStopPriceService {

}
