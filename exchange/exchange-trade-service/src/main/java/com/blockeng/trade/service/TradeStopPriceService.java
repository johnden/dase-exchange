package com.blockeng.trade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.trade.entity.TradeStopPrice;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface TradeStopPriceService extends IService<TradeStopPrice> {

}
