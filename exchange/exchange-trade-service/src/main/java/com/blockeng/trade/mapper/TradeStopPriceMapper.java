package com.blockeng.trade.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.trade.entity.TradeStopPrice;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface TradeStopPriceMapper extends BaseMapper<TradeStopPrice> {

}
