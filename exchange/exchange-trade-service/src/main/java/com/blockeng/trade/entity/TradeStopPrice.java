package com.blockeng.trade.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("trade_stop_price")
public class TradeStopPrice extends Model<TradeStopPrice> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 日期
     */
    @TableField("date_time")
    private LocalDateTime dateTime;
    /**
     * 昨日收盘价(凌晨1点后执行)
     */
    @TableField("close_price")
    private BigDecimal closePrice;
    /**
     * 当日最高涨停价
     */
    @TableField("top_price")
    private BigDecimal topPrice;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
