package com.blockeng.test.controller;

import com.blockeng.test.util.DateTimeUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月13日
 * @修改记录
 */
@RestController
@RequestMapping("coin")
public class CoinPriceController {
    private static final Logger logger = LoggerFactory.getLogger(CoinPriceController.class);


    @RequestMapping("getPrice")
    public ResponseEntity<String> getPrice() {
        logger.info("计算价格开始:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        String result = "";
        // 创建httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String url = "https://api.binance.com/api/v3/ticker/price?symbol=ETHUSDT";
        // 创建get方式请求对象
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("Content-type", "application/json");

        try {
            // 通过请求对象获取响应对象
            CloseableHttpResponse response = httpClient.execute(httpGet);

            // 获取结果实体
            // 判断网络连接状态码是否正常(0--200都数正常)
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity(), "utf-8");
            }
            // 释放链接
            response.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        logger.info("计算价格结束:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        return  ResponseEntity.ok(result);
    }
}
