package com.blockeng.test.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月04日
 * @修改记录
 */
public class DateTimeUtils {
    /**
     * 把long 转换成 日期 再转换成String类型
     */
    public static String convertLongToDate(Long millSec) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(millSec);
        return sdf.format(date);
    }
}
