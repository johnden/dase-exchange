package com.blockeng.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExchangeTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExchangeTestApplication.class, args);
    }

}
