package com.blockeng.feign.hystrix;

import com.blockeng.dto.SmsDTO;
import com.blockeng.feign.SmsServiceClient;
import org.springframework.stereotype.Component;

/**
 * @author qiang
 */
@Component
public class SmsServiceClientFallback implements SmsServiceClient {

    @Override
    public void sendTo(SmsDTO sms) {

    }
}
