package com.blockeng.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.entity.MockInvestLog;
import com.blockeng.mapper.MockInvestLogMapper;
import com.blockeng.service.MockInvestLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
@Service
public class MockInvestLogServiceImpl extends ServiceImpl<MockInvestLogMapper, MockInvestLog> implements MockInvestLogService {

}
