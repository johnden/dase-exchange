package com.blockeng.mapper;

import com.blockeng.entity.CoinLockPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface CoinLockPositionMapper extends BaseMapper<CoinLockPosition> {
    Long queryCoinStrategy(@Param("coinId") long coin_id,
                                    @Param("configType") int config_type);
}
