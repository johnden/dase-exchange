package com.blockeng.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.entity.MockInvestLog;
import com.blockeng.entity.User;
import com.blockeng.entity.UserInviteAward;
import com.blockeng.entity.UserInviteStatistics;
import com.blockeng.framework.http.Response;
import com.blockeng.framework.security.UserDetails;
import com.blockeng.service.MockInvestLogService;
import com.blockeng.service.UserInviteAwardService;
import com.blockeng.service.UserInviteStatisticsService;
import com.blockeng.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * 邀请管理控制类
 * add by hugo
 * 2018年5月14日18:31:05
 */
@RestController
@RequestMapping("/invite")
@Slf4j
@Api(value = "用户邀请-邀请管理", description = "用户邀请-邀请管理")
public class InviteController {

    @Autowired
    private UserInviteAwardService userInviteAwardService;

    @Autowired
    private UserInviteStatisticsService userInviteStatisticsService;

    @Autowired
    private MockInvestLogService mockInvestLogService;

    /**
     * 获取邀请奖励
     *
     * @return
     */
    @GetMapping(value = "/getInviteAward")
    @ApiOperation(value = "获取邀请奖励", notes = "奖励账户", httpMethod = "GET", authorizations = {@Authorization(value = "Authorization")})
    @PreAuthorize("isAuthenticated()")
    public Object getInviteAward(@ApiIgnore @AuthenticationPrincipal UserDetails userDetails) {
        QueryWrapper<UserInviteAward> userInviteAwardWrapper = new QueryWrapper<>();
        userInviteAwardWrapper.eq("user_id", userDetails.getId());
        List<UserInviteAward> userInviteAwards = userInviteAwardService.selectList(userInviteAwardWrapper);
        return Response.ok(userInviteAwards);
    }

    /**
     * 获取邀请统计
     *
     * @return
     */
    @GetMapping(value = "/getInviteStatistics")
    @ApiOperation(value = "获取邀请统计", notes = "奖励账户", httpMethod = "GET", authorizations = {@Authorization(value = "Authorization")})
    @PreAuthorize("isAuthenticated()")
    public Object getInviteStatistics(@ApiIgnore @AuthenticationPrincipal UserDetails userDetails) {
        QueryWrapper<UserInviteStatistics> inviteStatisticsWrapper = new QueryWrapper<>();
        inviteStatisticsWrapper.eq("user_id", userDetails.getId()).and(wrapper->wrapper.gt("invite_level",0));
        List<UserInviteStatistics> userInviteStatistics = userInviteStatisticsService.selectList(inviteStatisticsWrapper);
        return Response.ok(userInviteStatistics);
    }


    /**
     * 获取奖励总额
     *
     * @return
     */
    @GetMapping(value = "/getInviteAwardCount")
    @ApiOperation(value = "获取奖励总额", notes = "奖励账户", httpMethod = "GET", authorizations = {@Authorization(value = "Authorization")})
    @PreAuthorize("isAuthenticated()")
    public Object getInviteAwardCount(@ApiIgnore @AuthenticationPrincipal UserDetails userDetails) {
        BigDecimal totalAmount = userInviteAwardService.getInviteAwardCount(userDetails.getId());
        if (!StringUtils.isEmpty(totalAmount)) {
            totalAmount = totalAmount.setScale(2, RoundingMode.HALF_UP);
        } else {
            totalAmount = BigDecimal.ZERO;
        }
        return Response.ok(totalAmount);
    }


    /**
     * 空投列表
     *
     * @param userDetails
     * @return
     */
    @GetMapping(value = "/invests")
    @ApiOperation(value = "空投列表", notes = "资金账户", httpMethod = "GET", authorizations = {@Authorization(value = "Authorization")})
    @PreAuthorize("isAuthenticated()")
    public Object accounts(@ApiIgnore @AuthenticationPrincipal UserDetails userDetails) {
        QueryWrapper<MockInvestLog> mockInvestLogWrapper = new QueryWrapper<>();
        mockInvestLogWrapper.eq("user_id",userDetails.getId());
        List<MockInvestLog> mockInvestLogs = mockInvestLogService.selectList(mockInvestLogWrapper);
        return Response.ok(mockInvestLogs);
    }
}
