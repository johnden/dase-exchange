package com.blockeng.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blockeng.entity.WorkIssue;
import com.blockeng.framework.enums.WorkIssueStatus;
import com.blockeng.framework.http.Response;
import com.blockeng.framework.security.UserDetails;
import com.blockeng.repository.TicketRepository;
import com.blockeng.service.WorkIssueService;
import com.blockeng.vo.TicketForm;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.Date;

/**
 * <p>
 * 工单记录 前端控制器
 * </p>
 *
 * @author qiang
 * @since 2018-05-31
 */
@RestController
@RequestMapping("/workIssue")
@Slf4j
@Api(value = "工单系统", description = "工单系统 REST API")
public class TicketController {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private WorkIssueService workIssueService;

    @PostMapping("/addWorkIssue")
    @ApiOperation(value = "添加工单",notes = "添加工单",httpMethod = "POST",authorizations = {@Authorization(value = "Authorization")})
    @PreAuthorize("isAuthenticated()")
    public Object addWorkIssue(@RequestBody @Valid TicketForm form, @ApiIgnore @AuthenticationPrincipal UserDetails userDetails) {
        //Ticket ticket = TicketMapper.INSTANCE.map(form)
        WorkIssue workIssue = new WorkIssue();
        workIssue.setQuestion(form.getQuestion()).
                setUserId(userDetails.getId()).
                setStatus(WorkIssueStatus.NOT_AN.getCode()).
                setCreated(new Date()).
                setLastUpdateTime(new Date());
        workIssueService.insert(workIssue);
        return Response.ok();
    }

    @GetMapping("/issueList/{current}/{size}")
    @ApiOperation(value = "获取工单列表",notes = "获取工单列表",httpMethod = "GET", authorizations = {@Authorization(value = "Authorization")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页码", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "size", value = "每页显示数据量", required = true, dataType = "int", paramType = "path")
    })
    @PreAuthorize("isAuthenticated()")
    public Object issueList(@PathVariable("current") int current,
                            @PathVariable("size") int size,
                            @ApiIgnore @AuthenticationPrincipal UserDetails userDetails) {
        QueryWrapper<WorkIssue> e = new QueryWrapper<>();
        e.eq("user_id", userDetails.getId());
        e.orderByDesc("created");
        IPage<WorkIssue> page = new Page<>(current,size);
        Pageable pageable = PageRequest.of(current, size);

        return Response.ok(workIssueService.selectPage(page, e));
    }
}