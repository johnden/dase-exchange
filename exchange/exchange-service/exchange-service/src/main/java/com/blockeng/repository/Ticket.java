package com.blockeng.repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 工单记录
 * </p>
 *
 * @author qiang
 * @since 2018-05-31
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "ticket")
public class Ticket implements Serializable {

    /**
     * 主键
     */
    @Id
    private String id;
    /**
     * 用户id(提问用户id)
     */
    @Field("user_id")
    @TextIndexed
    private Long userId;
    /**
     * 回复人id
     */
    @Field("answer_user_id")
    @TextIndexed
    private Long answerUserId;
    /**
     * 回复人名称
     */
    @Field("answer_name")
    @TextIndexed
    private String answerName;
    /**
     * 工单内容
     */
    @TextIndexed
    private String question;
    /**
     * 回答内容
     */
    private String answer;
    /**
     * 状态：1-待回答；2-已回答；
     */
    @TextIndexed
    private Integer status;
    /**
     * 修改时间
     */
    @Field("last_update_time")
    private Date lastUpdateTime;
    /**
     * 创建时间
     */
    @TextIndexed
    private Date created;
}
