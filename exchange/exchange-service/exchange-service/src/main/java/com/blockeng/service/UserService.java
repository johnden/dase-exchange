package com.blockeng.service;

import com.blockeng.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserService extends IService<User> {

    /**
     * 获取用户所有邀请记录
     * @param userId
     * @return
     */
    List<User> getInviteList(Long userId);
}
