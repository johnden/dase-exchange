package com.blockeng.service.impl;

import com.blockeng.entity.CoinPrice;
import com.blockeng.mapper.CoinPriceMapper;
import com.blockeng.service.CoinPriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-13
 */
@Service
public class CoinPriceServiceImpl extends ServiceImpl<CoinPriceMapper, CoinPrice> implements CoinPriceService {

}
