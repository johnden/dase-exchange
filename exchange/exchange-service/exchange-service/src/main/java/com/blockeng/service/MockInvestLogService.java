package com.blockeng.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.entity.MockInvestLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
public interface MockInvestLogService extends IService<MockInvestLog> {

}
