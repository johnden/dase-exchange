package com.blockeng.mapper;

import com.blockeng.entity.CoinPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-13
 */
public interface CoinPriceMapper extends BaseMapper<CoinPrice> {

}
