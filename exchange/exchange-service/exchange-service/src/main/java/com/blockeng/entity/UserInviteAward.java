package com.blockeng.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_invite_award")
public class UserInviteAward extends Model<UserInviteAward> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 奖励时间
     */
    @TableField("invite_date")
    private LocalDateTime inviteDate;
    /**
     * 奖励金额
     */
    @TableField("invite_amount")
    private BigDecimal inviteAmount;
    /**
     * 裂变级别
     */
    @TableField("invite_level")
    private Integer inviteLevel;
    /**
     * 用户编号
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 订单编号
     */
    @TableField("order_id")
    private Long orderId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
