package com.blockeng.mapper;

import com.blockeng.entity.UserInviteStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserInviteStatisticsMapper extends BaseMapper<UserInviteStatistics> {

}
