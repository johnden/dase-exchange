package com.blockeng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.entity.MockInvestLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
public interface MockInvestLogMapper extends BaseMapper<MockInvestLog> {

}
