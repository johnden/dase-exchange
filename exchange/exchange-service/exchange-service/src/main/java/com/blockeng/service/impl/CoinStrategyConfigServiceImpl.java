package com.blockeng.service.impl;

import com.blockeng.entity.CoinStrategyConfig;
import com.blockeng.mapper.CoinStrategyConfigMapper;
import com.blockeng.service.CoinStrategyConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class CoinStrategyConfigServiceImpl extends ServiceImpl<CoinStrategyConfigMapper, CoinStrategyConfig> implements CoinStrategyConfigService {

}
