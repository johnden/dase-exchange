package com.blockeng.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("mock_invest_log")
public class MockInvestLog extends Model<MockInvestLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 投资数量
     */
    @TableField("invest_value")
    private BigDecimal investValue;
    /**
     * 投资类型
     */
    @TableField("invest_type")
    private Integer investType;
    /**
     * 创建时间
     */
    private Date created;
    /**
     * 空投名称
     */
    @TableField("invest_name")
    private String investName;
    /**
     * 币名称
     */
    @TableField("coin_name")
    private String coinName;
    /**
     * 投资类型
     */
    @TableField("is_check")
    private Integer isCheck;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
