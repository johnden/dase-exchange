package com.blockeng.service.impl;

import com.blockeng.entity.User;
import com.blockeng.mapper.UserMapper;
import com.blockeng.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    /**
     * 获取用户所有邀请记录
     * @param userId
     * @return
     */
    public List<User> getInviteList(Long userId)
    {
        return  baseMapper.getInviteList(userId);
    }
}
