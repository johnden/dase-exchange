package com.blockeng.service.impl;

import com.blockeng.entity.CoinLockPosition;
import com.blockeng.mapper.CoinLockPositionMapper;
import com.blockeng.service.CoinLockPositionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class CoinLockPositionServiceImpl extends ServiceImpl<CoinLockPositionMapper, CoinLockPosition> implements CoinLockPositionService {
    public Long queryCoinStrategy(long coin_id,
                                       int config_type){
        return baseMapper.queryCoinStrategy(coin_id,config_type);
    }
}
