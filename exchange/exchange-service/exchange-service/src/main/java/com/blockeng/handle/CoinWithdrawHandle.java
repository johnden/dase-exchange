package com.blockeng.handle;

import com.blockeng.dto.SmsDTO;
import com.blockeng.entity.Account;
import com.blockeng.entity.CoinWithdraw;
import com.blockeng.feign.SmsServiceClient;
import com.blockeng.framework.enums.BusinessType;
import com.blockeng.framework.enums.CoinWithdrawStatus;
import com.blockeng.framework.enums.SmsTemplate;
import com.blockeng.framework.utils.GsonUtil;
import com.blockeng.service.AccountService;
import com.blockeng.service.CoinWithdrawService;
import com.blockeng.user.dto.UserDTO;
import com.blockeng.user.feign.UserServiceClient;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 数字货币提现打币成功
 * @Author: Chen Long
 * @Date: Created in 2018/6/30 下午10:54
 * @Modified by: Chen Long
 */
@Component
@Slf4j
public class CoinWithdrawHandle {

    @Autowired
    private CoinWithdrawService coinWithdrawService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private SmsServiceClient smsServiceClient;

    @Autowired
    private UserServiceClient userServiceClient;

    /**
     * 数字货币提现打币成功
     *
     * @param message 提币消息
     */
    @RabbitListener(queues = {"finance.withdraw.result"})
    public void coinRechargeSuccess(String message) {
        log.error(message);
        if (StringUtils.isEmpty(message)) {
            log.error("数字货币提现结果消息为空");
            return;
        }
        CoinWithdraw coinWithdrawResult;
        try {
            coinWithdrawResult = new Gson().fromJson(message, CoinWithdraw.class);
            if (null != coinWithdrawResult) {
                CoinWithdraw coinWithdraw = coinWithdrawService.selectById(coinWithdrawResult.getId());
                if (coinWithdraw == null) {
                    log.error("提现申请单ID错误");
                    return;
                }
                if (coinWithdrawResult.getStatus() == 5) {//打币成
                    // 更新提币申请单状态
                    coinWithdraw.setStatus(CoinWithdrawStatus.SUCCESS.getCode());
                    coinWithdraw.setTxid(coinWithdrawResult.getTxid());
                    coinWithdraw.setFee(coinWithdrawResult.getFee());
                    coinWithdrawService.updateById(coinWithdraw);
                    // 扣减账户资金
                    accountService.subtractAmount(coinWithdraw.getUserId(),
                            coinWithdraw.getCoinId(),
                            coinWithdraw.getNum(),
                            BusinessType.WITHDRAW,
                            BusinessType.WITHDRAW.getDesc(),
                            coinWithdraw.getId());

                    // 短信通知用户
                    UserDTO user = userServiceClient.selectById(coinWithdraw.getUserId());
                    Map<String, Object> templateParam = new HashMap<>();
                    templateParam.put("amount", coinWithdraw.getMum());
                    templateParam.put("coinName", coinWithdraw.getCoinName());
                    SmsDTO smsDTO = new SmsDTO();
                    smsDTO.setCountryCode(user.getCountryCode())
                            .setMobile(user.getMobile())
                            .setEmail(user.getEmail())
                            .setTemplateCode(SmsTemplate.WITHDRAW_SUCCESS.getCode())
                            .setTemplateParam(templateParam);
                    smsServiceClient.sendTo(smsDTO);
                } else {
                    if (coinWithdrawResult.getStatus() == CoinWithdrawStatus.REFUSE.getCode()) {
                        // 提币失败
                        coinWithdraw.setStatus(CoinWithdrawStatus.REFUSE.getCode())
                                .setWalletMark(coinWithdrawResult.getWalletMark());
                        coinWithdrawService.updateById(coinWithdraw);
                        // 解冻资金
                        accountService.unlockAmount(coinWithdraw.getUserId(),
                                coinWithdraw.getCoinId(),
                                coinWithdraw.getNum(),
                                BusinessType.WITHDRAW,
                                coinWithdraw.getId());
                    } else {
                        // 打币失败
                        coinWithdraw.setStatus(CoinWithdrawStatus.FAILED.getCode())
                                .setWalletMark(coinWithdrawResult.getWalletMark());
                        coinWithdrawService.updateById(coinWithdraw);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数字货币提现结果处理失败, 堆栈信息：{}", e.getMessage());
        }
    }
}
