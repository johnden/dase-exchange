package com.blockeng.web;

import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月15日
 * @修改记录
 */
@RestController
@RequestMapping("/wallet")
@Slf4j
@Api(value = "钱包管理-获取充值数据", description = "钱包管理-获取充值数据")
public class WalletController {

    /**
     * 获取充值数据
     *
     * @return
     */
    @GetMapping(value = "/walletRechargeSuccess")
    @ApiOperation(value = "获取充值数据", notes = "钱包管理", httpMethod = "POST", authorizations = {@Authorization(value = "Authorization")})
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", value = "记录编号", required = true, dataType = "Long"),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户编号", required = true, dataType = "Long"),
            @ApiImplicitParam(paramType = "query", name = "coinId", value = "币编号", required = true, dataType = "Long"),
            @ApiImplicitParam(paramType = "query", name = "coinName", value = "币名称", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "coinType", value = "币类型", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "address", value = "地址", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "confirm", value = "是否确认", required = true, dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "txid", value = "交易编号", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "amount", value = "金额", required = true, dataType = "BigDecimal")
    })
    @PreAuthorize("isAuthenticated()")
    public Object WalletRechargeSuccess() {
        return ResponseEntity.ok();
    }
}
