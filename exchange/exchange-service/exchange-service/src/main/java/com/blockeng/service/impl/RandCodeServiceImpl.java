package com.blockeng.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.dto.RandCode;
import com.blockeng.dto.RandCodeVerifyDTO;
import com.blockeng.dto.Sms;
import com.blockeng.entity.Config;
import com.blockeng.service.ConfigService;
import com.blockeng.service.RandCodeService;
import com.blockeng.service.SendService;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author qiang
 */
@Service
@Slf4j
public class RandCodeServiceImpl implements RandCodeService {
    private static final Logger logger = LoggerFactory.getLogger(RandCodeServiceImpl.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ConfigService configService;

    @Autowired
    private SendService sendService;

    @Override
    public void sendTo(RandCode randCode) {
        String code = RandomStringUtils.randomNumeric(6);
        QueryWrapper<Config> ew = new QueryWrapper<>();
        ew.eq("type", "SMS");
        ew.eq("code", randCode.getTemplateCode());
        Config config = configService.selectOne(ew);
        if (Optional.ofNullable(config).isPresent()) {
            String content = config.getValue();
            QueryWrapper<Config> signQueryWrapper = new QueryWrapper<>();
            signQueryWrapper.eq("type", "SMS");
            signQueryWrapper.eq("code", "SIGN");
            Config signConfig = configService.selectOne(signQueryWrapper);
            if (Optional.ofNullable(signConfig).isPresent()) {
                content = content.replaceAll("\\$\\{".concat("sign").concat("\\}"), signConfig.getValue());
            }
            content = content.replaceAll("\\$\\{".concat("code").concat("\\}"), code);
            logger.info("******hugo-短信调用开始******");
            if (!Strings.isNullOrEmpty(randCode.getPhone())) {
                String key = String.format("CAPTCHA:%s:%s", randCode.getTemplateCode(), randCode.getCountryCode()+randCode.getPhone());
                stringRedisTemplate.opsForValue().set(key, code);
                stringRedisTemplate.expire(key, 30L, TimeUnit.MINUTES);

                Sms sms = new Sms();
                sms.setCountryCode(randCode.getCountryCode());
                sms.setPhone(randCode.getPhone());
                sms.setContent(content);
                sendService.sendTo(sms);
                logger.info("******hugo-短信调用正常，手机注册******，号码为{}",randCode.getPhone());
            }else if (!Strings.isNullOrEmpty(randCode.getEmail())) {
                String key = String.format("CAPTCHA:%s:%s", randCode.getTemplateCode(), randCode.getEmail());
                stringRedisTemplate.opsForValue().set(key, code);
                stringRedisTemplate.expire(key, 30L, TimeUnit.MINUTES);

                String to = randCode.getEmail();
                String subject = config.getName();
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(to);
                message.setSubject(subject);
                message.setText(content);
                sendService.sendTo(message);
                logger.info("******hugo-短信调用正常，邮箱注册******,邮箱为{}",randCode.getEmail());
            }
        }else {
            logger.info("******hugo-短信调用异常，模版不存在******");
            throw new RuntimeException("短信模板不存在！");
        }
    }

    @Override
    public boolean verify(RandCodeVerifyDTO randCodeVerifyDTO) {
        String account = randCodeVerifyDTO.getEmail();
        if (!Strings.isNullOrEmpty(randCodeVerifyDTO.getPhone())) {
            account = randCodeVerifyDTO.getCountryCode()+randCodeVerifyDTO.getPhone();
            logger.info("用户帐号{}信息",account);
        }
        String key = String.format("CAPTCHA:%s:%s", randCodeVerifyDTO.getTemplateCode(), account);
        logger.info("查询到的key{}",key);
        String value = stringRedisTemplate.opsForValue().get(key);
        if(!Strings.isNullOrEmpty(account) && !Strings.isNullOrEmpty(value) && value.equals(randCodeVerifyDTO.getCode())) {
            logger.info("删除账户{}",account);
            stringRedisTemplate.delete(key);
            return true;
        }else {
            return false;
        }
    }
}