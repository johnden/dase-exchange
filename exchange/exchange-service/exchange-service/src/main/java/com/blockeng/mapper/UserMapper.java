package com.blockeng.mapper;

import com.blockeng.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 获取用户所有邀请记录
     * @param userId
     * @return
     */
    List<User> getInviteList(@Param("user_id") Long userId);
}
