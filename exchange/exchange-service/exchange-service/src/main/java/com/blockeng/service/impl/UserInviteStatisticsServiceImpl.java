package com.blockeng.service.impl;

import com.blockeng.entity.UserInviteStatistics;
import com.blockeng.mapper.UserInviteStatisticsMapper;
import com.blockeng.service.UserInviteStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
@Service
public class UserInviteStatisticsServiceImpl extends ServiceImpl<UserInviteStatisticsMapper, UserInviteStatistics> implements UserInviteStatisticsService {

}
