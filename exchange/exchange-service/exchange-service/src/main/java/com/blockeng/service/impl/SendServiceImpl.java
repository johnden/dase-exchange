package com.blockeng.service.impl;

import com.blockeng.dto.Sms;
import com.blockeng.service.SendService;
import com.lmax.disruptor.spring.boot.DisruptorTemplate;
import com.lmax.disruptor.spring.boot.event.DisruptorBindEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * @author qiang
 */
@Service
@Slf4j
public class SendServiceImpl implements SendService {

    @Autowired
    protected DisruptorTemplate disruptorTemplate;

    @Override
    public void sendTo(Sms sms) {
        DisruptorBindEvent event = new DisruptorBindEvent(sms, "message " + Math.random());

        event.setEvent("Event-Output");
        event.setTag("SendSms-Output");
        event.setKey("id-" + Math.random());

        disruptorTemplate.publishEvent(event);
    }

    @Override
    public void sendTo(SimpleMailMessage email) {
        DisruptorBindEvent event = new DisruptorBindEvent(email, "message " + Math.random());

        event.setEvent("Event-Output");
        event.setTag("SendMail-Output");
        event.setKey("id-" + Math.random());

        disruptorTemplate.publishEvent(event);
    }
}