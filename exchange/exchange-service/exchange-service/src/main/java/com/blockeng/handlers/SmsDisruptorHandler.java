package com.blockeng.handlers;

import com.blockeng.api.SmsApi;
import com.blockeng.dto.Sms;
import com.blockeng.repository.SendRecord;
import com.blockeng.repository.SendRecordRepository;
import com.blockeng.vo.mappers.SendRecordMapper;
import com.lmax.disruptor.spring.boot.annotation.EventRule;
import com.lmax.disruptor.spring.boot.event.DisruptorBindEvent;
import com.lmax.disruptor.spring.boot.event.handler.DisruptorHandler;
import com.lmax.disruptor.spring.boot.event.handler.chain.HandlerChain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author qiang
 */
@EventRule("/Event-Output/SendSms-Output/**")
@Component
@Slf4j
public class SmsDisruptorHandler implements DisruptorHandler<DisruptorBindEvent> {

    @Autowired
    private SendRecordRepository sendRecordRepository;

    @Override
    public void doHandler(DisruptorBindEvent event, HandlerChain<DisruptorBindEvent> handlerChain) {
        Sms sms = (Sms) event.getSource();
        SendRecord sendRecord = SendRecordMapper.INSTANCE.map(sms);
        sendRecord.setStatus(1);
        String result = SmsApi.sendTo(sms.getCountryCode(), sms.getPhone(), sms.getContent());
        sendRecord.setRemark(result);
        sendRecordRepository.save(sendRecord);
        if (log.isDebugEnabled()) {
            log.debug(sms.toString());
        }
    }
}