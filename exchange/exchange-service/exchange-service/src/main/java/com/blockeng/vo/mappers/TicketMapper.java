package com.blockeng.vo.mappers;

import com.blockeng.repository.Ticket;
import com.blockeng.vo.TicketForm;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author qiang
 */
@Mapper
public interface TicketMapper {

    TicketMapper INSTANCE = Mappers.getMapper(TicketMapper.class);

    Ticket map(TicketForm form);
}
