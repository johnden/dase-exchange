package com.blockeng.service;

import com.blockeng.entity.CoinStrategyConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface CoinStrategyConfigService extends IService<CoinStrategyConfig> {

}
