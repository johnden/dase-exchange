package com.blockeng.mapper;

import com.blockeng.entity.UserInviteAward;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserInviteAwardMapper extends BaseMapper<UserInviteAward> {
    /**
     * 查询奖励总额
     * @param userId
     * @return
     */
    BigDecimal getInviteAwardCount(@Param("user_id")Long userId);
}
