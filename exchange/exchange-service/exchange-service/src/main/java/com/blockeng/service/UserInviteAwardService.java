package com.blockeng.service;

import com.blockeng.entity.UserInviteAward;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserInviteAwardService extends IService<UserInviteAward> {
    /**
     * 查询奖励总额
     * @param userId
     * @return
     */
    BigDecimal getInviteAwardCount(Long userId);
}
