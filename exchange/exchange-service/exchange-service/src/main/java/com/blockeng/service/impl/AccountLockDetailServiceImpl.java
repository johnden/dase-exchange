package com.blockeng.service.impl;

import com.blockeng.entity.AccountLockDetail;
import com.blockeng.mapper.AccountLockDetailMapper;
import com.blockeng.service.AccountLockDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class AccountLockDetailServiceImpl extends ServiceImpl<AccountLockDetailMapper, AccountLockDetail> implements AccountLockDetailService {

}
