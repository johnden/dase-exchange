package com.blockeng.service;

import com.blockeng.entity.CoinPrice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-13
 */
public interface CoinPriceService extends IService<CoinPrice> {

}
