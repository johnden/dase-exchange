package com.blockeng.handle;

import com.blockeng.framework.dto.UnlockDTO;
import com.blockeng.framework.utils.GsonUtil;
import com.blockeng.service.AccountService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;


public class PoolUnlockHandle {

    @Autowired
    private AccountService accountService;

    /**
     * 刷新交易对缓存
     */
    @RabbitListener(queues = {"pool.unlock"})
    public void mineUnlock(String msg) {
        UnlockDTO unlockDTO = GsonUtil.convertObj(msg, UnlockDTO.class);
        accountService.addAmount(unlockDTO.getUserId(),
                unlockDTO.getCoinId(),
                unlockDTO.getAmount(),
                unlockDTO.getBusinessType(),
                unlockDTO.getDesc(),
                unlockDTO.getOrderId());

    }
}
