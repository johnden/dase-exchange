package com.blockeng.mapper;

import com.blockeng.entity.AccountLockDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface AccountLockDetailMapper extends BaseMapper<AccountLockDetail> {

}
