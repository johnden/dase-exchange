package com.blockeng.service;

import com.blockeng.entity.UserInviteStatistics;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserInviteStatisticsService extends IService<UserInviteStatistics> {

}
