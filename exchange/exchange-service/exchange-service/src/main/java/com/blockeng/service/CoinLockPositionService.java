package com.blockeng.service;

import com.blockeng.entity.CoinLockPosition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface CoinLockPositionService extends IService<CoinLockPosition> {
    Long queryCoinStrategy(long coin_id,
                                       int config_type);
}
