package com.blockeng.service.impl;

import com.blockeng.entity.AccountWhiteList;
import com.blockeng.mapper.AccountWhiteListMapper;
import com.blockeng.service.AccountWhiteListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class AccountWhiteListServiceImpl extends ServiceImpl<AccountWhiteListMapper, AccountWhiteList> implements AccountWhiteListService {

}
