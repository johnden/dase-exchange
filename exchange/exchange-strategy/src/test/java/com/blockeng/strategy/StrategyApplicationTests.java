package com.blockeng.strategy;

import com.blockeng.strategy.enums.BusinessType;
import com.blockeng.strategy.enums.StrategyType;
import com.blockeng.strategy.service.AccountDetailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author hugo
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategyApplicationTests {
    @Autowired
    private AccountDetailService accountDetailService;

    @Test
    public void getInviteList() {
        List<Long> list = accountDetailService.getAmountByBusinessType(BusinessType.RECHARGE.getCode());
        if (!StringUtils.isEmpty(list)) {

        }
    }
}
