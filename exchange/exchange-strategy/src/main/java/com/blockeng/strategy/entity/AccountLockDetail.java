package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("account_lock_detail")
public class AccountLockDetail extends Model<AccountLockDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 累计锁仓数量
     */
    @TableField("total_lock_amount")
    private BigDecimal totalLockAmount;
    /**
     * 待解锁数量
     */
    @TableField("wait_lock_amount")
    private BigDecimal waitLockAmount;
    /**
     * 每天最多解锁数量
     */
    @TableField("unlock_day_amount")
    private BigDecimal unlockDayAmount;

    public AccountLockDetail(Long id,Long userId,Long coinId,BigDecimal totalLockAmount,BigDecimal waitLockAmount,BigDecimal unlockDayAmount)
    {
        this.id = id;
        this.userId = userId;
        this.coinId = coinId;
        this.totalLockAmount = totalLockAmount;
        this.waitLockAmount = waitLockAmount;
        this.unlockDayAmount = unlockDayAmount;
    }


    public AccountLockDetail(Long userId,Long coinId,BigDecimal totalLockAmount,BigDecimal waitLockAmount,BigDecimal unlockDayAmount)
    {
        this.userId = userId;
        this.coinId = coinId;
        this.totalLockAmount = totalLockAmount;
        this.waitLockAmount = waitLockAmount;
        this.unlockDayAmount = unlockDayAmount;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
