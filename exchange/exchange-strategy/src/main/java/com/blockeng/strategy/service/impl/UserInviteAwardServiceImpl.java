package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.UserInviteAward;
import com.blockeng.strategy.mapper.UserInviteAwardMapper;
import com.blockeng.strategy.service.UserInviteAwardService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
@Service
public class UserInviteAwardServiceImpl extends ServiceImpl<UserInviteAwardMapper, UserInviteAward> implements UserInviteAwardService {
    /**
     * 查询奖励总额
     * @param userId
     * @return
     */
    public BigDecimal getInviteAwardCount(Long userId)
    {
        return baseMapper.getInviteAwardCount(userId);
    }
}
