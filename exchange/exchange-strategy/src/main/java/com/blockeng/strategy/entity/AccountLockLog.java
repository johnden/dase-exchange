package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@Accessors(chain = true)
@TableName("account_lock_log")
public class AccountLockLog extends Model<AccountLockLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 解锁交易编号
     */
    @TableField("turnover_order_id")
    private Long turnoverOrderId;
    /**
     * 本次解锁数量
     */
    @TableField("unlock_amount")
    private BigDecimal unlockAmount;
    /**
     * 解锁时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
