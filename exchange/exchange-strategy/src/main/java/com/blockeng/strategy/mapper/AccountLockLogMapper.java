package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.AccountLockLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */

@Mapper
public interface AccountLockLogMapper extends BaseMapper<AccountLockLog> {

    /**
     * 查询当天解锁数量
     *
     * @param createTime 今天时间
     * @param userId     用户编号
     * @param coinId     币编号
     * @return
     */
    BigDecimal selectDayAmount(@Param("create_time") String createTime,@Param("user_id") Long userId,@Param("coin_id") Long coinId);

}
