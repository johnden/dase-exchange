package com.blockeng.strategy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_invite_statistics")
public class UserInviteStatistics extends Model<UserInviteStatistics> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 邀请总人数
     */
    @TableField("invite_number")
    private Integer inviteNumber;
    /**
     * 邀请交易人数
     */
    @TableField("invite_trade")
    private Integer inviteTrade;
    /**
     * 完成裂变的人数
     */
    @TableField("invite_trade_task")
    private Integer inviteTradeTask;
    /**
     * 裂变级别
     */
    @TableField("invite_level")
    private Integer inviteLevel;
    /**
     * 用户编号
     */
    @TableField("user_id")
    private Long userId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
