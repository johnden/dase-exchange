package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.UserInviteStatistics;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserInviteStatisticsService extends IService<UserInviteStatistics> {

}
