package com.blockeng.strategy.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("coin_mock_invest")
public class CoinMockInvest extends Model<CoinMockInvest> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 时间
     */
    private LocalDateTime created;
    /**
     * 投资类型
     */
    @TableField("invest_id")
    private Long investId;
    /**
     * 空投总量
     */
    private BigDecimal amount;




    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
