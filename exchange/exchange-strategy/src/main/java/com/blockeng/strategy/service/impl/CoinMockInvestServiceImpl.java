package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.CoinMockInvest;
import com.blockeng.strategy.mapper.CoinMockInvestMapper;
import com.blockeng.strategy.service.CoinMockInvestService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
@Service
public class CoinMockInvestServiceImpl extends ServiceImpl<CoinMockInvestMapper, CoinMockInvest> implements CoinMockInvestService {
    public List<CoinMockInvest> queryInvestByInvestType(int investType)
    {
        return  baseMapper.queryInvestByInvestType(investType);
    }
}
