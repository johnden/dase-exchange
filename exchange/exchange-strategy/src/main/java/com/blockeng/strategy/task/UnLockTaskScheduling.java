package com.blockeng.strategy.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.blockeng.strategy.entity.*;
import com.blockeng.strategy.enums.BusinessType;
import com.blockeng.strategy.enums.StrategyType;
import com.blockeng.strategy.service.*;
import com.blockeng.strategy.util.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月02日
 * @修改记录
 */
@Component
public class UnLockTaskScheduling {

    private static final Logger logger = LoggerFactory.getLogger(UnLockTaskScheduling.class);
    private Calendar calendar = Calendar.getInstance();

    @Autowired
    private TurnoverOrderService turnoverOrderService;

    @Autowired
    private AccountLockDetailService accountLockDetailService;

    @Autowired
    private AccountLockLogService accountLockLogService;

    @Autowired
    private MarketService marketService;

    @Autowired
    private CoinLockPositionService coinLockPositionService;

    @Autowired
    private CoinStrategyConfigService coinStrategyConfigService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountWhiteListService accountWhiteListService;

    /**
     * 每隔10秒执行一次解仓操作
     */
    @Scheduled(cron = "0/10 * * * * ?")
    public void StrategyTask() {
        logger.info("******解仓开始时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));

        //查询目前处理过的最大的订单Id
        QueryWrapper<AccountLockLog> accountLockLogWrapper = new QueryWrapper<>();
        accountLockLogWrapper.orderByDesc("turnover_order_id");
        AccountLockLog accountLockLog = accountLockLogService.selectOne(accountLockLogWrapper);
        Long beginId = 1097738613086654520L;
        if (!StringUtils.isEmpty(accountLockLog)) {
            beginId = accountLockLog.getTurnoverOrderId() + 1;
        }
        QueryWrapper<TurnoverOrder> turnoverOrderWrapper = new QueryWrapper<>();
        turnoverOrderWrapper.orderByAsc("id");
        turnoverOrderWrapper.between("id", beginId, beginId + 20);
        List<TurnoverOrder> turnoverOrderList = turnoverOrderService.selectList(turnoverOrderWrapper);

        for (TurnoverOrder turnoverOrder : turnoverOrderList) {

            Long marketId = turnoverOrder.getMarketId();
            Long userId = turnoverOrder.getSellUserId();
            Long buyUserId = turnoverOrder.getBuyUserId();
            //查询币编号
            QueryWrapper<Market> marketWrapper = new QueryWrapper<>();
            marketWrapper.eq("id", marketId);
            Market market = marketService.selectOne(marketWrapper);
            Long coinId = market.getSellCoinId();

            List<CoinLockPosition> coinLockPositions = getCoinLockPosition(coinId);

            //查询卖用户是否在白名单中
            QueryWrapper<AccountWhiteList> whiteListWrapper = new QueryWrapper<>();
            whiteListWrapper.eq("user_id", userId);
            AccountWhiteList accountWhiteList = accountWhiteListService.selectOne(whiteListWrapper);

            //查询买用户是否在白名单中
            QueryWrapper<AccountWhiteList> whiteBuyListWrapper = new QueryWrapper<>();
            whiteBuyListWrapper.eq("user_id", buyUserId);
            AccountWhiteList accountWhiteBuyList = accountWhiteListService.selectOne(whiteBuyListWrapper);

            //判断是卖用户和币种是否在策略中
            for (CoinLockPosition coinLockPosition : coinLockPositions) {
                //卖用户白名单
                if (StringUtils.isEmpty(accountWhiteList)) {
                    //查询策略配置
                    Long strategyId = Long.parseLong(coinLockPosition.getStrategyId());
                    CoinStrategyConfig coinStrategyConfig = getCoinStrategyConfig(strategyId);
                    if (!StringUtils.isEmpty(coinStrategyConfig)) {
                        Integer configType = coinStrategyConfig.getConfigType();
                        // 类型为5的策略:卖解锁交易0.004策略
                        // 类型为3的策略:卖解锁0.01
                        if (configType == StrategyType.UNLOCK_ONE_FOUR.getCode() || configType == StrategyType.UNLOCK_ONE.getCode() || configType == StrategyType.UNLOCK_ONE_ONE.getCode()) {
                            //查询解仓信息
                            AccountLockDetail accountLockDetail = getAccountLockDetail(userId, coinId);
                            if (!StringUtils.isEmpty(accountLockDetail)) {
                                //待解锁量
                                BigDecimal waitLockAmount = accountLockDetail.getWaitLockAmount();

                                //计算今天已经解锁数量
                                calendar.setTime(new Date());
                                calendar.set(Calendar.HOUR_OF_DAY, 0);
                                calendar.set(Calendar.MINUTE, 0);
                                calendar.set(Calendar.SECOND, 0);
                                Date start = calendar.getTime();
                                String todayDate = DateTimeUtils.convertLongToDate(start.getTime());
                                BigDecimal todayLockAmount = accountLockLogService.selectDayAmount(todayDate, userId, coinId);
                                logger.info("******unlocked today:{}", todayLockAmount);

                                //每天最大解锁数量
                                BigDecimal unlockDayAmount = accountLockDetail.getUnlockDayAmount();
                                unlockDayAmount = unlockDayAmount.setScale(8, RoundingMode.HALF_UP);
                                BigDecimal currentUnlockAmount = computeSellStrategy(StrategyType.getByCode(configType), turnoverOrder);
                                processLockAmount(userId, coinId, currentUnlockAmount, unlockDayAmount, todayLockAmount, accountLockDetail, waitLockAmount, turnoverOrder);
                            }
                        }
                    }
                }

                //买用户白名单
                if (StringUtils.isEmpty(accountWhiteBuyList)) {
                    //查询策略配置
                    Long strategyId = Long.parseLong(coinLockPosition.getStrategyId());
                    CoinStrategyConfig coinStrategyConfig = getCoinStrategyConfig(strategyId);
                    if (!StringUtils.isEmpty(coinStrategyConfig)) {
                        Integer configType = coinStrategyConfig.getConfigType();
                        // 类型为5的策略:买解锁交易0.001策略
                        // 类型为3的策略:买卖解锁0.01
                        if (configType == StrategyType.UNLOCK_ONE_FOUR.getCode() || configType == StrategyType.UNLOCK_ONE.getCode() || configType == StrategyType.UNLOCK_ONE_ONE.getCode()) {
                            //查询解仓信息
                            AccountLockDetail accountLockDetail = getAccountLockDetail(buyUserId, coinId);
                            if (!StringUtils.isEmpty(accountLockDetail)) {
                                //待解锁量
                                BigDecimal waitLockAmount = accountLockDetail.getWaitLockAmount();
                                //计算今天已经解锁数量
                                calendar.setTime(new Date());
                                calendar.set(Calendar.HOUR_OF_DAY, 0);
                                calendar.set(Calendar.MINUTE, 0);
                                calendar.set(Calendar.SECOND, 0);
                                Date start = calendar.getTime();
                                String todayDate = DateTimeUtils.convertLongToDate(start.getTime());
                                BigDecimal todayLockAmount = accountLockLogService.selectDayAmount(todayDate, buyUserId, coinId);
                                logger.info("******unlocked today:{}", todayLockAmount);

                                //每天最大解锁数量
                                BigDecimal unlockDayAmount = accountLockDetail.getUnlockDayAmount();
                                unlockDayAmount = unlockDayAmount.setScale(8, RoundingMode.HALF_UP);
                                BigDecimal currentUnlockAmount = computeBuyStrategy(StrategyType.getByCode(configType), turnoverOrder);
                                processLockAmount(buyUserId, coinId, currentUnlockAmount, unlockDayAmount, todayLockAmount, accountLockDetail, waitLockAmount, turnoverOrder);
                            }
                        }
                    }
                }
            }
            logger.info("******解仓结束时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        }
    }


    /**
     * 对用户执行解仓操作
     *
     * @param userId
     * @param coinId
     * @param currentUnlockAmount
     * @param unlockDayAmount
     * @param todayLockAmount
     * @param accountLockDetail
     * @param waitLockAmount
     * @param turnoverOrder
     */
    public void processLockAmount(Long userId, Long coinId, BigDecimal currentUnlockAmount, BigDecimal
            unlockDayAmount,
                                  BigDecimal todayLockAmount, AccountLockDetail accountLockDetail, BigDecimal
                                          waitLockAmount, TurnoverOrder turnoverOrder) {
        //应解锁数量必须大于0才操作
        if (currentUnlockAmount.compareTo(BigDecimal.ZERO) > 0) {
            logger.info("******should unlock num:{}", currentUnlockAmount);
            //今天还能解锁数量=今天最大解锁数量-今天已解锁数量
            BigDecimal canLockAmount = unlockDayAmount.subtract(todayLockAmount);
            canLockAmount = canLockAmount.setScale(8, RoundingMode.HALF_UP);
            BigDecimal unLockAmount;

            //当前解锁数量>今天能解锁数量，则取今天能解锁数量
            if (currentUnlockAmount.compareTo(canLockAmount) > 0) {
                unLockAmount = canLockAmount;
                logger.info("******reach the gap, in fact unLockAmount is:{}", unLockAmount);
            } else {
                unLockAmount = currentUnlockAmount;
                logger.info("******Not Reach the gap，in fact unlockAmount is:{}", unLockAmount);
            }
            unLockAmount = unLockAmount.setScale(8, RoundingMode.HALF_UP);
            //解仓操作
            accountService.addAmount(userId,
                    coinId,
                    unLockAmount,
                    BusinessType.UNLOCK_POSITION,
                    BusinessType.UNLOCK_POSITION.getDesc(),
                    System.currentTimeMillis());

            //更新解仓数量
            UpdateWrapper<AccountLockDetail> accountLockDetailWrapper = new UpdateWrapper<>();
            accountLockDetailWrapper.eq("user_id", userId).and(wrapper -> wrapper.eq("coin_id", coinId));
            accountLockDetail.setWaitLockAmount(waitLockAmount.subtract(unLockAmount).setScale(8, RoundingMode.HALF_UP));
            accountLockDetailService.update(accountLockDetail, accountLockDetailWrapper);

            //写解仓日志
            AccountLockLog accountLockLog = new AccountLockLog();
            accountLockLog.setUserId(userId);
            accountLockLog.setCoinId(coinId);
            accountLockLog.setTurnoverOrderId(turnoverOrder.getId());
            accountLockLog.setUnlockAmount(unLockAmount);
            insertAccountLockLog(accountLockLog);
            logger.info("******finished,unLockAmount is:{}", unLockAmount);
        }
    }

    /**
     * 针对卖用户执行计算配置金额策略
     * 执行价格对比
     *
     * @param strategyType
     * @param turnoverOrder
     * @return
     */
    public BigDecimal computeSellStrategy(StrategyType strategyType, TurnoverOrder turnoverOrder) {
        BigDecimal unLockAmount = BigDecimal.ZERO;
        Long sellUserId = turnoverOrder.getSellUserId();
        Long orderId = turnoverOrder.getId();
        BigDecimal price = turnoverOrder.getPrice();
        BigDecimal volume = turnoverOrder.getVolume();
        List<TurnoverOrder> turnoverOrders = getTurnoverOrder(orderId, sellUserId);
        switch (strategyType) {
            case UNLOCK_ONE_FOUR:
                //买解锁交易1‰策略,卖解锁交易4‰策略
                logger.info("sell price is :{}", price);
                for (TurnoverOrder turnoverOrderOne : turnoverOrders) {
                    BigDecimal turnoverOnePrice = turnoverOrderOne.getPrice();
                    logger.info("last buy price is :{}", turnoverOnePrice);
                    //卖价>买价
                    if (price.compareTo(turnoverOnePrice) > 0) {
                        unLockAmount = volume.multiply(new BigDecimal(Double.toString(0.004)));
                        logger.info("sell user strategy 0.004 is :{}", unLockAmount);
                        break;
                    }
                }
                break;
            case UNLOCK_ONE:
                //买卖解锁1%策略
                logger.info("sell price is :{}", price);
                for (TurnoverOrder turnoverOrderOne : turnoverOrders) {
                    BigDecimal turnoverOnePrice = turnoverOrderOne.getPrice();
                    logger.info("last buy price is :{}", turnoverOnePrice);
                    //卖价>买价
                    if (price.compareTo(turnoverOnePrice) > 0) {
                        unLockAmount = volume.multiply(new BigDecimal(Double.toString(0.01)));
                        unLockAmount = unLockAmount.add(unLockAmount);
                        logger.info("sell user strategy 0.01 is :{}", unLockAmount);
                        break;
                    }
                }
                break;
            case UNLOCK_ONE_ONE:
                //买解锁买入量的1%,卖解锁卖出量的1%策略
                logger.info("sell price is :{}", price);
                for (TurnoverOrder turnoverOrderOne : turnoverOrders) {
                    BigDecimal turnoverOnePrice = turnoverOrderOne.getPrice();
                    logger.info("last buy price is :{}", turnoverOnePrice);
                    //卖价>买价
                    if (price.compareTo(turnoverOnePrice) > 0) {
                        unLockAmount = volume.multiply(new BigDecimal(Double.toString(0.01)));
                        logger.info("sell user strategy 0.01 is :{}", unLockAmount);
                        break;
                    }
                }
                break;

        }
        unLockAmount = unLockAmount.setScale(8, RoundingMode.HALF_UP);
        return unLockAmount;
    }

    /**
     * 针对买用户执行计算配置金额策略
     * 不用执行价格对比
     *
     * @param strategyType
     * @param turnoverOrder
     * @return
     */
    public BigDecimal computeBuyStrategy(StrategyType strategyType, TurnoverOrder turnoverOrder) {
        BigDecimal unLockAmount = BigDecimal.ZERO;
        BigDecimal volume = turnoverOrder.getVolume();
        switch (strategyType) {
            case UNLOCK_ONE_FOUR:
                //买解锁交易1‰策略,卖解锁交易4‰策略
                unLockAmount = volume.multiply(new BigDecimal(Double.toString(0.001)));
                logger.info("buy user strategy 0.001 is :{}", unLockAmount);
                break;
            case UNLOCK_ONE:
                //买卖解锁1%策略

                break;
            case UNLOCK_ONE_ONE:
                //买解锁买入量的1%,卖解锁卖出量的1%策略
                unLockAmount = volume.multiply(new BigDecimal(Double.toString(0.01)));
                logger.info("buy user strategy 0.01 is :{}", unLockAmount);
                break;

        }
        unLockAmount = unLockAmount.setScale(8, RoundingMode.HALF_UP);
        return unLockAmount;
    }

    /**
     * 查询用户上一次买单的数据
     *
     * @param orderId
     * @param sellUserId
     * @return
     */
    private List<TurnoverOrder> getTurnoverOrder(Long orderId, Long sellUserId) {
        QueryWrapper<TurnoverOrder> turnoverOrderWrapper = new QueryWrapper<>();
        turnoverOrderWrapper.lt("id", orderId).and(wrapper -> wrapper.eq("buy_user_id", sellUserId));
        turnoverOrderWrapper.orderByDesc("id");
        List<TurnoverOrder> turnoverOrders = turnoverOrderService.selectList(turnoverOrderWrapper);
        //add by Jack
        logger.info("getTurnoverOrder,orderId:{},sellUserId:{}", orderId, sellUserId);
        return turnoverOrders;
    }

    /**
     * 查询策略配置
     *
     * @param strategyId
     * @return
     */
    private CoinStrategyConfig getCoinStrategyConfig(Long strategyId) {
        QueryWrapper<CoinStrategyConfig> coinStrategyConfigWrapper = new QueryWrapper<>();
        coinStrategyConfigWrapper.eq("strategy_id", strategyId);
        CoinStrategyConfig coinStrategyConfig = coinStrategyConfigService.selectOne(coinStrategyConfigWrapper);
        //add by Jack
        logger.info("getCoinStrategyConfig，strategyId:{}", strategyId);
        return coinStrategyConfig;
    }

    /**
     * 查询锁仓配置
     *
     * @param coinId
     * @return
     */
    public List<CoinLockPosition> getCoinLockPosition(Long coinId) {
        QueryWrapper<CoinLockPosition> coinLockPositionWrapper = new QueryWrapper<>();
        coinLockPositionWrapper.eq("coin_id", coinId);
        logger.info("getCoinLockPosition，coinId:{}", coinId);
        List<CoinLockPosition> coinLockPositions = coinLockPositionService.selectList(coinLockPositionWrapper);
        return coinLockPositions;
    }

    /**
     * 查询用户锁仓信息
     *
     * @param userId
     * @param coinId
     * @return
     */
    public AccountLockDetail getAccountLockDetail(Long userId, Long coinId) {
        QueryWrapper<AccountLockDetail> accountLockDetailWrapper = new QueryWrapper<>();
        accountLockDetailWrapper.eq("user_id", userId).and(wrapper -> wrapper.eq("coin_id", coinId));
        AccountLockDetail accountLockDetail = accountLockDetailService.selectOne(accountLockDetailWrapper);
        //add by Jack
        logger.info("getAccountLockDetail,userId:{},coinId:{}", userId, coinId);
        return accountLockDetail;
    }

    /**
     * 插入解锁日志
     *
     * @param accountLockLog
     * @return
     */
    public boolean insertAccountLockLog(AccountLockLog accountLockLog) {
        //add by Jack
        logger.info("insertAccountLockLog");
        return accountLockLogService.insert(accountLockLog);
    }
}
