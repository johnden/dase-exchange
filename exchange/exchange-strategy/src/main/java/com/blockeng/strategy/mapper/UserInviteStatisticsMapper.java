package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.UserInviteStatistics;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
public interface UserInviteStatisticsMapper extends BaseMapper<UserInviteStatistics> {

}
