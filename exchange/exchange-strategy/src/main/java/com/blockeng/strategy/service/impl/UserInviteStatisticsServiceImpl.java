package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.UserInviteStatistics;
import com.blockeng.strategy.mapper.UserInviteStatisticsMapper;
import com.blockeng.strategy.service.UserInviteStatisticsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-08
 */
@Service
public class UserInviteStatisticsServiceImpl extends ServiceImpl<UserInviteStatisticsMapper, UserInviteStatistics> implements UserInviteStatisticsService {

}
