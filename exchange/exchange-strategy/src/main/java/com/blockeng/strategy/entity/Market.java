package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 交易对配置信息
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Market extends Model<Market> {

    private static final long serialVersionUID = 1L;

    /**
     * 市场ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 类型：1-数字货币；2：创新交易
     */
    private Integer type;
    /**
     * 交易区域ID
     */
    @TableField("trade_area_id")
    private Long tradeAreaId;
    /**
     * 卖方市场ID
     */
    @TableField("sell_coin_id")
    private Long sellCoinId;
    /**
     * 买方币种ID
     */
    @TableField("buy_coin_id")
    private Long buyCoinId;
    /**
     * 交易对标识
     */
    private String symbol;
    /**
     * 名称
     */
    private String name;
    /**
     * 标题
     */
    private String title;
    /**
     * 市场logo
     */
    private String img;
    /**
     * 开盘价格
     */
    @TableField("open_price")
    private BigDecimal openPrice;
    /**
     * 买入手续费率
     */
    @TableField("fee_buy")
    private BigDecimal feeBuy;
    /**
     * 卖出手续费率
     */
    @TableField("fee_sell")
    private BigDecimal feeSell;
    /**
     * 保证金占用比例
     */
    @TableField("margin_rate")
    private BigDecimal marginRate;
    /**
     * 单笔最小委托量
     */
    @TableField("num_min")
    private BigDecimal numMin;
    /**
     * 单笔最大委托量
     */
    @TableField("num_max")
    private BigDecimal numMax;
    /**
     * 单笔最小成交额
     */
    @TableField("trade_min")
    private BigDecimal tradeMin;
    /**
     * 单笔最大成交额
     */
    @TableField("trade_max")
    private BigDecimal tradeMax;
    /**
     * 价格小数位
     */
    @TableField("price_scale")
    private Integer priceScale;
    /**
     * 数量小数位
     */
    @TableField("num_scale")
    private Integer numScale;
    /**
     * 合约单位
     */
    @TableField("contract_unit")
    private Integer contractUnit;
    /**
     * 点
     */
    @TableField("point_value")
    private BigDecimal pointValue;
    /**
     * 合并深度（格式：4,3,2）4:表示为0.0001；3：表示为0.001
     */
    @TableField("merge_depth")
    private String mergeDepth;
    /**
     * 交易时间
     */
    @TableField("trade_time")
    private String tradeTime;
    /**
     * 交易周期
     */
    @TableField("trade_week")
    private String tradeWeek;
    /**
     * 排序列
     */
    private Integer sort;
    /**
     * 状态
0禁用
1启用
     */
    private Boolean status;
    /**
     * 福汇API交易对
     */
    @TableField("fxcm_symbol")
    private String fxcmSymbol;
    /**
     * 对应雅虎金融API交易对
     */
    @TableField("yahoo_symbol")
    private String yahooSymbol;
    /**
     * 对应阿里云API交易对
     */
    @TableField("aliyun_symbol")
    private String aliyunSymbol;
    /**
     * 更新时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
