package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.UserInviteStatus;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-09
 */
public interface UserInviteStatusMapper extends BaseMapper<UserInviteStatus> {
    /**
     * 更新记录状态
     *
     * @param id         记录ID
     * @param dealStatus 处理状态
     * @return
     */
    int updateDealStatus(@Param("deal_status") int dealStatus,
                         @Param("id")
                                 long id);
}
