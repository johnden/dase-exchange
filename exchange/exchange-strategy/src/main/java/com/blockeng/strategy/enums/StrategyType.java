package com.blockeng.strategy.enums;


/**
 * @Description: 策略类型
 */
public enum StrategyType {

    LIMIT_PRICE(10, "涨停策略"),
    LOCK_BALANCE(20, "锁仓策略"),
    UNLOCK_ONE_FOUR(30, "买解锁交易1‰策略,卖解锁交易4‰策略"),
    UNLOCK_ONE(31, "买卖解锁1%策略"),
    UNLOCK_ONE_ONE(32, "买解锁买入量的1%,卖解锁卖出量的1%策略"),
    WITHDRAW_AMOUNT(40, "提币策略,锁仓币可提");

    /**
     *
     */
    private int code;

    /**
     * 标识
     */
    private String desc;

    StrategyType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static StrategyType getByCode(int code) {
        for (StrategyType type : StrategyType.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}