package com.blockeng.strategy.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月04日
 * @修改记录
 */
@Configuration
@MapperScan("com.blockeng.strategy.mapper")
public class MybatisPlusConfig {
}
