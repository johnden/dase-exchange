package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 资金账户流水
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("account_detail")
public class AccountDetail extends Model<AccountDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 账户id
     */
    @TableField("account_id")
    private Long accountId;
    /**
     * 关联方的账户ID
     */
    @TableField("ref_account_id")
    private Long refAccountId;
    /**
     * 订单ID
     */
    @TableField("order_id")
    private Long orderId;
    /**
     * 资金方向：入账为-1；出账为-2
     */
    private Integer direction;
    /**
     * 业务类型
     */
    @TableField("business_type")
    private String businessType;
    /**
     * 数量/金额
     */
    private BigDecimal amount;
    /**
     * 手续费
     */
    private BigDecimal fee;
    /**
     * 备注
     */
    private String remark;
    /**
     * 时间
     */
    private LocalDateTime created;

    public AccountDetail(){}

    public AccountDetail(Long userId,
                         Long coinId,
                         Long accountId,
                         Long refAccountId,
                         Long orderId,
                         Integer direction,
                         String businessType,
                         BigDecimal amount,
                         String remark) {
        this.userId = userId;
        this.coinId = coinId;
        this.accountId = accountId;
        this.refAccountId = refAccountId;
        this.orderId = orderId;
        this.direction = direction;
        this.businessType = businessType;
        this.amount = amount;
        this.remark = remark;
    }
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
