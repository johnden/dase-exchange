package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.UserInviteStatus;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-09
 */
public interface UserInviteStatusService extends IService<UserInviteStatus> {
    /**
     * 更新记录状态
     *
     * @param id 记录ID
     * @param dealStatus 处理状态
     * @return
     */
    int updateDealStatus(int dealStatus, long id);
}
