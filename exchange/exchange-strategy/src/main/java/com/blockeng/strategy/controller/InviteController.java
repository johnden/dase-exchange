package com.blockeng.strategy.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.strategy.entity.UserInviteAward;
import com.blockeng.strategy.entity.UserInviteStatistics;
import com.blockeng.strategy.response.ResponseCode;
import com.blockeng.strategy.response.ResponseResultUtil;
import com.blockeng.strategy.service.UserInviteAwardService;
import com.blockeng.strategy.service.UserInviteStatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * 邀请管理控制类
 * add by hugo
 * 2018年5月14日18:31:05
 */
@RestController
@RequestMapping("/invite")
@Slf4j
@Api(value = "用户邀请-邀请管理", description = "用户邀请-邀请管理")
public class InviteController {

    @Autowired
    private UserInviteAwardService userInviteAwardService;

    @Autowired
    private UserInviteStatisticsService userInviteStatisticsService;

    /**
     * 获取邀请奖励
     *
     * @return
     */
    @GetMapping(value = "/getInviteAward")
    @ApiOperation(value = "获取邀请奖励", notes = "奖励账户", httpMethod = "GET")
    public Object getInviteAward(@RequestParam("userId") Long userId) {
        QueryWrapper<UserInviteAward> userInviteAwardWrapper = new QueryWrapper<>();
        userInviteAwardWrapper.eq("user_id", userId);
        List<UserInviteAward> userInviteAwards = userInviteAwardService.selectList(userInviteAwardWrapper);
        if (!StringUtils.isEmpty(userInviteAwards)) {
            return new ResponseResultUtil().success(userInviteAwards);
        }
        return new ResponseResultUtil().error(ResponseCode.OPERATE_FAIL);
    }

    /**
     * 获取邀请统计
     *
     * @return
     */
    @GetMapping(value = "/getInviteStatistics")
    @ApiOperation(value = "获取邀请统计", notes = "奖励账户", httpMethod = "GET")
    public Object getInviteStatistics(@RequestParam("userId") Long userId) {
        QueryWrapper<UserInviteStatistics> inviteStatisticsWrapper = new QueryWrapper<>();
        inviteStatisticsWrapper.eq("user_id", userId);
        List<UserInviteStatistics> userInviteStatistics = userInviteStatisticsService.selectList(inviteStatisticsWrapper);
        if (!StringUtils.isEmpty(userInviteStatistics)) {
            return new ResponseResultUtil().success(userInviteStatistics);
        }
        return new ResponseResultUtil().error(ResponseCode.OPERATE_FAIL);
    }

    /**
     * 获取奖励总额
     *
     * @return
     */
    @GetMapping(value = "/getInviteAwardCount")
    @ApiOperation(value = "获取奖励总额", notes = "奖励账户", httpMethod = "GET")
    public Object getInviteAwardCount(@RequestParam("userId") Long userId) {
        BigDecimal totalAmount = userInviteAwardService.getInviteAwardCount(userId);
        totalAmount = totalAmount.setScale(8, RoundingMode.HALF_UP);
        return new ResponseResultUtil().success(totalAmount);
    }
}
