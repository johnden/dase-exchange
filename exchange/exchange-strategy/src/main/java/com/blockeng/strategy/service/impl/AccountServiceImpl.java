package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.*;
import com.blockeng.strategy.enums.AmountDirection;
import com.blockeng.strategy.enums.BaseStatus;
import com.blockeng.strategy.enums.BusinessType;
import com.blockeng.strategy.enums.StrategyType;
import com.blockeng.strategy.exception.AccountException;
import com.blockeng.strategy.mapper.AccountMapper;
import com.blockeng.strategy.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 资金账户 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
@Slf4j
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Autowired
    private AccountDetailService accountDetailService;

    @Autowired
    private CoinLockPositionService coinLockPositionService;


    @Autowired
    private AccountWhiteListService accountWhiteListService;


    @Autowired
    private AccountLockDetailService accountLockDetailService;


    @Autowired
    private CoinStrategyConfigService coinStrategyConfigService;

    @Autowired
    private CoinPriceService coinPriceService;


    /**
     * 增加资金
     *
     * @param userId       用户ID
     * @param coinId       币种ID
     * @param amount       冻结金额
     * @param businessType 业务类型
     * @param remark       备注
     * @param orderId      关联订单号
     * @return
     */
    @Override
    public boolean addAmount(long userId,
                             long coinId,
                             BigDecimal amount,
                             BusinessType businessType,
                             String remark,
                             long orderId) throws AccountException {
        Account account = this.queryByUserIdAndCoinId(userId, coinId);
        if (account == null) {
            log.error("增加资金-资金账户异常，userId:{}, coinId:{}", userId, coinId);
            throw new AccountException("资金账户异常");
        }
        if (baseMapper.addAmount(account.getId(), amount) > 0) {
            // 保存流水
            AccountDetail accountDetail = new AccountDetail(userId,
                    coinId,
                    account.getId(),
                    account.getId(),
                    orderId,
                    AmountDirection.INCOME.getType(),
                    businessType.getCode(),
                    amount,
                    remark);
            accountDetailService.insert(accountDetail);
            return true;
        }
        log.error("增加资金失败，orderId:{}, userId:{}, coinId:{}, amount:{}, businessType:{}",
                orderId, userId, coinId, amount, businessType.getCode());
        throw new AccountException("增加资金失败");
    }

    /**
     * 根据用户ID和币种名称查询资金账户
     *
     * @param userId 用户ID
     * @param coinId 币种ID
     * @return
     */
    @Override
    public Account queryByUserIdAndCoinId(long userId, long coinId) {
        return queryByUserIdAndCoinId(userId, coinId, false);
    }

    /**
     * 锁仓账户资金
     * add by hugo
     *
     * @param userId       用户ID
     * @param coinId       币种ID
     * @param amount       冻结金额
     * @param businessType 业务类型
     * @param remark       备注
     * @param orderId      关联订单号
     * @return
     */
    @Override
    public boolean lockPositionAmount(long userId,
                                      long coinId,
                                      BigDecimal amount,
                                      BusinessType businessType,
                                      String remark,
                                      long orderId) throws AccountException {
        Account account = this.queryByUserIdAndCoinId(userId, coinId);
        if (account == null) {
            log.error("扣减资金-资金账户异常，userId:{}, coinId:{}", userId, coinId);
            throw new AccountException("资金账户异常");
        }

        //查询币种是否在锁仓策略里
        QueryWrapper<CoinLockPosition> coinLockWrapper = new QueryWrapper<>();
        coinLockWrapper.eq("coin_id", coinId);
        List<CoinLockPosition> coinLockPositions = coinLockPositionService.selectList(coinLockWrapper);

        //查询用户是否在白名单中
        QueryWrapper<AccountWhiteList> whiteListWrapper = new QueryWrapper<>();
        whiteListWrapper.eq("user_id", userId);
        AccountWhiteList accountWhiteList = accountWhiteListService.selectOne(whiteListWrapper);

        //查询币种是否已经记录在详情表里
        QueryWrapper<AccountLockDetail> accountLockDetailWrapper = new QueryWrapper<>();
        accountLockDetailWrapper.eq("coin_id", coinId).and(wrapper -> wrapper.eq("user_id", userId));
        AccountLockDetail accountLockDetail = accountLockDetailService.selectOne(accountLockDetailWrapper);


        //减去锁仓金额
        if (baseMapper.lockPositionAmount(account.getId(), amount) > 0) {
            // 保存流水
            AccountDetail accountDetail = new AccountDetail(userId,
                    coinId,
                    account.getId(),
                    account.getId(),
                    orderId,
                    AmountDirection.OUT.getType(),
                    businessType.getCode(),
                    amount,
                    remark);
            accountDetailService.insert(accountDetail);
            for (CoinLockPosition coinLockPosition : coinLockPositions) {
                if (StringUtils.isEmpty(accountWhiteList)) {
                    Long strategyId = Long.parseLong(coinLockPosition.getStrategyId());
                    //查询币种是否已经记录在详情表里
                    QueryWrapper<CoinStrategyConfig> coinStrategyWrapper = new QueryWrapper<>();
                    coinStrategyWrapper.eq("strategy_id", strategyId);
                    CoinStrategyConfig coinStrategyConfig = coinStrategyConfigService.selectOne(coinStrategyWrapper);
                    if (!StringUtils.isEmpty(coinStrategyConfig)) {
                        int configType = coinStrategyConfig.getConfigType();
                        if (configType == StrategyType.LOCK_BALANCE.getCode()) {
                            BigDecimal configValue = coinStrategyConfig.getConfigValue();
                            BigDecimal totalLockAmount = amount;
                            BigDecimal waitLockAmount = amount;
                            BigDecimal unlockDayAmount = configValue.multiply(totalLockAmount);

                            if (StringUtils.isEmpty(accountLockDetail)) {
                                accountLockDetail = new AccountLockDetail(userId, coinId, totalLockAmount, waitLockAmount, unlockDayAmount);
                                accountLockDetailService.insert(accountLockDetail);
                            } else {
                                totalLockAmount = accountLockDetail.getTotalLockAmount().add(amount);
                                unlockDayAmount = configValue.multiply(totalLockAmount);
                                waitLockAmount = accountLockDetail.getWaitLockAmount().add(amount);
                                accountLockDetail.setTotalLockAmount(totalLockAmount);
                                accountLockDetail.setWaitLockAmount(waitLockAmount);
                                accountLockDetail.setUnlockDayAmount(unlockDayAmount);
                                UpdateWrapper<AccountLockDetail> accountLockUpdateWrapper = new UpdateWrapper<>();
                                accountLockUpdateWrapper.eq("coin_id", coinId).and(wrapper -> wrapper.eq("user_id", userId));
                                accountLockDetailService.update(accountLockDetail, accountLockUpdateWrapper);
                            }
                            log.info("用户充值锁仓成功 userId:{}, coinId:{}，totalLockAmount:{},waitLockAmount:{},unlockDayAmount:{}", userId, coinId, totalLockAmount, waitLockAmount, unlockDayAmount);
                            return true;
                        }
                    }
                }
            }
        }
        log.error("扣减资金失败，orderId:{}, userId:{}, coinId:{}, amount:{}, businessType:{}",
                orderId, userId, coinId, amount, businessType.getCode());
        throw new AccountException("扣减资金失败");
    }
    /**
     * 根据用户ID和币种名称查询资金账户
     *
     * @param userId 用户ID
     * @param coinId 币种ID
     * @return
     */
    @Override
    public Account queryByUserIdAndCoinId(long userId, long coinId, boolean containEnable) {
        QueryWrapper<Account> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId)
                .eq("coin_id", coinId);
        if (!containEnable) {
            wrapper.eq("status", BaseStatus.EFFECTIVE.getCode());
        }
        wrapper.last("LIMIT 1");
        return baseMapper.selectOne(wrapper);
    }
}
