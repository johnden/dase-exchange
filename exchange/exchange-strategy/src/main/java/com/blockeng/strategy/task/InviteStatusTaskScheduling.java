package com.blockeng.strategy.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.strategy.entity.TurnoverOrder;
import com.blockeng.strategy.entity.User;
import com.blockeng.strategy.entity.UserInviteStatus;
import com.blockeng.strategy.enums.DealStatus;
import com.blockeng.strategy.service.TurnoverOrderService;
import com.blockeng.strategy.service.UserInviteStatusService;
import com.blockeng.strategy.service.UserService;
import com.blockeng.strategy.util.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月07日
 * @修改记录
 */
@Component
public class InviteStatusTaskScheduling {
    private static final Logger logger = LoggerFactory.getLogger(InviteStatusTaskScheduling.class);
    private Calendar calendar = Calendar.getInstance();
    @Autowired
    private UserService userService;

    @Autowired
    private TurnoverOrderService turnoverOrderService;

    @Autowired
    private UserInviteStatusService userInviteStatusService;

    /**
     * 每日凌晨1点钟执行一次
     */
    @Scheduled(cron = "0 0 1 1/1 * ?")
    public void InviteUserStatusTask() {
        logger.info("计算推荐状态开始执行:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date start = calendar.getTime();
        String endDate = DateTimeUtils.convertLongToDate(start.getTime());
        calendar.add(Calendar.DATE, -1);
        String beginDates = DateTimeUtils.convertLongToDate(calendar.getTimeInMillis());

        //查询昨天所有交易记录
        QueryWrapper<TurnoverOrder> turnoverOrderWrapper = new QueryWrapper<>();
        turnoverOrderWrapper.orderByDesc("id");
        turnoverOrderWrapper.between("created", beginDates, endDate);
        List<TurnoverOrder> turnoverOrders = turnoverOrderService.selectList(turnoverOrderWrapper);
        for (TurnoverOrder turnoverOrder : turnoverOrders) {
            Long sellUserId = turnoverOrder.getSellUserId();
            Long buyUserId = turnoverOrder.getBuyUserId();
            Long orderId = turnoverOrder.getId();

            //成交卖出手续费
            BigDecimal dealSellFee = turnoverOrder.getDealSellFee();
            //成交买入手续费
            BigDecimal dealBuyFee = turnoverOrder.getDealBuyFee();

            //插入卖用户条件数据
            if (!StringUtils.isEmpty(sellUserId)) {
                insertUserInviteCondition(sellUserId, orderId, dealSellFee);
            }
            //插入买用户条件数据
            if (!StringUtils.isEmpty(sellUserId)) {
                insertUserInviteCondition(buyUserId, orderId, dealBuyFee);
            }
        }
        logger.info("计算推荐状态执行完成:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }

    /**
     * 插入用户条件数据
     *
     * @param userId
     * @param orderId
     * @param tradeFee
     */
    public void insertUserInviteCondition(Long userId, Long orderId, BigDecimal tradeFee) {
        UserInviteStatus userInviteStatus = new UserInviteStatus();
        User user = userService.selectById(userId);
        String inviteRelation = user.getInviteRelation();


        //级别条件
        String oneLevelStatus = "0,0";
        String twoLevelStatus = "0,0";
        String threeLevelStatus = "0,0";
        String fourLevelStatus = "0,0";
        //级别返佣
        BigDecimal oneLevelAward = BigDecimal.ZERO;
        BigDecimal twoLevelAward = BigDecimal.ZERO;
        BigDecimal threeLevelAward = BigDecimal.ZERO;
        BigDecimal fourLevelAward = BigDecimal.ZERO;
        if (!StringUtils.isEmpty(inviteRelation)) {
            String[] inviteRelations = inviteRelation.split(",");
            int userStatus = 0;
            //有上级
            if (inviteRelations.length == 1) {
                Long parentUserId = Long.parseLong(inviteRelations[0]);
                userStatus = getOneUserStatusUserId(parentUserId);
                oneLevelStatus = parentUserId + "," + userStatus;
                oneLevelAward = tradeFee.multiply(new BigDecimal(Double.toString(0.3)));
                oneLevelAward = oneLevelAward.setScale(8, RoundingMode.HALF_UP);
                userInviteStatus.setUserLevel(1);
            }
            //有上上级
            if (inviteRelations.length == 2) {
                Long parentUserId = Long.parseLong(inviteRelations[0]);
                userStatus = getOneUserStatusUserId(parentUserId);
                oneLevelStatus = parentUserId + "," + userStatus;
                oneLevelAward = tradeFee.multiply(new BigDecimal(Double.toString(0.3)));
                oneLevelAward = oneLevelAward.setScale(8, RoundingMode.HALF_UP);

                parentUserId = Long.parseLong(inviteRelations[1]);
                userStatus = getTwoUserStatusUserId(parentUserId);
                twoLevelStatus = parentUserId + "," + userStatus;
                twoLevelAward = tradeFee.multiply(new BigDecimal(Double.toString(0.15)));
                twoLevelAward = twoLevelAward.setScale(8, RoundingMode.HALF_UP);
                userInviteStatus.setUserLevel(2);
            }
            //有上上上级
            if (inviteRelations.length == 3) {

                Long parentUserId = Long.parseLong(inviteRelations[0]);
                userStatus = getOneUserStatusUserId(parentUserId);
                oneLevelStatus = parentUserId + "," + userStatus;
                oneLevelAward = tradeFee.multiply(new BigDecimal(Double.toString(0.3)));
                oneLevelAward = oneLevelAward.setScale(8, RoundingMode.HALF_UP);

                parentUserId = Long.parseLong(inviteRelations[1]);
                userStatus = getTwoUserStatusUserId(parentUserId);
                twoLevelStatus = parentUserId + "," + userStatus;
                twoLevelAward = tradeFee.multiply(new BigDecimal(Double.toString(0.15)));
                twoLevelAward = twoLevelAward.setScale(8, RoundingMode.HALF_UP);

                parentUserId = Long.parseLong(inviteRelations[2]);
                userStatus = getThreeUserStatusUserId(parentUserId);
                threeLevelStatus = parentUserId + "," + userStatus;
                threeLevelAward = tradeFee.multiply(new BigDecimal(Double.toString(0.05)));
                threeLevelAward = threeLevelAward.setScale(8, RoundingMode.HALF_UP);

                userInviteStatus.setUserLevel(3);
            }
            if (inviteRelations.length == 4) {
                Long topParentUserId = Long.parseLong(inviteRelations[3]);
                Long parentUserId = Long.parseLong(inviteRelations[2]);
                userStatus = getFourUserStatusUserId(parentUserId);
                fourLevelStatus = topParentUserId + "," + userStatus;
                fourLevelAward = tradeFee.multiply(new BigDecimal(Double.toString(0.02)));
                fourLevelAward = fourLevelAward.setScale(8, RoundingMode.HALF_UP);
                userInviteStatus.setUserLevel(4);
            }
        } else {
            //无上级
            userInviteStatus.setUserLevel(0);
        }
        userInviteStatus.setOneLevelStatus(oneLevelStatus);
        userInviteStatus.setTwoLevelStatus(twoLevelStatus);
        userInviteStatus.setThreeLevelStatus(threeLevelStatus);
        userInviteStatus.setFourLevelStatus(fourLevelStatus);
        userInviteStatus.setOneLevelAward(oneLevelAward);
        userInviteStatus.setTwoLevelAward(twoLevelAward);
        userInviteStatus.setThreeLevelAward(threeLevelAward);
        userInviteStatus.setFourLevelAward(fourLevelAward);
        userInviteStatus.setUserId(userId);
        userInviteStatus.setOrderId(orderId);
        userInviteStatus.setTradeFee(tradeFee);
        userInviteStatus.setDealStatus(DealStatus.UN_DEAL.getCode());
        userInviteStatusService.insert(userInviteStatus);
    }

    /**
     * 计算平级状态
     *
     * @param userId
     * @return
     */
    public int getFourUserStatusUserId(Long userId) {
        List<User> users = userService.getInviteList(userId);
        int oneLevelCount = 0;
        int twoLevelCount = 0;
        int threeLevelCount = 0;
        int userStatus = 0;
        for (User user : users) {
            String inviteRelation = user.getInviteRelation();
            String[] inviteRelations = inviteRelation.split(",");
            //查询是否有交易
            int orderCount = userService.getOrderCountByUserId(user.getId());
            if (inviteRelations.length == 1 && orderCount > 0 && oneLevelCount < 1) {
                oneLevelCount++;
            }
            if (inviteRelations.length == 2 && orderCount > 0 && twoLevelCount < 1) {
                twoLevelCount++;
            }
            if (inviteRelations.length == 3 && orderCount > 0 && threeLevelCount < 1) {
                threeLevelCount++;
            }
        }
        if (oneLevelCount >= 1 && twoLevelCount >= 1 && threeLevelCount >= 1) {
            userStatus = 1;
        }
        return userStatus;
    }

    /**
     * 查询上级是否满足条件
     *
     * @param userId
     * @return
     */
    public int getThreeUserStatusUserId(Long userId) {
        List<User> users = userService.getInviteList(userId);
        int oneLevelCount = 0;
        int twoLevelCount = 0;
        int threeLevelCount = 0;
        int userStatus = 0;
        for (User user : users) {
            String inviteRelation = user.getInviteRelation();
            String[] inviteRelations = inviteRelation.split(",");
            int orderCount = userService.getOrderCountByUserId(user.getId());
            if (inviteRelations.length == 1 && orderCount > 0 && oneLevelCount < 1) {
                oneLevelCount++;

            }
            if (inviteRelations.length == 2 && orderCount > 0 && twoLevelCount < 1) {
                twoLevelCount++;
            }
            if (inviteRelations.length == 3 && orderCount > 0 && threeLevelCount < 1) {
                threeLevelCount++;
            }
        }
        if (oneLevelCount >= 1 && twoLevelCount >= 1 && threeLevelCount >= 1) {
            userStatus = 1;
        }
        return userStatus;
    }

    /**
     * 查询上级是否满足条件
     *
     * @param userId
     * @return
     */
    public int getTwoUserStatusUserId(Long userId) {
        List<User> users = userService.getInviteList(userId);
        int oneLevelCount = 0;
        int twoLevelCount = 0;
        int userStatus = 0;
        for (User user : users) {
            String inviteRelation = user.getInviteRelation();
            String[] inviteRelations = inviteRelation.split(",");
            int orderCount = userService.getOrderCountByUserId(user.getId());
            if (inviteRelations.length == 1 && orderCount > 0 && oneLevelCount < 1) {
                oneLevelCount++;

            }
            if (inviteRelations.length == 2 && orderCount > 0 && twoLevelCount < 1) {
                twoLevelCount++;
            }
        }
        if (oneLevelCount >= 1 && twoLevelCount >= 1) {
            userStatus = 1;
        }
        return userStatus;
    }

    /**
     * 查询上级是否满足条件
     *
     * @param userId
     * @return
     */
    public int getOneUserStatusUserId(Long userId) {
        //获取其下级用户
        List<User> users = userService.getInviteList(userId);
        int oneLevelCount = 0;
        int userStatus = 0;
        for (User user : users) {
            String inviteRelation = user.getInviteRelation();
            String[] inviteRelations = inviteRelation.split(",");
            int orderCount = userService.getOrderCountByUserId(user.getId());
            if (inviteRelations.length == 1 && orderCount > 0 && oneLevelCount < 1) {
                oneLevelCount++;

            }
        }
        if (oneLevelCount >= 1) {
            userStatus = 1;
        }
        return userStatus;
    }
}

