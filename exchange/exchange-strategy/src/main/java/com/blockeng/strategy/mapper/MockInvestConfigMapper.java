package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.MockInvestConfig;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
public interface MockInvestConfigMapper extends BaseMapper<MockInvestConfig> {

}
