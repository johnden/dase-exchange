package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.TurnoverOrder;
import com.blockeng.strategy.mapper.TurnoverOrderMapper;
import com.blockeng.strategy.service.TurnoverOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 成交订单 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class TurnoverOrderServiceImpl extends ServiceImpl<TurnoverOrderMapper, TurnoverOrder> implements TurnoverOrderService {

}
