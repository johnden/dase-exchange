package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.UserInviteStatus;
import com.blockeng.strategy.mapper.UserInviteStatusMapper;
import com.blockeng.strategy.service.UserInviteStatusService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-09
 */
@Service
public class UserInviteStatusServiceImpl extends ServiceImpl<UserInviteStatusMapper, UserInviteStatus> implements UserInviteStatusService {
    /**
     * 更新记录状态
     *
     * @param id 记录ID
     * @param dealStatus 处理状态
     * @return
     */
    public int updateDealStatus(int dealStatus,long id)
    {
        return baseMapper.updateDealStatus(dealStatus,id);
    }
}
