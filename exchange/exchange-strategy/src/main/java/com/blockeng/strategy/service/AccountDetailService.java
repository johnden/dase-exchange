package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.AccountDetail;

import java.util.List;

/**
 * <p>
 * 资金账户流水 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface AccountDetailService extends IService<AccountDetail> {
    List<Long> getAmountByBusinessType(String business_type);
}
