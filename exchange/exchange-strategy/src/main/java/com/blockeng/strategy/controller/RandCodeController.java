package com.blockeng.strategy.controller;

import com.blockeng.strategy.dto.RandCodeVerifyDTO;
import com.blockeng.strategy.entity.Response;
import com.blockeng.strategy.enums.ResultCode;
import com.blockeng.strategy.service.RandCodeService;
import com.google.common.base.Strings;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.concurrent.TimeUnit;

/**
 * @author qiang
 */
@RestController
@RequestMapping("/user/sms")
@Slf4j
@Api(value = "发送验证码", description = "发送验证码 REST API")
public class RandCodeController {

    @Autowired
    private RandCodeService randCodeService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 校验验证码
     *
     * @return
     */
    @PostMapping("/verify")
    public Object verifyNew(@RequestBody RandCodeVerifyDTO randCodeVerifyDTO) {
        boolean flag = randCodeService.verify(randCodeVerifyDTO);
        if (flag) {
            return Response.ok();
        }
        return Response.err(ResultCode.MOBILE_SMSCODE_ERROR);
    }
}