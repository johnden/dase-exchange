package com.blockeng.strategy.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("mock_invest_config")
public class MockInvestConfig extends Model<MockInvestConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * 投资编号
     */
    @TableId(value = "invest_id", type = IdType.AUTO)
    private Long investId;
    /**
     * 空投名称
     */
    @TableField("invest_name")
    private String investName;
    /**
     * 空投数量
     */
    @TableField("invest_value")
    private BigDecimal investValue;
    /**
     * 投资类型
     */
    @TableField("invest_type")
    private Integer investType;


    @Override
    protected Serializable pkVal() {
        return this.investId;
    }

}
