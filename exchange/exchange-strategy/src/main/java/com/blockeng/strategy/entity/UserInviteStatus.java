package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_invite_status")
public class UserInviteStatus extends Model<UserInviteStatus> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 一级返佣奖励
     */
    @TableField("one_level_award")
    private BigDecimal oneLevelAward;
    /**
     * 二级返佣奖励
     */
    @TableField("two_level_award")
    private BigDecimal twoLevelAward;
    /**
     * 三级返佣奖励
     */
    @TableField("three_level_award")
    private BigDecimal threeLevelAward;
    /**
     * 四级返佣奖励
     */
    @TableField("four_level_award")
    private BigDecimal fourLevelAward;
    /**
     * 订单编号
     */
    @TableField("order_id")
    private Long orderId;
    /**
     * 用户编号
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 一级用户编号_是否满足条件
     */
    @TableField("one_level_status")
    private String oneLevelStatus;
    /**
     * 二级用户编号_是否满足条件
     */
    @TableField("two_level_status")
    private String twoLevelStatus;
    /**
     * 三级用户编号_是否满足条件
     */
    @TableField("three_level_status")
    private String threeLevelStatus;
    /**
     * 四级用户编号_是否满足条件
     */
    @TableField("four_level_status")
    private String fourLevelStatus;
    /**
     * 用户所在级别
     */
    @TableField("user_level")
    private Integer userLevel;
    /**
     * 交易手续费
     */
    @TableField("trade_fee")
    private BigDecimal tradeFee;

    /**
     * 当前订单处理状态
     */
    @TableField("deal_status")
    private Integer dealStatus;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
