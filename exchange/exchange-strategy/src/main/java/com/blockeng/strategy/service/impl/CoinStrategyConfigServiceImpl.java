package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.CoinStrategyConfig;
import com.blockeng.strategy.mapper.CoinStrategyConfigMapper;
import com.blockeng.strategy.service.CoinStrategyConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class CoinStrategyConfigServiceImpl extends ServiceImpl<CoinStrategyConfigMapper, CoinStrategyConfig> implements CoinStrategyConfigService {

}
