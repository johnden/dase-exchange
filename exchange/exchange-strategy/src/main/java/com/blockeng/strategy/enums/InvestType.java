package com.blockeng.strategy.enums;


/**
 * @Description: 策略类型
 */
public enum InvestType {

    REGISTER_INVEST(10, "注册空投"),
    AUTH_INVEST(20, "认证空投"),
    RECHARGE_INVEST(30, "充值空投"),
    INVITE_INVEST(40, "邀请空投");

    /**
     *
     */
    private int code;

    /**
     * 标识
     */
    private String desc;

    InvestType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static InvestType getByCode(int code) {
        for (InvestType type : InvestType.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}