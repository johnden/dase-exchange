package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.MockInvestConfig;
import com.blockeng.strategy.mapper.MockInvestConfigMapper;
import com.blockeng.strategy.service.MockInvestConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
@Service
public class MockInvestConfigServiceImpl extends ServiceImpl<MockInvestConfigMapper, MockInvestConfig> implements MockInvestConfigService {

}
