package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.AccountWhiteList;
import com.blockeng.strategy.mapper.AccountWhiteListMapper;
import com.blockeng.strategy.service.AccountWhiteListService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class AccountWhiteListServiceImpl extends ServiceImpl<AccountWhiteListMapper, AccountWhiteList> implements AccountWhiteListService {

}
