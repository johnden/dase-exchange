package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.CoinPrice;
import com.blockeng.strategy.mapper.CoinPriceMapper;
import com.blockeng.strategy.service.CoinPriceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-13
 */
@Service
public class CoinPriceServiceImpl extends ServiceImpl<CoinPriceMapper, CoinPrice> implements CoinPriceService {

}
