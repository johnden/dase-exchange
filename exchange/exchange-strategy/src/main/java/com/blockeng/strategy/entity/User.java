package com.blockeng.strategy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author hugo
 * @since 2019-03-07
 */
@Data
@Accessors(chain = true)
@TableName("user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户类型：1-普通用户；2-代理人
     */
    private Integer type;
    /**
     * 用户名
     */
    private String username;
    /**
     * 国际电话区号
     */
    @TableField("country_code")
    private String countryCode;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 密码
     */
    private String password;
    /**
     * 交易密码
     */
    private String paypassword;
    /**
     * 交易密码设置状态
     */
    @TableField("paypass_setting")
    private Boolean paypassSetting;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 真实姓名
     */
    @TableField("real_name")
    private String realName;
    /**
     * 证件类型:1，身份证；2，军官证；3，护照；4，台湾居民通行证；5，港澳居民通行证；9，其他；
     */
    @TableField("id_card_type")
    private Boolean idCardType;
    /**
     * 认证状态：0-未认证；1-初级实名认证；2-高级实名认证
     */
    @TableField("auth_status")
    private Integer authStatus;
    /**
     * Google令牌秘钥
     */
    @TableField("ga_secret")
    private String gaSecret;
    /**
     * Google认证开启状态,0,未启用，1启用
     */
    @TableField("ga_status")
    private Boolean gaStatus;
    /**
     * 身份证号
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 代理商级别
     */
    private Integer level;
    /**
     * 认证时间
     */
    private LocalDateTime authtime;
    /**
     * 登录数
     */
    private Integer logins;
    /**
     * 状态：0，禁用；1，启用；
     */
    private Integer status;
    /**
     * 邀请码
     */
    @TableField("invite_code")
    private String inviteCode;
    /**
     * 邀请关系
     */
    @TableField("invite_relation")
    private String inviteRelation;
    /**
     * 直接邀请人ID
     */
    @TableField("direct_inviteid")
    private String directInviteid;
    /**
     * 0 否 1是  是否开启平台币抵扣手续费
     */
    @TableField("is_deductible")
    private Integer isDeductible;
    /**
     * 审核状态,1通过,2拒绝,0,待审核
     */
    @TableField("reviews_status")
    private Integer reviewsStatus;
    /**
     * 代理商拒绝原因
     */
    @TableField("agent_note")
    private String agentNote;
    /**
     * API的KEY
     */
    @TableField("access_key_id")
    private String accessKeyId;
    /**
     * API的密钥
     */
    @TableField("access_key_secret")
    private String accessKeySecret;
    /**
     * 引用认证状态id
     */
    @TableField("refe_auth_id")
    private Long refeAuthId;
    /**
     * 修改时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
