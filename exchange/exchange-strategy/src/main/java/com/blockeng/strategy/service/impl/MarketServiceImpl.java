package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.Market;
import com.blockeng.strategy.mapper.MarketMapper;
import com.blockeng.strategy.service.MarketService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易对配置信息 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class MarketServiceImpl extends ServiceImpl<MarketMapper, Market> implements MarketService {

}
