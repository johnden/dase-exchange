package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.strategy.dto.RandCodeVerifyDTO;
import com.blockeng.strategy.service.RandCodeService;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author qiang
 */
@Service
@Slf4j
public class RandCodeServiceImpl implements RandCodeService {
    private static final Logger logger = LoggerFactory.getLogger(RandCodeServiceImpl.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean verify(RandCodeVerifyDTO randCodeVerifyDTO) {
        String account = randCodeVerifyDTO.getEmail();
        if (!Strings.isNullOrEmpty(randCodeVerifyDTO.getPhone())) {
            account = randCodeVerifyDTO.getCountryCode()+randCodeVerifyDTO.getPhone();
            logger.info("用户帐号{}信息",account);
        }
        String key = String.format("CAPTCHA:%s:%s", randCodeVerifyDTO.getTemplateCode(), account);
        logger.info("查询到的key{}",key);
        String value = stringRedisTemplate.opsForValue().get(key);
        if(!Strings.isNullOrEmpty(account) && !Strings.isNullOrEmpty(value) && value.equals(randCodeVerifyDTO.getCode())) {
            logger.info("删除账户{}",account);
            stringRedisTemplate.delete(key);
            return true;
        }else {
            return false;
        }
    }
}