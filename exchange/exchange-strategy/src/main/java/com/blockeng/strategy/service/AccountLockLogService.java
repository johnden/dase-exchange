package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.AccountLockLog;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface AccountLockLogService extends IService<AccountLockLog> {
    /**
     * 查询当天解锁数量
     * @param createTime 今天时间
     * @param userId 用户编号
     * @param coinId 币编号
     * @return
     */
    BigDecimal selectDayAmount(String createTime,Long userId,Long coinId);

}
