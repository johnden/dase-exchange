package com.blockeng.strategy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@Accessors(chain = true)
@TableName("coin_lock_position")
public class CoinLockPosition extends Model<CoinLockPosition> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 时间
     */
    private LocalDateTime created;
    /**
     * 策略组合
     */
    @TableField("strategy_id")
    private String strategyId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
