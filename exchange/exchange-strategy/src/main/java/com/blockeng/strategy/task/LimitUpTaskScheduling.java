package com.blockeng.strategy.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.strategy.entity.*;
import com.blockeng.strategy.enums.StrategyType;
import com.blockeng.strategy.service.*;
import com.blockeng.strategy.util.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月02日
 * @修改记录
 */
@Component
public class LimitUpTaskScheduling {

    private static final Logger logger = LoggerFactory.getLogger(LimitUpTaskScheduling.class);

    private Calendar calendar = Calendar.getInstance();


    @Autowired
    private TurnoverOrderService turnoverOrderService;

    @Autowired
    private MarketService marketService;

    @Autowired
    private TradeStopPriceService tradeStopPriceService;

    @Autowired
    private CoinLockPositionService coinLockPositionService;


    @Autowired
    private CoinStrategyConfigService coinStrategyConfigService;


    /**
     * 每日凌晨1点钟执行一次
     */
    @Scheduled(cron = "0 0 1 1/1 * ?")
    public void LimitUpTask() {
        logger.info("涨停开始执行时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));

        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date start = calendar.getTime();

        String endDate = DateTimeUtils.convertLongToDate(start.getTime());

        calendar.add(Calendar.DATE, -1);
        String beginDate = DateTimeUtils.convertLongToDate(calendar.getTimeInMillis());

        //取最后一条交易记录
        QueryWrapper<TurnoverOrder> turnoverOrderWrapper = new QueryWrapper<>();
        turnoverOrderWrapper.orderByDesc("id");
        turnoverOrderWrapper.between("created", beginDate, endDate);
        TurnoverOrder turnoverOrder = turnoverOrderService.selectOne(turnoverOrderWrapper);

        if (!StringUtils.isEmpty(turnoverOrder)) {
            //取交易市场记录
            QueryWrapper<Market> marketWrapper = new QueryWrapper<>();
            marketWrapper.eq("id", turnoverOrder.getMarketId());
            Market market = marketService.selectOne(marketWrapper);

            if (!StringUtils.isEmpty(market)) {
                //取锁仓记录
                QueryWrapper<CoinLockPosition> coinLockPositionWrapper = new QueryWrapper<>();
                coinLockPositionWrapper.eq("coin_id", String.valueOf(market.getSellCoinId()));
                List<CoinLockPosition> coinLockPositions = coinLockPositionService.selectList(coinLockPositionWrapper);
                for (CoinLockPosition coinLockPosition : coinLockPositions) {
                    Long strategyId = Long.parseLong(coinLockPosition.getStrategyId());
                    //取锁仓策略
                    QueryWrapper<CoinStrategyConfig> coinStrategyWrapper = new QueryWrapper<>();
                    coinStrategyWrapper.eq("strategy_id", strategyId);
                    CoinStrategyConfig coinStrategyConfig = coinStrategyConfigService.selectOne(coinStrategyWrapper);

                    if (!StringUtils.isEmpty(coinStrategyConfig) && coinStrategyConfig.getConfigType() == StrategyType.LIMIT_PRICE.getCode()) {

                        BigDecimal price = turnoverOrder.getPrice();
                        price = price.setScale(8, RoundingMode.HALF_UP);
                        BigDecimal add_price = price.multiply(coinStrategyConfig.getConfigValue());
                        add_price = add_price.setScale(8, RoundingMode.HALF_UP);
                        TradeStopPrice tradeStopPrice = new TradeStopPrice();
                        tradeStopPrice.setCoinId(market.getSellCoinId());
                        tradeStopPrice.setClosePrice(price);
                        tradeStopPrice.setTopPrice(price.add(add_price).setScale(8, RoundingMode.HALF_UP));
                        tradeStopPriceService.insert(tradeStopPrice);
                    }
                }
            }
        }
        logger.info("涨停执行完成时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }
}
