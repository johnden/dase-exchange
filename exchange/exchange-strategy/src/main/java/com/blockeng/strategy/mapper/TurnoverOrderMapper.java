package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.TurnoverOrder;

/**
 * <p>
 * 成交订单 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface TurnoverOrderMapper extends BaseMapper<TurnoverOrder> {

}
