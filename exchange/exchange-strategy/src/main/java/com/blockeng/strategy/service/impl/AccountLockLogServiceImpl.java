package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.AccountLockLog;
import com.blockeng.strategy.mapper.AccountLockLogMapper;
import com.blockeng.strategy.service.AccountLockLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
@Slf4j
public class AccountLockLogServiceImpl extends ServiceImpl<AccountLockLogMapper, AccountLockLog> implements AccountLockLogService {

    /**
     * 查询当天解锁数量
     * @param createTime 今天时间
     * @param userId 用户编号
     * @param coinId 币编号
     * @return
     */
    public BigDecimal selectDayAmount(String createTime,Long userId,Long coinId)
    {
        return  baseMapper.selectDayAmount(createTime,userId,coinId);
    }
}
