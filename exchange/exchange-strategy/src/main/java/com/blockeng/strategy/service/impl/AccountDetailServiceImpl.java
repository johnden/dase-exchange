package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.AccountDetail;
import com.blockeng.strategy.mapper.AccountDetailMapper;
import com.blockeng.strategy.service.AccountDetailService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 资金账户流水 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class AccountDetailServiceImpl extends ServiceImpl<AccountDetailMapper, AccountDetail> implements AccountDetailService {
    public List<Long> getAmountByBusinessType(String business_type) {
        return baseMapper.getAmountByBusinessType(business_type);
    }
}
