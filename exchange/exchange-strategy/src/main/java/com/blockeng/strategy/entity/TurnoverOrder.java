package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 成交订单
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("turnover_order")
public class TurnoverOrder extends Model<TurnoverOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 市场ID
     */
    @TableField("market_id")
    private Long marketId;
    /**
     * 交易对类型：1-币币交易；2-创新交易；
     */
    @TableField("market_type")
    private Integer marketType;
    /**
     * 交易类型:1 买 2卖
     */
    @TableField("trade_type")
    private Integer tradeType;
    /**
     * 交易对标识符
     */
    private String symbol;
    /**
     * 交易对名称
     */
    @TableField("market_name")
    private String marketName;
    /**
     * 卖方用户ID
     */
    @TableField("sell_user_id")
    private Long sellUserId;
    /**
     * 卖方币种ID
     */
    @TableField("sell_coin_id")
    private Long sellCoinId;
    /**
     * 卖方委托订单ID
     */
    @TableField("sell_order_id")
    private Long sellOrderId;
    /**
     * 卖方委托价格
     */
    @TableField("sell_price")
    private BigDecimal sellPrice;
    /**
     * 卖方手续费率
     */
    @TableField("sell_fee_rate")
    private BigDecimal sellFeeRate;
    /**
     * 卖方委托数量
     */
    @TableField("sell_volume")
    private BigDecimal sellVolume;
    /**
     * 买方用户ID
     */
    @TableField("buy_user_id")
    private Long buyUserId;
    /**
     * 买方币种ID
     */
    @TableField("buy_coin_id")
    private Long buyCoinId;
    /**
     * 买方委托订单ID
     */
    @TableField("buy_order_id")
    private Long buyOrderId;
    /**
     * 买方委托数量
     */
    @TableField("buy_volume")
    private BigDecimal buyVolume;
    /**
     * 买方委托价格
     */
    @TableField("buy_price")
    private BigDecimal buyPrice;
    /**
     * 买方手续费率
     */
    @TableField("buy_fee_rate")
    private BigDecimal buyFeeRate;
    /**
     * 委托订单ID
     */
    @TableField("order_id")
    private Long orderId;
    /**
     * 成交总额
     */
    private BigDecimal amount;
    /**
     * 成交价格
     */
    private BigDecimal price;
    /**
     * 成交数量
     */
    private BigDecimal volume;
    /**
     * 成交卖出手续费
     */
    @TableField("deal_sell_fee")
    private BigDecimal dealSellFee;
    /**
     * 成交卖出手续费率
     */
    @TableField("deal_sell_fee_rate")
    private BigDecimal dealSellFeeRate;
    /**
     * 成交买入手续费
     */
    @TableField("deal_buy_fee")
    private BigDecimal dealBuyFee;
    /**
     * 成交买入成交率费
     */
    @TableField("deal_buy_fee_rate")
    private BigDecimal dealBuyFeeRate;
    /**
     * 状态0待成交，1已成交，2撤销，3.异常
     */
    private Boolean status;
    /**
     * 更新时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
