package com.blockeng.strategy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月07日
 * @修改记录
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UserLockDetailAmountDto {

    /**
     * 待解锁金额
     */
    private BigDecimal waitLockAmount;

    /**
     * 已解锁金额
     */
    private BigDecimal unlockAmount;

    /**
     * 总金额=余额+待解锁金额
     */
    private BigDecimal totalAmount;

    public void setWaitLockAmount(BigDecimal waitLockAmount) {
        this.waitLockAmount = waitLockAmount.setScale(8, RoundingMode.HALF_UP);
    }


    public void setUnlockAmount(BigDecimal unlockAmount) {
        this.unlockAmount = unlockAmount.setScale(8, RoundingMode.HALF_UP);
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount.setScale(8, RoundingMode.HALF_UP);
    }

}
