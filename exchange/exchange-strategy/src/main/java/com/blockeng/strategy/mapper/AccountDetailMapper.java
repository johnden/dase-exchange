package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.AccountDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 资金账户流水 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface AccountDetailMapper extends BaseMapper<AccountDetail> {
    List<Long> getAmountByBusinessType(@Param("businessType") String business_type);
}
