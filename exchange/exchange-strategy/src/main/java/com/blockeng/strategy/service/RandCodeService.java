package com.blockeng.strategy.service;

import com.blockeng.strategy.dto.RandCodeVerifyDTO;

/**
 * @author qiang
 */
public interface RandCodeService {
    /**
     * 验证短信验证码
     * @return
     */
    boolean verify(RandCodeVerifyDTO randCodeVerifyDTO);
}