package com.blockeng.strategy.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blockeng.strategy.entity.UserInviteAward;
import com.blockeng.strategy.entity.UserInviteStatus;
import com.blockeng.strategy.enums.DealStatus;
import com.blockeng.strategy.service.UserInviteAwardService;
import com.blockeng.strategy.service.UserInviteStatusService;
import com.blockeng.strategy.util.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月09日
 * @修改记录
 */
@Component
public class InviteAwardTaskScheduling {
    private static final Logger logger = LoggerFactory.getLogger(InviteAwardTaskScheduling.class);
    private Calendar calendar = Calendar.getInstance();

    @Autowired
    private UserInviteStatusService userInviteStatusService;

    @Autowired
    private UserInviteAwardService userInviteAwardService;


    /**
     * 每日凌晨1:30点钟执行一次
     */
    @Scheduled(cron = "0 0/5 * * * ? ")
    public void InviteUserAwardTask() {
        logger.info("计算推荐收益开始执行:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        QueryWrapper<UserInviteStatus> userInviteStatusWrapper = new QueryWrapper<>();
        //只会扫描出上级都未处理完的订单
        userInviteStatusWrapper.gt("deal_status", -1);
        List<UserInviteStatus> userInviteStatuses = userInviteStatusService.selectList(userInviteStatusWrapper);

        for (UserInviteStatus userInviteStatus : userInviteStatuses) {
            int userLevel = userInviteStatus.getUserLevel();
            Long orderId = userInviteStatus.getOrderId();
            int dealStatus = userInviteStatus.getDealStatus();
            //上级用户
            if (userLevel == 1 && dealStatus != DealStatus.ALL_DEAL.getCode()) {
                boolean oneFlag = dealOneLevelAward(orderId, userLevel, userInviteStatus);
                if (oneFlag) {
                    userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                }
            }
            //上上级用户
            if (userLevel == 2 && dealStatus != DealStatus.ALL_DEAL.getCode()) {
                if (dealStatus == DealStatus.ONE_DEAL.getCode()) {
                    boolean twoFlag = dealTwoLevelAward(orderId, userLevel, userInviteStatus);
                    if (twoFlag) {
                        userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                    }
                } else {
                    if (dealStatus == DealStatus.TWO_DEAL.getCode()) {
                        boolean oneFlag = dealOneLevelAward(orderId, userLevel, userInviteStatus);
                        if (oneFlag) {
                            userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                        }
                    } else {
                        boolean oneFlag = dealOneLevelAward(orderId, userLevel, userInviteStatus);
                        boolean twoFlag = dealTwoLevelAward(orderId, userLevel, userInviteStatus);
                        if (oneFlag && twoFlag) {
                            userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                        } else {
                            if (twoFlag) {
                                userInviteStatusService.updateDealStatus(DealStatus.TWO_DEAL.getCode(), userInviteStatus.getId());
                            }
                            if (oneFlag) {
                                userInviteStatusService.updateDealStatus(DealStatus.ONE_DEAL.getCode(), userInviteStatus.getId());
                            }
                        }
                    }
                }
            }
            //上上级用户
            if (userLevel == 3 && dealStatus != DealStatus.ALL_DEAL.getCode()) {
                if (dealStatus == DealStatus.ONE_DEAL.getCode()) {
                    boolean twoFlag = dealTwoLevelAward(orderId, userLevel, userInviteStatus);
                    boolean threeFlag = dealThreeLevelAward(orderId, userLevel, userInviteStatus);
                    if (twoFlag && threeFlag) {
                        userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                    } else {
                        if (threeFlag) {
                            userInviteStatusService.updateDealStatus(DealStatus.ONETHREE_DEAL.getCode(), userInviteStatus.getId());
                        }
                        if (twoFlag) {
                            userInviteStatusService.updateDealStatus(DealStatus.ONETWO_DEAL.getCode(), userInviteStatus.getId());
                        }
                    }
                } else {
                    if (dealStatus == DealStatus.TWO_DEAL.getCode()) {
                        boolean oneFlag = dealOneLevelAward(orderId, userLevel, userInviteStatus);
                        boolean threeFlag = dealThreeLevelAward(orderId, userLevel, userInviteStatus);
                        if (oneFlag && threeFlag) {
                            userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                        } else {
                            if (threeFlag) {
                                userInviteStatusService.updateDealStatus(DealStatus.TWOTHREE_DEAL.getCode(), userInviteStatus.getId());
                            }
                            if (oneFlag) {
                                userInviteStatusService.updateDealStatus(DealStatus.ONETWO_DEAL.getCode(), userInviteStatus.getId());
                            }
                        }
                    } else {
                        if (dealStatus == DealStatus.THREE_DEAL.getCode()) {
                            boolean oneFlag = dealOneLevelAward(orderId, userLevel, userInviteStatus);
                            boolean twoFlag = dealTwoLevelAward(orderId, userLevel, userInviteStatus);
                            if (oneFlag && twoFlag) {
                                userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                            } else {
                                if (twoFlag) {
                                    userInviteStatusService.updateDealStatus(DealStatus.TWOTHREE_DEAL.getCode(), userInviteStatus.getId());
                                }
                                if (oneFlag) {
                                    userInviteStatusService.updateDealStatus(DealStatus.ONETHREE_DEAL.getCode(), userInviteStatus.getId());
                                }
                            }
                        } else {
                            boolean oneFlag = dealOneLevelAward(orderId, userLevel, userInviteStatus);
                            boolean twoFlag = dealTwoLevelAward(orderId, userLevel, userInviteStatus);
                            boolean threeFlag = dealThreeLevelAward(orderId, userLevel, userInviteStatus);
                            if (oneFlag) {
                                if (twoFlag && threeFlag) {
                                    userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                                } else {
                                    if (twoFlag) {
                                        userInviteStatusService.updateDealStatus(DealStatus.ONETWO_DEAL.getCode(), userInviteStatus.getId());
                                    } else {
                                        if (threeFlag) {
                                            userInviteStatusService.updateDealStatus(DealStatus.ONETHREE_DEAL.getCode(), userInviteStatus.getId());
                                        } else {
                                            userInviteStatusService.updateDealStatus(DealStatus.ONE_DEAL.getCode(), userInviteStatus.getId());
                                        }
                                    }
                                }
                            } else {
                                if (twoFlag && threeFlag) {
                                    userInviteStatusService.updateDealStatus(DealStatus.TWOTHREE_DEAL.getCode(), userInviteStatus.getId());
                                } else {
                                    if (twoFlag) {
                                        userInviteStatusService.updateDealStatus(DealStatus.TWO_DEAL.getCode(), userInviteStatus.getId());
                                    }
                                    if (threeFlag) {
                                        userInviteStatusService.updateDealStatus(DealStatus.THREE_DEAL.getCode(), userInviteStatus.getId());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //上上上上级用户
            if (userLevel == 4 && dealStatus != DealStatus.ALL_DEAL.getCode()) {
                boolean fourFlag = dealFourLevelAward(orderId, userLevel, userInviteStatus);
                if (fourFlag) {
                    userInviteStatusService.updateDealStatus(DealStatus.ALL_DEAL.getCode(), userInviteStatus.getId());
                }
            }
        }
        logger.info("计算推荐收益执行完成:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }

    /**
     * 处理上级用户奖励
     *
     * @param orderId
     * @param userLevel
     * @param userInviteStatus
     */
    public boolean dealOneLevelAward(Long orderId, int userLevel, UserInviteStatus userInviteStatus) {
        //上级用的user_id,status;
        String oneLevelStatus = userInviteStatus.getOneLevelStatus();
        String[] oneLevels = oneLevelStatus.split(",");
        Long userId = Long.parseLong(oneLevels[0]);
        int userStatus = Integer.parseInt(oneLevels[1]);
        if (userStatus == 1) {
            return insertUserInviteAward(userId, orderId, userLevel, userInviteStatus.getOneLevelAward());
        }
        return false;
    }

    /**
     * 处理上上级级用户奖励
     *
     * @param orderId
     * @param userLevel
     * @param userInviteStatus
     */
    public boolean dealTwoLevelAward(Long orderId, int userLevel, UserInviteStatus userInviteStatus) {
        //上上级user_id,status;
        String twoLevelStatus = userInviteStatus.getTwoLevelStatus();
        String[] oneLevels = twoLevelStatus.split(",");
        Long userId = Long.parseLong(oneLevels[0]);
        int userStatus = Integer.parseInt(oneLevels[1]);
        if (userStatus == 1) {
            return insertUserInviteAward(userId, orderId, userLevel, userInviteStatus.getTwoLevelAward());
        }
        return false;
    }

    /**
     * 处理上上上级用户奖励
     *
     * @param orderId
     * @param userLevel
     * @param userInviteStatus
     */
    public boolean dealThreeLevelAward(Long orderId, int userLevel, UserInviteStatus userInviteStatus) {
        //上上上级user_id,status;
        String twoLevelStatus = userInviteStatus.getThreeLevelStatus();
        String[] oneLevels = twoLevelStatus.split(",");
        Long userId = Long.parseLong(oneLevels[0]);
        int userStatus = Integer.parseInt(oneLevels[1]);
        if (userStatus == 1) {
            return insertUserInviteAward(userId, orderId, userLevel, userInviteStatus.getThreeLevelAward());
        }
        return false;
    }

    /**
     * 处理平级用户奖励
     *
     * @param orderId
     * @param userLevel
     * @param userInviteStatus
     */
    public boolean dealFourLevelAward(Long orderId, int userLevel, UserInviteStatus userInviteStatus) {
        //平级用的user_id,status;
        String fourLevelStatus = userInviteStatus.getFourLevelStatus();
        String[] fourLevels = fourLevelStatus.split(",");
        Long userId = Long.parseLong(fourLevels[0]);
        int userStatus = Integer.parseInt(fourLevels[1]);
        if (userStatus == 1) {
            return insertUserInviteAward(userId, orderId, userLevel, userInviteStatus.getFourLevelAward());
        }
        return false;
    }

    /**
     * 插入奖励流水
     *
     * @param userId
     * @param orderId
     * @param inviteLevel
     * @param inviteAmount
     */
    public boolean insertUserInviteAward(Long userId, Long orderId, int inviteLevel, BigDecimal inviteAmount) {
        UserInviteAward userInviteAward = new UserInviteAward();
        userInviteAward.setUserId(userId);
        userInviteAward.setOrderId(orderId);
        userInviteAward.setInviteLevel(inviteLevel);
        userInviteAward.setInviteAmount(inviteAmount);
        return userInviteAwardService.insert(userInviteAward);
    }
}
