package com.blockeng.strategy.enums;

/**
 * @author hugo
 * @Description: 处理状态
 * @since 2019-03-09
 */
public enum DealStatus {

    ALL_DEAL(-1, "已处理"),
    UN_DEAL(0, "未处理"),
    ONE_DEAL(1, "一级处理完成"),
    TWO_DEAL(2, "二级处理完成"),
    THREE_DEAL(3, "三级处理完成"),
    ONETWO_DEAL(4, "一二级处理完成"),
    ONETHREE_DEAL(5, "一三级处理完成"),
    TWOTHREE_DEAL(6, "二三级处理完成");

    /**
     * 代码
     */
    private int code;

    /**
     * 描述
     */
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    DealStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
