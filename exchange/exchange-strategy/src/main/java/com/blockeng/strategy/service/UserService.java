package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.User;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-07
 */
public interface UserService extends IService<User> {
    /**
     * 获取用户所有邀请记录
     * @param userId
     * @return
     */
    List<User> getInviteList(Long userId);

    /**
     * 获取用户所有邀请总数
     * @param userId
     * @return
     */
    int getInviteCount(Long userId);

    /**
     * 获取用户订单总数
     * @param userId
     * @return
     */
    int getOrderCountByUserId(Long userId);
}
