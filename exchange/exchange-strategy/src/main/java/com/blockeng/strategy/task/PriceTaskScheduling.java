package com.blockeng.strategy.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.blockeng.strategy.entity.CoinPrice;
import com.blockeng.strategy.service.CoinPriceService;
import com.blockeng.strategy.util.DateTimeUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月02日
 * @修改记录
 */
@Component
public class PriceTaskScheduling {

    private static final Logger logger = LoggerFactory.getLogger(PriceTaskScheduling.class);

    @Autowired
    private CoinPriceService coinPriceService;

    /**
     * 每隔1分钟执行一次取价格操作
     */
    @Scheduled(cron = "0 1/1 * * * ?")
    public void StrategyTask() {
        logger.info("计算价格开始:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        QueryWrapper<CoinPrice> coinPriceWrapper = new QueryWrapper<>();
        coinPriceWrapper.eq("is_sync", 1);
        CoinPrice coinPrice = coinPriceService.selectOne(coinPriceWrapper);
        String url = "https://api.binance.com/api/v3/ticker/price?symbol=ETHUSDT";
        // 创建httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        if (!StringUtils.isEmpty(coinPrice)) {
            url = coinPrice.getUrl();

            // 创建get方式请求对象
            HttpGet httpGet = new HttpGet(url);
            httpGet.addHeader("Content-type", "application/json");

            try {
                CloseableHttpResponse response = httpClient.execute(httpGet);

                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    String result = EntityUtils.toString(response.getEntity(), "utf-8");
                    if (!StringUtils.isEmpty(result)) {
                        JSONObject jsonObject =  JSON.parseObject(result);
                        UpdateWrapper<CoinPrice> coinPriceUpdateWrapper = new UpdateWrapper<>();
                        coinPriceUpdateWrapper.eq("id", coinPrice.getId()).and(wrapper->wrapper.eq("is_sync", 1));
                        BigDecimal price = jsonObject.getBigDecimal("price");
                        if (!StringUtils.isEmpty(price)) {
                            price = price.setScale(2, RoundingMode.HALF_UP);
                            coinPrice.setPrice(price);
                            coinPriceService.update(coinPrice, coinPriceUpdateWrapper);
                        }
                    }
                }
                response.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        logger.info("计算价格结束:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }
}
