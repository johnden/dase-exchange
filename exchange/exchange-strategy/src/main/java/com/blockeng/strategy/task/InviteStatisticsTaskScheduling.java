package com.blockeng.strategy.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.blockeng.strategy.entity.TurnoverOrder;
import com.blockeng.strategy.entity.User;
import com.blockeng.strategy.entity.UserInviteStatistics;
import com.blockeng.strategy.entity.UserInviteStatus;
import com.blockeng.strategy.enums.DealStatus;
import com.blockeng.strategy.service.TurnoverOrderService;
import com.blockeng.strategy.service.UserInviteStatisticsService;
import com.blockeng.strategy.service.UserInviteStatusService;
import com.blockeng.strategy.service.UserService;
import com.blockeng.strategy.util.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月09日
 * @修改记录
 */
@Component
public class InviteStatisticsTaskScheduling {
    private static final Logger logger = LoggerFactory.getLogger(InviteAwardTaskScheduling.class);
    private Calendar calendar = Calendar.getInstance();
    @Autowired
    private UserInviteStatisticsService userInviteStatisticsService;

    @Autowired
    private UserService userService;

    @Autowired
    private TurnoverOrderService turnoverOrderService;

    @Autowired
    private UserInviteStatusService userInviteStatusService;

    /**
     * 每隔一个小时执行一次
     */
    @Scheduled(cron = "0 0 1/1 * * ?")
    public void InviteStatisticsTask() {
        logger.info("计算推荐统计开始执行:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date start = calendar.getTime();
        String endDate = DateTimeUtils.convertLongToDate(start.getTime());
        calendar.add(Calendar.DATE, -1);
        String beginDates = DateTimeUtils.convertLongToDate(calendar.getTimeInMillis());


        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        //userWrapper.between("created", beginDates, endDate);
        userWrapper.gt("id", 0);
        List<User> users = userService.selectList(userWrapper);

        for (User user : users) {
            Long userId = user.getId();
            //推荐人列表
            List<User> userList = userService.getInviteList(userId);
            int oneInviteNumber = 0;
            int twoInviteNumber = 0;
            int threeInviteNumber = 0;
            int inviteLevel = 0;
            int oneInviteTrade = 0;
            int oneInviteTradeTask = 0;
            int twoInviteTrade = 0;
            int twoInviteTradeTask = 0;
            int threeInviteTrade = 0;
            int threeInviteTradeTask = 0;
            if (!StringUtils.isEmpty(userList) && userList.size() > 0) {
                for (User userItem : userList) {
                    String[] inviteRelations = userItem.getInviteRelation().split(",");
                    inviteLevel = inviteRelations.length;
                    QueryWrapper<TurnoverOrder> turnoverOrderWrapper = new QueryWrapper<>();
                    turnoverOrderWrapper.eq("sell_user_id", userItem.getId()).or(wrapper -> wrapper.eq("buy_user_id", userItem.getId()));
                    TurnoverOrder turnoverOrder = turnoverOrderService.selectOne(turnoverOrderWrapper);
                    if (inviteLevel == 1) {
                        oneInviteNumber++;
                        if (!StringUtils.isEmpty(turnoverOrder)) {
                            oneInviteTrade++;
                            oneInviteTradeTask += getInviteTradeTask(userItem, DealStatus.ALL_DEAL);
                        }
                    }
                    if (inviteLevel == 2) {
                        twoInviteNumber++;
                        if (!StringUtils.isEmpty(turnoverOrder)) {
                            twoInviteTrade++;
                            twoInviteTradeTask += getInviteTradeTask(userItem, DealStatus.ALL_DEAL);
                        }
                    }

                    if (inviteLevel == 3) {
                        threeInviteNumber++;
                        if (!StringUtils.isEmpty(turnoverOrder)) {
                            threeInviteTrade++;
                            threeInviteTradeTask += getInviteTradeTask(userItem, DealStatus.ALL_DEAL);
                        }
                    }
                }
                insertUserInviteStatistics(oneInviteNumber, oneInviteTrade, oneInviteTradeTask, 1, userId);
                insertUserInviteStatistics(twoInviteNumber, twoInviteTrade, twoInviteTradeTask, 2, userId);
                insertUserInviteStatistics(threeInviteNumber, threeInviteTrade, threeInviteTradeTask, 3, userId);
            } else {
                insertUserInviteStatistics(0, 0, 0, 0, userId);
            }
        }
        logger.info("计算推荐统计执行完成:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }


    /**
     * 计算完成裂变人数
     *
     * @param user
     * @param dealStatus
     * @return
     */
    public int getInviteTradeTask(User user, DealStatus dealStatus) {
        int inviteTradeTask = 0;
        QueryWrapper<UserInviteStatus> inviteStatusWrapper = new QueryWrapper<>();
        inviteStatusWrapper.eq("user_id", user.getId()).and(wrapper -> wrapper.eq("deal_status", dealStatus.getCode()));
        UserInviteStatus userInviteStatus = userInviteStatusService.selectOne(inviteStatusWrapper);
        if (!StringUtils.isEmpty(userInviteStatus)) {
            inviteTradeTask = 1;
        }
        return inviteTradeTask;
    }

    /**
     * 插入统计数据
     *
     * @param inviteNumber
     * @param inviteTrade
     * @param inviteTradeTask
     * @param inviteLevel
     * @param userId
     */
    private void insertUserInviteStatistics(int inviteNumber, int inviteTrade, int inviteTradeTask, int inviteLevel, long userId) {
        QueryWrapper<UserInviteStatistics> inviteStatisticsWrapper = new QueryWrapper<>();
        inviteStatisticsWrapper.eq("user_id", userId).and(wrapper -> wrapper.eq("invite_level", inviteLevel));
        UserInviteStatistics userInviteStatistics = userInviteStatisticsService.selectOne(inviteStatisticsWrapper);

        if (!StringUtils.isEmpty(userInviteStatistics)) {
            userInviteStatistics.setInviteNumber(inviteNumber);
            userInviteStatistics.setInviteTrade(inviteTrade);
            userInviteStatistics.setInviteTradeTask(inviteTradeTask);
            UpdateWrapper<UserInviteStatistics> userInviteStatisticsUpdateWrapper = new UpdateWrapper<>();
            userInviteStatisticsUpdateWrapper.eq("user_id", userId).and(wrapper -> wrapper.eq("invite_level", inviteLevel));;
            userInviteStatisticsService.update(userInviteStatistics, userInviteStatisticsUpdateWrapper);
        } else {
            userInviteStatistics = new UserInviteStatistics();
            userInviteStatistics.setInviteNumber(inviteNumber);
            userInviteStatistics.setInviteTrade(inviteTrade);
            userInviteStatistics.setInviteTradeTask(inviteTradeTask);
            userInviteStatistics.setInviteLevel(inviteLevel);
            userInviteStatistics.setUserId(userId);
            userInviteStatisticsService.insert(userInviteStatistics);
        }
    }
}
