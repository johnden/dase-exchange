package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.Market;

/**
 * <p>
 * 交易对配置信息 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface MarketMapper extends BaseMapper<Market> {

}
