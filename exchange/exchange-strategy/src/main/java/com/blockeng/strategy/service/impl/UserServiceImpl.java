package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.User;
import com.blockeng.strategy.mapper.UserMapper;
import com.blockeng.strategy.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    /**
     * 获取用户所有邀请记录
     * @param userId
     * @return
     */
    public List<User> getInviteList(Long userId)
    {
        return  baseMapper.getInviteList(userId);
    }

    /**
     * 获取用户所有邀请总数
     * @param userId
     * @return
     */
    public int getInviteCount(Long userId)
    {
        return  baseMapper.getInviteCount(userId);
    }


    /**
     * 获取用户订单总数
     * @param userId
     * @return
     */
    public int getOrderCountByUserId(Long userId)
    {
        return baseMapper.getOrderCountByUserId(userId);
    }
}
