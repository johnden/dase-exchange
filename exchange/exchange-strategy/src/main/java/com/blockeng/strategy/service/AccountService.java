package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.Account;
import com.blockeng.strategy.enums.BusinessType;
import com.blockeng.strategy.exception.AccountException;

import java.math.BigDecimal;

/**
 * <p>
 * 资金账户 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface AccountService extends IService<Account> {

    /**
     * 根据用户ID和币种名称查询资金账户
     *
     * @param userId 用户ID
     * @param coinId 币种ID
     * @return
     */
    Account queryByUserIdAndCoinId(long userId, long coinId);


    /**
     * 根据用户ID和币种名称查询资金账户
     *
     * @param userId 用户ID
     * @param coinId 币种ID
     * @param containEnable 是否包含冻结
     * @return
     */
    Account queryByUserIdAndCoinId(long userId, long coinId,boolean containEnable);

    /**
     * 增加资金
     *
     * @param userId       用户ID
     * @param coinId       币种ID
     * @param amount       冻结金额
     * @param businessType 业务类型
     * @param remark       备注
     * @param orderId      关联订单号
     * @return
     */
    boolean addAmount(long userId,
                      long coinId,
                      BigDecimal amount,
                      BusinessType businessType,
                      String remark,
                      long orderId) throws AccountException;

    /**
     * 锁仓资金
     * add by hugo
     * @param userId       用户ID
     * @param coinId       币种ID
     * @param amount       冻结金额
     * @param businessType 业务类型
     * @param remark       备注
     * @param orderId      关联订单号
     * @return
     */
    boolean lockPositionAmount(long userId,
                               long coinId,
                               BigDecimal amount,
                               BusinessType businessType,
                               String remark,
                               long orderId) throws AccountException;
}
