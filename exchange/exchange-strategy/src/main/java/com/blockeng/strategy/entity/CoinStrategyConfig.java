package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@Accessors(chain = true)
@TableName("coin_strategy_config")
public class CoinStrategyConfig extends Model<CoinStrategyConfig> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "strategy_id", type = IdType.AUTO)
    private Long strategyId;
    /**
     * 配置项名称
     */
    @TableField("config_name")
    private String configName;
    /**
     * 配置项值
     */
    @TableField("config_value")
    private BigDecimal configValue;
    /**
     * 1、解锁策略
2、涨停%配置
     */
    @TableField("config_type")
    private Integer configType;
    /**
     * 是否启用
     */
    @TableField("is_open")
    private Integer isOpen;


    @Override
    protected Serializable pkVal() {
        return this.strategyId;
    }

}
