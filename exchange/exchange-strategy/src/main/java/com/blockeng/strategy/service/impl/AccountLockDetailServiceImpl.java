package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.AccountLockDetail;
import com.blockeng.strategy.mapper.AccountLockDetailMapper;
import com.blockeng.strategy.service.AccountLockDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class AccountLockDetailServiceImpl extends ServiceImpl<AccountLockDetailMapper, AccountLockDetail> implements AccountLockDetailService {

}
