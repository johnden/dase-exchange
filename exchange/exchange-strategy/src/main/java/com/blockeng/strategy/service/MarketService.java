package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.Market;

/**
 * <p>
 * 交易对配置信息 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface MarketService extends IService<Market> {

}
