package com.blockeng.strategy.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 资金账户
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Account extends Model<Account> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 币种id
     */
    @TableField("coin_id")
    private Long coinId;
    /**
     * 账号状态：1，正常；2，冻结；
     */
    private int status;
    /**
     * 币种可用金额
     */
    @TableField("balance_amount")
    private BigDecimal balanceAmount;
    /**
     * 币种冻结金额
     */
    @TableField("freeze_amount")
    private BigDecimal freezeAmount;
    /**
     * 累计充值金额
     */
    @TableField("recharge_amount")
    private BigDecimal rechargeAmount;
    /**
     * 累计提现金额
     */
    @TableField("withdrawals_amount")
    private BigDecimal withdrawalsAmount;
    /**
     * 净值
     */
    @TableField("net_value")
    private BigDecimal netValue;
    /**
     * 占用保证金
     */
    @TableField("lock_margin")
    private BigDecimal lockMargin;
    /**
     * 持仓盈亏/浮动盈亏
     */
    @TableField("float_profit")
    private BigDecimal floatProfit;
    /**
     * 总盈亏
     */
    @TableField("total_profit")
    private BigDecimal totalProfit;
    /**
     * 充值地址
     */
    @TableField("rec_addr")
    private String recAddr;
    /**
     * 版本号
     */
    private Long version;
    /**
     * 更新时间
     */
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 创建时间
     */
    private LocalDateTime created;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }



}
