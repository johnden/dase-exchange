package com.blockeng.strategy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blockeng.strategy.entity.TradeStopPrice;
import com.blockeng.strategy.mapper.TradeStopPriceMapper;
import com.blockeng.strategy.service.TradeStopPriceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
@Service
public class TradeStopPriceServiceImpl extends ServiceImpl<TradeStopPriceMapper, TradeStopPrice> implements TradeStopPriceService {

}
