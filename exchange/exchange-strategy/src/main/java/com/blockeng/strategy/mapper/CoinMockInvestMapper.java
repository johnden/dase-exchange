package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.CoinMockInvest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
public interface CoinMockInvestMapper extends BaseMapper<CoinMockInvest> {
    List<CoinMockInvest> queryInvestByInvestType(@Param("investType")int investType);
}
