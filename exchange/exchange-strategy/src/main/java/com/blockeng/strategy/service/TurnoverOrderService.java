package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.TurnoverOrder;

/**
 * <p>
 * 成交订单 服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface TurnoverOrderService extends IService<TurnoverOrder> {

}
