package com.blockeng.strategy.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.blockeng.strategy.entity.*;
import com.blockeng.strategy.enums.BusinessType;
import com.blockeng.strategy.enums.InvestType;
import com.blockeng.strategy.service.*;
import com.blockeng.strategy.util.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述:
 *
 * @version 1.0.0
 * @作者 hzx
 * @创建时间 2019年03月25日
 * @修改记录
 */
@Component
public class MockInvestTaskScheduling {

    private static final Logger logger = LoggerFactory.getLogger(MockInvestTaskScheduling.class);
    private Calendar calendar = Calendar.getInstance();

    @Autowired
    private CoinMockInvestService coinMockInvestService;
    @Autowired
    private UserService userService;
    @Autowired
    private MockInvestConfigService mockInvestConfigService;
    @Autowired
    private MockInvestLogService mockInvestLogService;
    @Autowired
    private AccountDetailService accountDetailService;
    @Autowired
    private AccountService accountService;

    /**
     * 每隔10分钟执行一次注册空投操作
     */
    @Scheduled(cron = "0 0/10 * * * ?")
    public void RegisterInvestStrategyTask() {
        logger.info("******注册空投开始时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        String endDate = DateTimeUtils.convertLongToDate(System.currentTimeMillis());
        calendar.add(Calendar.DATE, -1);
        String beginDate = DateTimeUtils.convertLongToDate(calendar.getTimeInMillis());


        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.between("created", beginDate, endDate);
        //userWrapper.gt("id", 0);
        List<User> users = userService.selectList(userWrapper);
        List<CoinMockInvest> coinMockInvests = coinMockInvestService.queryInvestByInvestType(InvestType.REGISTER_INVEST.getCode());
        for (CoinMockInvest coinMockInvest : coinMockInvests) {
            QueryWrapper<MockInvestConfig> mockInvestConfigWrapper = new QueryWrapper<>();
            mockInvestConfigWrapper.eq("invest_id", coinMockInvest.getId());
            MockInvestConfig mockInvestConfig = mockInvestConfigService.selectOne(mockInvestConfigWrapper);
            for (User user : users) {
                if (!StringUtils.isEmpty(mockInvestConfig)) {
                    BigDecimal amount = coinMockInvest.getAmount();
                    BigDecimal investValue = mockInvestConfig.getInvestValue();
                    if (amount.compareTo(investValue) >= 0) {
                        int invest_type = mockInvestConfig.getInvestType();
                        //判断只能送一次
                        QueryWrapper<MockInvestLog> mockInvestLogWrapper = new QueryWrapper<>();
                        mockInvestLogWrapper.eq("user_id", user.getId()).and(wrapper -> wrapper.eq("invest_type", invest_type)).and(wrapper -> wrapper.eq("coin_id", coinMockInvest.getCoinId()));
                        MockInvestLog mockInvestLog = mockInvestLogService.selectOne(mockInvestLogWrapper);
                        if (StringUtils.isEmpty(mockInvestLog)) {
                            mockInvestLog = new MockInvestLog();
                            mockInvestLog.setCoinId(coinMockInvest.getCoinId());
                            mockInvestLog.setUserId(user.getId());
                            mockInvestLog.setInvestName(mockInvestConfig.getInvestName());
                            mockInvestLog.setInvestValue(investValue);
                            mockInvestLog.setInvestType(invest_type);
                            mockInvestLog.setIsCheck(0);
                            mockInvestLog.setCoinName(getCoinName(coinMockInvest.getCoinId()));
                            boolean flag = mockInvestLogService.insert(mockInvestLog);
                            if (flag) {
                                //更新金额
                                coinMockInvest.setAmount(amount.subtract(investValue));
                                UpdateWrapper<CoinMockInvest> coinMockInvestWrapper = new UpdateWrapper<>();
                                coinMockInvestWrapper.eq("id", coinMockInvest.getId());
                                coinMockInvestService.update(coinMockInvest, coinMockInvestWrapper);
                            }
                        }
                    }
                }
            }
        }
        logger.info("******注册空投结束时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }

    public String getCoinName(Long coinId) {
        String coinName = "";
        if (coinId == 1016274886906429442L) {
            coinName = "ETH";
        } else if (coinId == 1016280674026733570L) {
            coinName = "SNT";
        } else if (coinId == 1016281401105137666L) {
            coinName = "AE";
        } else if (coinId == 1016282735795585026L) {
            coinName = "OMG";
        } else if (coinId == 1016283115346542593L) {
            coinName = "FTL";
        } else if (coinId == 1016283348063305729L) {
            coinName = "BTC";
        } else if (coinId == 1016283668663320577L) {
            coinName = "CNYB";
        } else if (coinId == 1016284178241896449L) {
            coinName = "LTC";
        } else if (coinId == 1016285174435233793L) {
            coinName = "USDT";
        } else if (coinId == 1019590799462277122L) {
            coinName = "SILK";
        } else if (coinId == 1020560182904922114L) {
            coinName = "XPS";
        } else if (coinId == 1051753802735316993L) {
            coinName = "MSCE";
        } else if (coinId == 1105672842469744642L) {
            coinName = "PGD";
        } else if (coinId == 1106058802386739202L) {
            coinName = "JSJC";
        } else if (coinId == 1106127272956747778L) {
            coinName = "BCDN";
        } else if (coinId == 1107182863003246593L) {
            coinName = "ETZ";
        } else if (coinId == 1107903439959318530L) {
            coinName = "ICT";
        }
        return coinName;
    }

    /**
     * 每隔10分钟执行一次认证空投操作
     */
    @Scheduled(cron = "0 0/10 * * * ?")
    public void AuthInvestStrategyTask() {
        logger.info("******认证空投开始时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));

        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.eq("auth_status", 2);//高级认证
        List<User> users = userService.selectList(userWrapper);
        List<CoinMockInvest> coinMockInvests = coinMockInvestService.queryInvestByInvestType(InvestType.AUTH_INVEST.getCode());
        for (CoinMockInvest coinMockInvest : coinMockInvests) {
            QueryWrapper<MockInvestConfig> mockInvestConfigWrapper = new QueryWrapper<>();
            mockInvestConfigWrapper.eq("invest_id", coinMockInvest.getId());
            MockInvestConfig mockInvestConfig = mockInvestConfigService.selectOne(mockInvestConfigWrapper);

            for (User user : users) {
                if (!StringUtils.isEmpty(mockInvestConfig)) {
                    BigDecimal amount = coinMockInvest.getAmount();
                    BigDecimal investValue = mockInvestConfig.getInvestValue();
                    if (amount.compareTo(investValue) >= 0) {
                        int invest_type = mockInvestConfig.getInvestType();
                        QueryWrapper<MockInvestLog> mockInvestLogWrapper = new QueryWrapper<>();
                        mockInvestLogWrapper.eq("user_id", user.getId()).and(wrapper -> wrapper.eq("invest_type", invest_type)).and(wrapper -> wrapper.eq("coin_id", coinMockInvest.getCoinId()));
                        MockInvestLog mockInvestLog = mockInvestLogService.selectOne(mockInvestLogWrapper);
                        //判断只能送一次
                        if (StringUtils.isEmpty(mockInvestLog)) {
                            mockInvestLog = new MockInvestLog();
                            mockInvestLog.setCoinId(coinMockInvest.getCoinId());
                            mockInvestLog.setUserId(user.getId());
                            mockInvestLog.setInvestName(mockInvestConfig.getInvestName());
                            mockInvestLog.setInvestValue(investValue);
                            mockInvestLog.setInvestType(invest_type);
                            mockInvestLog.setIsCheck(0);
                            mockInvestLog.setCoinName(getCoinName(coinMockInvest.getCoinId()));
                            boolean flag = mockInvestLogService.insert(mockInvestLog);
                            if (flag) {
                                coinMockInvest.setAmount(amount.subtract(investValue));
                                UpdateWrapper<CoinMockInvest> coinMockInvestWrapper = new UpdateWrapper<>();
                                coinMockInvestWrapper.eq("id", coinMockInvest.getId());
                                coinMockInvestService.update(coinMockInvest, coinMockInvestWrapper);
                            }
                        }
                    }
                }
            }
        }
        logger.info("******认证空投结束时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }

    /**
     * 每隔10分钟执行一次充值空投操作
     */
    @Scheduled(cron = "0 0/10 * * * ?")
    public void RechargeInvestStrategyTask() {
        logger.info("******充值空投开始时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        List<Long> users = accountDetailService.getAmountByBusinessType(BusinessType.RECHARGE.getCode());
        List<CoinMockInvest> coinMockInvests = coinMockInvestService.queryInvestByInvestType(InvestType.RECHARGE_INVEST.getCode());
        for (CoinMockInvest coinMockInvest : coinMockInvests) {
            QueryWrapper<MockInvestConfig> mockInvestConfigWrapper = new QueryWrapper<>();
            mockInvestConfigWrapper.eq("invest_id", coinMockInvest.getId());
            MockInvestConfig mockInvestConfig = mockInvestConfigService.selectOne(mockInvestConfigWrapper);
            for (Long userId : users) {
                if (!StringUtils.isEmpty(mockInvestConfig)) {
                    BigDecimal amount = coinMockInvest.getAmount();
                    BigDecimal investValue = mockInvestConfig.getInvestValue();
                    if (amount.compareTo(investValue) >= 0) {
                        int invest_type = mockInvestConfig.getInvestType();
                        QueryWrapper<MockInvestLog> mockInvestLogWrapper = new QueryWrapper<>();
                        mockInvestLogWrapper.eq("user_id", userId).and(wrapper -> wrapper.eq("invest_type", invest_type)).and(wrapper -> wrapper.eq("coin_id", coinMockInvest.getCoinId()));
                        MockInvestLog mockInvestLog = mockInvestLogService.selectOne(mockInvestLogWrapper);
                        if (StringUtils.isEmpty(mockInvestLog)) {
                            mockInvestLog = new MockInvestLog();
                            mockInvestLog.setCoinId(coinMockInvest.getCoinId());
                            mockInvestLog.setUserId(userId);
                            mockInvestLog.setInvestName(mockInvestConfig.getInvestName());
                            mockInvestLog.setInvestValue(investValue);
                            mockInvestLog.setInvestType(invest_type);
                            mockInvestLog.setIsCheck(0);
                            mockInvestLog.setCoinName(getCoinName(coinMockInvest.getCoinId()));
                            boolean flag = mockInvestLogService.insert(mockInvestLog);
                            if (flag) {
                                coinMockInvest.setAmount(amount.subtract(investValue));
                                UpdateWrapper<CoinMockInvest> coinMockInvestWrapper = new UpdateWrapper<>();
                                coinMockInvestWrapper.eq("id", coinMockInvest.getId());
                                coinMockInvestService.update(coinMockInvest, coinMockInvestWrapper);
                            }
                        }
                    }
                }
            }
        }
        logger.info("******充值空投结束时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }

    /**
     * 每隔10分钟执行一次推荐空投操作
     */
    @Scheduled(cron = "0 0/10 * * * ?")
    public void InviteInvestStrategyTask() {
        logger.info("******推荐空投开始时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        String endDate = DateTimeUtils.convertLongToDate(System.currentTimeMillis());
        calendar.add(Calendar.DATE, -1);
        String beginDate = DateTimeUtils.convertLongToDate(calendar.getTimeInMillis());


        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.between("created", beginDate, endDate);
        //userWrapper.gt("id", 0);
        List<User> users = userService.selectList(userWrapper);
        List<CoinMockInvest> coinMockInvests = coinMockInvestService.queryInvestByInvestType(InvestType.INVITE_INVEST.getCode());
        for (CoinMockInvest coinMockInvest : coinMockInvests) {
            QueryWrapper<MockInvestConfig> mockInvestConfigWrapper = new QueryWrapper<>();
            mockInvestConfigWrapper.eq("invest_id", coinMockInvest.getId());
            MockInvestConfig mockInvestConfig = mockInvestConfigService.selectOne(mockInvestConfigWrapper);
            for (User user : users) {
                int count = userService.getInviteCount(user.getId());
                if (count > 0) {
                    if (!StringUtils.isEmpty(mockInvestConfig)) {
                        BigDecimal amount = coinMockInvest.getAmount();
                        BigDecimal investValue = mockInvestConfig.getInvestValue();
                        if (amount.compareTo(investValue) >= 0) {
                            int invest_type = mockInvestConfig.getInvestType();
                            QueryWrapper<MockInvestLog> mockInvestLogWrapper = new QueryWrapper<>();
                            mockInvestLogWrapper.eq("user_id", user.getId()).and(wrapper -> wrapper.eq("invest_type", invest_type)).and(wrapper -> wrapper.eq("coin_id", coinMockInvest.getCoinId()));
                            MockInvestLog mockInvestLog = mockInvestLogService.selectOne(mockInvestLogWrapper);
                            if (StringUtils.isEmpty(mockInvestLog)) {
                                mockInvestLog = new MockInvestLog();
                                mockInvestLog.setCoinId(coinMockInvest.getCoinId());
                                mockInvestLog.setUserId(user.getId());
                                mockInvestLog.setInvestName(mockInvestConfig.getInvestName());
                                mockInvestLog.setInvestValue(investValue);
                                mockInvestLog.setInvestType(invest_type);
                                mockInvestLog.setIsCheck(0);
                                mockInvestLog.setCoinName(getCoinName(coinMockInvest.getCoinId()));
                                boolean flag = mockInvestLogService.insert(mockInvestLog);
                                if (flag) {
                                    coinMockInvest.setAmount(amount.subtract(investValue));
                                    UpdateWrapper<CoinMockInvest> coinMockInvestWrapper = new UpdateWrapper<>();
                                    coinMockInvestWrapper.eq("id", coinMockInvest.getId());
                                    coinMockInvestService.update(coinMockInvest, coinMockInvestWrapper);
                                }
                            } else {
                                UpdateWrapper<MockInvestLog> mockInvestLogUpdateWrapper = new UpdateWrapper<>();
                                mockInvestLogUpdateWrapper.eq("id", mockInvestLog.getId());
                                mockInvestLog.setInvestValue(investValue.multiply(new BigDecimal(count)));
                                mockInvestLogService.update(mockInvestLog, mockInvestLogUpdateWrapper);
                            }
                        }
                    }
                }
            }
        }
        logger.info("******推荐空投结束时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }


    /**
     * 每隔5分钟执行一次加币操作
     */
    @Scheduled(cron = "0 0/5 * * * ?")
    public void InvestAccountTask() {
        logger.info("******加币开始时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
        QueryWrapper<MockInvestLog> mockInvestLogWrapper = new QueryWrapper<>();
        mockInvestLogWrapper.eq("is_check", 0);
        List<MockInvestLog> mockInvestLogs = mockInvestLogService.selectList(mockInvestLogWrapper);
        for (MockInvestLog mockInvestLog : mockInvestLogs) {
            BigDecimal amount = mockInvestLog.getInvestValue();
            int count = amount.divide(new BigDecimal(10)).intValue();
            int invest_type = mockInvestLog.getInvestType();
            if (invest_type == InvestType.INVITE_INVEST.getCode()) {
                for (int i = 0; i < count; i++) {
                    boolean flag = accountService.addAmount(mockInvestLog.getUserId(), mockInvestLog.getCoinId(), amount, BusinessType.EMPTY_INVEST,
                            BusinessType.EMPTY_INVEST.getDesc(),
                            mockInvestLog.getId());
                    try {
                        accountService.lockPositionAmount(mockInvestLog.getUserId(),
                                mockInvestLog.getCoinId(),
                                amount,
                                BusinessType.LOCK_POSITION,
                                BusinessType.LOCK_POSITION.getDesc(),
                                mockInvestLog.getId());
                    } catch (Exception ex) {
                        logger.info("扣减资金-资金账户异常，userId:{}, coinId:{}", mockInvestLog.getUserId(), mockInvestLog.getCoinId());
                    }
                    if (flag) {
                        UpdateWrapper<MockInvestLog> mockInvestLogUpdateWrapper = new UpdateWrapper<>();
                        mockInvestLogUpdateWrapper.eq("id", mockInvestLog.getId());
                        mockInvestLog.setIsCheck(1);
                        mockInvestLogService.update(mockInvestLog, mockInvestLogUpdateWrapper);
                    }
                }
            }
        }
        logger.info("******加币结束时间:{}", DateTimeUtils.convertLongToDate(System.currentTimeMillis()));
    }
}
