package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.AccountLockDetail;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface AccountLockDetailService extends IService<AccountLockDetail> {

}
