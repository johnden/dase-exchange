package com.blockeng.strategy.response;

public final class ResponseResultUtil {
    /**
     * @param code      响应码
     * @param message   相应信息
     * @param any       返回的数据
     * @description     请求成功返回对象
     */
    public final BaseResponse success(int code, String message, Object any) {
        return new BaseResponse(code, message, any);
    }

    /**
     * @param any   返回的数据
     * @description 请求成功返回对象
     */
    public final BaseResponse success(Object any) {
        int code = ResponseCode.SUCCESS.getCode();
        String message = ResponseCode.SUCCESS.getMessage();
        return this.success(code, message, any);
    }


    /**
     * @description 请求成功返回对象
     */
    public final BaseResponse success() {
        return this.success(null);
    }

    /**
     * @param responseCode  返回的响应码所对应的枚举类
     * @description         请求失败返回对象
     */
    public final BaseResponse error(ResponseCode responseCode) {
        return new BaseResponse(responseCode.getCode(), responseCode.getMessage(), 0);
    }

    /**
     * @param code      响应码
     * @param message   相应信息
     * @description     请求失败返回对象
     */
    public final BaseResponse error(int code, String message) {
        return new BaseResponse(code, message, 0);
    }
}
