package com.blockeng.strategy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blockeng.strategy.entity.MockInvestConfig;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hugo
 * @since 2019-03-25
 */
public interface MockInvestConfigService extends IService<MockInvestConfig> {

}
