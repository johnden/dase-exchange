package com.blockeng.strategy.response;

public enum ResponseCode {
    // 系统通用
    SUCCESS(200, "操作成功"),

    OPERATE_FAIL(201, "操作失败");

    private Integer code;
    private String message;

    ResponseCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public final Integer getCode() {
        return this.code;
    }

    public final String getMessage() {
        return this.message;
    }
}
