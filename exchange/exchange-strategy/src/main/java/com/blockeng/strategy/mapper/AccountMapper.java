package com.blockeng.strategy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blockeng.strategy.entity.Account;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 资金账户 Mapper 接口
 * </p>
 *
 * @author hugo
 * @since 2019-03-02
 */
public interface AccountMapper extends BaseMapper<Account> {
    /**
     * 增加账户资金
     *
     * @param accountId 资金账户ID
     * @param amount    增加金额
     * @return
     */
    int addAmount(@Param("accountId") long accountId, @Param("amount") BigDecimal amount);

    /**
     * 锁仓账户资金
     *
     * @param accountId 资金账户ID
     * @param amount    增加金额
     * @return
     */
    int lockPositionAmount(@Param("accountId") long accountId, @Param("amount") BigDecimal amount);
}
